Source: libpappsomspp
Maintainer: The Debichem Group <debichem-devel@lists.alioth.debian.org>
Uploaders: Filippo Rusconi <lopippo@debian.org>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 13),
 dh-exec,
 cmake,
 d-shlibs,
 libxkbcommon-x11-dev,
 qt6-base-dev,
 libqt6core5compat6-dev,
 libqt6svg6-dev,
 qt6-documentation-tools,
 libpwizlite-dev (>= 3.0.8),
 libqcustomplot-dev (>= 2.1.0),
 libodsstream-dev (>= 0.9.7),
 libsqlite3-dev,
 libzstd-dev,
 liblzf-dev,
 zlib1g-dev,
 libquazip1-qt6-dev,
 libboost-dev,
 libboost-iostreams-dev,
 libboost-thread-dev,
 libboost-system-dev,
 libboost-filesystem-dev,
 libboost-chrono-dev,
 libboost-container-dev,
 libhdf5-dev,
 doxygen,
 catch2
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debichem-team/libpappsomspp
Vcs-Git: https://salsa.debian.org/debichem-team/libpappsomspp.git
Homepage: http://pappso.inrae.fr/bioinfo


Package: libpappsomspp0
Architecture: any
Multi-Arch: same
Conflicts: libpappsomspp0-qt6
Replaces: libpappsomspp0-qt6
Breaks: libpappsomspp0-qt6
Depends: libqt6sql6-sqlite,
 ${shlibs:Depends},
 ${misc:Depends}
Description: C++ library to handle mass spectrometry data (non-GUI runtime)
 libpappsomspp provides a simple API to perform a variety of
 tasks related to mass spectrometry. Although the library is
 proteomics oriented, it also features interesting functions to perform
 mass spectral data integrations. The main features are:
 .
 - abstractions for peptides, ions, amino acid modifications...
 - integrations to mass spectra, drift spectra, XIC chromatograms...
 .
 This package ships the non-GUI library.


Package: libpappsomspp-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Replaces: libpappsomspp-dev (<< ${binary:Version})
Depends: libpappsomspp0 (= ${binary:Version}),
 ${misc:Depends}
Recommends: libpappsomspp-doc
Description: C++ library to handle mass spectrometry data (development files)
 libpappsomspp provides a simple API to perform a variety of
 tasks related to mass spectrometry. Although the library is
 proteomics oriented, it also features interesting functions to perform
 mass spectral data integrations. The main features are:
 .
 - abstractions for peptides, ions, amino acid modifications...
 - integrations to mass spectra, drift spectra, XIC chromatograms...
 .
 This package ships the development files for the non-GUI library.


Package: libpappsomspp-widget0
Architecture: any
Multi-Arch: same
Conflicts: libpappsomspp-widget0-qt6
Breaks: libpappsomspp-widget0-qt6
Replaces: libpappsomspp-widget0-qt6, libpappsomspp-widget0 (<< ${binary:Version})
Depends: libpappsomspp0 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: C++ library to handle mass spectrometry data (GUI runtime)
 libpappsomspp provides a simple API to perform a variety of
 tasks related to mass spectrometry. Although the library is
 proteomics oriented, it also features interesting functions to perform
 mass spectral data integrations. The main features are:
 .
 - abstractions for peptides, ions, amino acid modifications...
 - integrations to mass spectra, drift spectra, XIC chromatograms...
 .
 This package ships the GUI library.


Package: libpappsomspp-widget-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Replaces: libpappsomspp-widget-dev (<< ${binary:Version})
Depends: libpappsomspp-widget0 (= ${binary:Version}),
         libpappsomspp-dev,
         ${misc:Depends}
Recommends: libpappsomspp-doc
Description: C++ library to handle mass spectrometry data (GUI development files)
 libpappsomspp provides a simple API to perform a variety of
 tasks related to mass spectrometry. Although the library is
 proteomics oriented, it also features interesting functions to perform
 mass spectral data integrations. The main features are:
 .
 - abstractions for peptides, ions, amino acid modifications...
 - integrations to mass spectra, drift spectra, XIC chromatograms...
 .
 This package ships the development files for the GUI library.


Package: libpappsomspp-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: C++ library to handle mass spectrometry data (developer documentation)
 libpappsomspp provides a simple API to perform a variety of
 tasks related to mass spectrometry. Although the library is
 proteomics oriented, it also features interesting functions to perform
 mass spectral data integrations. The main features are:
 .
 - abstractions for peptides, ions, amino acid modifications...
 - integrations to mass spectra, drift spectra, XIC chromatograms...
 .
 This package contains the developer documentation.
