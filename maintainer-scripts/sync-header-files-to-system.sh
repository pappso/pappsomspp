#!/bin/sh


if [ ! -d "pappsomspp" ]
then
  echo "Please, this works only if you are located in the src directory that"
  echo "should contain the pappsomspp directory"

  exit 1
fi

for file in $(find -name "*.h")
do 
  if [ ! -d /usr/include/$(dirname ${file}) ]
  then 
    mkdir -p /usr/include/$(dirname ${file})
  fi

  cp -v ${file} /usr/include/$(dirname ${file})/$(basename ${file})
done
