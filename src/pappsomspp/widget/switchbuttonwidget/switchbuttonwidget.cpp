/**
 * \file pappsomspp/widget/switchbuttonwidget/switchbuttonwidget.cpp
 * \date 26/07/2021
 * \author Thomas Renne
 * \brief widget to transform a push button to a switch button
 */


/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#include "switchbuttonwidget.h"

pappso::SwitchWidget::SwitchWidget(QWidget *parent) : QPushButton(parent)
{
  setSwitchValue(false);
  setIconSize(QSize(40, 16));
  setFlat(true);
  setMaximumSize(40, 16);

  connect(this, &SwitchWidget::clicked, this, &SwitchWidget::updateSwitchValue);
}

pappso::SwitchWidget::~SwitchWidget()
{
}

void
pappso::SwitchWidget::setSwitchValue(bool switch_value)
{
  m_switchButtonValue = switch_value;
  if(m_switchButtonValue)
    {
      setIcon(QIcon(":/icons/resources/icons/switch_on.svg"));
    }
  else
    {
      setIcon(QIcon(":/icons/resources/icons/switch_off.svg"));
    }
}


bool
pappso::SwitchWidget::getSwitchValue()
{
  return m_switchButtonValue;
}

void
pappso::SwitchWidget::updateSwitchValue()
{
  setSwitchValue(!m_switchButtonValue);
}
