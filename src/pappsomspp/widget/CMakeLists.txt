# CMake script for PAPPSOms++ library
# Author: Olivier Langella
# Created: 22/12/2017
# Rework: April 2020 (Coronavirus confinement)

message(\n${BoldRed}"Now configuring src/widget for ${PROJECT_NAME}"${ColourReset}\n)

# Find includes in corresponding build directories

set(CMAKE_INCLUDE_CURRENT_DIR ON)



if(NOT QCustomPlotQt6_FOUND)
	message("Still searching for QCustomPlotQt6")
endif()


########################################################
# Files
SET(PAPPSOWIDGET_CPP_FILES
  graphicdevicewidget.cpp
  httpbutton/httpbutton.cpp
  precisionwidget/precisionwidget.cpp
  massspectrumwidget/qcpspectrum.cpp
  massspectrumwidget/massspectrumwidget.cpp
  obo/obochooserwidget/obochooserwidget.cpp
  obo/obolistwidget/obolistmodel.cpp
  obo/obolistwidget/obolistproxymodel.cpp
  obo/obolistwidget/obolistwidget.cpp
  obo/obotermform/obotermform.cpp
  xicwidget/qcpxic.cpp
  xicwidget/xicwidget.cpp
  plotwidget/baseplotcontext.cpp
  plotwidget/massspectraceplotcontext.cpp
  plotwidget/colormapplotconfig.cpp
  plotwidget/baseplotwidget.cpp
  plotwidget/basetraceplotwidget.cpp
  plotwidget/ticxicchromtraceplotwidget.cpp
  plotwidget/massspectraceplotwidget.cpp
  plotwidget/driftspectraceplotwidget.cpp
  plotwidget/basecolormapplotwidget.cpp
  plotwidget/ticxicchrommassspeccolormapplotwidget.cpp
  plotwidget/ticxicchromdriftspeccolormapplotwidget.cpp
  plotwidget/driftspecmassspeccolormapplotwidget.cpp
  switchbuttonwidget/switchbuttonwidget.cpp
)


set(GUI_UIS
  ./obo/obochooserwidget/uiobochooserwidget.ui
  ./obo/obolistwidget/uiobolistwidget.ui
  ./obo/obotermform/uiobotermform.ui
)


# this will run uic on .ui files:
qt6_wrap_ui( GUI_UI_HDRS ${GUI_UIS} )

#############################################################
# Build the static lib
add_library(pappsomspp-widget-static STATIC ${PAPPSOWIDGET_CPP_FILES} ${GUI_UI_HDRS})

set_target_properties(pappsomspp-widget-static
  PROPERTIES OUTPUT_NAME pappsomspp-widget
  CLEAN_DIRECT_OUTPUT 1
)

message(STATUS "${BoldGreen}Setting definition -DPMSPP_LIBRARY for symbol DLL export.${ColourReset}")
target_compile_definitions(pappsomspp-widget-static PRIVATE -DPMSPP_LIBRARY)

target_link_libraries(pappsomspp-widget-static
  Qt6::Core
  Qt6::Widgets
  Qt6::Svg
  Qt6::PrintSupport
  Boost::iostreams
  Boost::filesystem
  Boost::thread
  Boost::chrono
  PwizLite::PwizLite
  liblzf::liblzf
  QCustomPlotQt6::QCustomPlotQt6
  OdsStream::Core
)


# FIXME: should check that this still required. It was years ago.
if(WIN32 OR _WIN32)

  target_link_libraries(pappsomspp-widget-static 
    stdc++
  )
endif()


#############################################################
# Build the shared lib
add_library(pappsomspp-widget-shared SHARED ${PAPPSOWIDGET_CPP_FILES} ${GUI_UI_HDRS})

set_target_properties(pappsomspp-widget-shared
	PROPERTIES OUTPUT_NAME pappsomspp-widget
	CLEAN_DIRECT_OUTPUT 1
	VERSION ${PAPPSOMSPP_VERSION}
	SOVERSION ${PAPPSOMSPP_VERSION_MAJOR}
	#COMPILE_DEFINITIONS " ${QT_DEFINITIONS}"
	#INCLUDE_DIRECTORIES "${QT_INCLUDE_DIR} ${QT_QTCORE_INCLUDE_DIR}"
	)


message(STATUS "${BoldGreen}Setting definition -DPMSPP_LIBRARY for symbol DLL export.${ColourReset}")
target_compile_definitions(pappsomspp-widget-shared PRIVATE -DPMSPP_LIBRARY)


target_link_libraries(pappsomspp-widget-shared

  PwizLite::PwizLite
  Boost::iostreams
  Boost::filesystem
  Boost::thread
  Boost::chrono
  SQLite::SQLite3
  QCustomPlotQt6::QCustomPlotQt6
  Qt6::PrintSupport
  Qt6::Gui
  Qt6::Widgets
  Qt6::Svg
  Qt6::Sql
  Qt6::Concurrent
  Qt6::Xml
  Qt6::Core5Compat 
  Qt6::Core
  ZLIB::ZLIB
  Zstd::Zstd
  liblzf::liblzf
  pappsomspp-shared
  OdsStream::Core
)


# Should check that this still required. It was years ago.
if(WIN32 OR _WIN32)

  target_link_libraries(pappsomspp-widget-shared 
    stdc++
  )

endif()


# Install libs

install(TARGETS pappsomspp-widget-shared 
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  # This one is for WIN32
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)

install(TARGETS pappsomspp-widget-static 
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR})


# Install headers
install(DIRECTORY pappsomspp/widget 
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/pappsomspp/widget
  FILES_MATCHING PATTERN "*.h"
  PATTERN "*.hpp")
