// Copyright Filippo Rusconi, GPLv3+

/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// Local includes
#include "colormapplotconfig.h"


namespace pappso
{

ColorMapPlotConfig::ColorMapPlotConfig()
{
}

ColorMapPlotConfig::ColorMapPlotConfig(DataKind x_axis_data_kind,
                                       DataKind y_axis_data_kind,

                                       AxisScale x_axis_scale,
                                       AxisScale y_axis_scale,
                                       AxisScale z_axis_scale,

                                       std::size_t key_cell_count,
                                       std::size_t mz_cell_count,

                                       double min_key_value,
                                       double max_key_value,

                                       double min_mz_value,
                                       double max_mz_value,

                                       double orig_min_z_value,
                                       double orig_max_z_value)
  : xAxisDataKind(x_axis_data_kind),
    yAxisDataKind(y_axis_data_kind),

    xAxisScale(x_axis_scale),
    yAxisScale(y_axis_scale),
    zAxisScale(z_axis_scale),

    keyCellCount(key_cell_count),
    mzCellCount(mz_cell_count),

    minKeyValue(min_key_value),
    maxKeyValue(max_key_value),

    minMzValue(min_mz_value),
    maxMzValue(max_mz_value),

    // Initialize both orig and last to the same value.
    origMinZValue(orig_min_z_value),
    lastMinZValue(orig_min_z_value),

    // Initialize both orig and last to the same value.
    origMaxZValue(orig_max_z_value),
    lastMaxZValue(orig_max_z_value)
{
}


ColorMapPlotConfig::ColorMapPlotConfig(const ColorMapPlotConfig &other)
{
  xAxisDataKind = other.xAxisDataKind;
  yAxisDataKind = other.yAxisDataKind;

  xAxisScale = other.xAxisScale;
  yAxisScale = other.yAxisScale;
  zAxisScale = other.zAxisScale;

  keyCellCount = other.keyCellCount;
  mzCellCount  = other.mzCellCount;

  minKeyValue = other.minKeyValue;
  maxKeyValue = other.maxKeyValue;

  minMzValue = other.minMzValue;
  maxMzValue = other.maxMzValue;

  origMinZValue = other.origMinZValue;
  lastMinZValue = other.lastMinZValue;

  origMaxZValue = other.origMaxZValue;
  lastMaxZValue = other.lastMaxZValue;
}


ColorMapPlotConfig &
ColorMapPlotConfig::operator=(const ColorMapPlotConfig &other)
{
  if(this == &other)
    return *this;

  xAxisDataKind = other.xAxisDataKind;
  yAxisDataKind = other.yAxisDataKind;

  xAxisScale = other.xAxisScale;
  yAxisScale = other.yAxisScale;
  zAxisScale = other.zAxisScale;

  keyCellCount = other.keyCellCount;
  mzCellCount  = other.mzCellCount;

  minKeyValue = other.minKeyValue;
  maxKeyValue = other.maxKeyValue;

  minMzValue = other.minMzValue;
  maxMzValue = other.maxMzValue;

  origMinZValue = other.origMinZValue;
  lastMinZValue = other.lastMinZValue;

  origMaxZValue = other.origMaxZValue;
  lastMaxZValue = other.lastMaxZValue;

  return *this;
}


void
ColorMapPlotConfig::setOrigMinZValue(double value)
{
  origMinZValue = value;
}

void
ColorMapPlotConfig::setOrigAndLastMinZValue(double value)
{
  origMinZValue = value;
  lastMinZValue = value;
}


void
ColorMapPlotConfig::setOrigMaxZValue(double value)
{
  origMaxZValue = value;
}

void
ColorMapPlotConfig::setOrigAndLastMaxZValue(double value)
{
  origMaxZValue = value;
  lastMaxZValue = value;
}


QString
ColorMapPlotConfig::toString() const
{
  QString text = QString("xAxisDataKind: %1 - yAxisDataKind: %2")
                   .arg(static_cast<int>(xAxisDataKind))
                   .arg(static_cast<int>(yAxisDataKind));

  text += QString("xAxisScale: %1 - yAxisScale: %2 - zAxisScale: %3 - ")
            .arg(static_cast<int>(xAxisScale))
            .arg(static_cast<int>(yAxisScale))
            .arg(static_cast<int>(zAxisScale));

  text += QString("keyCellCount: %1 - mzCellCount: %2 - ")
            .arg(mzCellCount)
            .arg(minKeyValue);

  text += QString(
            "minKeyValue: %8 - maxKeyValue: %9 - minMzValue: %10 - maxMzValue: "
            "%11 - lastMinZValue: %12 - lastMaxZValue: %13")
            .arg(keyCellCount)
            .arg(maxKeyValue)
            .arg(minMzValue)
            .arg(maxMzValue)
            .arg(lastMinZValue)
            .arg(lastMaxZValue);

  return text;
}

} // namespace pappso
