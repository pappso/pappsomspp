/**
 * \file pappsomspp/widget/obo/obotermform/obotermform.h
 * \date 20/04/2021
 * \author Olivier Langella
 * \brief display an obo term form
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <QWidget>
#include "../../../exportinmportconfig.h"
#include "../../../obo/obopsimodterm.h"
namespace Ui
{
class OboTermForm;
}

namespace pappso
{

class PMSPP_LIB_DECL OboTermForm : public QWidget
{
  Q_OBJECT
  public:
  /**
   * Default constructor
   */
  explicit OboTermForm(QWidget *parent = nullptr);

  /**
   * Destructor
   */
  ~OboTermForm();


  /** @brief tells if an OBO term is displayed
   */
  bool isOboTerm() const;

  /** @brief get the obo term or an exception
   */
  const OboPsiModTerm &getOboPsiModTerm() const;

  public slots:
  void displayOboTerm(OboPsiModTerm oboTerm);


  protected:
  /** @brief parse the definition label to extract external links
   * */
  void parseDefinitionLabel();


  private:
  Ui::OboTermForm *ui;

  OboPsiModTerm m_oboPsiModTerm;


  static QRegularExpression m_findExternalLinks;
};
} // namespace pappso
