/**
 * \file pappsomspp/widget/obo/obochooserwidget/obochooserwidget.h
 * \date 19/04/2021
 * \author Olivier Langella
 * \brief display obo term list and choose items
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <QWidget>
#include "../../../exportinmportconfig.h"
#include "../../../obo/obopsimodterm.h"
#include "../../../precision.h"
namespace Ui
{
class OboChooserWidgetForm;
}

namespace pappso
{

class PMSPP_LIB_DECL OboChooserWidget : public QWidget
{
  Q_OBJECT
  public:
  /**
   * Default constructor
   */
  explicit OboChooserWidget(QWidget *parent = nullptr);

  /**
   * Destructor
   */
  ~OboChooserWidget();


  /** @brief tells if an OBO term has been selected
   */
  bool isOboTermSelected() const;

  /** @brief get the selected obo term or an exception
   */
  const OboPsiModTerm &getOboPsiModTermSelected() const;

  /** @brief set mz target to filter among possible modifications
   */
  void setMzTarget(double target_mz);


  /** @brief set mz precision to filter among possible modifications
   */
  void setPrecision(PrecisionPtr precision);


  private:
  Ui::OboChooserWidgetForm *ui;
};
} // namespace pappso
