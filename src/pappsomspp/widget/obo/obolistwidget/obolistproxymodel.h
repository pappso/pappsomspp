/**
 * \file pappsomspp/widget/obo/obolistwidget/obolistproxymodel.h
 * \date 19/04/2021
 * \author Olivier Langella
 * \brief MVC proxy model of OBO term list
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QSortFilterProxyModel>
#include "obolistwidget.h"
#include "obolistmodel.h"

namespace pappso
{

/**
 * @todo write docs
 */
class OboListProxyModel : public QSortFilterProxyModel
{
  public:
  /**
   * Default constructor
   */
  OboListProxyModel(OboListModel *source_model, QObject *parent = nullptr);

  /**
   * Destructor
   */
  ~OboListProxyModel();

  void filterMzPrecision(double target_mz, PrecisionPtr precision);

  protected:
  bool lessThan(const QModelIndex &source_left,
                const QModelIndex &source_right) const override;
  bool filterAcceptsRow(int source_row,
                        const QModelIndex &source_parent) const override;

  private:
  PrecisionPtr m_precisionPtr = nullptr;
  double m_mzTarget           = 0;
  OboListModel *mp_sourceModel;
};

} // namespace pappso
