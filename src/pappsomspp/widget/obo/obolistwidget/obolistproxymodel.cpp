/**
 * \file pappsomspp/widget/obo/obolistwidget/obolistproxymodel.cpp
 * \date 19/04/2021
 * \author Olivier Langella
 * \brief MVC proxy model of OBO term list
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "obolistproxymodel.h"
#include "../../../obo/obopsimod.h"
#include "../../../mzrange.h"
#include <QDebug>

using namespace pappso;

OboListProxyModel::OboListProxyModel(OboListModel *source_model,
                                     QObject *parent)
  : QSortFilterProxyModel(parent)
{
  setFilterRole(Qt::DisplayRole);
  mp_sourceModel = source_model;
}

OboListProxyModel::~OboListProxyModel()
{
}

bool
pappso::OboListProxyModel::lessThan(const QModelIndex &source_left,
                                    const QModelIndex &source_right) const
{
  // qDebug();

  QVariant leftData  = sourceModel()->data(source_left, Qt::UserRole);
  QVariant rightData = sourceModel()->data(source_right, Qt::UserRole);

  return leftData.value<OboPsiModTerm>().m_diffMono <
         rightData.value<OboPsiModTerm>().m_diffMono;
}

void
pappso::OboListProxyModel::filterMzPrecision(double target_mz,
                                             pappso::PrecisionPtr precision)
{
  m_precisionPtr = precision;
  m_mzTarget     = target_mz;
  invalidateFilter();
}

bool
pappso::OboListProxyModel::filterAcceptsRow(int source_row,
                                            const QModelIndex &source_parent
                                            [[maybe_unused]]) const
{
  // qDebug();
  if(m_precisionPtr != nullptr)
    {
      MzRange range(m_mzTarget, m_precisionPtr);
      OboPsiModTerm term = mp_sourceModel->getOboPsiModTerm(source_row);
      /*
      qDebug() << source_row << " " << source_parent.row() << " "
               << range.toString() << " " << term.m_diffMono << " "
               << term.m_accession;
               */
      if(range.contains(term.m_diffMono))
        {
          // qDebug();
        }
      else
        {
          // qDebug() << "false";
          return false;
        }
    }
  return true;
}
