/**
 * \file pappsomspp/processing/specself/selfspectrumdatapoint.h
 * \date 23/03/2024
 * \author Olivier Langella
 * \brief spectrum self data points stored in the self spectrum matrix
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms++.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once
#include "../../amino_acid/aastringcodemassmatching.h"
#include "../../trace/datapoint.h"

namespace pappso
{
namespace specself
{

struct SelfSpectrumDataPoint
{
  void fillSelfSpectrumDataPoint(
    const pappso::AaStringCodeMassMatching &codec_mass_matching,
    const DataPoint &peak1,
    const DataPoint &peak2);
  void fillAntiSpectrumDataPoint(
    double precursor_mass,
    const pappso::AaStringCodeMassMatching &codec_mass_matching,
    const DataPoint &peak1,
    const DataPoint &peak2);


  void setVariableModification(
    const pappso::AaStringCodeMassMatching &codec_mass_matching,
    const Aa &aa,
    int quantifier);

  bool m_isComplement=false;
  double massDelta;
  double antiMassDelta;
  std::vector<uint32_t> aaCodeList;
  std::vector<uint32_t> antiAaCodeList;
};

} // namespace specself
} // namespace pappso

