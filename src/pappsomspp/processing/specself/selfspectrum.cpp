/**
 * \file pappsomspp/processing/specself/selfspectrum.cpp
 * \date 23/03/2024
 * \author Olivier Langella
 * \brief spectrum self operations
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms++.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "selfspectrum.h"
#include <QDebug>

namespace pappso
{
namespace specself
{

SelfSpectrum::SelfSpectrum(
  const pappso::AaStringCodeMassMatching &codec_mass_matching,
  const Trace &trace)
{
  m_trace = trace;
  m_matrix.resize(m_trace.size(), m_trace.size());

  qDebug() << m_trace.size();
  for(auto itmi = ++m_matrix.begin1(); itmi != m_matrix.end1(); ++itmi)
    {
      qDebug() << itmi.index1();
      for(auto itmj = ++itmi.begin(); itmj != itmi.end(); itmj++)
        {
          // itmj.fillSelfDataPoint(m_trace[itmj.index1()],
          // m_trace[itmj.index2()]);
          (*itmj).fillSelfSpectrumDataPoint(codec_mass_matching,
                                            m_trace[itmj.index1()],
                                            m_trace[itmj.index2()]);
        }
    }

  qDebug();
}

SelfSpectrum::~SelfSpectrum()
{
}

const SelfSpectrumMatrix &
SelfSpectrum::getMatrix() const
{
  return m_matrix;
}


const Trace &
SelfSpectrum::getTrace() const
{
  return m_trace;
}


void
SelfSpectrum::setPrecursorMass(
  const pappso::AaStringCodeMassMatching &codec_mass_matching,
  double precursor_mass)
{

  for(auto itmi = ++m_matrix.begin1(); itmi != m_matrix.end1(); ++itmi)
    {
      for(auto itmj = ++itmi.begin(); itmj != itmi.end(); itmj++)
        {
          // itmj.fillSelfDataPoint(m_trace[itmj.index1()],
          // m_trace[itmj.index2()]);
          (*itmj).fillAntiSpectrumDataPoint(precursor_mass,
                                            codec_mass_matching,
                                            m_trace[itmj.index1()],
                                            m_trace[itmj.index2()]);
        }
    }
}


void
SelfSpectrum::setVariableModification(
  const pappso::AaStringCodeMassMatching &codec_mass_matching,
  const Aa &aa,
  int quantifier)
{

  for(auto itmi = ++m_matrix.begin1(); itmi != m_matrix.end1(); ++itmi)
    {
      for(auto itmj = ++itmi.begin(); itmj != itmi.end(); itmj++)
        {
          // itmj.fillSelfDataPoint(m_trace[itmj.index1()],
          // m_trace[itmj.index2()]);
          (*itmj).setVariableModification(codec_mass_matching, aa, quantifier);
        }
    }
}

} // namespace specself
} // namespace pappso
