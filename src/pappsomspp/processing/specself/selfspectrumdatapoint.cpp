/**
 * \file pappsomspp/processing/specself/selfspectrumdatapoint.h
 * \date 23/03/2024
 * \author Olivier Langella
 * \brief spectrum self data points stored in the self spectrum matrix
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms++.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "selfspectrumdatapoint.h"


namespace pappso
{
namespace specself
{

void
SelfSpectrumDataPoint::fillSelfSpectrumDataPoint(
  const pappso::AaStringCodeMassMatching &codec_mass_matching,
  const DataPoint &peak1,
  const DataPoint &peak2)
{

  massDelta = peak1.x - peak2.x;

  // qDebug() << massDelta;
  if(massDelta > 0)
    {
      aaCodeList = codec_mass_matching.getAaCodeFromMass(massDelta);
    }
  // m_targetMzSum = m_precursorMass + pappso::MHPLUS + pappso::MHPLUS;
  // double precusorBion = m_targetMzSum - pappso::MASSH2O - pappso::MPROTIUM;
  // if peak2 is the other ion suite (Y <=> B)
  antiMassDelta = 0;
  antiAaCodeList.clear();

  // qDebug();
}


void
SelfSpectrumDataPoint::fillAntiSpectrumDataPoint(
  double precursor_mass,
  const pappso::AaStringCodeMassMatching &codec_mass_matching,
  const DataPoint &peak1,
  const DataPoint &peak2)
{
  precursor_mass = precursor_mass + MHPLUS + MHPLUS;
  antiMassDelta  = (precursor_mass - peak1.x) - peak2.x;

  m_isComplement = false;
  if(std::abs(antiMassDelta) <
     codec_mass_matching.getPrecisionPtr()->getNominal())
    {
      m_isComplement = true;
    }
  else
    {

      if(antiMassDelta > 0)
        {
          antiAaCodeList = codec_mass_matching.getAaCodeFromMass(antiMassDelta);
        }
    }
}


void
SelfSpectrumDataPoint::setVariableModification(
  const pappso::AaStringCodeMassMatching &codec_mass_matching,
  const Aa &aa,
  int quantifier)
{

  if(massDelta > 0)
    {
      std::vector<uint32_t> new_code_list =
        codec_mass_matching.getAaCodeFromMassWearingModification(
          massDelta, aa, quantifier);
      std::copy(new_code_list.begin(),
                new_code_list.end(),
                std::back_inserter(aaCodeList));
    }
  if(antiMassDelta > 0)
    {
      std::vector<uint32_t> new_code_list =
        codec_mass_matching.getAaCodeFromMassWearingModification(
          antiMassDelta, aa, quantifier);
      std::copy(new_code_list.begin(),
                new_code_list.end(),
                std::back_inserter(antiAaCodeList));
    }
}

} // namespace specself
} // namespace pappso
