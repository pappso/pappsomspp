/**
 * \file pappsomspp/processing/specself/selfspectrum.h
 * \date 23/03/2024
 * \author Olivier Langella
 * \brief spectrum self operations
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms++.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../../trace/trace.h"
#include <boost/numeric/ublas/matrix.hpp>
#include "selfspectrumdatapoint.h"

namespace pappso
{
namespace specself
{

typedef boost::numeric::ublas::matrix<SelfSpectrumDataPoint> SelfSpectrumMatrix;
/**
 * @todo write docs
 */
class SelfSpectrum
{
  public:
  /**
   * Default constructor
   */
  SelfSpectrum(const pappso::AaStringCodeMassMatching &codec_mass_matching,
               const Trace &trace);
  /**
   * Destructor
   */
  virtual ~SelfSpectrum();

  void
  setPrecursorMass(const pappso::AaStringCodeMassMatching &codec_mass_matching,
                   double precursor_mass);


  void setVariableModification(
    const pappso::AaStringCodeMassMatching &codec_mass_matching,
    const Aa &aa,
    int quantifier);

  const SelfSpectrumMatrix &getMatrix() const;
  const Trace &getTrace() const;

  private:
  Trace m_trace;
  SelfSpectrumMatrix m_matrix;
};
} // namespace specself
} // namespace pappso
