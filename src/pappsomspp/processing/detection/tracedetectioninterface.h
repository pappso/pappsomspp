
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "tracepeak.h"

namespace pappso
{
class TraceDetectionInterface;

typedef std::shared_ptr<TraceDetectionInterface> TraceDetectionInterfaceSPtr;
typedef std::shared_ptr<const TraceDetectionInterface>
  TraceDetectionInterfaceCstSPtr;

class TraceDetectionSinkInterface
{
  public:
  virtual void setTracePeak(TracePeak &xic_peak) = 0;
};

class TraceDetectionInterface
{
  public:
  /** @brief detect peaks on a trace
   * @param trace the trace to detect peaks on
   * @param sink the object to store peaks or stream it
   * @param remove_peak_base if true, removes the area under the base of the
   * peak
   */
  virtual void detect(const Trace &trace,
                      TraceDetectionSinkInterface &sink,
                      bool remove_peak_base) const = 0;
};


} // namespace pappso
