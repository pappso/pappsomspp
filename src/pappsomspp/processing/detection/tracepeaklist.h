/**
 * \file pappsomspp/processing/detection/tracepeaklist.h
 * \date 15/09/2021
 * \author Olivier Langella
 * \brief trace peak list
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once

#include "tracepeak.h"
#include "tracedetectioninterface.h"


namespace pappso
{
/**
 * @todo write docs
 */
class PMSPP_LIB_DECL TracePeakList : public std::vector<TracePeak>,
                                     public TraceDetectionSinkInterface
{
  public:
  /**
   * Default constructor
   */
  TracePeakList();

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  TracePeakList(const TracePeakList &other);

  /**
   * Destructor
   */
  virtual ~TracePeakList();

  virtual void setTracePeak(TracePeak &xic_peak) override;
};


/** @brief find the best peak matching a list of retention times
 *
 * @param begin begin iterator
 * @param end end iterator
 * @param rt_list retention time list in seconds
 * @param nb_peaks retrieve the number of detected peaks concerned by the
 * retention times
 * @return iterator on the best peak (bigger area) concerned by the retention
 * times
 *
 * */
PMSPP_LIB_DECL std::vector<TracePeak>::iterator
findBestTracePeakGivenRtList(std::vector<TracePeak>::iterator begin,
                             std::vector<TracePeak>::iterator end,
                             const std::vector<double> &rt_list,
                             std::size_t &nb_peaks);


/** @brief find the peak matching a retention time
 *
 * @param begin begin iterator
 * @param end end iterator
 * @param rt retention time in seconds
 * @return iterator on the peak concerned by the retention time
 * */
PMSPP_LIB_DECL std::vector<TracePeak>::iterator
findTracePeakGivenRt(std::vector<TracePeak>::iterator begin,
                     std::vector<TracePeak>::iterator end,
                     double rt);


} // namespace pappso
