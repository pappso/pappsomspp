/**
 * \file pappsomspp/processing/detection/tracepeaklist.cpp
 * \date 15/09/2021
 * \author Olivier Langella
 * \brief trace peak list
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include "tracepeaklist.h"

using namespace pappso;

TracePeakList::TracePeakList()
{
}

TracePeakList::TracePeakList(const TracePeakList &other)
  : std::vector<TracePeak>(other)
{
}

TracePeakList::~TracePeakList()
{
}

void
pappso::TracePeakList::setTracePeak(pappso::TracePeak &xic_peak)
{
  push_back(xic_peak);
}


std::vector<TracePeak>::iterator
pappso::findBestTracePeakGivenRtList(std::vector<TracePeak>::iterator begin,
                                     std::vector<TracePeak>::iterator end,
                                     const std::vector<double> &rt_list,
                                     std::size_t &nb_peaks)
{
  // get peak containing rt ahead :
  auto itend = std::partition(begin, end, [rt_list](const TracePeak &a) {
    for(double rt : rt_list)
      {
        if(a.containsRt(rt))
          {
            return true;
          };
      }
    return false;
  });
  nb_peaks   = std::distance(begin, itend);
  if(nb_peaks == 0)
    return end;
  return std::max_element(
    begin, itend, [](const TracePeak &a, const TracePeak &b) {
      return a.getArea() < b.getArea();
    });
}

std::vector<TracePeak>::iterator
pappso::findTracePeakGivenRt(std::vector<TracePeak>::iterator begin,
                             std::vector<TracePeak>::iterator end,
                             double rt)
{
  return find_if(
    begin, end, [rt](const TracePeak &a) { return a.containsRt(rt); });
}
