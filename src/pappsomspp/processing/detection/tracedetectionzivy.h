
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once


#include "tracedetectioninterface.h"
#include "../../processing/filters/filtermorpho.h"
#include "../../msrun/msrunreader.h"


namespace pappso
{

class PMSPP_LIB_DECL TraceDetectionZivy : public TraceDetectionInterface
{

  public:
  TraceDetectionZivy(unsigned int smoothing_half_window_length,
                     unsigned int minmax_half_window_length,
                     unsigned int maxmin_half_window_length,
                     double detection_threshold_on_minmax,
                     double detection_threshold_on_maxmin);

  TraceDetectionZivy(const TraceDetectionZivy &other);
  virtual ~TraceDetectionZivy();

  void guessParametersFromMsRunReader(const MsRunReader &reader);

  void setFilterMorphoMean(const FilterMorphoMean &smooth);
  void setFilterMorphoMinMax(const FilterMorphoMinMax &m_minMax);
  void setFilterMorphoMaxMin(const FilterMorphoMaxMin &maxMin);
  void setDetectionThresholdOnMinmax(double detectionThresholdOnMinMax);
  void setDetectionThresholdOnMaxmin(double detectionThresholdOnMaxMin);

  unsigned int getSmoothingHalfEdgeWindows() const;
  unsigned int getMaxMinHalfEdgeWindows() const;

  unsigned int getMinMaxHalfEdgeWindows() const;
  double getDetectionThresholdOnMinmax() const;
  double getDetectionThresholdOnMaxmin() const;

  void
  detect(const Trace &xic, TraceDetectionSinkInterface &sink, bool remove_peak_base) const override;


  private:
  FilterMorphoMean m_smooth;
  FilterMorphoMinMax m_minMax;
  FilterMorphoMaxMin m_maxMin;
  double m_detectionThresholdOnMinMax;
  double m_detectionThresholdOnMaxMin;
};

} // namespace pappso
