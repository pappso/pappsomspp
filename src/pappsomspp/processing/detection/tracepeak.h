/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include "../../exportinmportconfig.h"
#include "../../trace/trace.h"
#include "../../types.h"

namespace pappso
{

class TracePeak;
typedef std::shared_ptr<const TracePeak> TracePeakCstSPtr;

/** @/brief Xic Peak stores peak boudaries detected from a Xic
 * */
class PMSPP_LIB_DECL TracePeak
{
  public:
  TracePeak();
  /** @brief construct a peak given a trace, begin and end x coordinate
   */
  TracePeak(std::vector<DataPoint>::const_iterator it_begin,
            std::vector<DataPoint>::const_iterator it_end);

  /** @brief construct a peak given a trace, begin and end x coordinate
   *
   * @param it_begin begining of trace
   * @param it_end end of trace
   * @param remove_base if true, remove the base peak signal
   */
  TracePeak(std::vector<DataPoint>::const_iterator it_begin,
            std::vector<DataPoint>::const_iterator it_end,
            bool remove_base);
  TracePeak(const TracePeak &other);
  ~TracePeak();


  TracePeakCstSPtr makeTracePeakCstSPtr() const;

  DataPoint &getMaxXicElement();
  const DataPoint &getMaxXicElement() const;
  void setMaxXicElement(const DataPoint &max);

  DataPoint &getLeftBoundary();
  const DataPoint &getLeftBoundary() const;
  void setLeftBoundary(const DataPoint &left);

  DataPoint &getRightBoundary();
  const DataPoint &getRightBoundary() const;
  void setRightBoundary(const DataPoint &right);
  pappso_double getArea() const;


  void setArea(pappso_double area);

  bool containsRt(pappso::pappso_double rt) const;


  bool operator==(const TracePeak &other) const;

  TracePeak &operator=(const TracePeak &other);


  protected:
  pappso_double m_area = 0;
  DataPoint m_max;
  DataPoint m_left;
  DataPoint m_right;
};


} // namespace pappso
