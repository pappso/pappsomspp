/**
 * \file pappsomspp/processing/cbor/cborstreamreaderinterface.h
 * \date 11/02/2025
 * \author Olivier Langella
 * \brief common interface to read CBOR streams with convenient framework
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella <Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "cborstreamreaderinterface.h"

pappso::CborStreamReaderInterface::CborStreamReaderInterface()
{
}

pappso::CborStreamReaderInterface::~CborStreamReaderInterface()
{
}


bool
pappso::CborStreamReaderInterface::getExpectedString()
{
  bool is_ok       = false;
  m_expectedString = "";
  if(m_cborReader.type() == QCborStreamReader::String)
    {
      is_ok  = true;
      auto r = m_cborReader.readString();
      while(r.status == QCborStreamReader::Ok)
        {
          m_expectedString = r.data;

          r = m_cborReader.readString();
        }
    }

  return is_ok;
}


void
pappso::CborStreamReaderInterface::initCborReader(QFile *pcborfile)
{

  // try to mmap the file, this is faster
  char *ptr = reinterpret_cast<char *>(
    pcborfile->map(0, pcborfile->size(), QFile::MapPrivateOption));
  if(ptr)
    {
      // worked
      m_data = QByteArray::fromRawData(ptr, pcborfile->size());
      m_cborReader.addData(m_data);
    }
  else if(pcborfile->isSequential())
    {
      // details requires full contents, so allocate memory
      m_data = pcborfile->readAll();
      m_cborReader.addData(m_data);
    }
  else
    {
      // just use the QIODevice
      m_cborReader.setDevice(pcborfile);
    }
}
