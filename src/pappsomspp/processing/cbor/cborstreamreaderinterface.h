/**
 * \file pappsomspp/processing/cbor/cborstreamreaderinterface.h
 * \date 11/02/2025
 * \author Olivier Langella
 * \brief common interface to read CBOR streams with convenient framework
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella <Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#pragma once

#include <QCborStreamReader>
#include <QFile>
#include "../../exportinmportconfig.h"

namespace pappso
{

/**
 * @todo write docs
 */
class PMSPP_LIB_DECL CborStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  CborStreamReaderInterface();

  /**
   * Destructor
   */
  virtual ~CborStreamReaderInterface();

  protected:
  bool getExpectedString();

  void initCborReader(QFile *pcborfile);

  protected:
  QCborStreamReader m_cborReader;
  QString m_expectedString;
  QByteArray m_data;
};
} // namespace pappso
