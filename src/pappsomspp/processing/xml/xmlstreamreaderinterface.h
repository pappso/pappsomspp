/**
 * \file pappsomspp/processing/xml/xmlstreamreaderinterface.h
 * \date 12/11/2021
 * \author Olivier Langella
 * \brief common interface to read all XML streams containing convenient
 * functions
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#pragma once

#include <QIODevice>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include "../../exportinmportconfig.h"

namespace pappso
{
/**
 * @brief convenient xml reader helper
 */
class PMSPP_LIB_DECL XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  XmlStreamReaderInterface();

  /**
   * Destructor
   */
  virtual ~XmlStreamReaderInterface();


  virtual bool readFile(const QString &fileName);

  bool read(QIODevice *device);
  bool read(const QString &xml_content);


  QString errorString() const;

  protected:
  virtual void readStream() = 0;

  void cloneStartElement(QXmlStreamWriter &output) const;

  void cloneElement(QXmlStreamWriter &output);
  void cloneNode(QXmlStreamWriter &output);

  protected:
  QXmlStreamReader m_qxmlStreamReader;
};
} // namespace pappso
