/**
 * \file processing/xml/mzidentmlwriter.h
 * \date 07/02/2024
 * \author Olivier Langella
 * \brief MzIdentML writer
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QString>
#include <QXmlStreamWriter>
#include "../../types.h"
#include "../../psm/deepprot/deepprotenum.h"
#include "../../amino_acid/aamodification.h"
#include "../../protein/protein.h"
#include "../../exportinmportconfig.h"

namespace pappso
{


/**
 * @todo write docs
 */
class PMSPP_LIB_DECL MzIdentMlWriter : public QXmlStreamWriter
{
  public:
  /**
   * Default constructor
   */
  MzIdentMlWriter();

  /**
   * Destructor
   */
  ~MzIdentMlWriter();
  
  void writeDBSequence(const QString &id,
                       const QString &db_ref_id,
                       const pappso::Protein & protein,
                       bool is_reverse);

  void writeDBSequence(const QString &id,
                       const QString &db_ref_id,
                       const QString &accession,
                       const QString &description,
                       const QString &sequence,
                       bool is_reverse);

  void writeStartMzIdentMlDocument(const QString &id, const QString &version);

  void writeCvList();

  void writeCvParam(pappso::AaModificationP modification);

  void writeCvParam(const pappso::OboPsiModTerm &term, const QString &value);

  void writeCvParam(const QString &cv_ref,
                    const QString &accession,
                    const QString &name,
                    const QString &value);
  void writeCvParamUo(const QString &cv_ref,
                      const QString &accession,
                      const QString &name,
                      const QString &value,
                      const QString &unit_cv_ref,
                      const QString &unit_accession,
                      const QString &unit_name);

  void writeCvParamUo(const QString &cv_ref,
                      const QString &accession,
                      const QString &name,
                      const QString &value,
                      pappso::PrecisionUnit precision_unit);
  void writeUserParam(const QString &name, const QString &value);
  void writeUserParam(const QString &name, double value);


  void writeUserParam(pappso::DeepProtMatchType matchType);
  void writeUserParam(pappso::DeepProtPeptideCandidateStatus status);

  static const QString toXmlMass(double mass);
  static const QString toXmlMassDelta(double mass);
  static const QString toXmlDouble(double value);

  private:
  // QString m_mzidentmlNamespace = "http://psidev.info/psi/pi/mzIdentML/1.2";
};
} // namespace pappso
