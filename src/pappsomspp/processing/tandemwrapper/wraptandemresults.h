/**
 * \file pappsomspp/processing/tandemwrapper/wraptandemresults.h
 * \date 13/11/2021
 * \author Olivier Langella
 * \brief rewrites tandem xml output file with temporary files
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDebug>
#include <QFile>
#include "../xml/xmlstreamreaderinterface.h"


namespace pappso
{


/**
 * @todo write docs
 */
class WrapTandemResults : public XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  WrapTandemResults(const QString &final_tandem_output,
                    const QString &original_msdata_file_name);

  /**
   * Destructor
   */
  virtual ~WrapTandemResults();

  void setInputParameters(const QString &label_name_attribute,
                          const QString &input_value);

  protected:
  virtual void readStream() override;

  private:
  void process_group_note();


  private:
  QFile m_destinationTandemOutputFile;
  QXmlStreamWriter m_writerXmlTandemOutput;
  QString m_originalMsDataFileName;

  std::map<QString, QString> m_mapTandemInputParameters;
};
} // namespace pappso
