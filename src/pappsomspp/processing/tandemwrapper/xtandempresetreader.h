/**
 * \file pappsomspp/processing/tandemwrapper/xtandempresetreader.h
 * \date 06/02/2020
 * \author Olivier Langella
 * \brief read tandem preset file to get centroid parameters and number of
 * threads
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDebug>
#include "../../types.h"
#include "../xml/xmlstreamreaderinterface.h"
/**
 * @todo write docs
 */
namespace pappso
{
class XtandemPresetReader : public XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  XtandemPresetReader();

  /**
   * Destructor
   */
  virtual ~XtandemPresetReader();

  int getNumberOfThreads() const;
  const QString getMs2FiltersOptions() const;
  int getCountNote() const;

  protected:
  virtual void readStream() override;

  private:
  void read_note();

  private:
  int m_countNote = -1;
  int m_threads   = -1;

  PrecisionUnit m_ms2precisionUnit = PrecisionUnit::dalton;
  double m_ms2precisionValue       = 0;
};
} // namespace pappso
