/**
 * \file pappsomspp/processing/uimonitor/uimonitorinterface.h
 * \date 14/5/2021
 * \author Olivier Langella
 * \brief generic interface to monitor any long process
 * A long process needs to be interrupted by the user and to report progress
 *while running. This interface is used by long process in this library.
 **/

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <numeric>
#include <QString>
#include "../../exportinmportconfig.h"

namespace pappso
{

/**
 * @todo pure virtual user interface class to monitor any long process
 * it contains :
 * - title, status, progress messages
 * - callback functions to interrupt long process
 * it does not contains any error messages functions : use
 * pappso::ExceptionInterrupted to handle this
 *
 */
class PMSPP_LIB_DECL UiMonitorInterface
{
  public:
  /** @brief should the procces be stopped ?
   * If true, then cancel process
   * Use this function at strategic point of your process in order to interrupt
   * it cleanly
   * Implementation **must** take care of thread resistance if implemented
   */
  virtual bool shouldIstop() = 0;

  /** @brief use it if the number of steps is known in an algorithm
   * the total number of steps is usefull to report to the user a progress
   * message in percents or with a progress bar
   * @param total_number_of_steps the total number of steps
   */
  virtual void
  setTotalSteps(std::size_t total_number_of_steps)
  {
    m_totalSteps = total_number_of_steps;
  };

  /** @brief count steps
   * report when a step is computed in an algorithm
   */
  virtual void count() = 0;

  /** @brief current kind of process running
   * @param title process title
   */
  virtual void setTitle(const QString &title) = 0;

  /** @brief current status of the process
   * @param status status message
   */
  virtual void setStatus(const QString &status) = 0;

  /** @brief append a text to a long report
   * @param text string to append in a long report
   */
  virtual void appendText(const QString &text) = 0;

  protected:
  std::size_t m_totalSteps = 0;
};
} // namespace pappso
