/**
 * \file pappsomspp/processing/uimonitor/uimonitortext.h
 * \date 14/5/2021
 * \author Olivier Langella
 * \brief simle text monitor implementation of the User Interface Monitor
 **/

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "uimonitorinterface.h"
#include <QTextStream>

namespace pappso
{


/**
 * @todo simple text monitor using text stream
 */
class PMSPP_LIB_DECL UiMonitorText : public UiMonitorInterface
{
  public:
  /**
   * Default constructor
   */
  UiMonitorText(QTextStream &output_stream);

  /**
   * Destructor
   */
  ~UiMonitorText();

  virtual bool shouldIstop() override;

  /** @brief count steps
   * report when a step is computed in an algorithm
   */
  virtual void count() override;

  /** @brief current kind of process running
   * @param title process title
   */
  virtual void setTitle(const QString &title) override;

  /** @brief current status of the process
   * @param status status message
   */
  virtual void setStatus(const QString &status) override;

  /** @brief append a text to a long report
   * @param text string to append in a long report
   */
  virtual void appendText(const QString &text) override;

  virtual void setTotalSteps(std::size_t total_number_of_steps);


  protected:
  QTextStream &m_outputStream;
  std::size_t m_count = 0;
};
} // namespace pappso
