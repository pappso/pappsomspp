/**
 * \file pappsomspp/processing/specglob/peptidespectrum.cpp
 * \date 06/11/2023
 * \author Olivier Langella
 * \brief transform a peptide to SpecGlob spectrum
 *
 * C++ implementation of the SpecGlob algorithm described in :
 * 1. Prunier, G. et al. Fast alignment of mass spectra in large proteomics
 * datasets, capturing dissimilarities arising from multiple complex
 * modifications of peptides. BMC Bioinformatics 24, 421 (2023).
 *
 *     HAL Id : hal-04296170 , version 1
 *     Mot de passe : hxo20cl
 *     DOI : 10.1186/s12859-023-05555-y
 */


/*
 * SpecGlobTool, Spectra to peptide alignment tool
 * Copyright (C) 2023  Olivier Langella
 * <olivier.langella@universite-paris-saclay.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "peptidespectrum.h"
#include "../../peptide/peptidefragmentionlistbase.h"
#include "../../peptide/peptiderawfragmentmasses.h"

namespace pappso
{
namespace specglob
{
PeptideSpectrum::PeptideSpectrum(const pappso::PeptideSp peptide_sp)
  : std::vector<PeptideSpectrumDataPoint>()
{
  pappso::PeptideIon ion_type = pappso::PeptideIon::b;
  msp_peptide                 = peptide_sp;
  std::list<pappso::PeptideIon> ion_list;
  ion_list.push_back(ion_type);


  pappso::PeptideFragmentIonListBase fragmented_ion_list(msp_peptide, ion_list);

  std::list<pappso::PeptideFragmentIonSp> peptide_ion_list =
    fragmented_ion_list.getPeptideFragmentIonSp(pappso::PeptideIon::b);

  push_back({pappso::MHPLUS, 0});
  // qDebug() << pappso::PeptideRawFragmentMasses::getDeltaMass(ion_type);
  for(auto &peptide_ion : peptide_ion_list)
    {
      push_back({peptide_ion.get()->getMz(1), 0});
    }
  push_back({(msp_peptide.get()->getMz(1) - pappso::MASSH2O +
              pappso::PeptideRawFragmentMasses::getDeltaMass(ion_type)),
             0});
  for(std::size_t i = 1; i < size(); i++)
    {
      at(i).diff_mz = (at(i).mz - at(i - 1).mz);
    }
}

PeptideSpectrum::PeptideSpectrum(const PeptideSpectrum &other)
  : std::vector<PeptideSpectrumDataPoint>(other)
{
  msp_peptide = other.msp_peptide;
}

PeptideSpectrum::~PeptideSpectrum()
{
}

std::vector<double>
PeptideSpectrum::getMassList() const
{
  std::vector<double> mass_list;
  for(const PeptideSpectrumDataPoint &n : *this)
    {
      mass_list.push_back(n.mz);
    };

  return mass_list;
}

pappso::PeptideSp
PeptideSpectrum::getPeptideSp() const
{
  return msp_peptide;
}
} // namespace specglob
} // namespace pappso
