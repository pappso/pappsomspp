/**
 * \file pappsomspp/processing/specglob/scorevalues.h
 * \date 06/11/2023
 * \author Olivier Langella
 * \brief scores to apply in comparisons
 *
 * C++ implementation of the SpecGlob algorithm described in :
 * 1. Prunier, G. et al. Fast alignment of mass spectra in large proteomics
 * datasets, capturing dissimilarities arising from multiple complex
 * modifications of peptides. BMC Bioinformatics 24, 421 (2023).
 *
 *     HAL Id : hal-04296170 , version 1
 *     Mot de passe : hxo20cl
 *     DOI : 10.1186/s12859-023-05555-y
 */


/*
 * SpecGlobTool, Spectra to peptide alignment tool
 * Copyright (C) 2023  Olivier Langella
 * <olivier.langella@universite-paris-saclay.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <cstdint>
#include "../../exportinmportconfig.h"
#include "types.h"


namespace pappso
{
namespace specglob
{

/**
 * @todo write docs
 */
class PMSPP_LIB_DECL ScoreValues
{
  public:
  /**
   * Default constructor
   */
  ScoreValues();


  ScoreValues(const ScoreValues &other);

  /**
   * Destructor
   */
  virtual ~ScoreValues();

  const ScoreValues &operator=(const ScoreValues &other);

  int get(ScoreValueType type);

  void set(ScoreValueType type, int value);

  private:
  int m_scoreTable[10];
};
} // namespace specglob
} // namespace pappso
