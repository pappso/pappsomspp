/**
 * \file pappsomspp/processing/specglob/scorevalues.h
 * \date 06/11/2023
 * \author Olivier Langella
 * \brief scores to apply in comparisons
 *
 * C++ implementation of the SpecGlob algorithm described in :
 * 1. Prunier, G. et al. Fast alignment of mass spectra in large proteomics
 * datasets, capturing dissimilarities arising from multiple complex
 * modifications of peptides. BMC Bioinformatics 24, 421 (2023).
 *
 *     HAL Id : hal-04296170 , version 1
 *     Mot de passe : hxo20cl
 *     DOI : 10.1186/s12859-023-05555-y
 */


/*
 * SpecGlobTool, Spectra to peptide alignment tool
 * Copyright (C) 2023  Olivier Langella
 * <olivier.langella@universite-paris-saclay.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "scorevalues.h"
#include <algorithm>


namespace pappso
{
namespace specglob
{
ScoreValues::ScoreValues()
{

  // #Scores to apply
  // #Score for non alignment (int)
  //  sg.scoreNonAlign=-4
  m_scoreTable[(std::uint8_t)ScoreValueType::scoreNonAlign] = -4;

  // #Score for re-alignment  (int)
  //  sg.scoreReAlignNative=-8
  m_scoreTable[(std::uint8_t)ScoreValueType::scoreReAlignNative] = -8;
  // sg.scoreReAlignSym=-8
  m_scoreTable[(std::uint8_t)ScoreValueType::scoreReAlignSym] = -8;
  // sg.scoreReAlignBoth=-6
  m_scoreTable[(std::uint8_t)ScoreValueType::scoreReAlignBoth] = -6;

  // #Score for good alignment(int)
  m_scoreTable[(std::uint8_t)ScoreValueType::scoreAlignNative] = 7;
  m_scoreTable[(std::uint8_t)ScoreValueType::scoreAlignSym]    = 7;
  m_scoreTable[(std::uint8_t)ScoreValueType::scoreAlignBoth]   = 10;


  // #Score for re - alignment without offset(int)
  m_scoreTable[(std::uint8_t)ScoreValueType::scoreReAlignNativeNO] = 4;
  m_scoreTable[(std::uint8_t)ScoreValueType::scoreReAlignSymNO]    = 4;
  m_scoreTable[(std::uint8_t)ScoreValueType::scoreReAlignBothNO]   = 7;
}

ScoreValues::ScoreValues(const ScoreValues &other)
{
  std::copy(other.m_scoreTable, other.m_scoreTable + 10, m_scoreTable);
}

ScoreValues::~ScoreValues()
{
}

int
ScoreValues::get(ScoreValueType type)
{
  return m_scoreTable[(std::uint8_t)type];
}

void
ScoreValues::set(ScoreValueType type, int value)
{
  m_scoreTable[(std::uint8_t)type] = value;
}

const ScoreValues &
ScoreValues::operator=(const ScoreValues &other)
{
  std::copy(other.m_scoreTable, other.m_scoreTable + 10, m_scoreTable);
  return *this;
}
} // namespace specglob
} // namespace pappso
