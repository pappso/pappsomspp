/**
 * \file pappsomspp/processing/specglob/types.h
 * \date 14/11/2023
 * \author Olivier Langella
 * \brief SpecGlobTool types definition
 *
 * C++ implementation of the SpecGlob algorithm described in :
 * 1. Prunier, G. et al. Fast alignment of mass spectra in large proteomics
 * datasets, capturing dissimilarities arising from multiple complex
 * modifications of peptides. BMC Bioinformatics 24, 421 (2023).
 *
 *     HAL Id : hal-04296170 , version 1
 *     Mot de passe : hxo20cl
 *     DOI : 10.1186/s12859-023-05555-y
 *
 * ANR founded project :
 * https://anr.fr/Project-ANR-18-CE45-0004
 */


/*
 * SpecGlobTool, Spectra to peptide alignment tool
 * Copyright (C) 2023  Olivier Langella
 * <olivier.langella@universite-paris-saclay.fr>
 *
 * This program is free software: you can redistribute ipetide to spectrum
 * alignmentt and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

namespace pappso
{
namespace specglob
{
enum class SpectralAlignmentType : std::uint8_t
{
  nonAlign =
    0, ///< the type of alignment to put in origin matrix NON Alignment (0 - NA)
  reAlign = 1, ///< Re Alignment (1 - RE)
  align   = 2, ///< Alignment (2 - AL)
};


enum class ScoreValueType : std::uint8_t
{
  scoreNonAlign = 0, ///< Score for non alignment (int)

  scoreReAlignNative = 1, ////algorithm< Score for re-alignment native  (int)
  scoreReAlignSym    = 2, ///< Score for re-alignment symmetric  (int)
  scoreReAlignBoth   = 3, ///< Score for re-alignment both  (int)


  scoreAlignNative = 4, ///< Score for good alignment native (int)
  scoreAlignSym    = 5, ///< Score for good alignment symmetric (int)
  scoreAlignBoth   = 6, ///< Score for good alignment both (int)

  scoreReAlignNativeNO =
    7, ///< Score for re-alignment without offset native (int)
  scoreReAlignSymNO =
    8, ///< Score for re-alignment without offset symmetric (int)
  scoreReAlignBothNO = 9, ///< Score for re-alignment without offset both (int)
};


enum class ExperimentalSpectrumDataPointType : std::uint8_t
{
  native = 0, ///< native peak, but has no mz counterpart (the complement ion),
              ///< we had to compute the symmetric mass
  symmetric =
    1, ///< new peak : computed symmetric mass from a corresponding native peak
  both =
    2, ///< both, the ion and the complement exists in the original spectrum
  synthetic =
    3, ///< does not correspond to existing peak, for computational purpose
};
} // namespace specglob
} // namespace pappso
