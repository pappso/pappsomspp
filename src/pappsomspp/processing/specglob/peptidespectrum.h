/**
 * \file pappsomspp/processing/specglob/peptidespectrum.h
 * \date 06/11/2023
 * \author Olivier Langella
 * \brief transform a peptide to SpecGlob spectrum
 *
 * C++ implementation of the SpecGlob algorithm described in :
 * 1. Prunier, G. et al. Fast alignment of mass spectra in large proteomics
 * datasets, capturing dissimilarities arising from multiple complex
 * modifications of peptides. BMC Bioinformatics 24, 421 (2023).
 *
 *     HAL Id : hal-04296170 , version 1
 *     Mot de passe : hxo20cl
 *     DOI : 10.1186/s12859-023-05555-y
 */


/*
 * SpecGlobTool, Spectra to peptide alignment tool
 * Copyright (C) 2023  Olivier Langella
 * <olivier.langella@universite-paris-saclay.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "../../peptide/peptide.h"
#include <memory>
#include "../../exportinmportconfig.h"

namespace pappso
{
namespace specglob
{
struct PeptideSpectrumDataPoint
{
  double mz;
  /** @brief mass difference between current and previous peptide amino acid
   */
  double diff_mz;
};

class PeptideSpectrum;

typedef std::shared_ptr<const PeptideSpectrum> PeptideSpectraCsp;
/**
 * @todo write docs
 */
class PMSPP_LIB_DECL PeptideSpectrum : public std::vector<PeptideSpectrumDataPoint>
{
  public:
  /**
   * Default constructor
   */
  PeptideSpectrum(const pappso::PeptideSp peptide_sp);

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  PeptideSpectrum(const PeptideSpectrum &other);

  /**
   * Destructor
   */
  virtual ~PeptideSpectrum();

  std::vector<double> getMassList() const;

  pappso::PeptideSp getPeptideSp() const;

  private:
  pappso::PeptideSp msp_peptide;
  // std::vector<double> m_massList;
};
} // namespace specglob
} // namespace pappso
