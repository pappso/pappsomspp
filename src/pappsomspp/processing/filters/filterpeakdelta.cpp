/**
 * \file pappsomspp/filers/filtepeakdelta.h
 * \date 05/04/2023
 * \author Olivier Langella
 * \brief transform trace into list of peak mass delta
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include "filterpeakdelta.h"

using namespace pappso;

FilterPeakDelta::FilterPeakDelta()
{
}

FilterPeakDelta::FilterPeakDelta(const FilterPeakDelta &other [[maybe_unused]])
{
}

FilterPeakDelta::~FilterPeakDelta()
{
}

pappso::Trace &
pappso::FilterPeakDelta::filter(pappso::Trace &data_points) const
{

  Trace old_trace(data_points);

  data_points.clear();

  auto it_out = old_trace.begin();
  auto it_in  = it_out + 1;
  auto it_end = old_trace.end();

  while(it_out != it_end)
    {
      while(it_in != it_end)
        {
          double intensity =
            (it_out->y + it_in->y) - (std::abs(it_out->y - it_in->y));
          data_points.push_back(
            DataPoint(std::abs(it_out->x - it_in->x), intensity));
          it_in++;
        }
      it_out++;
      it_in = it_out + 1;
    }
  data_points.sortX();
  return data_points;
}
