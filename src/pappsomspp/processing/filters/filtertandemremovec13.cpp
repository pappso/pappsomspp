/**
 * \file pappsomspp/filers/filtertandemremovec13.h
 * \date 26/04/2019
 * \author Olivier Langella
 * \brief new implementation of the X!Tandem filter to remove isotopes in an MS2
 * signal
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "filtertandemremovec13.h"

#include "../../massspectrum/massspectrum.h"
#include <algorithm>

using namespace pappso;

FilterTandemDeisotope::FilterTandemDeisotope(double mz_range_max,
                                             double minimum_mz)
{
  m_arbitrary_minimum_mz             = minimum_mz;
  m_arbitrary_range_between_isotopes = mz_range_max;
}


FilterTandemDeisotope::FilterTandemDeisotope(const FilterTandemDeisotope &other)
{
  m_arbitrary_minimum_mz             = other.m_arbitrary_minimum_mz;
  m_arbitrary_range_between_isotopes = other.m_arbitrary_range_between_isotopes;
}

MassSpectrum &
FilterTandemDeisotope::filter(MassSpectrum &data_points) const
{

  MassSpectrum massSpectrum;

  if(data_points.size() > 0)
    {
      // auto it_write   = data_points.begin();
      auto it_read = data_points.begin() + 1;
      auto it_end  = data_points.end();


      DataPoint value_write = *data_points.begin();
      double last_mz        = value_write.x;
      while(it_read != it_end)
        {
          if((it_read->x - last_mz) >= m_arbitrary_range_between_isotopes ||
             it_read->x < m_arbitrary_minimum_mz)
            {
              massSpectrum.push_back(value_write);
              value_write = *it_read;
              last_mz     = value_write.x;
            }
          else if(it_read->y > value_write.y)
            {
              value_write = *it_read;
            }
          it_read++;
        }
      massSpectrum.push_back(value_write);
    }

  data_points = std::move(massSpectrum);
  return data_points;
}
