/**
 * \file pappsomspp/processing/filters/filterexclusionmz.h
 * \date 09/02/2021
 * \author Thomas Renne
 * \brief Delete small peaks in the exclusion range
 */

/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "filternameinterface.h"
#include "../../trace/trace.h"

namespace pappso
{

class PMSPP_LIB_DECL FilterMzExclusion : public FilterNameInterface
{
  public:
  /**
   * Default constructor
   */
  FilterMzExclusion(PrecisionPtr precision_ptr);


  /**
   * @param strBuildParams string to build the filter
   * "mzExclusion|0.02dalton"
   */
  FilterMzExclusion(const QString &strBuildParams);

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  FilterMzExclusion(const FilterMzExclusion &other);

  /**
   * Destructor
   */
  virtual ~FilterMzExclusion();

  QString name() const override;
  QString toString() const override;

  /**
   * @brief get all the datapoints and remove different isotope and add their
   * intensity and change to charge = 1 when the charge is known
   * @return a list of datapoint
   */
  Trace &filter(Trace &data_points) const override;

  private:
  void buildFilterFromString(const QString &strBuildParams) override;
  Trace removeTraceInExclusionMargin(Trace &points) const;


  private:
  PrecisionPtr m_exclusionPrecision;
};
} // namespace pappso
