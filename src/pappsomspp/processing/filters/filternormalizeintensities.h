/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2021 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


#include <QObject>

#include "../../trace/trace.h"
#include "../../exportinmportconfig.h"
#include "filternameinterface.h"


namespace pappso
{


class FilterNormalizeIntensities;

typedef std::shared_ptr<FilterNormalizeIntensities>
  FilterNormalizeIntensitiesSPtr;
typedef std::shared_ptr<const FilterNormalizeIntensities>
  FilterNormalizeIntensitiesCstSPtr;


/**
 * @brief Sets the maximum intensity of the trace to the provided value
 *
 * All the other values are modified by applying the same modification ratio.
 *
 * The amplitude of the trace is computed (maxValue - minValue)
 * The new maxValue is set to the required intensity.
 * All the other data points have their intensity modified:
 *
 * new_intensity = previous_intensity / old_max_value * new_max_value
 */
class PMSPP_LIB_DECL FilterNormalizeIntensities : public FilterNameInterface
{
  public:
  FilterNormalizeIntensities(double new_max_y_value);
  FilterNormalizeIntensities(const QString &parameters);
  FilterNormalizeIntensities(const FilterNormalizeIntensities &other);

  virtual ~FilterNormalizeIntensities();

  FilterNormalizeIntensities &
  operator=(const FilterNormalizeIntensities &other);

  Trace &filter(Trace &data_points) const override;
  QString name() const override;
  QString toString() const override;

  protected:
  void buildFilterFromString(const QString &strBuildParams) override;

  private:
  static constexpr double nan = std::numeric_limits<double>::quiet_NaN();

  double m_newYMax;
};
} // namespace pappso
