/**
 * \file pappsomspp/filers/filtepeakdelta.h
 * \date 05/04/2023
 * \author Olivier Langella
 * \brief transform trace into list of peak mass delta
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once

#include "filternameinterface.h"
#include "../../trace/trace.h"

namespace pappso
{

class PMSPP_LIB_DECL FilterPeakDelta : public FilterInterface
{
  public:
  /**
   * Default constructor
   */
  FilterPeakDelta();

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  FilterPeakDelta(const FilterPeakDelta &other);

  /**
   * Destructor
   */
  virtual ~FilterPeakDelta();


  virtual Trace &filter(Trace &data_points) const override;
};
} // namespace pappso
