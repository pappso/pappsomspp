/**
 * \file pappsomspp/processing/filters/filtercomplementionenhancer.h
 * \date 21/08/2020
 * \author Olivier Langella
 * \brief enhance ion intensity of ion fragment complement
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "filterinterface.h"

#include "../../trace/trace.h"
#include "../../massspectrum/qualifiedmassspectrum.h"
#include "filternameinterface.h"


namespace pappso
{

/**
 * @brief try to detect complementary ions and assign maximum intensity of both
 * elements
 *
 * experimental filter
 */
class PMSPP_LIB_DECL FilterComplementIonEnhancer : public FilterNameInterface
{
  public:
  /**
   * @param target_mz the targeted mass of the peak pair
   * @param precision_ptr matching precision
   */
  FilterComplementIonEnhancer(double target_mz, PrecisionPtr precision_ptr);

  /**
   * @param qmass_spectrum qualified mass spectrum to compute the targeted mass
   * of the peak pair
   * @param precision_ptr matching precision
   */
  FilterComplementIonEnhancer(const QualifiedMassSpectrum &qmass_spectrum,
                              PrecisionPtr precision_ptr);


  /**
   * @param strBuildParams string to build the filter
   * "complementIonEnhancer|456.567;0.02dalton"
   */
  FilterComplementIonEnhancer(const QString &strBuildParams);

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  FilterComplementIonEnhancer(const FilterComplementIonEnhancer &other);

  /**
   * Destructor
   */
  virtual ~FilterComplementIonEnhancer();

  Trace &filter(Trace &data_points) const override;

  void buildFilterFromString(const QString &strBuildParams) override;

  QString name() const override;
  QString toString() const override;

  private:
  void
  enhanceComplementMassInRange(double new_intensity,
                               double mz_lower_bound,
                               double mz_upper_bound,
                               std::vector<DataPoint>::iterator it_begin,
                               std::vector<DataPoint>::iterator it_end) const;

  private:
  double m_targetMzSum;
  PrecisionPtr m_precisionPtr;
};

} // namespace pappso
