/**
 * \file pappsomspp/processing/filters/filterremovec13.h
 * \date 19/08/2020
 * \author Olivier Langella
 * \brief sum peaks under a triangle base
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "filterinterface.h"

#include "../../trace/trace.h"

/**
 * @brief tries to keep as much as possible monoisotopes, removing any possible
 * C13 peaks
 *
 * first sort peaks by decreasing intensities, then keep most intense ones and
 * avoid any peaks in possible C13 windows after
 */

namespace pappso
{
class PMSPP_LIB_DECL FilterRemoveC13 : public FilterInterface
{
  public:
  /**
   * Default constructor
   */
  FilterRemoveC13(PrecisionPtr precision_ptr);

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  FilterRemoveC13(const FilterRemoveC13 &other);

  /**
   * Destructor
   */
  virtual ~FilterRemoveC13();


  Trace &filter(Trace &data_points) const override;

  private:
  bool notExcluded(std::vector<std::pair<double, double>> &exclusionMassMap,
                   double mass) const;
  void addExclusionMap(std::vector<std::pair<double, double>> &exclusionMassMap,
                       double mass) const;

  private:
  double m_diffC12C13_z1;
  double m_diffC12C13_z2;
  PrecisionPtr m_precisionPtr;
};
} // namespace pappso
