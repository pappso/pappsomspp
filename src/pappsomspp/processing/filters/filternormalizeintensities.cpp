/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2021 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#include <qmath.h>

#include <limits>
#include <iterator>

#include <QVector>
#include <QDebug>

#include "filternormalizeintensities.h"
#include "../../exception/exceptionnotrecognized.h"
#include "../../exception/exceptionnotpossible.h"

namespace pappso
{


FilterNormalizeIntensities::FilterNormalizeIntensities(double new_max_y_value)
  : m_newYMax(new_max_y_value)
{
}


FilterNormalizeIntensities::FilterNormalizeIntensities(
  const QString &parameters)
{
  buildFilterFromString(parameters);
}


FilterNormalizeIntensities::FilterNormalizeIntensities(
  const FilterNormalizeIntensities &other)
{
  m_newYMax = other.m_newYMax;
}


FilterNormalizeIntensities::~FilterNormalizeIntensities()
{
}


FilterNormalizeIntensities &
FilterNormalizeIntensities::operator=(const FilterNormalizeIntensities &other)
{
  if(&other == this)
    return *this;

  m_newYMax = other.m_newYMax;

  return *this;
}


void
FilterNormalizeIntensities::buildFilterFromString(const QString &parameters)
{
  // Typical string: "FloorAmplitudePercentage|15"
  if(parameters.startsWith(QString("%1|").arg(name())))
    {
      QStringList params = parameters.split("|").back().split(";");

      m_newYMax = params.at(0).toDouble();
    }
  else
    {
      throw pappso::ExceptionNotRecognized(
        QString("Building of FilterNormalizeIntensities from string %1 failed")
          .arg(parameters));
    }
}


Trace &
FilterNormalizeIntensities::filter(Trace &trace) const
{
  // Start by looking at the most intense data point (greatest y value of the
  // whole trace)

  auto max_dp_iter = maxYDataPoint(trace.cbegin(), trace.cend());

  if(max_dp_iter == trace.cend())
    throw pappso::ExceptionNotPossible(
      QString("Failed to find the max intensity data point in the trace."));

  if(!max_dp_iter->y)
    throw pappso::ExceptionNotPossible(
      QString("The max intensity data point in the trace has intensity 0."));

  double ratio = m_newYMax / max_dp_iter->y;

  for(DataPoint &dp : trace)
    {
      dp.y *= ratio;
    }

  // #if 0
  max_dp_iter = maxYDataPoint(trace.cbegin(), trace.cend());
  qDebug() << "Now max int:" << max_dp_iter->y;

  // #endif

  return trace;
}


//! Return a string with the textual representation of the configuration data.
QString
FilterNormalizeIntensities::toString() const
{
  return QString("%1").arg(name()).arg(QString::number(m_newYMax, 'f', 2));
}


QString
FilterNormalizeIntensities::name() const
{
  return "FilterNormalizeIntensities";
}

} // namespace pappso
