/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2021 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


#include <QObject>

#include "../../trace/trace.h"
#include "../../exportinmportconfig.h"
#include "filternameinterface.h"


namespace pappso
{


class FilterLowIntensitySignalRemoval;

typedef std::shared_ptr<FilterLowIntensitySignalRemoval>
  FilterLowIntensitySignalRemovalSPtr;
typedef std::shared_ptr<const FilterLowIntensitySignalRemoval>
  FilterLowIntensitySignalRemovalCstSPtr;


/**
 * @brief Redefines the floor intensity of the Trace
 *
 * The amplitude of the trace is computed (maxValue - minValue)
 * Its fraction is calculated = amplitude * (percentage / 100)
 * The threshold value is computed as (minValue + fraction)
 *
 * When the values to be filtered are below that threshold they acquire that
 * threshold value.
 *
 * When the values to be filtered are above that threshold they remain
 * unchanged.
 *
 * This effectively re-floors the values to threshold.
 */
class PMSPP_LIB_DECL FilterLowIntensitySignalRemoval
  : public FilterNameInterface
{
  public:
  FilterLowIntensitySignalRemoval(double mean,
                                  double std_dev,
                                  double threshold);
  FilterLowIntensitySignalRemoval(const QString &parameters);
  FilterLowIntensitySignalRemoval(const FilterLowIntensitySignalRemoval &other);

  virtual ~FilterLowIntensitySignalRemoval();

  FilterLowIntensitySignalRemoval &
  operator=(const FilterLowIntensitySignalRemoval &other);

  Trace &filter(Trace &data_points) const override;

  double getThreshold() const;
  QString name() const override;

  QString toString() const override;

  protected:
  void buildFilterFromString(const QString &strBuildParams) override;

  Trace &nonConstFilter(Trace &data_points);

  std::size_t detectClusterApices(const Trace &trace);

  Trace::const_iterator backwardFindApex(const Trace &trace,
                                         Trace::const_iterator iter,
                                         double distance_threshold);

  Trace::const_iterator forwardFindApex(const Trace &trace,
                                        Trace::const_iterator iter,
                                        double distance_threshold);
  Trace reconstructTrace(const Trace &trace);

  private:
  static constexpr double nan = std::numeric_limits<double>::quiet_NaN();

  double m_threshold;
  double m_noiseMean;
  double m_noiseStdDev;

  constexpr static double INTRA_CLUSTER_INTER_PEAK_DISTANCE = 1.1;

  const std::size_t m_minIntPointCount   = 5;
  const std::size_t m_minIntStdDevFactor = 2;

  double m_min;
  double m_max;
  double m_minMean;
  double m_minStdDev;
  double m_noiseLevel;

  bool m_seen_upward_phase = false;

  using TraceCIter = Trace::const_iterator;

  // Beware HYPER-ROUGH inialization of the iterator !
  TraceCIter m_prevApex = static_cast<Trace::const_iterator>(0);
  TraceCIter m_curApex  = static_cast<Trace::const_iterator>(0);
  TraceCIter m_curIter  = static_cast<Trace::const_iterator>(0);
  TraceCIter m_prevIter = static_cast<Trace::const_iterator>(0);

  using ClusterApices = std::vector<TraceCIter>;
  using ApicesSPtr    = std::shared_ptr<ClusterApices>;

  // All the cluster apices
  std::vector<ApicesSPtr> m_clusters;
};
} // namespace pappso
