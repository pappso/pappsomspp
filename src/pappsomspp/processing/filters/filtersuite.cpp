/**
 * \file pappsomspp/filers/filtersuite.cpp
 * \date 02/05/2019
 * \author Olivier Langella
 * \brief apply a suite of filters on a trace
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "filtersuite.h"
#include <QDebug>

using namespace pappso;


FilterSuite::FilterSuite()
{
}
FilterSuite::FilterSuite(const FilterSuite &other)
  : std::vector<FilterInterfaceSPtr>(other)
{
}

Trace &
FilterSuite::filter(Trace &data_points) const
{

  qDebug();
  for(auto &&filter : *this)
    {

      qDebug();
      filter.get()->filter(data_points);
    }

  qDebug();
  return data_points;
}
