/**
 * \file pappsomspp/processing/filters/filterremovec13.h
 * \date 19/08/2020
 * \author Olivier Langella
 * \brief sum peaks under a triangle base
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "filterremovec13.h"

using namespace pappso;

FilterRemoveC13::FilterRemoveC13(PrecisionPtr precision_ptr)
  : m_precisionPtr(precision_ptr)
{
  m_diffC12C13_z1 = DIFFC12C13;
  m_diffC12C13_z2 = DIFFC12C13 / 2;
}

FilterRemoveC13::FilterRemoveC13(const FilterRemoveC13 &other)
  : m_precisionPtr(other.m_precisionPtr)
{
  m_diffC12C13_z1 = DIFFC12C13;
  m_diffC12C13_z2 = DIFFC12C13 / 2;
}

FilterRemoveC13::~FilterRemoveC13()
{
}

Trace &
FilterRemoveC13::filter(Trace &data_points) const
{

  std::vector<std::pair<double, double>> exclusionMassMap;
  // qDebug() << data_points.size();
  std::sort(data_points.begin(),
            data_points.end(),
            [](const DataPoint &a, const DataPoint &b) { return (a.y > b.y); });

  Trace new_trace;

  for(auto &data_point : data_points)
    {
      if(notExcluded(exclusionMassMap, data_point.x))
        {
          new_trace.push_back(data_point);
        }
      addExclusionMap(exclusionMassMap, data_point.x);
    }
  new_trace.sortX();
  // qDebug() << new_trace.size();
  data_points = std::move(new_trace);
  // qDebug() << data_points.size();
  return data_points;
}

bool
pappso::FilterRemoveC13::notExcluded(
  std::vector<std::pair<double, double>> &exclusionMassMap, double mass) const
{
  for(auto &mass_range : exclusionMassMap)
    {
      if((mass_range.first <= mass) && (mass_range.second >= mass))
        {
          return false;
        }
    }
  return true;
}

void
pappso::FilterRemoveC13::addExclusionMap(
  std::vector<std::pair<double, double>> &exclusionMassMap, double mass) const
{
  MzRange range1(mass + m_diffC12C13_z1, m_precisionPtr);

  exclusionMassMap.push_back(
    std::pair<double, double>(range1.lower(), range1.upper()));


  MzRange range2(mass + m_diffC12C13_z2, m_precisionPtr);

  exclusionMassMap.push_back(
    std::pair<double, double>(range2.lower(), range2.upper()));
}
