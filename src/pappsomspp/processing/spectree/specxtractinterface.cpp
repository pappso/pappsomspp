/**
 * \file pappsomspp/processing/spectree/specxtractinterface.cpp
 * \date 11/12/2023
 * \author Olivier Langella
 * \brief Matthieu David's SpecTree structure
 *
 * C++ implementation of algorithm already described in :
 * 1. David, M., Fertin, G., Rogniaux, H. & Tessier, D. SpecOMS: A Full Open
 * Modification Search Method Performing All-to-All Spectra Comparisons within
 * Minutes. J. Proteome Res. 16, 3030–3038 (2017).
 *
 * https://www.theses.fr/2019NANT4092
 */


/*
 * SpecTree
 * Copyright (C) 2023  Olivier Langella
 * <olivier.langella@universite-paris-saclay.fr>
 *
 * This program is free software: you can redistribute ipetide to spectrum
 * alignmentt and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "specxtractinterface.h"
#include <QDebug>


namespace pappso
{
namespace spectree
{

SpecXtractInterface::SpecXtractInterface()
{
}

SpecXtractInterface::~SpecXtractInterface()
{
}

void
SpecXtractInterface::endItemCartExtraction(std::size_t cart_id_a
                                           [[maybe_unused]])
{
}
void
SpecXtractInterface::beginItemCartExtraction(std::size_t cart_id_a
                                             [[maybe_unused]])
{
}

void
SpecXtractMap::reportSimilarity(std::size_t cart_id_a,
                                std::size_t cart_id_b,
                                std::size_t similarity)
{
  qDebug() << " a=" << cart_id_a << " b=" << cart_id_b
           << " count=" << similarity;
  auto it_line =
    m_mapSimilarities.insert({cart_id_a, std::map<std::size_t, std::size_t>()});

  auto it_line2 = it_line.first->second.insert({cart_id_b, similarity});

  if(it_line2.second == false)
    {
      it_line2.first->second = similarity;
    }
}

SpecXtractMap::SpecXtractMap()
{
}

SpecXtractMap::~SpecXtractMap()
{
}

const std::map<std::size_t, std::map<std::size_t, std::size_t>> &
SpecXtractMap::getMapSimilarities() const
{
  return m_mapSimilarities;
}

void
SpecXtractMap::clear()
{
  m_mapSimilarities.clear();
}

} // namespace spectree
} // namespace pappso
