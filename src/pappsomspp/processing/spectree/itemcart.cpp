/**
 * \file pappsomspp/processing/spectree/itemcart.cpp
 * \date 13/12/2023
 * \author Olivier Langella
 * \brief basic object to study using spectree
 *
 * C++ implementation of algorithm already described in :
 * 1. David, M., Fertin, G., Rogniaux, H. & Tessier, D. SpecOMS: A Full Open
 * Modification Search Method Performing All-to-All Spectra Comparisons within
 * Minutes. J. Proteome Res. 16, 3030–3038 (2017).
 *
 * https://www.theses.fr/2019NANT4092
 */


/*
 * SpecTree
 * Copyright (C) 2023  Olivier Langella
 * <olivier.langella@universite-paris-saclay.fr>
 *
 * This program is free software: you can redistribute ipetide to spectrum
 * alignmentt and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "itemcart.h"

namespace pappso
{
namespace spectree
{
ItemCart::ItemCart()
{
}

ItemCart::ItemCart(std::size_t id, const std::vector<std::size_t> &item_list)
  : m_id(id), m_itemList(item_list)
{
}


ItemCart::ItemCart(const ItemCart &other)
{
  m_id       = other.m_id;
  m_itemList = other.m_itemList;
}

ItemCart::~ItemCart()
{
}

std::size_t
ItemCart::getId() const
{
  return m_id;
}

ItemCart &
ItemCart::operator=(const ItemCart &other)
{
  m_id       = other.m_id;
  m_itemList = other.m_itemList;
  return *this;
}

const std::vector<std::size_t> &
ItemCart::getItemList() const
{
  return m_itemList;
}
} // namespace spectree
} // namespace pappso
