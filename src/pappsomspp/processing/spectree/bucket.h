/**
 * \file pappsomspp/processing/spectree/bucket.h
 * \date 13/12/2023
 * \author Olivier Langella
 * \brief bucket for spectree
 *
 * C++ implementation of algorithm already described in :
 * 1. David, M., Fertin, G., Rogniaux, H. & Tessier, D. SpecOMS: A Full Open
 * Modification Search Method Performing All-to-All Spectra Comparisons within
 * Minutes. J. Proteome Res. 16, 3030–3038 (2017).
 *
 * https://www.theses.fr/2019NANT4092
 */


/*
 * SpecTree
 * Copyright (C) 2023  Olivier Langella
 * <olivier.langella@universite-paris-saclay.fr>
 *
 * This program is free software: you can redistribute ipetide to spectrum
 * alignmentt and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of th^e GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <vector>

namespace pappso
{
namespace spectree
{

/***
 * Contains the identifiers from spectra which possess a mass correspoding to
 * the one associated to the bucket. Once the filling is complete, a bucket must
 * be sorted. Spectra identifiers need to be positive integers to ensure sort
 * correctness.
 *
 * @author Matthieu David
 * @version 0.1
 */

class Bucket
{
  public:
  /**
   * Bucket creation with appropriated initialisation for the identifiers and
   * the mass value.
   * @param val the mass value (item id) associated to the bucket
   * @return b a bucket object properly instanciated
   * @since 0.1
   */
  Bucket(std::size_t val);

  Bucket(const Bucket &other);

  /**
   * Accessor to the mass value (item id) associated to the bucket.
   * @return the mass value
   * @since 0.1
   */
  std::size_t getId() const;

  /**
   * Insertion of a new identifier in the bucket. Insertion happen to the end.
   * @param cart The cart identifier to add in the bucket
   * @since 0.1
   */
  void push_back(std::size_t cart);

  /**
   * Return the number of identifiers contained in the bucket.
   * @return the number of identifiers
   * @since 0.1
   */
  std::size_t size() const;

  /**
   * Implementation of the comparable interface in order to be able to compare
   * two buckets and sort a collection of buckets lexicographically.
   * @param b The bucket to compare with the current one
   * @return -x if the bucket is lexicographically smaller, +x if the bucket is
   * lexicographically bigger and 0 if they have the same content, where x is a
   * non-zero integer.
   * @since 0.1
   */
  bool operator<(const Bucket &bucket_two) const;

  const std::vector<std::size_t> &getCartList() const;

  /** @brief get the first cart id of the list
   * */
  std::size_t front() const;

  /** @brief get the last cart id of the list
   * */
  std::size_t back() const;

  private:
  /**
   * The mass value associated to the bucket.
   * This value cannot be modified.
   * @since 0.1
   */

  std::size_t m_itemId;


  /**
   * Spectrum identifiers stored in an optimized array to reduce the memory
   * occupation (fastutils). This list can only be filled, sorted or accessed in
   * read-only.
   * @since 0.1
   */
  std::vector<std::size_t> m_cartList;
};
} // namespace spectree
} // namespace pappso
