/**
 * \file pappsomspp/processing/spectree/specxtractinterface.h
 * \date 11/12/2023
 * \author Olivier Langella
 * \brief Matthieu David's SpecTree structure
 *
 * C++ implementation of algorithm already described in :
 * 1. David, M., Fertin, G., Rogniaux, H. & Tessier, D. SpecOMS: A Full Open
 * Modification Search Method Performing All-to-All Spectra Comparisons within
 * Minutes. J. Proteome Res. 16, 3030–3038 (2017).
 *
 * https://www.theses.fr/2019NANT4092
 */


/*
 * SpecTree
 * Copyright (C) 2023  Olivier Langella
 * <olivier.langella@universite-paris-saclay.fr>
 *
 * This program is free software: you can redistribute ipetide to spectrum
 * alignmentt and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <map>
#include "../../exportinmportconfig.h"

namespace pappso
{
namespace spectree
{

/**
 * @brief yield similarities between pairs of ItemCart
 *
 * The best thing is to implement this interface to process similarity at the
 * time of the spectree extraction
 */
class PMSPP_LIB_DECL SpecXtractInterface
{
  public:
  /**
   * Default constructor
   */
  SpecXtractInterface();

  /**
   * Destructor
   */
  virtual ~SpecXtractInterface();

  virtual void beginItemCartExtraction(std::size_t cart_id_a);
  virtual void reportSimilarity(std::size_t cart_id_a,
                                std::size_t cart_id_b,
                                std::size_t similarity) = 0;
  virtual void endItemCartExtraction(std::size_t cart_id_a);
};

/** @brief store all similarities in vectors an map : very costly for memory and
 * CPU
 *
 * only for testing purpose
 */
class SpecXtractMap : public SpecXtractInterface
{

  public:
  /**
   * Default constructor
   */
  SpecXtractMap();

  /**
   * Destructor
   */
  virtual ~SpecXtractMap();

  void reportSimilarity(std::size_t cart_id_a,
                        std::size_t cart_id_b,
                        std::size_t similarity) override;

  const std::map<std::size_t, std::map<std::size_t, std::size_t>> &
  getMapSimilarities() const;

  void clear();

  private:
  std::map<std::size_t, std::map<std::size_t, std::size_t>> m_mapSimilarities;
};

} // namespace spectree
} // namespace pappso
