/**
 * \file pappsomspp/processing/spectree/itemcart.h
 * \date 13/12/2023
 * \author Olivier Langella
 * \brief basic object to study using spectree
 *
 * C++ implementation of algorithm already described in :
 * 1. David, M., Fertin, G., Rogniaux, H. & Tessier, D. SpecOMS: A Full Open
 * Modification Search Method Performing All-to-All Spectra Comparisons within
 * Minutes. J. Proteome Res. 16, 3030–3038 (2017).
 *
 * https://www.theses.fr/2019NANT4092
 */


/*
 * SpecTree
 * Copyright (C) 2023  Olivier Langella
 * <olivier.langella@universite-paris-saclay.fr>
 *
 * This program is free software: you can redistribute ipetide to spectrum
 * alignmentt and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of th^e GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <vector>
#include "../../exportinmportconfig.h"

namespace pappso
{
namespace spectree
{
/**
 * @brief container for spectree items
 * 
 * ItemCart is the basic container to use SpecTree algorithm
 * each cart has an id (long integer) and a list of items (list of integers)
 * 
 * SpecTree will be able to quickly count common items between every cart
 */
 
class PMSPP_LIB_DECL ItemCart
{
  public:
  /**
   * Default constructor
   */
  ItemCart();

  ItemCart(std::size_t id, const std::vector<std::size_t> &item_list);

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  ItemCart(const ItemCart &other);

  /**
   * Destructor
   */
  virtual ~ItemCart();

  const std::vector<std::size_t> &getItemList() const;

  std::size_t getId() const;

  ItemCart &operator=(const ItemCart &other);

  protected:
  std::size_t m_id;
  std::vector<std::size_t> m_itemList;
};
} // namespace spectree
} // namespace pappso
