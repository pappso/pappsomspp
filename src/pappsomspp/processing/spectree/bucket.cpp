/**
 * \file pappsomspp/processing/spectree/bucket.cpp
 * \date 13/12/2023
 * \author Olivier Langella
 * \brief bucket for spectree
 *
 * C++ implementation of algorithm already described in :
 * 1. David, M., Fertin, G., Rogniaux, H. & Tessier, D. SpecOMS: A Full Open
 * Modification Search Method Performing All-to-All Spectra Comparisons within
 * Minutes. J. Proteome Res. 16, 3030–3038 (2017).
 *
 * https://www.theses.fr/2019NANT4092
 */


/*
 * SpecTree
 * Copyright (C) 2023  Olivier Langella
 * <olivier.langella@universite-paris-saclay.fr>
 *
 * This program is free software: you can redistribute ipetide to spectrum
 * alignmentt and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of th^e GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "bucket.h"
#include <algorithm>


namespace pappso
{
namespace spectree
{

Bucket::Bucket(std::size_t val)
{
  m_itemId = val;
}

Bucket::Bucket(const Bucket &other) : m_cartList(other.m_cartList)
{
  m_itemId = other.m_itemId;
}

void
Bucket::push_back(std::size_t cart)
{
  m_cartList.push_back(cart);
}

std::size_t
Bucket::size() const
{
  return m_cartList.size();
}

std::size_t
Bucket::getId() const
{
  return m_itemId;
}


bool
Bucket::operator<(const Bucket &bucket_b) const
{

  auto it_bucket_end   = m_cartList.end();
  auto it_bucket_b_end = bucket_b.m_cartList.end();

  auto it_bucket   = m_cartList.begin();
  auto it_bucket_b = bucket_b.m_cartList.begin();

  for(; it_bucket != it_bucket_end && it_bucket_b != it_bucket_b_end;
      it_bucket++, it_bucket_b++)
    {
      if(*it_bucket != *it_bucket_b)
        return (*it_bucket < *it_bucket_b);
    }

  // When the buckets have an equal content until the limit, the shortest one
  // is returned first
  return (m_cartList.size() < bucket_b.m_cartList.size());
}

std::size_t
Bucket::front() const
{
  return m_cartList.front();
}

std::size_t
Bucket::back() const
{
  return m_cartList.back();
}

const std::vector<std::size_t> &
Bucket::getCartList() const
{
  return m_cartList;
}
} // namespace spectree
} // namespace pappso
