#pragma once

#include <vector>
#include <memory>

#include <QDataStream>

#include "../../exportinmportconfig.h"
#include "../../types.h"
#include "../../massspectrum/massspectrum.h"
#include "massspectrumcombiner.h"

namespace pappso
{

class MassSpectrumPlusCombiner;

typedef std::shared_ptr<const MassSpectrumPlusCombiner>
  MassSpectrumPlusCombinerCstSPtr;

typedef std::shared_ptr<MassSpectrumPlusCombiner> MassSpectrumPlusCombinerSPtr;


class PMSPP_LIB_DECL MassSpectrumPlusCombiner : public MassSpectrumCombiner
{

  public:
  MassSpectrumPlusCombiner();
  MassSpectrumPlusCombiner(int decimal_places);
  MassSpectrumPlusCombiner(const MassSpectrumPlusCombiner &other);
  MassSpectrumPlusCombiner(MassSpectrumPlusCombinerCstSPtr other);

  virtual ~MassSpectrumPlusCombiner();

  MassSpectrumPlusCombiner &operator=(const MassSpectrumPlusCombiner &other);

  virtual MapTrace &combine(MapTrace &map_trace,
                            const Trace &trace) const override;
  virtual MapTrace &combine(MapTrace &map_trace_out,
                            const MapTrace &map_trace_in) const override;
};


} // namespace pappso
