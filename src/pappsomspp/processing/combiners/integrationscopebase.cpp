// Copyright 2021 Filippo Rusconi
// GPLv3+


/////////////////////// StdLib includes
#include <limits>
#include <cmath>


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// Local includes
#include "integrationscopebase.h"


namespace pappso
{

IntegrationScopeBase::IntegrationScopeBase()
{
  // qDebug() << "Constructing" << this;
}


IntegrationScopeBase::IntegrationScopeBase(
  [[maybe_unused]] const IntegrationScopeBase &other)
{
  // qDebug() << "Constructing" << this;
}

IntegrationScopeBase::~IntegrationScopeBase()
{
  // qDebug() << "Destructing" << this;
}


bool
IntegrationScopeBase::getPoint([[maybe_unused]] QPointF &point) const
{
  return false;
}

bool
IntegrationScopeBase::getPoints(
  [[maybe_unused]] std::vector<QPointF> &points) const
{
  return false;
}

IntegrationScopeFeatures
IntegrationScopeBase::getLeftMostPoint([[maybe_unused]] QPointF &point) const
{
  return IntegrationScopeFeatures::FAILURE;
}

IntegrationScopeFeatures
IntegrationScopeBase::getLeftMostPoints(
  [[maybe_unused]] std::vector<QPointF> &points) const
{
  return IntegrationScopeFeatures::FAILURE;
}

IntegrationScopeFeatures
IntegrationScopeBase::getLeftMostTopPoint([[maybe_unused]] QPointF &point) const
{
  return IntegrationScopeFeatures::FAILURE;
}

IntegrationScopeFeatures
IntegrationScopeBase::getLeftMostBottomPoint(
  [[maybe_unused]] QPointF &point) const
{
  return IntegrationScopeFeatures::FAILURE;
}

IntegrationScopeFeatures
IntegrationScopeBase::getRightMostPoint([[maybe_unused]] QPointF &point) const
{
  return IntegrationScopeFeatures::FAILURE;
}

IntegrationScopeFeatures
IntegrationScopeBase::getRightMostPoints(
  [[maybe_unused]] std::vector<QPointF> &points) const
{
  return IntegrationScopeFeatures::FAILURE;
}

IntegrationScopeFeatures
IntegrationScopeBase::getRightMostTopPoint(
  [[maybe_unused]] QPointF &point) const
{
  return IntegrationScopeFeatures::FAILURE;
}

IntegrationScopeFeatures
IntegrationScopeBase::getRightMostBottomPoint(
  [[maybe_unused]] QPointF &point) const
{
  return IntegrationScopeFeatures::FAILURE;
}

IntegrationScopeFeatures
IntegrationScopeBase::getTopMostPoint([[maybe_unused]] QPointF &point) const
{
  return IntegrationScopeFeatures::FAILURE;
}

IntegrationScopeFeatures
IntegrationScopeBase::getTopMostPoints(
  [[maybe_unused]] std::vector<QPointF> &points) const
{
  return IntegrationScopeFeatures::FAILURE;
}

IntegrationScopeFeatures
IntegrationScopeBase::getBottomMostPoint([[maybe_unused]] QPointF &point) const
{
  return IntegrationScopeFeatures::FAILURE;
}

IntegrationScopeFeatures
IntegrationScopeBase::getBottomMostPoints(
  [[maybe_unused]] std::vector<QPointF> &points) const
{
  return IntegrationScopeFeatures::FAILURE;
}

IntegrationScopeFeatures
IntegrationScopeBase::getWidth([[maybe_unused]] double &width) const
{
  return IntegrationScopeFeatures::FAILURE;
}

IntegrationScopeFeatures
IntegrationScopeBase::getHeight([[maybe_unused]] double &height) const
{
  return IntegrationScopeFeatures::FAILURE;
}

IntegrationScopeFeatures
IntegrationScopeBase::getRhombHorizontalSize([[maybe_unused]] double &size) const
{
  return IntegrationScopeFeatures::FAILURE;
}

IntegrationScopeFeatures
IntegrationScopeBase::getRhombVerticalSize([[maybe_unused]] double &size) const
{
 return IntegrationScopeFeatures::FAILURE;
}

bool
IntegrationScopeBase::range([[maybe_unused]] Axis axis,
                            [[maybe_unused]] double &start,
                            [[maybe_unused]] double &end) const
{
  return false;
}

void
IntegrationScopeBase::setDataKindX([[maybe_unused]] DataKind data_kind)
{
}

void
IntegrationScopeBase::setDataKindY([[maybe_unused]] DataKind data_kind)
{
}

bool
IntegrationScopeBase::getDataKindX([[maybe_unused]] DataKind &data_kind)
{
  return false;
}

bool
IntegrationScopeBase::getDataKindY([[maybe_unused]] DataKind &data_kind)
{
  return false;
}

bool
IntegrationScopeBase::is1D() const
{
  return false;
}

bool
IntegrationScopeBase::is2D() const
{
  return false;
}

bool
IntegrationScopeBase::isRectangle() const
{
  return false;
}

bool
IntegrationScopeBase::isRhomboid() const
{
  return false;
}

bool
IntegrationScopeBase::transpose()
{
  return false;
}

bool
IntegrationScopeBase::contains([[maybe_unused]] const QPointF &point) const
{
  return false;
}

QString
IntegrationScopeBase::toString() const
{
  return QString();
}

void
IntegrationScopeBase::reset()
{
}

} // namespace pappso
