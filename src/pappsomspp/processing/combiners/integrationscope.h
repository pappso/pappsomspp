// Copyright 2021 Filippo Rusconi
// GPLv3+

#pragma once

/////////////////////// StdLib includes
#include <vector>
#include <limits>

/////////////////////// Qt includes
#include <QString>
#include <QPointF>


/////////////////////// Local includes
#include "../../exportinmportconfig.h"
#include "../../types.h"
#include "integrationscopebase.h"

namespace pappso
{

// This class represents the case where the user draws a line in a trace plot
// widget, with the aim of integrating data in single-dimension manner (over a
// range in the X axis). Having no height, this IntegrationScope is like a line.

/* Like this:

P---------------------------+

|--------- m_width ---------|

With P the m_point.

 */

class PMSPP_LIB_DECL IntegrationScope : public IntegrationScopeBase
{
  public:
  IntegrationScope();
  IntegrationScope(const QPointF &point, double width);
  IntegrationScope(const QPointF &point, double width, DataKind data_kind);
  IntegrationScope(const IntegrationScope &other);

  virtual ~IntegrationScope();

  virtual IntegrationScope &operator=(const IntegrationScope &other);

  virtual void setPoint(const QPointF &point);
  virtual bool getPoint(QPointF &point) const override;

  virtual IntegrationScopeFeatures getLeftMostPoint(QPointF &point) const override;
  virtual IntegrationScopeFeatures getRightMostPoint(QPointF &point) const override;
    
  virtual IntegrationScopeFeatures getTopMostPoint(QPointF &point) const override;
  virtual IntegrationScopeFeatures getBottomMostPoint(QPointF &point) const override;

  virtual void setWidth(double width);
  virtual IntegrationScopeFeatures getWidth(double &width) const override;

  virtual bool range(Axis axis, double &start, double &end) const override;

  virtual void setDataKindX(DataKind data_kind) override;
  virtual bool getDataKindX(DataKind &data_kind) override;
  
  bool is1D() const override;
  bool is2D() const override;
  
  virtual bool isRectangle() const override;
  virtual bool isRhomboid() const override;
  
  virtual bool transpose() override;

  virtual void update(const QPointF &point, double width);

  virtual bool contains(const QPointF &point) const override;

  virtual QString toString() const override;

  virtual void reset() override;

  protected:
  QPointF m_point;
  double m_width;
  DataKind m_dataKindX = DataKind::unset;
};

typedef std::shared_ptr<IntegrationScope> IntegrationScopeSPtr;
typedef std::shared_ptr<const IntegrationScope> IntegrationScopeCstSPtr;

} // namespace pappso
