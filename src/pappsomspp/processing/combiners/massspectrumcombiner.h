#pragma once

#include <vector>
#include <memory>

#include <QDataStream>

#include "../../exportinmportconfig.h"
#include "../../types.h"
#include "../../mzrange.h"

#include "../../massspectrum/massspectrum.h"
#include "../../trace/datapoint.h"
#include "../../trace/maptrace.h"
#include "../filters/filterresample.h"
#include "massdatacombinerinterface.h"


namespace pappso
{

class MassSpectrumCombiner;

typedef std::shared_ptr<const MassSpectrumCombiner> MassSpectrumCombinerCstSPtr;
typedef std::shared_ptr<MassSpectrumCombiner> MassSpectrumCombinerSPtr;


class PMSPP_LIB_DECL MassSpectrumCombiner : public MassDataCombinerInterface
{

  public:
  MassSpectrumCombiner();
  MassSpectrumCombiner(int decimal_places);
  MassSpectrumCombiner(std::vector<pappso_double> bins, int decimalPlaces = -1);
  MassSpectrumCombiner(MassSpectrumCombinerCstSPtr other);
  MassSpectrumCombiner(const MassSpectrumCombiner &other);
  MassSpectrumCombiner(const MassSpectrumCombiner &&other);

  virtual ~MassSpectrumCombiner();

  void setBins(std::vector<pappso_double> bins);
  const std::vector<pappso_double> &getBins() const;
  std::size_t binCount() const;

  QString binsAsString() const;

  protected:
  std::vector<pappso_double> m_bins;

  std::vector<pappso_double>::iterator findBin(pappso_double mz);
};


} // namespace pappso
