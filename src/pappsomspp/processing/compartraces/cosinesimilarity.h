/**
 * \file pappsomspp/processing/compartraces/cosinesimilarity.h
 * \date 12/06/2023
 * \author Olivier Langella
 * \brief computes cosine similarity of 2 traces vector
 * https://en.wikipedia.org/wiki/Cosine_similarity
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include "../../exportinmportconfig.h"
#include "../../trace/trace.h"
#include "../../precision.h"
#include "../filters/filterexclusionmz.h"


namespace pappso
{
/**
 * @todo write docs
 */
class PMSPP_LIB_DECL CosineSimilarity
{
  public:
  /**
   * Default constructor
   */
  CosineSimilarity(PrecisionPtr precision);

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  CosineSimilarity(const CosineSimilarity &other);

  /**
   * Destructor
   */
  ~CosineSimilarity();

  double similarity(pappso::Trace trace_a, pappso::Trace trace_b) const;

  private:
  PrecisionPtr mp_precision;
  FilterInterfaceSPtr msp_filterExclusion;
};
} // namespace pappso
