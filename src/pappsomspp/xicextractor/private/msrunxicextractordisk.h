/**
 * \file pappsomspp/xicextractor/private/msrunxicextractordisk.h
 * \date 12/05/2018
 * \author Olivier Langella
 * \brief MsRunReader based XIC extractor featuring disk cache
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once


#include "msrunxicextractor.h"
#include "msrunslice.h"
#include <QTemporaryDir>
#include <deque>
#include <QMutex>


class MsRunXicExtractorFactory;

namespace pappso
{

class MsRunXicExtractorDisk : public MsRunXicExtractor
{
  friend MsRunXicExtractorFactory;

  public:
  MsRunXicExtractorDisk(MsRunReaderSPtr &msrun_reader);
  MsRunXicExtractorDisk(const MsRunXicExtractorDisk &other);
  virtual ~MsRunXicExtractorDisk();


  protected:
  MsRunXicExtractorDisk(MsRunReaderSPtr &msrun_reader,
                        const QDir &temporary_dir);

  virtual void protectedExtractXicCoordSPtrList(
    UiMonitorInterface &monitor,
    std::vector<XicCoordSPtr>::iterator it_xic_coord_list_begin,
    std::vector<XicCoordSPtr>::iterator it_xic_coord_list_end) override;


  void prepareExtractor();

  void serializeMsRun();


  /** @brief store MassSpectrum slices (by daltons) for a given retention time
   * @param slice_vector mass spectrum chunks (by daltons)
   * @param ipos the position in the retention time vector
   */
  virtual void storeSlices(std::map<unsigned int, MassSpectrum> &slice_vector,
                           std::size_t ipos);

  /** @brief append a slice on disk (in a file)
   * @param slice_number the slice number == dalton integer
   * @param spectrum the part of the mass spectrum (mz/intensity) in the range
   * of the slice number
   * @param ipos the position in the retention time vector
   */
  void appendSliceOnDisk(unsigned int slice_number,
                         MassSpectrum &spectrum,
                         std::size_t ipos);

  /** @brief retrieve all the slices corresponding to a given mz_range
   * @param mz_range desired mz range
   */
  std::vector<MsRunSliceSPtr> acquireSlices(const MzRange &mz_range);

  /** @brief get one slice from disk by her slice number (dalton)
   * @param slice_number the slice number == dalton integer
   */
  MsRunSliceSPtr unserializeSlice(unsigned int slice_number);

  virtual void endPwizRead();

  protected:
  QString m_temporaryDirectory;
  QTemporaryDir *mpa_temporaryDirectory = nullptr;
  std::vector<pappso::pappso_double> m_retentionTimeList;

  pappso::pappso_double m_maxMz = 0;
  pappso::pappso_double m_minMz = 5000;

  std::size_t m_rtSize = 0;

  std::deque<MsRunSliceSPtr> m_msRunSliceListCache;

  QMutex m_mutex;


  private:
  void extractOneXicCoord(XicCoord &xic_coord);
};


} // namespace pappso
