/**
 * \file pappsomspp/masschroq/core/msrun.h
 * \date 24/09/2024
 * \author Olivier Langella
 * \brief msrun model in masschroqlite
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include "../peptide/peptide.h"
#include "../msrun/msrunreader.h"
#include "../xicextractor/msrunxicextractorinterface.h"
#include "precursor.h"
#include "input/precursorparser.h"


namespace pappso::masschroq
{

class MsRun;

typedef std::shared_ptr<MsRun> MsRunSp;
/**
 * @description group together an msrun reader object and list of precursors
 * (MS1 peak chosen for fragmentation)
 */
class MsRun
{
  public:
  /**
   * Default constructor
   */
  MsRun(pappso::MsRunReaderSPtr msrun_reader);

  /**
   * Destructor
   */
  virtual ~MsRun();


  const pappso::MsRunReaderSPtr &getMsRunReaderSPtr() const;
  pappso::MsRunReaderSPtr &getMsRunReaderSPtr();


  const PrecursorSp &getPrecursorSPtrBySpectrumIndex(std::size_t spectrum_index) const;
  const PrecursorSp &getPrecursorSPtrByScanNumber(std::size_t scan_number) const;

  const std::vector<double> &getRetentionTimeLine() const;

  bool hasTimsTofMobilityIndex() const;

  private:
  pappso::MsRunReaderSPtr msp_msRunReader;

  /** @brief new map dedicated to spectrum index to replace obsolete scan number
   */
  std::map<std::size_t, PrecursorSp> m_spectrumIndex2precursorMap;

  pappso::MsRunXicExtractorInterfaceSp msp_msRunXicExtractorInterfaceSp;

  PrecursorParser *mpa_precursorParser;

  std::vector<double> m_retentionTimeLine;

  bool m_hasTimsTofMobilityIndex = false;
};
} // namespace pappso::masschroq
