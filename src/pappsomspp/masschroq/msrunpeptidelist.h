/**
 * \file pappsomspp/masschroq/msrunpeptidelist.h
 * \date 25/09/2024
 * \author Olivier Langella
 * \brief msrun model including fragmented peptides in masschroqlite
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include "msrun.h"
#include "peptidelabel.h"
#include "peptideobservation.h"
#include "peptidemeasurements.h"
#include "mbrpeptidemeasurements.h"
#include <cstdint>
#include "alignmentmethod.h"
#include "quantificationmethod.h"
#include "../msrun/alignment/msrunretentiontime.h"

namespace pappso::masschroq
{
class CborOutputStream;
class MsRunPeptideList;

typedef std::shared_ptr<MsRunPeptideList> MsRunPeptideListSp;

/**
 * group together an msrun, available peptide observations, and peptide
 * measurements
 */
class MsRunPeptideList
{
  public:
  /**
   * Default constructor
   */
  MsRunPeptideList(MsRunSp msrun);

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  MsRunPeptideList(const MsRunPeptideList &other);

  /**
   * Destructor
   */
  virtual ~MsRunPeptideList();

  void addPeptideSpectrumIndexObservation(PeptideSp peptide_sp,
                                          PeptideLabel *p_label,
                                          std::size_t spectrum_index,
                                          std::uint8_t charge);
  void addPeptideScanNumberObservation(PeptideSp peptide_sp,
                                       PeptideLabel *p_label,
                                       std::size_t spectrum_index,
                                       std::uint8_t charge);

  void quantify(const MsRunGroup *msrun_group_p,
                const QString &tmp_dir,
                pappso::UiMonitorInterface &m_uiMonitor,
                const QuantificationMethodSp &quantification_method);


  void quantifyMatchBetweenRun(const MsRunGroup *msrun_group_p,
                               const std::vector<PeptideSp> &peptide_mbr_list,
                               const QString &tmp_dir,
                               pappso::UiMonitorInterface &m_uiMonitor,
                               const QuantificationMethodSp &quantification_method);


  const MsRunSp &getMsRunSp() const;

  /** flush (remove) transient chromatogram traces after peak detection to save
   * space in memory
   */
  void flushChromatogramTraces();

  /** @brief clear all measurements MBR or not
   * Clearing measurements also removes every chromatogram
   */
  void clearMeasurements();


  /** @brief build a retention time vector with seamarks using MS2 best
   * intensities
   * @param alignment_method parameters to use for alignment
   */
  std::shared_ptr<pappso::MsRunRetentionTime<QString>> &
  buildMsRunRetentionTimeSpOnPeptideObservations(const AlignmentMethodSp &alignment_method);

  /** @brief collect peak retention times
   * collect all quantified peptides retention times and convert it to the
   * reference time to store it for each peptides
   */
  void collectPeptidePeakRetentionTime(
    const pappso::MsRunRetentionTime<QString> *msrun_retention_time_reference_p) const;

  /** @brief collect ms2 retention times
   * collect all MS2 events retention times and convert it to the
   * reference time to store it for each peptides
   */
  void collectPeptideMs2RetentionTime(
    const pappso::MsRunRetentionTime<QString> *msrun_retention_time_reference_p) const;

  const pappso::MsRunRetentionTime<QString> *getMsRunRetentionTimeConstPtr() const;

  pappso::MsRunRetentionTime<QString> *getMsRunRetentionTimePtr() const;

  const std::vector<PeptideMeasurementsSp> &getPeptideMeasurementsList() const;

  const std::vector<MbrPeptideMeasurementsSp> &getMbrPeptideMeasurementsList() const;

  private:
  std::vector<pappso::XicCoordSPtr>
  buildXicCoordList(const MsRunGroup *msrun_group_p,
                    const QuantificationMethodSp &quantification_method);

  void detectQuantifyPeaks(pappso::UiMonitorInterface &m_uiMonitor,
                           const QuantificationMethodSp &quantification_method);


  void mbrDetectQuantifyPeaks(pappso::UiMonitorInterface &m_uiMonitor,
                              const QuantificationMethodSp &quantification_method);

  void addPeptideObservation2XicCoordList(const MsRunGroup *msrun_group_p,
                                          const QuantificationMethodSp &quantification_method,
                                          const PeptideObservationSp &peptide_events_sp,
                                          std::vector<pappso::XicCoordSPtr> &xic_coord_list);

  void addMbrPeptideMeasurementsSp2XicCoordList(const MsRunGroup *msrun_group_p,
                                                const QuantificationMethodSp &quantification_method,
                                                const PeptideSp &peptide,
                                                std::vector<pappso::XicCoordSPtr> &xic_coord_list);

  private:
  MsRunSp msp_msRun;

  std::map<Peptide *, PeptideObservationSp> m_peptideObservationList;

  std::vector<PeptideMeasurementsSp> m_peptideMeasurementsList;

  std::shared_ptr<pappso::MsRunRetentionTime<QString>> msp_msRunRetentionTime = nullptr;


  std::vector<MbrPeptideMeasurementsSp> m_mbrPeptideMeasurementsList;
};
} // namespace pappso::masschroq
