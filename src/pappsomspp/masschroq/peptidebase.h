/**
 * \file pappsomspp/masschroq/peptidebase.h
 * \date 24/01/2025
 * \author Olivier Langella
 * \brief peptide model base in masschroqlite
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include "../peptide/peptidenaturalisotopelist.h"

namespace pappso::masschroq
{
/**
 * @todo write docs
 */
class PeptideBase
{
  public:
  /**
   * Default constructor
   */
  PeptideBase(const pappso::PeptideSp &peptide_sp);

  /**
   * Destructor
   */
  virtual ~PeptideBase();


  /** @brief get the peptide sequence
   * @return pappso::PeptideSp const reference
   */
  virtual const pappso::PeptideSp &getPappsoPeptideSp() const;
  
  /** @brief compute possible isotopes for this molecule
   * @param ni_min_abundance the minimal isotop abundance proportion to cover
   * (0.9 will compute isotopes to reach at least 90% of its theoretical
   * abundance
   */
  virtual void computeIsotopologues(double ni_min_abundance);

  /** @brief get list of isotopes for this peptide
   * needs computeIsotopologues before
   *
   * @return pappso::PeptideNaturalIsotopeList pointer to all possible isotopes
   * (C13, H2, N15...)
   */
  virtual const pappso::PeptideNaturalIsotopeList *
  getPeptideNaturalIsotopeList() const;


  /** @brief get possible and distinguishable masses of isotopes
   * get list of distinguishable isotopes given the charge and mass precision
   * @param precision mass spectrometer MS1 precision
   * @param charge number of H+
   * @param ni_min_abundance select isotopes to regroup
   * @return std::vector<pappso::PeptideNaturalIsotopeAverageSp> list
   */
  virtual const std::vector<pappso::PeptideNaturalIsotopeAverageSp> &
  getPeptideNaturalIsotopeAverageSpList(pappso::PrecisionPtr precision,
                                        std::uint8_t charge,
                                        double ni_min_abundance);

  protected:
  const pappso::PeptideSp msp_peptide;
  pappso::PeptideNaturalIsotopeList *mpa_peptideNaturalIsotopeList = nullptr;
  std::map<std::uint8_t, std::vector<pappso::PeptideNaturalIsotopeAverageSp>>
    m_peptideNaturalIsotopeAverageSpListByCharge;
  double m_niMinAbundance = 0;
  QMutex m_mutex;
};
} // namespace mcql
