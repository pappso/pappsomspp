/**
 * \file pappsomspp/masschroq/alignmentmethod.h
 * \date 25/10/2024
 * \author Olivier Langella
 * \brief store parameters related to msrun alignment
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <memory>
#include <QString>
#include "../processing/project/projectparameters.h"
#include <QJsonObject>
#include "../exportinmportconfig.h"

namespace pappso::masschroq
{

class PMSPP_LIB_DECL AlignmentMethod;

typedef std::shared_ptr<AlignmentMethod> AlignmentMethodSp;

/**
 * @todo write docs
 */
class AlignmentMethod
{
  public:
  /**
   * Default constructor
   */
  AlignmentMethod(const QString &id);

  AlignmentMethod(const AlignmentMethod &other);

  /**
   * Destructor
   */
  ~AlignmentMethod();

  const QString &getId() const;


  void setMs2TendencyWindow(std::uint8_t ms2_tendency_window);

  void setMs2SmoothingWindow(std::uint8_t ms2_smoothing_window);

  void setMs1SmoothingWindow(std::uint8_t ms1_smoothing_window);

  void setJsonObject(const QJsonObject &json_object);

  QJsonObject getJsonObject() const;


  std::uint8_t getMs2TendencyWindow() const;

  std::uint8_t getMs2SmoothingWindow() const;

  std::uint8_t getMs1SmoothingWindow() const;

  pappso::ProjectParameters getProjectParameters() const;

  private:
  const QString m_id;

  quint8 m_ms2TendencyWindow;
  quint8 m_ms2SmoothingWindow;
  quint8 m_ms1SmoothingWindow;
};
} // namespace pappso::masschroq
