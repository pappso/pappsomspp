/**
 * \file pappsomspp/masschroq/quantificationmethod.h
 * \date 24/10/2024
 * \author Olivier Langella
 * \brief store parameters related to peak detection and quantification
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <memory>
#include <QString>
#include "../exportinmportconfig.h"
#include "../types.h"
#include "../mzrange.h"
#include "../processing/filters/filtersuitestring.h"
#include "../processing/detection/tracedetectioninterface.h"
#include "../processing/project/projectparameters.h"
#include <QJsonObject>

namespace pappso::masschroq
{

class QuantificationMethod;

typedef std::shared_ptr<QuantificationMethod> QuantificationMethodSp;

/**
 * @todo write docs
 */
class PMSPP_LIB_DECL QuantificationMethod
{
  public:
  /**
   * Default constructor
   */
  QuantificationMethod(const QString &id);

  QuantificationMethod(const QuantificationMethod &other);

  /**
   * Destructor
   */
  virtual ~QuantificationMethod();

  const QString &getId() const;

  void setXicExtractMethod(pappso::XicExtractMethod method);

  pappso::XicExtractMethod getXicExtractMethod() const;

  void setXicExtractionLowerPrecisionPtr(pappso::PrecisionPtr precision);

  void setXicExtractionUpperPrecisionPtr(pappso::PrecisionPtr precision);

  pappso::PrecisionPtr getXicExtractionMeanPrecisionPtr() const;

  void addXicFilter(const pappso::FilterNameInterfaceSPtr &filter);
  void setXicFilter(const pappso::FilterNameInterfaceSPtr &filter);

  void setTraceDetectionInterfaceCstSPtr(const pappso::TraceDetectionInterfaceCstSPtr &detection);


  const pappso::TraceDetectionInterfaceCstSPtr &getTraceDetectionInterfaceCstSPtr() const;

  const pappso::FilterNameInterfaceSPtr getXicFilter() const;

  const FilterSuiteStringSPtr &getFilterSuiteStringSPtr() const;


  const pappso::MzRange getXicExtractionMzRange(double mz) const;
  pappso::ProjectParameters getProjectParameters() const;


  void setJsonObject(const QJsonObject &json_object);

  QJsonObject getJsonObject() const;

  pappso::PrecisionPtr getXicExtractionLowerPrecisionPtr() const;
  pappso::PrecisionPtr getXicExtractionUppersPrecisionPtr() const;

  bool getMatchBetweenRun() const;

  void setIsotopeMinimumRatio(double ratio);
  double getIsotopeMinimumRatio() const;


  void setXicExtractionRtRange(double rt_range);
  double getXicExtractionRtRange() const;

  private:
  const QString m_id;
  pappso::XicExtractMethod m_xicExtractMethod = pappso::XicExtractMethod::max;

  pappso::PrecisionPtr mp_xicExtractionLowerPrecisionPtr  = nullptr;
  pappso::PrecisionPtr mp_xicExtractionUppersPrecisionPtr = nullptr;


  /** @brief set the retention time range in seconds around the target rt
   *
   * only the interesting part of the xic will be extracted, form the rt target
   * - range_in_seconds to rt target + range in seconds by default, all the LC
   * run time is extracted
   *
   * @param range_in_seconds range in seconds
   */
  double m_xicExtractionRetentionTimeAroundTarget = 300;


  /// the xic filters
  pappso::FilterSuiteStringSPtr m_xicFilterSuite;


  /// the peak detection method for this quantification
  pappso::TraceDetectionInterfaceCstSPtr mcsp_traceDetectionInterfaceCstSPtr;

  bool m_matchBetweenRun = true;

  /** @brief the minimum percentage of theoretical intensity of the isotope
   * pattern to compute
   */
  double m_isotopeMinimumRatio = 0;
};
} // namespace pappso::masschroq
