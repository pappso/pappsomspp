/**
 * \file pappsomspp/masschroq/peptidemeasurements.h
 * \date 26/09/2024
 * \author Olivier Langella
 * \brief peptide extracted measures model in masschroqlite
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <cstdint>
#include "../msrun/msrunid.h"

#include "peptidebase.h"

#include "peptidemeasurementsbase.h"


namespace pappso::masschroq
{

class PeptideObservation;

typedef std::shared_ptr<PeptideObservation> PeptideObservationSp;


class PeptideMeasurements;

typedef std::shared_ptr<PeptideMeasurements> PeptideMeasurementsSp;

class MsRunGroup;

/**
 * group together a peptide and measurements made on this peptide
 */
class PeptideMeasurements : public PeptideMeasurementsBase
{
  public:
  /**
   * Default constructor
   */
  PeptideMeasurements(const PeptideObservationSp &peptide_observations);

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  PeptideMeasurements(const PeptideMeasurements &other);

  /**
   * Destructor
   */
  virtual ~PeptideMeasurements();

  void prepareMeasurements(const pappso::MsRunId &targeted_msrun,
                           const MsRunGroup *msrun_group_p,
                           const QuantificationMethodSp &quantification_method);


  virtual void detectQuantifyPeaks(const QuantificationMethodSp &quantification_method) override;

  const PeptideObservationSp &getPeptideObservationSp() const;


  private:
  PeptideObservationSp msp_peptideObservation;
};
} // namespace pappso::masschroq
