/**
 * \file pappsomspp/masschroq/peptideobservation.h
 * \date 24/09/2024
 * \author Olivier Langella
 * \brief peptide model in masschroqlite
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include "precursor.h"
#include "peptide.h"
#include <cstdint>


namespace pappso::masschroq
{

class PeptideObservation;

typedef std::shared_ptr<PeptideObservation> PeptideObservationSp;

/**
 * @todo group together one Peptide and observations on this peptide
 */
class PeptideObservation
{
  public:
  /**
   * Default constructor
   */
  PeptideObservation(PeptideSp peptide);

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  PeptideObservation(const PeptideObservation &other);

  /**
   * Destructor
   */
  virtual ~PeptideObservation();

  const PeptideSp &getPeptideSp() const;

  struct Observation
  {
    /// The MS scan_number(mzxml) it has been observed in
    std::size_t m_scanNumber = 0;
    /// The charge it has been observed in
    std::uint8_t m_charge = 0;

    PrecursorSp msp_precursor = nullptr;

    bool m_isSpectrumIndex = true;

    PeptideLabel *p_label = nullptr;
  };

  void addObservation(const PeptideObservation::Observation &observation);

  std::vector<std::uint8_t> getObservedChargeStates() const;

  double getBestRtOverallChargeStates() const;

  std::vector<double> getObservedRetentionTimesOverallChargeStates() const;

  pappso::XicCoordSPtr getBestXicCoordSPtrForCharge(std::uint8_t charge) const;

  pappso::XicCoordSPtr getBestXicCoord() const;

  const std::vector<Observation> &getObservationList() const;

  private:
  PeptideSp msp_peptide;
  std::vector<Observation> m_observationList;
};
} // namespace pappso::masschroq
