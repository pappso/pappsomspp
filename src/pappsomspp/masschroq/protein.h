/**
 * \file pappsomspp/masschroq/protein.h
 * \date 10/10/2024
 * \author Olivier Langella
 * \brief protein model in masschroqlite
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <memory>
#include <QString>

namespace pappso::masschroq
{

class Protein;

typedef std::shared_ptr<Protein> ProteinSp;

/**
 * @todo write docs
 */
class Protein
{
  public:
  /**
   * Default constructor
   */
  Protein(const QString &id, const QString &description);

  Protein(const Protein &other);

  /**
   * Destructor
   */
  virtual ~Protein();
  const QString &getId() const;
  const QString &getDescription() const;

  private:
  const QString m_id;
  const QString m_description;
};
} // namespace mcql
