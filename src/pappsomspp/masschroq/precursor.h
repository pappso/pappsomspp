/**
 * \file pappsomspp/masschroq/precursor.h
 * \date 04/11/2024
 * \author Olivier Langella
 * \brief MS1 precursor information
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <memory>
#include "../msrun/xiccoord/xiccoord.h"

namespace pappso::masschroq
{

class Precursor;

typedef std::shared_ptr<Precursor> PrecursorSp;

/**
 * \class Precursor
 * \brief A Precursor object represents the parent/precursor of the MS level 2
 * scan with number _scan_num in the mzxml file.

 * In mzxml files, MS scans of level 2 have each a precursor object
 * representing their MS level 1 parent/precursor. A Precursor object has :
 * - a spectrum index
 * - a retention time value
 * - an intensity value
 * - an mz value
 * - Xic coordinates
 */

class Precursor
{
  public:
  /**
   * Default constructor
   */
  Precursor(std::size_t spectrum_index,
            double mz,
            double intensity,
            pappso::XicCoordSPtr xic_coord);

  /**
   * Destructor
   */
  virtual ~Precursor();
  double getIntensity() const;

  std::size_t getSpectrumIndex() const;

  const pappso::XicCoordSPtr &getXicCoordSPtr() const;

  private:
  ///< MS level 2 scan number whome this Precursor is parent
  std::size_t m_spectrumIndex;
  /// intensity of this precursor
  double m_intensity;
  /// selected m/z of this precursor
  double m_mz;
  /// Xic coordinates
  pappso::XicCoordSPtr msp_xicCoord;
};
} // namespace pappso::masschroq
