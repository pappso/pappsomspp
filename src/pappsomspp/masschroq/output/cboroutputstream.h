
/**
 * \file pappsomspp/masschroq/output/cboroutputstream.h
 * \date 02/01/2025
 * \author Olivier Langella
 * \brief quantification result cbor writer for MassChroqLight
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QStringList>
#include <QCborStreamWriter>
#include <QJsonObject>
#include "../msrun.h"
#include "../msrunpeptidelist.h"
#include "../peptidemeasurements.h"
#include "../mbrpeptidemeasurements.h"
#include "../../msrun/alignment/msrunretentiontime.h"
#include "../../processing/project/projectparameters.h"
#include "../../exportinmportconfig.h"


namespace pappso::masschroq
{

/**
 * @todo write docs
 */
class PMSPP_LIB_DECL CborOutputStream
{
  public:
  /**
   * Default constructor
   */
  CborOutputStream(QIODevice *device);

  /**
   * Destructor
   */
  virtual ~CborOutputStream();

  /** @brief set trace output flag
   * */
  void setIsTraceOutput(bool is_trace_output);


  /** @brief set peak shape output flag
   * */
  void setIsPeakShapeOutput(bool is_shape_output, double margin_in_seconds);

  void writeJsonObject(const QString &name, const QJsonObject &json_object);

  void writeProjectParameters(const pappso::ProjectParameters &project_parameters);


  void writeActionBegin();

  void close();


  QCborStreamWriter &getCborStreamWriter();


  void writeQrDataBlock(pappso::UiMonitorInterface &m_uiMonitor,
                        const MsRunPeptideList &msrun_peptide_list,
                        bool is_mbr);

  void writeMsRunRetentionTime(const pappso::MsRunRetentionTime<QString> &msrun_retention_time);

  private:
  void
  writeMbrPeptideMeasurements(const MbrPeptideMeasurements &peptide_measurements,
                              const pappso::MsRunRetentionTime<QString> *msrun_retention_time_p);

  void writeMeasurement(const PeptideMeasurements::Measurement &measurement,
                        const pappso::MsRunRetentionTime<QString> *msrun_retention_time_p);
  void writeTrace(const pappso::Trace &trace);
  void writeVectorDouble(const std::vector<double> &vector);
  void writeTracePeak(const pappso::TracePeak &peak,
                      const pappso::MsRunRetentionTime<QString> *msrun_retention_time_p);
  void writeActionEnd();


  void writeMsRun(const MsRun &msrun);

  void writePeptideMeasurements(const PeptideMeasurements &peptide_measurements,
                                const pappso::MsRunRetentionTime<QString> *msrun_retention_time_p);


  private:
  QCborStreamWriter *mpa_writer = nullptr;
  bool m_isTraceOutput          = false;
  bool m_isPeakShapeOutput      = false;
  bool m_isXicCoordOutput       = true;

  double m_peakShapeMarginInSeconds = 20;
};
} // namespace pappso::masschroq
