
/**
 * \file pappsomspp/masschroq/input/precursorparser.h
 * \date 25/09/2024
 * \author Olivier Langella
 * \brief read presurcors in ms run files for MassChroqLight
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../../msrun/msrunreader.h"
#include "../precursor.h"
namespace pappso::masschroq
{

/**
 * @todo write docs
 */
class PrecursorParser : public pappso::SpectrumCollectionHandlerInterface
{
  public:
  public:
  PrecursorParser(pappso::MsRunReaderCstSPtr msrun_reader);

  virtual ~PrecursorParser();

  virtual void setQualifiedMassSpectrum(
    const pappso::QualifiedMassSpectrum &qspectrum) override;
  virtual bool needPeakList() const override;
  virtual bool isReadAhead() const override;

  const PrecursorSp &
  getPrecursorSPtrBySpectrumIndex(std::size_t spectrum_index) const;
  const PrecursorSp &
  getPrecursorSPtrByScanNumber(std::size_t spectrum_index) const;


  private:
  /// map precursor to its scan number as parsed in the xml file of this msrun
  void mapScanNumberPrecursor(std::size_t scan_num, PrecursorSp precursor);

  /** @brief map spectrum index to precursor
   * new modern method to replace obsolete scan number
   */
  void mapSpectrumIndexToPrecursor(std::size_t spectrum_index,
                                   PrecursorSp precursor);


  private:
  pappso::MsRunReaderCstSPtr msp_msrunReader;

  std::map<std::size_t, PrecursorSp> m_scanNumber2PrecursorMap;
  std::map<std::size_t, PrecursorSp> m_spectrumIndex2PrecursorMap;
};
} // namespace mcql
