
/**
 * \file pappsomspp/masschroq/input/precursorparser.h
 * \date 25/09/2024
 * \author Olivier Langella
 * \brief read presurcors in ms run files for MassChroqLight
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "precursorparser.h"
#include "../../pappsoexception.h"
#include "../../utils.h"


pappso::masschroq::PrecursorParser::PrecursorParser(pappso::MsRunReaderCstSPtr msrun_reader)
{
  msp_msrunReader = msrun_reader;
}


pappso::masschroq::PrecursorParser::~PrecursorParser()
{
}

bool
pappso::masschroq::PrecursorParser::needPeakList() const
{
  return false;
}
void
pappso::masschroq::PrecursorParser::setQualifiedMassSpectrum(
  const pappso::QualifiedMassSpectrum &qspectrum)
{

  if(qspectrum.getMsLevel() > 1)
    {

      pappso::masschroq::PrecursorSp precursor = std::make_shared<pappso::masschroq::Precursor>(
        qspectrum.getPrecursorSpectrumIndex(),
        qspectrum.getPrecursorMz(),
        qspectrum.getPrecursorIntensity(),
        msp_msrunReader.get()->newXicCoordSPtrFromQualifiedMassSpectrum(
          qspectrum, pappso::PrecisionFactory::getPpmInstance(10)));


      qDebug() << " native_id=" << qspectrum.getMassSpectrumId().getNativeId()
               << " scan idx="
               << qspectrum.getMassSpectrumId().getSpectrumIndex()
               << " precursor_idx=" << qspectrum.getPrecursorSpectrumIndex()
               << " precursor.get()->getXicCoordSPtr().get()->toString="
               << precursor.get()->getXicCoordSPtr().get()->toString();
      if(precursor.get()->getXicCoordSPtr().get() == nullptr)
        {
          throw pappso::PappsoException(
            QObject::tr("precursor.get()->getXicCoordSPtr().get() == "
                        "nullptr\nindex=%1 %2 %3 %4")
              .arg(qspectrum.getMassSpectrumId().getNativeId())
              .arg(__FILE__)
              .arg(__FUNCTION__)
              .arg(__LINE__));
        }
      /// adding this new precursor to the msrun's hash map of scan_num ->
      /// precursor
      qDebug();
      if(qspectrum.getMassSpectrumId().getNativeId().contains("scan="))
        {
          qDebug();
          mapScanNumberPrecursor(
            pappso::Utils::extractScanNumberFromMzmlNativeId(
              qspectrum.getMassSpectrumId().getNativeId()),
            precursor);
          mapSpectrumIndexToPrecursor(
            qspectrum.getMassSpectrumId().getSpectrumIndex(), precursor);
          qDebug();
        }
      else
        {
          qDebug();
          mapScanNumberPrecursor(
            qspectrum.getMassSpectrumId().getSpectrumIndex(), precursor);

          mapSpectrumIndexToPrecursor(
            qspectrum.getMassSpectrumId().getSpectrumIndex(), precursor);
          qDebug();
        }
      qDebug();
    }
}

bool
pappso::masschroq::PrecursorParser::isReadAhead() const
{
  return true;
}

void
pappso::masschroq::PrecursorParser::mapScanNumberPrecursor(std::size_t scan_num,
                                              pappso::masschroq::PrecursorSp precursor)
{

  if(precursor.get()->getXicCoordSPtr().get() == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr(
          "precursor.get()->getXicCoordSPtr().get() == nullptr scan_num=%1\n")
          .arg(scan_num));
    }
  m_scanNumber2PrecursorMap[scan_num] = precursor;
}

void
pappso::masschroq::PrecursorParser::mapSpectrumIndexToPrecursor(std::size_t spectrum_index,
                                                   pappso::masschroq::PrecursorSp precursor)
{

  qDebug() << " spectrum_index=" << spectrum_index;

  if(precursor.get()->getXicCoordSPtr().get() == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("precursor.get()->getXicCoordSPtr().get() == nullptr \n")
          .arg(msp_msrunReader.get()->getMsRunId()->getXmlId())
          .arg(spectrum_index));
    }
  m_spectrumIndex2PrecursorMap[spectrum_index] = precursor;
  qDebug() << " spectrum_index=" << spectrum_index;
}

const pappso::masschroq::PrecursorSp &
pappso::masschroq::PrecursorParser::getPrecursorSPtrBySpectrumIndex(
  std::size_t spectrum_index) const
{
  auto it = m_spectrumIndex2PrecursorMap.find(spectrum_index);
  if(it != m_spectrumIndex2PrecursorMap.end())
    {
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("spectrum index %1 not found in %2")
          .arg(spectrum_index)
          .arg(msp_msrunReader.get()->getMsRunId()->getXmlId()));
    }
  return it->second;
}

const pappso::masschroq::PrecursorSp &
pappso::masschroq::PrecursorParser::getPrecursorSPtrByScanNumber(
  std::size_t spectrum_index) const
{
  auto it = m_scanNumber2PrecursorMap.find(spectrum_index);
  if(it != m_scanNumber2PrecursorMap.end())
    {
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("scan number %1 not found in %2")
          .arg(spectrum_index)
          .arg(msp_msrunReader.get()->getMsRunId()->getXmlId()));
    }
  return it->second;
}
