
/**
 * \file pappsomspp/masschroq/input/jsoninput.h
 * \date 02/01/2025
 * \author Olivier Langella
 * \brief process json document input
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include "../../processing/uimonitor/uimonitorinterface.h"
#include "../../processing/project/projectparameters.h"
#include <QJsonDocument>
#include "../output/cboroutputstream.h"
#include "../msrungroup.h"
#include "../../exportinmportconfig.h"

namespace pappso::masschroq
{

/**
 * @todo write docs
 */
class PMSPP_LIB_DECL JsonInput
{
  public:
  /**
   * Default constructor
   */
  JsonInput(pappso::UiMonitorInterface &ui_monitor,
            const QString &tmp_dir_name,
            const QJsonDocument &json_doc);

  /**
   * Destructor
   */
  ~JsonInput();

  void action(CborOutputStream &cbor_output);

  private:
  const QJsonValue documentFind(const QString &key1, const QString &key2) const;
  void readMzDataFiles();
  void readAlignmentMethodSp();
  void readQuantificationMethodSp();
  void readProjectParameters();
  void read_protein_list();
  void read_peptide_list();
  void read_msrun_peptide_observations();
  void readAction(CborOutputStream &cbor_output);

  void computeIsotopologues(double ni_ratio);

  private:
  const QJsonDocument &m_jsonDocument;
  const QString &m_tmpDirName;
  pappso::UiMonitorInterface &m_uiMonitor;


  std::map<QString, MsRunPeptideListSp> m_msfileList;
  AlignmentMethodSp msp_alignmentMethod;
  QuantificationMethodSp msp_quantificationMethod;
  std::map<QString, ProteinSp> m_proteinMap;
  std::vector<PeptideSp> m_peptideStore;
  std::map<QString, PeptideSp> m_peptideMap;
  std::map<QString, MsRunGroupSp> m_msRunGroupSpList;

  std::shared_ptr<pappso::IonMobilityGrid> msp_ionMobilityGrid;

  pappso::ProjectParameters m_projectParameters;

  bool m_isMatchBetweenRun = true;
};
} // namespace pappso::masschroq
