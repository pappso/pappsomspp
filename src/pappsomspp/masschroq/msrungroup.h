/**
 * \file pappsomspp/masschroq/msrungroup.h
 * \date 10/10/2024
 * \author Olivier Langella
 * \brief group msrun together in masschroqlite
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "msrunpeptidelist.h"
#include "quantificationmethod.h"
#include "alignmentmethod.h"
#include "../msrun/xiccoord/ionmobilitygrid.h"
#include "output/cboroutputstream.h"

namespace pappso::masschroq
{

class MsRunGroup;

typedef std::shared_ptr<MsRunGroup> MsRunGroupSp;

/**
 * @todo write docs
 */
class MsRunGroup
{
  public:
  /**
   * Default constructor
   */
  MsRunGroup(const QString &id, const std::vector<MsRunPeptideListSp> &msrun_list);

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  MsRunGroup(const MsRunGroup &other);

  /**
   * Destructor
   */
  virtual ~MsRunGroup();

  void setIonMobilityGridSp(const std::shared_ptr<pappso::IonMobilityGrid> &ion_mobility_grid_sp);


  void align(CborOutputStream &cbor_output,
             const QString &align_id,
             pappso::UiMonitorInterface &m_uiMonitor) const;

  void quantify(CborOutputStream &cbor_output,
                const QString &quantify_id,
                const QString &tmp_dir,
                pappso::UiMonitorInterface &m_uiMonitor,
                const QuantificationMethodSp &quantification_method,
                const std::vector<PeptideSp> &peptide_sp_list) const;

  void setAlignmentMethodSp(const AlignmentMethodSp &alignment_method_sp,
                            const QString &msrun_ref_id);

  bool contains(const MsRun *msrun_p) const;

  bool hasTimsTofMobilityIndex() const;

  const std::shared_ptr<pappso::IonMobilityGrid> &getIonMobilityGridSp() const;

  const QString &getId() const;

  private:
  void alignRetentionTimeBetweenMsRuns(CborOutputStream &cbor_output,
                                       pappso::UiMonitorInterface &m_uiMonitor) const;


  private:
  const QString m_id;
  const std::vector<MsRunPeptideListSp> m_msRunPeptideListSpList;
  AlignmentMethodSp msp_alignmentMethod;
  MsRunPeptideListSp msp_msRunPeptideListAlignmentReference;
  bool m_hasTimsTofMobilityIndex = false;
  std::shared_ptr<pappso::IonMobilityGrid> msp_ionMobilityGrid;
};
} // namespace pappso::masschroq
