/**
 * \file pappsomspp/masschroq/quantificationmethod.cpp
 * \date 24/10/2024
 * \author Olivier Langella
 * \brief store parameters related to peak detection and quantification
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "quantificationmethod.h"
#include "utils.h"
#include "../pappsoexception.h"
#include "../exception/exceptionnotfound.h"
#include "../processing/detection/tracedetectionzivy.h"

pappso::masschroq::QuantificationMethod::QuantificationMethod(const QString &id) : m_id(id)
{
  // by default
  qDebug();
  mp_xicExtractionLowerPrecisionPtr  = pappso::PrecisionFactory::getPpmInstance(10);
  mp_xicExtractionUppersPrecisionPtr = pappso::PrecisionFactory::getPpmInstance(10);
  m_isotopeMinimumRatio              = 0.8;

  qDebug();
  setXicFilter(std::make_shared<pappso::FilterMorphoAntiSpike>(5));

  qDebug();
  mcsp_traceDetectionInterfaceCstSPtr =
    std::make_shared<pappso::TraceDetectionZivy>(1, 3, 2, 5000, 3000);
  qDebug();
}

pappso::masschroq::QuantificationMethod::QuantificationMethod(
  const pappso::masschroq::QuantificationMethod &other)
  : m_id(other.m_id)
{
  m_isotopeMinimumRatio                    = other.m_isotopeMinimumRatio;
  m_matchBetweenRun                        = other.m_matchBetweenRun;
  m_xicExtractMethod                       = other.m_xicExtractMethod;
  m_xicFilterSuite                         = other.m_xicFilterSuite;
  mcsp_traceDetectionInterfaceCstSPtr      = other.mcsp_traceDetectionInterfaceCstSPtr;
  mp_xicExtractionLowerPrecisionPtr        = other.mp_xicExtractionLowerPrecisionPtr;
  mp_xicExtractionUppersPrecisionPtr       = other.mp_xicExtractionUppersPrecisionPtr;
  m_xicExtractionRetentionTimeAroundTarget = other.m_xicExtractionRetentionTimeAroundTarget;
}


pappso::masschroq::QuantificationMethod::~QuantificationMethod()
{
}

const QString &
pappso::masschroq::QuantificationMethod::getId() const
{
  return m_id;
}

double
pappso::masschroq::QuantificationMethod::getXicExtractionRtRange() const
{
  return m_xicExtractionRetentionTimeAroundTarget;
}

void
pappso::masschroq::QuantificationMethod::setXicExtractionRtRange(double rt_range)
{
  m_xicExtractionRetentionTimeAroundTarget = rt_range;
}

bool
pappso::masschroq::QuantificationMethod::getMatchBetweenRun() const
{
  return m_matchBetweenRun;
}

void
pappso::masschroq::QuantificationMethod::setIsotopeMinimumRatio(double ratio)
{
  if((ratio < 1) && (ratio >= 0))
    {
      m_isotopeMinimumRatio = ratio;
    }
}

double
pappso::masschroq::QuantificationMethod::getIsotopeMinimumRatio() const
{
  return m_isotopeMinimumRatio;
}

void
pappso::masschroq::QuantificationMethod::setXicFilter(const pappso::FilterNameInterfaceSPtr &filter)
{
  m_xicFilterSuite = std::make_shared<pappso::FilterSuiteString>(filter.get()->toString());
}


void
pappso::masschroq::QuantificationMethod::addXicFilter(const pappso::FilterNameInterfaceSPtr &filter)
{
  if(m_xicFilterSuite.get() == nullptr)
    {
      m_xicFilterSuite = std::make_shared<pappso::FilterSuiteString>(filter.get()->toString());
    }
  else
    {
      m_xicFilterSuite.get()->addFilter(filter);
    }
}

const pappso::FilterSuiteStringSPtr &
pappso::masschroq::QuantificationMethod::getFilterSuiteStringSPtr() const
{
  return m_xicFilterSuite;
}


pappso::PrecisionPtr
pappso::masschroq::QuantificationMethod::getXicExtractionMeanPrecisionPtr() const
{

  if(mp_xicExtractionLowerPrecisionPtr->unit() == pappso::PrecisionUnit::ppm)
    {
      return pappso::PrecisionFactory::getPpmInstance(
        (mp_xicExtractionLowerPrecisionPtr->getNominal() +
         mp_xicExtractionUppersPrecisionPtr->getNominal()) /
        2);
    }
  return pappso::PrecisionFactory::getDaltonInstance(
    (mp_xicExtractionLowerPrecisionPtr->getNominal() +
     mp_xicExtractionUppersPrecisionPtr->getNominal()) /
    2);
}

pappso::XicExtractMethod
pappso::masschroq::QuantificationMethod::getXicExtractMethod() const
{
  return m_xicExtractMethod;
}

void
pappso::masschroq::QuantificationMethod::setTraceDetectionInterfaceCstSPtr(
  const pappso::TraceDetectionInterfaceCstSPtr &detection)
{
  mcsp_traceDetectionInterfaceCstSPtr = detection;
}

void
pappso::masschroq::QuantificationMethod::setXicExtractionLowerPrecisionPtr(
  pappso::PrecisionPtr precision)
{
  mp_xicExtractionLowerPrecisionPtr = precision;
}

void
pappso::masschroq::QuantificationMethod::setXicExtractionUpperPrecisionPtr(
  pappso::PrecisionPtr precision)
{
  mp_xicExtractionUppersPrecisionPtr = precision;
}

void
pappso::masschroq::QuantificationMethod::setXicExtractMethod(pappso::XicExtractMethod method)
{
  m_xicExtractMethod = method;
}

const pappso::FilterNameInterfaceSPtr
pappso::masschroq::QuantificationMethod::getXicFilter() const
{
  return m_xicFilterSuite;
}

const pappso::MzRange
pappso::masschroq::QuantificationMethod::getXicExtractionMzRange(double mz) const
{

  return pappso::MzRange(mz, mp_xicExtractionLowerPrecisionPtr, mp_xicExtractionUppersPrecisionPtr);
}

const pappso::TraceDetectionInterfaceCstSPtr &
pappso::masschroq::QuantificationMethod::getTraceDetectionInterfaceCstSPtr() const
{
  return mcsp_traceDetectionInterfaceCstSPtr;
}

pappso::ProjectParameters
pappso::masschroq::QuantificationMethod::getProjectParameters() const
{
  pappso::ProjectParameters parameters;

  pappso::ProjectParam project_param(
    {pappso::ProjectParamCategory::quantification, "", QVariant()});
  // xic_type="max"
  project_param.name = "mcq_xic_extraction_type";
  project_param.value.setValue(pappso::masschroq::Utils::enumToString(m_xicExtractMethod));
  parameters.setProjectParam(project_param);

  project_param.name = "mcq_mbr";
  project_param.value.setValue(getMatchBetweenRun());
  parameters.setProjectParam(project_param);

  project_param.name = "mcq_isotope_minimum_ratio";
  project_param.value.setValue(getIsotopeMinimumRatio());
  parameters.setProjectParam(project_param);


  if(mp_xicExtractionLowerPrecisionPtr->unit() == pappso::PrecisionUnit::ppm)
    {

      //<!--For XIC extraction on Da use: mz_range-->
      //<ppm_range min="10" max="10"/>

      project_param.name = "mcq_xic_ppm_range_min";
      project_param.value.setValue(mp_xicExtractionLowerPrecisionPtr->getNominal());
      parameters.setProjectParam(project_param);

      project_param.name = "mcq_xic_ppm_range_max";
      project_param.value.setValue(mp_xicExtractionUppersPrecisionPtr->getNominal());
      parameters.setProjectParam(project_param);
    }
  if(mp_xicExtractionLowerPrecisionPtr->unit() == pappso::PrecisionUnit::dalton)
    {

      //<!--For XIC extraction on Da use: mz_range-->
      //<ppm_range min="10" max="10"/>

      project_param.name = "mcq_xic_mz_range_min";
      project_param.value.setValue(mp_xicExtractionLowerPrecisionPtr->getNominal());
      parameters.setProjectParam(project_param);

      project_param.name = "mcq_xic_mz_range_max";
      project_param.value.setValue(mp_xicExtractionUppersPrecisionPtr->getNominal());
      parameters.setProjectParam(project_param);
    }

  project_param.name = "mcq_xic_pre_filter";
  if(m_xicFilterSuite.get() != nullptr)
    project_param.value.setValue(m_xicFilterSuite.get()->toString());
  else
    project_param.value.setValue("");
  parameters.setProjectParam(project_param);


  const pappso::TraceDetectionZivy *detection_zivy =
    dynamic_cast<const pappso::TraceDetectionZivy *>(mcsp_traceDetectionInterfaceCstSPtr.get());

  if(detection_zivy != nullptr)
    {
      project_param.name = "mcq_detection_zivy";
      project_param.value.setValue(QString("%1 %2 %3 %4 %5")
                                     .arg(detection_zivy->getSmoothingHalfEdgeWindows())
                                     .arg(detection_zivy->getMinMaxHalfEdgeWindows())
                                     .arg(detection_zivy->getMaxMinHalfEdgeWindows())
                                     .arg(detection_zivy->getDetectionThresholdOnMinmax())
                                     .arg(detection_zivy->getDetectionThresholdOnMaxmin()));
      parameters.setProjectParam(project_param);
    }
  return parameters;
}

QJsonObject
pappso::masschroq::QuantificationMethod::getJsonObject() const
{
  QJsonObject method;

  qDebug();

  QString xic_type = Utils::enumToString(m_xicExtractMethod);

  if(mp_xicExtractionUppersPrecisionPtr == nullptr)
    {
      throw pappso::PappsoException(
        "mp_xicExtractionUppersPrecisionPtr == "
        "nullptr");
    }
  if(mp_xicExtractionLowerPrecisionPtr == nullptr)
    {
      throw pappso::PappsoException(
        "mp_xicExtractionLowerPrecisionPtr == "
        "nullptr");
    }
  qDebug();
  QJsonObject precision;
  precision.insert("unit", Utils::enumToString(mp_xicExtractionUppersPrecisionPtr->unit()));
  precision.insert("down", mp_xicExtractionLowerPrecisionPtr->getNominal());
  precision.insert("up", mp_xicExtractionUppersPrecisionPtr->getNominal());


  QJsonObject extraction;
  extraction.insert("integration", xic_type);
  extraction.insert("precision", precision);

  method.insert("extraction", extraction);
  method.insert("match_between_run", m_matchBetweenRun);
  method.insert("isotope_minimum_ratio", m_isotopeMinimumRatio);
  method.insert("rt_range", m_xicExtractionRetentionTimeAroundTarget);

  // xic_filters

  if(m_xicFilterSuite.get() == nullptr)
    {
      throw pappso::PappsoException(
        "m_xicFilterSuite.get() == "
        "nullptr");
    }
  method.insert("pre_filter", m_xicFilterSuite.get()->toString());


  const pappso::TraceDetectionZivy *detection_zivy =
    dynamic_cast<const pappso::TraceDetectionZivy *>(getTraceDetectionInterfaceCstSPtr().get());

  if(detection_zivy == nullptr)
    {
      throw pappso::PappsoException(
        "m_quantificationMethod.getTraceDetectionInterfaceCstSPtr().get() == "
        "nullptr");
    }

  QJsonObject detection;
  detection.insert("type", "zivy");
  detection.insert("meanfilter", (int)detection_zivy->getSmoothingHalfEdgeWindows());
  detection.insert("minmax", (int)detection_zivy->getMinMaxHalfEdgeWindows());
  detection.insert("maxmin", (int)detection_zivy->getMaxMinHalfEdgeWindows());
  detection.insert("threshold_on_max", detection_zivy->getDetectionThresholdOnMinmax());
  detection.insert("threshold_on_min", detection_zivy->getDetectionThresholdOnMaxmin());
  method.insert("detection", detection);


  return method;
}

void
pappso::masschroq::QuantificationMethod::setJsonObject(const QJsonObject &quantification_method)
{
  m_matchBetweenRun     = quantification_method.value("match_between_run").toBool();
  m_isotopeMinimumRatio = quantification_method.value("isotope_minimum_ratio").toDouble();
  QString filter_str    = quantification_method.value("pre_filter").toString();

  qDebug() << filter_str;
  if(!filter_str.isEmpty())
    {
      pappso::FilterNameInterfaceSPtr filter_sp =
        std::make_shared<pappso::FilterSuiteString>(filter_str);
      addXicFilter(filter_sp);
      qDebug() << filter_str;
    }
  QJsonObject extraction = quantification_method.value("extraction").toObject();

  QJsonValue rt_range = extraction.value("rt_range");
  if(!rt_range.isUndefined())
    {
      setXicExtractionRtRange(rt_range.toDouble());
    }

  QString integration = extraction.value("integration").toString();
  if(integration == "sum")
    {
      setXicExtractMethod(pappso::XicExtractMethod::sum);
    }
  else if(integration == "max")
    {
      setXicExtractMethod(pappso::XicExtractMethod::max);
    }
  else
    {
      throw pappso::ExceptionNotFound(
        QObject::tr("missing "
                    "masschroq_methods>quantification_method>extraction>"
                    "integration %1 value")
          .arg(integration));
    }

  QJsonObject precision = extraction.value("precision").toObject();
  qDebug() << precision.value("down").toDouble();
  qDebug() << precision.value("up").toDouble();

  if(precision.value("unit").toString() == "dalton")
    {
      setXicExtractionLowerPrecisionPtr(
        pappso::PrecisionFactory::getDaltonInstance(precision.value("down").toDouble()));
      setXicExtractionUpperPrecisionPtr(
        pappso::PrecisionFactory::getDaltonInstance(precision.value("up").toDouble()));
    }
  else if(precision.value("unit").toString() == "ppm")
    {
      setXicExtractionLowerPrecisionPtr(
        pappso::PrecisionFactory::getPpmInstance(precision.value("down").toDouble()));
      setXicExtractionUpperPrecisionPtr(
        pappso::PrecisionFactory::getPpmInstance(precision.value("up").toDouble()));
    }
  else
    {
      throw pappso::ExceptionNotFound(
        QObject::tr("missing "
                    "masschroq_methods>quantification_method>extraction>precision>"
                    "unit"));
    }


  QJsonObject detection = quantification_method.value("detection").toObject();
  if(detection.value("type").toString() == "zivy")
    {
      std::shared_ptr<pappso::TraceDetectionZivy> sp_detection_zivy =
        std::make_shared<pappso::TraceDetectionZivy>(detection.value("meanfilter").toInt(),
                                                     detection.value("minmax").toInt(),
                                                     detection.value("maxmin").toInt(),
                                                     detection.value("threshold_on_max").toInt(),
                                                     detection.value("threshold_on_min").toInt());
      setTraceDetectionInterfaceCstSPtr(sp_detection_zivy);
    }
  else
    {

      throw pappso::ExceptionNotFound(
        QObject::tr("only masschroq_methods>quantification_method>detection>type == zivy "
                    "allowed"));
    }
}

pappso::PrecisionPtr
pappso::masschroq::QuantificationMethod::getXicExtractionLowerPrecisionPtr() const
{
  return mp_xicExtractionLowerPrecisionPtr;
}

pappso::PrecisionPtr
pappso::masschroq::QuantificationMethod::getXicExtractionUppersPrecisionPtr() const
{
  return mp_xicExtractionUppersPrecisionPtr;
}
