/**
 * \file pappsomspp/masschroq/peptidemeasurements.h
 * \date 24/01/2025
 * \author Olivier Langella
 * \brief peptide measurement model base in masschroqlite
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include "types.h"
#include "../msrun/xiccoord/xiccoord.h"
#include "../processing/detection/tracepeak.h"
#include "../peptide/peptidenaturalisotopelist.h"
#include "peptidebase.h"
#include "quantificationmethod.h"


namespace pappso::masschroq
{

class PeptideLabel;
class Peptide;
/**
 * @todo write docs
 */
class PeptideMeasurementsBase
{
  public:
  /**
   * Default constructor
   */
  PeptideMeasurementsBase();

  PeptideMeasurementsBase(const PeptideMeasurementsBase &other);

  /**
   * Destructor
   */
  virtual ~PeptideMeasurementsBase();


  /** the same peptide can be measured multiple times :
   * - for each charge state
   * - for each isotopologue
   * One measurement contains :
   * - a transient chromatogram extraction
   * - extracted XIC and coordinates
   * - peak quality : how the detected peak in the chromatogram was chosen
   *
   */
  struct Measurement
  {
    std::uint8_t m_charge;
    pappso::XicCoordSPtr msp_xicCoord;
    PeakQualityCategory m_peakQualityCategory = PeakQualityCategory::nomatch;
    const PeptideLabel *mp_peptideLabel;
    pappso::TracePeakCstSPtr m_tracePeakSp;
    pappso::PeptideNaturalIsotopeAverageSp m_peptideNaturalIsotopeAverageSp;
  };


  virtual const std::vector<Measurement> &getMeasurementList() const;


  virtual void pushBackXicCoordList(std::vector<pappso::XicCoordSPtr> &xic_coord_list) const final;

  virtual void detectQuantifyPeaks(const QuantificationMethodSp &quantification_method) = 0;


  virtual void flushXics() final;


  /** @brief clear the measurement vector
   */
  virtual void clear() final;


  protected:
  virtual void prepareMeasurementsForPeptide(Peptide &the_peptide,
                                             const QuantificationMethodSp &quantification_method,
                                             const pappso::XicCoordSPtr &best_xic_coord,
                                             std::uint8_t charge,
                                             double best_rt) final;

  virtual void
  generateMeasurementsForIsotopeList(PeptideBase &peptide_base,
                                     const PeptideLabel *label_p,
                                     const QuantificationMethodSp &quantification_method,
                                     std::uint8_t charge,
                                     double isotope_minimum_ratio,
                                     const pappso::XicCoordSPtr &best_xic_coord,
                                     double best_rt) final;

  protected:
  std::vector<Measurement> m_measurementList;
};
} // namespace pappso::masschroq
