/**
 * \file pappsomspp/masschroq/alignmentmethod.cpp
 * \date 25/10/2024
 * \author Olivier Langella
 * \brief store parameters related to msrun alignment
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "alignmentmethod.h"

pappso::masschroq::AlignmentMethod::AlignmentMethod(const QString &id) : m_id(id)
{
  m_ms1SmoothingWindow = 15;
  m_ms2SmoothingWindow = 5;
  m_ms2TendencyWindow  = 10;
}

pappso::masschroq::AlignmentMethod::AlignmentMethod(const pappso::masschroq::AlignmentMethod &other)
  : m_id(other.m_id)
{
  m_ms1SmoothingWindow = other.m_ms1SmoothingWindow;
  m_ms2SmoothingWindow = other.m_ms2SmoothingWindow;
  m_ms2TendencyWindow  = other.m_ms2TendencyWindow;
}


pappso::masschroq::AlignmentMethod::~AlignmentMethod()
{
}

const QString &
pappso::masschroq::AlignmentMethod::getId() const
{
  return m_id;
}

void
pappso::masschroq::AlignmentMethod::setMs1SmoothingWindow(std::uint8_t ms1_smoothing_window)
{
  m_ms1SmoothingWindow = ms1_smoothing_window;
}

void
pappso::masschroq::AlignmentMethod::setMs2SmoothingWindow(std::uint8_t ms2_smoothing_window)
{
  m_ms2SmoothingWindow = ms2_smoothing_window;
}

void
pappso::masschroq::AlignmentMethod::setMs2TendencyWindow(std::uint8_t ms2_tendency_window)
{
  m_ms2TendencyWindow = ms2_tendency_window;
}


std::uint8_t
pappso::masschroq::AlignmentMethod::getMs1SmoothingWindow() const
{
  return m_ms1SmoothingWindow;
}

std::uint8_t
pappso::masschroq::AlignmentMethod::getMs2SmoothingWindow() const
{
  return m_ms2SmoothingWindow;
}

std::uint8_t
pappso::masschroq::AlignmentMethod::getMs2TendencyWindow() const
{
  return m_ms2TendencyWindow;
}

pappso::ProjectParameters
pappso::masschroq::AlignmentMethod::getProjectParameters() const
{
  pappso::ProjectParameters parameters;
  /*
          pappso::ProjectParam project_param(
            {pappso::ProjectParamCategory::quantification, "", QVariant()});

          project_param.name = "mcq_alignment_msrun_reference_for_rt_alignment";
          QString msrunid    = m_qxmlStreamReader.attributes()
                              .value("reference_data_id")
                              .toString();
          project_param.value.setValue(
            m_mcqrMetadataSet.getMcqrMetadaByDataId(msrunid).m_msrunFile);
          m_projectParameters.setProjectParam(project_param);
          */

  pappso::ProjectParam project_param(
    {pappso::ProjectParamCategory::quantification, "", QVariant()});
  project_param.name = "mcq_alignment_ms2_tendency_window";
  project_param.value.setValue((std::size_t)m_ms2TendencyWindow);
  parameters.setProjectParam(project_param);
  project_param.name = "mcq_alignment_ms1_smoothing_window";
  project_param.value.setValue((std::size_t)m_ms1SmoothingWindow);
  parameters.setProjectParam(project_param);
  project_param.name = "mcq_alignment_ms2_smoothing_window";
  project_param.value.setValue((std::size_t)m_ms2SmoothingWindow);
  parameters.setProjectParam(project_param);

  return parameters;
}

void
pappso::masschroq::AlignmentMethod::setJsonObject(const QJsonObject &align_method)
{
  setMs1SmoothingWindow(align_method.value("ms1_smoothing").toInt());
  setMs2SmoothingWindow(align_method.value("ms2_smoothing").toInt());
  setMs2TendencyWindow(align_method.value("ms2_tendency").toInt());
}

QJsonObject
pappso::masschroq::AlignmentMethod::getJsonObject() const
{
  QJsonObject method;

  method.insert("ms1_smoothing", m_ms1SmoothingWindow);

  method.insert("ms2_smoothing", m_ms2SmoothingWindow);

  method.insert("ms2_tendency", m_ms2TendencyWindow);


  return method;
}
