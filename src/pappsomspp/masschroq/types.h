/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file mcq_types.h
 * \date January 10, 2011
 * \author Edlira Nano
 * \brief This header contains all the type re-definitions and all
 * the global variables definitions used in MassChroQ.
 *
 * For configuration global variable definitions see config.h.cmake file.
 */

#include <QString>

#pragma once


namespace pappso::masschroq
{
/** \def PeakQualityCategory give an idea of the quality/confidence in a peak
 * measurement
 *
 */
enum class PeakQualityCategory : std::int8_t
{
  nomatch, ///< there is no category, because no matching is done (mz list for
           ///< example)
  aa,  ///< best possible : more than one direct MS2 fragmentation in same MSRUN
  a,   ///< peak detected using a single direct MS2 observation
  ab,  ///< peak detected using direct MS2 observation... but the peak is
       ///< fragmented. all MS2 retention times are not givins the same peak
  zaa, ///<
  za,  ///<
  zab, ///<
  b,   ///< less good : match between run, peak found at the aligned retention
       ///< time
  c,   ///< less good : match between run, peak found at the aligned retention
       ///< time, but multiple choice
  d,   ///< less good : match between run, peak found but in the margin of peak
       ///< (second chance)
  post_matching, ///< post matching category : new retention times are computed
                 ///< using real XIC, and aligned rt give a match
  missed,        ///< no peak found at all
  last
};



}
