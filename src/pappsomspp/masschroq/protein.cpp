/**
 * \file pappsomspp/masschroq/protein.cpp
 * \date 10/10/2024
 * \author Olivier Langella
 * \brief protein model in masschroqlite
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "protein.h"

pappso::masschroq::Protein::Protein(const QString &id, const QString &description)
  : m_id(id), m_description(description)
{
}

pappso::masschroq::Protein::Protein(const pappso::masschroq::Protein &other)
  : m_id(other.m_id), m_description(other.m_description)
{
}

pappso::masschroq::Protein::~Protein()
{
}

const QString &
pappso::masschroq::Protein::getId() const
{
  return m_id;
}
const QString &
pappso::masschroq::Protein::getDescription() const
{
  return m_description;
}
