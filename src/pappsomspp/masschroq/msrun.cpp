/**
 * \file pappsomspp/masschroq/core/msrun.cpp
 * \date 24/09/2024
 * \author Olivier Langella
 * \brief msrun model in masschroqlite
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include "msrun.h"
#include "../pappsoexception.h"
#include "../msrun/private/timsmsrunreaderms2.h"


pappso::masschroq::MsRun::MsRun(pappso::MsRunReaderSPtr msrun_reader) : msp_msRunReader(msrun_reader)
{

  pappso::TimsMsRunReaderMs2 *tims2_reader =
    dynamic_cast<pappso::TimsMsRunReaderMs2 *>(msp_msRunReader.get());
  if(tims2_reader != nullptr)
    {
      m_hasTimsTofMobilityIndex = true;
    }

  m_retentionTimeLine = msp_msRunReader.get()->getRetentionTimeLine();
  mpa_precursorParser = new PrecursorParser(msp_msRunReader);

  pappso::MsRunReadConfig config;
  config.setNeedPeakList(false);
  config.setMsLevels({2});
  qDebug();
  // msrunA01.get()->readSpectrumCollection(spectrum_list_reader);
  msp_msRunReader.get()->readSpectrumCollection2(config, *mpa_precursorParser);
}

pappso::masschroq::MsRun::~MsRun()
{
  delete mpa_precursorParser;
}

const pappso::MsRunReaderSPtr &
pappso::masschroq::MsRun::getMsRunReaderSPtr() const
{
  return msp_msRunReader;
}

pappso::MsRunReaderSPtr &
pappso::masschroq::MsRun::getMsRunReaderSPtr()
{
  return msp_msRunReader;
}


const pappso::masschroq::PrecursorSp &
pappso::masschroq::MsRun::getPrecursorSPtrByScanNumber(std::size_t scan_number) const
{
  return mpa_precursorParser->getPrecursorSPtrByScanNumber(scan_number);
}
const pappso::masschroq::PrecursorSp &
pappso::masschroq::MsRun::getPrecursorSPtrBySpectrumIndex(std::size_t spectrum_index) const
{
  return mpa_precursorParser->getPrecursorSPtrBySpectrumIndex(spectrum_index);
}

const std::vector<double> &
pappso::masschroq::MsRun::getRetentionTimeLine() const
{
  return m_retentionTimeLine;
}

bool
pappso::masschroq::MsRun::hasTimsTofMobilityIndex() const
{
  return m_hasTimsTofMobilityIndex;
}
