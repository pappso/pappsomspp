/**
 * \file pappsomspp/masschroq/peptidebase.cpp
 * \date 24/01/2025
 * \author Olivier Langella
 * \brief peptide model base in masschroqlite
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "peptidebase.h"

pappso::masschroq::PeptideBase::PeptideBase(const pappso::PeptideSp &peptide_sp)
  : msp_peptide(peptide_sp)
{
}

pappso::masschroq::PeptideBase::~PeptideBase()
{
}


const pappso::PeptideSp &
pappso::masschroq::PeptideBase::getPappsoPeptideSp() const
{
  return msp_peptide;
}

void
pappso::masschroq::PeptideBase::computeIsotopologues(double ni_min_abundance)
{
  qDebug();
  if(ni_min_abundance > 0)
    {
      qDebug() << "if (_minimum_isotope_abundance > 0)";
      mpa_peptideNaturalIsotopeList =
        new pappso::PeptideNaturalIsotopeList(msp_peptide, 0.001);
    }
  qDebug();
}


const pappso::PeptideNaturalIsotopeList *
pappso::masschroq::PeptideBase::getPeptideNaturalIsotopeList() const
{
  return mpa_peptideNaturalIsotopeList;
}

const std::vector<pappso::PeptideNaturalIsotopeAverageSp> &
pappso::masschroq::PeptideBase::getPeptideNaturalIsotopeAverageSpList(
  pappso::PrecisionPtr precision, std::uint8_t charge, double ni_min_abundance)
{

  QMutexLocker lock(&m_mutex);
  if(m_niMinAbundance == ni_min_abundance)
    {
    }
  else
    {
      m_peptideNaturalIsotopeAverageSpListByCharge.clear();
      m_niMinAbundance = ni_min_abundance;
    }

  auto it = m_peptideNaturalIsotopeAverageSpListByCharge.find(charge);
  if(it != m_peptideNaturalIsotopeAverageSpListByCharge.end())
    {
      if(it->second.size() == 0)
        {
          it->second = mpa_peptideNaturalIsotopeList->getByIntensityRatio(
            charge, precision, ni_min_abundance);
        }

      return it->second;
    }
  else
    {
      auto it_insert = m_peptideNaturalIsotopeAverageSpListByCharge.insert(
        {charge,
         mpa_peptideNaturalIsotopeList->getByIntensityRatio(
           charge, precision, ni_min_abundance)});

      return it_insert.first->second;
    }
}
