/**
 * \file pappsomspp/masschroq/precursor.h
 * \date 04/11/2024
 * \author Olivier Langella
 * \brief MS1 precursor information
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "precursor.h"
#include "../pappsoexception.h"
#include <QObject>

pappso::masschroq::Precursor::Precursor(std::size_t spectrum_index,
                           double mz,
                           double intensity,
                           pappso::XicCoordSPtr xic_coord)
{
  m_spectrumIndex = spectrum_index;
  m_intensity     = intensity;
  m_mz            = mz;

  msp_xicCoord = xic_coord;

  if(msp_xicCoord.get() == nullptr)
    {
      throw pappso::PappsoException(
        QObject::tr("xic_coord == nullptr\nindex=%1 %2 %3 %4")
          .arg(spectrum_index)
          .arg(__FILE__)
          .arg(__FUNCTION__)
          .arg(__LINE__));
    }
}

pappso::masschroq::Precursor::~Precursor()
{
}

double
pappso::masschroq::Precursor::getIntensity() const
{
  return m_intensity;
}

std::size_t
pappso::masschroq::Precursor::getSpectrumIndex() const
{
  return m_spectrumIndex;
}

const pappso::XicCoordSPtr &
pappso::masschroq::Precursor::getXicCoordSPtr() const
{
  return msp_xicCoord;
}
