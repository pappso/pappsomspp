/**
 * \file pappsomspp/masschroq/mbrpeptidemeasurements.h
 * \date 27/10/2024
 * \author Olivier Langella
 * \brief peptide extracted measures in MBR mode model in masschroqlite
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <cstdint>
#include "peptide.h"
#include "peptidemeasurementsbase.h"
#include "../msrun/alignment/msrunretentiontime.h"


namespace pappso::masschroq
{


class MbrPeptideMeasurements;

typedef std::shared_ptr<MbrPeptideMeasurements> MbrPeptideMeasurementsSp;
/**
 * @todo write docs
 */
class MbrPeptideMeasurements : public PeptideMeasurementsBase
{
  public:
  /**
   * Default constructor
   */
  MbrPeptideMeasurements(const PeptideSp &peptide_sp);

  MbrPeptideMeasurements(const MbrPeptideMeasurements &other);

  /**
   * Destructor
   */
  virtual ~MbrPeptideMeasurements();


  void prepareMeasurements(const pappso::MsRunId &targeted_msrun,
                           const MsRunGroup *msrun_group_p,
                           const pappso::MsRunRetentionTime<QString> &msrun_retention_time,
                           const QuantificationMethodSp &quantification_method);


  virtual void detectQuantifyPeaks(const QuantificationMethodSp &quantification_method) override;

  const PeptideSp &getPeptideSp() const;


  private:
  PeptideSp msp_peptide;

  double m_consensusMs2RetentionTime = 0;
};
} // namespace pappso::masschroq
