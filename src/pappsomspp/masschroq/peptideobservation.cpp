/**
 * \file pappsomspp/masschroq/peptideobservation.cpp
 * \date 24/09/2024
 * \author Olivier Langella
 * \brief peptide model in masschroqlite
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include "peptideobservation.h"
#include "../pappsoexception.h"


pappso::masschroq::PeptideObservation::PeptideObservation(pappso::masschroq::PeptideSp peptide)
  : msp_peptide(peptide)
{
}


pappso::masschroq::PeptideObservation::PeptideObservation(const PeptideObservation &other)
  : msp_peptide(other.msp_peptide)
{
  m_observationList = other.m_observationList;
}

pappso::masschroq::PeptideObservation::~PeptideObservation()
{
}

void
pappso::masschroq::PeptideObservation::addObservation(
  const pappso::masschroq::PeptideObservation::Observation &observation)
{
  m_observationList.push_back(observation);
}
std::vector<std::uint8_t>
pappso::masschroq::PeptideObservation::getObservedChargeStates() const
{
  std::vector<std::uint8_t> charge_list;
  for(auto &observation : m_observationList)
    {
      charge_list.push_back(observation.m_charge);
    }
  std::sort(charge_list.begin(), charge_list.end());

  charge_list.erase(std::unique(charge_list.begin(), charge_list.end()),
                    charge_list.end());
  return charge_list;
}

const pappso::masschroq::PeptideSp &
pappso::masschroq::PeptideObservation::getPeptideSp() const
{
  return msp_peptide;
}

double
pappso::masschroq::PeptideObservation::getBestRtOverallChargeStates() const
{
  double best_rt   = 0;
  double intensity = -1;
  for(auto &observation : m_observationList)
    {
      if(observation.msp_precursor.get()->getIntensity() > intensity)
        {
          best_rt =
            observation.msp_precursor.get()->getXicCoordSPtr().get()->rtTarget;
          intensity = observation.msp_precursor.get()->getIntensity();
        }
    }

  return best_rt;
}

pappso::XicCoordSPtr
pappso::masschroq::PeptideObservation::getBestXicCoordSPtrForCharge(
  std::uint8_t charge) const
{

  // get XIC coordinates of the most intense precursor
  pappso::pappso_double intensity = -1;
  pappso::XicCoordSPtr best_xic_coord;

  for(auto &observation : m_observationList)
    {
      if(observation.m_charge == charge)
        {
          if(observation.msp_precursor.get()->getIntensity() > intensity)
            {
              intensity = observation.msp_precursor.get()->getIntensity();
              best_xic_coord =
                observation.msp_precursor.get()->getXicCoordSPtr();
              qDebug();
            }
        }
    }
  return best_xic_coord;
}

pappso::XicCoordSPtr
pappso::masschroq::PeptideObservation::getBestXicCoord() const
{

  // get XIC coordinates of the most intense precursor
  pappso::pappso_double intensity = -1;
  pappso::XicCoordSPtr best_xic_coord;

  for(auto &observation : m_observationList)
    {
      if(observation.msp_precursor.get()->getIntensity() > intensity)
        {
          intensity      = observation.msp_precursor.get()->getIntensity();
          best_xic_coord = observation.msp_precursor.get()->getXicCoordSPtr();
          qDebug();
        }
    }
  return best_xic_coord;
}


std::vector<double>
pappso::masschroq::PeptideObservation::getObservedRetentionTimesOverallChargeStates() const
{
  std::vector<double> rt_list;
  for(auto &observation : m_observationList)
    {
      rt_list.push_back(
        observation.msp_precursor.get()->getXicCoordSPtr().get()->rtTarget);
    }

  return rt_list;
}

const std::vector<pappso::masschroq::PeptideObservation::Observation> &
pappso::masschroq::PeptideObservation::getObservationList() const
{
  return m_observationList;
}
