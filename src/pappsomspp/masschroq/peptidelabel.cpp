/**
 * \file pappsomspp/masschroq/peptidelabel.h
 * \date 09/01/2025
 * \author Olivier Langella
 * \brief peptide label model in masschroqlite
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include "peptidelabel.h"
#include "../peptide/peptideproformaparser.h"

pappso::masschroq::PeptideLabel::PeptideLabel(const pappso::PeptideSp &labelled_peptide_p,
                                 const pappso::masschroq::Peptide *parent_peptide_p,
                                 const QString &label)
  : PeptideBase(labelled_peptide_p), mp_mcqlPeptide(parent_peptide_p)
{
  m_label = label;
}

pappso::masschroq::PeptideLabel::~PeptideLabel()
{
  if(mpa_peptideNaturalIsotopeList != nullptr)
    delete mpa_peptideNaturalIsotopeList;
}

const QString &
pappso::masschroq::PeptideLabel::getLabel() const
{
  return m_label;
}

