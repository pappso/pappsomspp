/**
 * \file pappsomspp/psm/deepprot/deepprotenum.cpp
 * \date 22/1/2021
 * \author Olivier Langella <olivier.langella@universite-paris-saclay.fr>
 * \brief base type definition to use in DeepProt
 *
 * DeepProt is the C++ implementation of the SpecOMS algorithm
 *
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<olivier.langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include <QObject>

#include "deepprotenum.h"
#include "../../pappsoexception.h"

pappso::DeepProtMatchType
pappso::DeepProtEnumStr::DeepProtMatchTypeFromString(const QString &name)
{
  DeepProtMatchType match_type = DeepProtMatchType::uncategorized;
  if(name == "delta_position")
    {
      match_type = DeepProtMatchType::DeltaPosition;
    }
  else if(name == "no_delta_position")
    {
      match_type = DeepProtMatchType::NoDeltaPosition;
    }
  else if(name == "uncategorized")
    {
      match_type = DeepProtMatchType::uncategorized;
    }
  else if(name == "zero_mass_delta")
    {
      match_type = DeepProtMatchType::ZeroMassDelta;
    }
  else if(name == "zero_mass_delta_mc")
    {
      match_type = DeepProtMatchType::ZeroMassDeltaMissedCleavage;
    }
  else if(name == "zero_mass_delta_st")
    {
      match_type = DeepProtMatchType::ZeroMassDeltaSemiTryptic;
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("DeepProtMatchType unknown :\n%1").arg(name));
    }
  return match_type;
}


pappso::DeepProtPeptideCandidateStatus
pappso::DeepProtEnumStr::DeepProtPeptideCandidateStatusFromString(
  const QString &name)
{

  DeepProtPeptideCandidateStatus status =
    DeepProtPeptideCandidateStatus::unmodified;
  if(name == "cter_removal")
    {
      status = DeepProtPeptideCandidateStatus::CterRemoval;
    }
  else if(name == "delta_position")
    {
      status = DeepProtPeptideCandidateStatus::DeltaPosition;
    }
  else if(name == "missed_cleavage")
    {
      status = DeepProtPeptideCandidateStatus::MissedCleavage;
    }
  else if(name == "no_delta_position")
    {
      status = DeepProtPeptideCandidateStatus::NoDeltaPosition;
    }
  else if(name == "nter_removal")
    {
      status = DeepProtPeptideCandidateStatus::NterRemoval;
    }
  else if(name == "zero_mass_delta")
    {
      status = DeepProtPeptideCandidateStatus::ZeroMassDelta;
    }

  else
    {
      throw pappso::PappsoException(
        QObject::tr("DeepProtPeptideCandidateStatus unknown :\n%1").arg(name));
    }
  return status;
}

const QString
pappso::DeepProtEnumStr::toString(pappso::DeepProtMatchType match_type)
{

  QString match_type_str;
  switch(match_type)
    {
      case DeepProtMatchType::DeltaPosition:
        match_type_str = "delta_position";
        break;
      case DeepProtMatchType::NoDeltaPosition:
        match_type_str = "no_delta_position";
        break;
      case DeepProtMatchType::uncategorized:
        match_type_str = "uncategorized";
        break;
      case DeepProtMatchType::ZeroMassDelta:
        match_type_str = "zero_mass_delta";
        break;
      case DeepProtMatchType::ZeroMassDeltaMissedCleavage:
        match_type_str = "zero_mass_delta_mc";
        break;
      case DeepProtMatchType::ZeroMassDeltaSemiTryptic:
        match_type_str = "zero_mass_delta_st";
        break;

      default:
        throw pappso::PappsoException(
          QObject::tr("DeepProtMatchType unknown :\n%1")
            .arg((std::uint8_t)match_type));
    }
  return match_type_str;
}

const QString
pappso::DeepProtEnumStr::toString(pappso::DeepProtPeptideCandidateStatus status)
{

  QString status_str;
  switch(status)
    {
      case DeepProtPeptideCandidateStatus::CterRemoval:
        status_str = "cter_removal";
        break;
      case DeepProtPeptideCandidateStatus::DeltaPosition:
        status_str = "delta_position";
        break;
      case DeepProtPeptideCandidateStatus::MissedCleavage:
        status_str = "missed_cleavage";
        break;
      case DeepProtPeptideCandidateStatus::NoDeltaPosition:
        status_str = "no_delta_position";
        break;
      case DeepProtPeptideCandidateStatus::NterRemoval:
        status_str = "nter_removal";
        break;
      case DeepProtPeptideCandidateStatus::ZeroMassDelta:
        status_str = "zero_mass_delta";
        break;

      default:
        throw pappso::PappsoException(
          QObject::tr("DeepProtPeptideCandidateStatus unknown :\n%1")
            .arg((std::uint8_t)status));
    }

  return status_str;
}
