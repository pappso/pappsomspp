/**
 * \file pappsomspp/psm/morpheusscore.cpp
 * \date 16/7/2016
 * \author Olivier Langella
 * \brief computation of Morpheus score
 * https://github.com/cwenger/Morpheus/blob/master/Morpheus/Morpheus/PeptideSpectrumMatch.cs
 */

/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "morpheusscore.h"
#include "../../trace/datapoint.h"
#include "../../massspectrum/massspectrum.h"


namespace pappso
{
MorpheusScore::MorpheusScore(const MassSpectrum &spectrum,
                             pappso::PeptideSp peptide_sp,
                             unsigned int parent_charge,
                             PrecisionPtr precision,
                             std::vector<PeptideIon> ion_list,
                             RawFragmentationMode fragmentation_mode)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  PeptideRawFragmentMasses calc_mass_list_proline(*peptide_sp.get(),
                                                  fragmentation_mode);

  std::vector<pappso_double> ion_products;
  unsigned int charge = parent_charge;
  while(charge > 0)
    {
      for(PeptideIon &ion : ion_list)
        {
          calc_mass_list_proline.pushBackIonMz(ion_products, ion, charge);
        }
      charge--;
    }


  // compute the number of matched peaks
  unsigned int number_of_matched_peaks = 0;

  std::sort(ion_products.begin(), ion_products.end());


  // compute ratio of matched peaks on total peaks
  std::vector<pappso_double>::const_iterator it_theoretical =
    ion_products.begin();
  std::vector<pappso_double>::const_iterator it_theoretical_end =
    ion_products.end();
  std::vector<DataPoint>::const_iterator it_spectrum     = spectrum.begin();
  std::vector<DataPoint>::const_iterator it_spectrum_end = spectrum.end();
  pappso::pappso_double sum_intensities                  = 0;
  pappso::pappso_double sum_matched_intensities          = 0;
  // unsigned int peak_number                               = spectrum.size();
  while((it_spectrum != it_spectrum_end) &&
        (it_theoretical != it_theoretical_end))
    {
      sum_intensities += it_spectrum->y;
      MzRange peak_range(it_spectrum->x, precision);

      while((it_theoretical != it_theoretical_end) &&
            (*it_theoretical < peak_range.lower()))
        {
          it_theoretical++;
        }
      while((it_theoretical != it_theoretical_end) &&
            peak_range.contains(*it_theoretical))
        {
          sum_matched_intensities += it_spectrum->y;
          number_of_matched_peaks++;
          it_theoretical++;
        }
      it_spectrum++;
    }
  while(it_spectrum != it_spectrum_end)
    {
      sum_intensities += it_spectrum->y;
      it_spectrum++;
    }


  // compute the sum of matching peak intensities

  // morpheus score = number of matched peaks + matching intensities ratio

  _morpheus_score = (pappso_double)number_of_matched_peaks +
                    (sum_matched_intensities / sum_intensities);

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

MorpheusScore::~MorpheusScore()
{
}

pappso::pappso_double
MorpheusScore::getMorpheusScore() const
{
  return _morpheus_score;
}
} // namespace pappso
