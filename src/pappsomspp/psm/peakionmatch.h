/**
 * \file pappsomspp/psm/peakionmatch.h
 * \date 4/4/2015
 * \author Olivier Langella
 * \brief associate a peak and a peptide + charge
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../exportinmportconfig.h"
#include "../massspectrum/massspectrum.h"
#include "../peptide/peptidefragmention.h"

namespace pappso
{
class PMSPP_LIB_DECL PeakIonMatch
{
  public:
  PeakIonMatch(const DataPoint &peak,
               const PeptideFragmentIonSp &ion_sp,
               unsigned int charge);
  PeakIonMatch(const PeakIonMatch &other);
  PeakIonMatch(PeakIonMatch &&other);
  virtual ~PeakIonMatch();

  PeakIonMatch &operator=(const PeakIonMatch &other);

  virtual const PeptideFragmentIonSp &getPeptideFragmentIonSp() const;

  const DataPoint &getPeak() const;

  unsigned int getCharge() const;

  PeptideIon getPeptideIonType() const;
  PeptideDirection getPeptideIonDirection() const;

  virtual QString toString() const;


  private:
  DataPoint _peak;
  PeptideFragmentIonSp _ion_sp;
  unsigned int _charge;
};
} // namespace pappso
