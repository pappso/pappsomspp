
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once
#include <QString>
#include "../exportinmportconfig.h"

namespace pappso
{
class FastaHandlerInterface
{
  public:
  virtual void setSequence(const QString &description,
                           const QString &sequence) = 0;
};


class FastaSeqHandler : public pappso::FastaHandlerInterface
{
  public:
  const QString &
  getDescription() const
  {
    return description;
  };
  const QString &
  getSequence() const
  {
    return sequence;
  };
  void
  setSequence(const QString &description_in,
              const QString &sequence_in) override
  {
    description = description_in;
    sequence    = sequence_in;
  };

  private:
  QString description;
  QString sequence;
};
} // namespace pappso
