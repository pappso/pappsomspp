
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include <QDebug>
#include <QTextStream>
#include "fastareader.h"
#include "../pappsoexception.h"

namespace pappso
{
FastaReader::FastaReader(FastaHandlerInterface &handler) : m_handler(handler)
{
}

FastaReader::~FastaReader()
{
}


void
FastaReader::parse(QFile &fastaFile)
{
  if(fastaFile.fileName().isEmpty())
    {
      throw PappsoException(QObject::tr("No FASTA file name specified"));
    }
  if(fastaFile.open(QIODevice::ReadOnly))
    {
      parse(&fastaFile);
      fastaFile.close();
    }
  else
    {
      throw PappsoException(QObject::tr("ERROR opening FASTA file %1 for read")
                              .arg(fastaFile.fileName()));
    }
}

void
FastaReader::parse(QIODevice *p_inputstream)
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  QTextStream p_in(p_inputstream);


  QString accession = "";
  QString sequence  = "";
  // Search accession conta
  // QTextStream in(p_in);
  QString line = p_in.readLine();
  while(!p_in.atEnd())
    {
      if(line.startsWith(">"))
        {
          if(!accession.isEmpty())
            {
              m_handler.setSequence(accession, sequence);
            }
          sequence  = "";
          accession = line.remove(0, 1);
        }
      else
        {
          sequence.append(line);
          // m_handler.setSequence(line);
        }
      line = p_in.readLine();
    }
  if(!accession.isEmpty())
    {
      sequence.append(line);
      m_handler.setSequence(accession, sequence);
    }
  // p_in->close();
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}


void
FastaReader::parseOnlyOne(QTextStream &p_in)
{


  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;


  QString accession = "";
  QString sequence  = "";
  // Search accession conta
  // QTextStream in(p_in);
  QString line = p_in.readLine();
  while(!p_in.atEnd())
    {
      if(line.startsWith(">"))
        {
          if(!accession.isEmpty())
            {
              m_handler.setSequence(accession, sequence);
              return;
            }
          sequence  = "";
          accession = line.remove(0, 1);
        }
      else
        {
          sequence.append(line);
          // m_handler.setSequence(line);
        }
      line = p_in.readLine();
    }
  if(!accession.isEmpty())
    {
      sequence.append(line);
      m_handler.setSequence(accession, sequence);
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

} // namespace pappso
