/**
 * \file pappsomspp/obo/obopsims.h
 * \date 2/10/2024
 * \author Olivier Langella
 * \brief OBO PSI:MS file parser
 **/

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../types.h"
#include "../exportinmportconfig.h"
#include "obopsimodterm.h"
#include "obopsimodhandlerinterface.h"

namespace pappso
{
class PMSPP_LIB_DECL OboPsiMs
{
  public:
  OboPsiMs(OboPsiModHandlerInterface &handler);
  virtual ~OboPsiMs();

  private:
  OboPsiModTerm m_term;
  OboPsiModHandlerInterface &m_handler;

  /** @brief starts reading obo file and reports each term with the callback
   * function
   */
  void parse();
};
} // namespace pappso
