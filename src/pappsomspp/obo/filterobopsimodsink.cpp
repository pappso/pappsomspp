/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "filterobopsimodsink.h"
#include "../exception/exceptionnotfound.h"

namespace pappso
{
FilterOboPsiModSink::FilterOboPsiModSink()
{
}

FilterOboPsiModSink::~FilterOboPsiModSink()
{
}

const std::list<OboPsiModTerm> &
FilterOboPsiModSink::getOboPsiModTermList()
{
  return m_oboPsiModTermList;
}

void
FilterOboPsiModSink::setOboPsiModTerm(const OboPsiModTerm &term)
{
  m_oboPsiModTermList.push_back(term);
}

const OboPsiModTerm &
FilterOboPsiModSink::getOne()
{
  if(m_oboPsiModTermList.size() == 1)
    {
      return *m_oboPsiModTermList.begin();
    }
  if(m_oboPsiModTermList.size() == 0)
    {
      throw ExceptionNotFound(QObject::tr("OBO term list is empty"));
    }
  else
    {
      throw ExceptionNotFound(
        QObject::tr("Too much OBO terms in list (size = %1)").arg(m_oboPsiModTermList.size()));
    }
}

const OboPsiModTerm &
FilterOboPsiModSink::getFirst()
{
  if(m_oboPsiModTermList.size() == 0)
    {
      throw ExceptionNotFound(QObject::tr("OBO term list is empty"));
    }
  else
    {
      return *m_oboPsiModTermList.begin();
    }
}
} // namespace pappso


std::size_t
pappso::FilterOboPsiModSink::size()
{
  return m_oboPsiModTermList.size();
}
