/**
 * \file pappsomspp/obo/obopsimodmap.cpp
 * \date 2/10/2024
 * \author Olivier Langella
 * \brief map container for obo psi mod terms
 **/

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "filterobopsimodmap.h"
#include "../exception/exceptionnotfound.h"


pappso::FilterOboPsiModMap::FilterOboPsiModMap()
{
}


pappso::FilterOboPsiModMap::~FilterOboPsiModMap()
{
}

void
pappso::FilterOboPsiModMap::setOboPsiModTerm(const pappso::OboPsiModTerm &term)
{
  m_mapOboPsiModTerm.insert({term.m_accession, term});
}

const pappso::OboPsiModTerm &
pappso::FilterOboPsiModMap::getOboPsiModTermWithAccession(const QString &accession) const
{
  auto it = m_mapOboPsiModTerm.find(accession);
  if(it == m_mapOboPsiModTerm.end())
    {
      throw pappso::ExceptionNotFound(
        QObject::tr("OBO term accession %1 not found").arg(accession));
    }
  return it->second;
}
