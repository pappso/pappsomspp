/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once


#include <list>

#include "obopsimod.h"

namespace pappso
{

class PMSPP_LIB_DECL FilterOboPsiModSink : public OboPsiModHandlerInterface
{

  public:
  FilterOboPsiModSink();
  virtual ~FilterOboPsiModSink();

  void setOboPsiModTerm(const OboPsiModTerm &term) override;

  const OboPsiModTerm &getOne();
  const OboPsiModTerm &getFirst();
  std::size_t size();

  const std::list<OboPsiModTerm> &getOboPsiModTermList();


  private:
  std::list<OboPsiModTerm> m_oboPsiModTermList;
};


} // namespace pappso
