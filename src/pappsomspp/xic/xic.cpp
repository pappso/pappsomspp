/*
 * *******************************************************************************
 * * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * *
 * * This file is part of MassChroqPRM.
 * *
 * *     MassChroqPRM is free software: you can redistribute it and/or modify
 * *     it under the terms of the GNU General Public License as published by
 * *     the Free Software Foundation, either version 3 of the License, or
 * *     (at your option) any later version.
 * *
 * *     MassChroqPRM is distributed in the hope that it will be useful,
 * *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * *     GNU General Public License for more details.
 * *
 * *     You should have received a copy of the GNU General Public License
 * *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 * *
 * * Contributors:
 * *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 * implementation
 * ******************************************************************************/

#include <QDebug>
#include <QObject>

#include "xic.h"
#include <algorithm>
#include <cmath>
#include "../exception/exceptionoutofrange.h"

namespace pappso
{


Xic::Xic()
{
  qDebug() << "Xic::Xic begin";
  qDebug() << "Xic::Xic end";
}

Xic::Xic(const Trace &other) : Trace(other)
{
}

Xic::~Xic()
{
}


XicCstSPtr
Xic::makeXicCstSPtr() const
{
  return std::make_shared<const Xic>(*this);
}

XicSPtr
Xic::makeXicSPtr() const
{
  return std::make_shared<Xic>(*this);
}

void
Xic::debugPrintValues() const
{
  for(auto &&peak : *this)
    {
      qDebug() << "rt = " << peak.x << ", int = " << peak.y;
    }
}

const DataPoint &
Xic::atRetentionTime(pappso_double rt) const
{
  for(auto &&peak : *this)
    {
      if(peak.y == rt)
        return peak;
    }
  throw ExceptionOutOfRange(
    QObject::tr("no intensity for this retention time"));
}

unsigned int
Xic::getMsPointDistance(pappso_double rt_first, pappso_double rt_second) const
{
  if(rt_first > rt_second)
    {
      std::swap(rt_first, rt_second);
    }
  unsigned int distance = 0;
  auto it               = this->begin();
  auto itend            = this->end();

  while((it->x < rt_first) && (it != itend))
    {
      it++;
    }
  while((rt_second > it->x) && (it != itend))
    {
      qDebug() << "Xic::getMsPointDistance " << rt_first << " it->rt " << it->x
               << " rt_second " << rt_second << distance;
      distance++;
      it++;
    }


  return distance;
}

void
Xic::sortByRetentionTime()
{
  sortX();
}

} // namespace pappso
