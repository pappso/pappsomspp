/*
 * *******************************************************************************
 * * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * *
 * * This file is part of MassChroqPRM.
 * *
 * *     MassChroqPRM is free software: you can redistribute it and/or modify
 * *     it under the terms of the GNU General Public License as published by
 * *     the Free Software Foundation, either version 3 of the License, or
 * *     (at your option) any later version.
 * *
 * *     MassChroqPRM is distributed in the hope that it will be useful,
 * *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * *     GNU General Public License for more details.
 * *
 * *     You should have received a copy of the GNU General Public License
 * *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 * *
 * * Contributors:
 * *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 * implementation
 * ******************************************************************************/

#pragma once


#include <vector>
#include <memory>
#include "xic.h"
#include "../msrun/msrunid.h"
#include "../mzrange.h"
#include "../peptide/peptide.h"
#include "../psm/peakionmatch.h"


namespace pappso
{


class QualifiedXic;
typedef std::shared_ptr<Xic> QualifiedXicSp;

class QualifiedXic
{
  private:
  const MsRunId m_msRunId;
  pappso_double m_mz;
  PrecisionPtr mp_precision;

  XicSPtr msp_xic;

  public:
  QualifiedXic(const MsRunId &msrun_id,
               pappso_double mz,
               PrecisionPtr precision);
  virtual ~QualifiedXic();
  QualifiedXic(const QualifiedXic &toCopy);


  pappso_double
  getMz() const
  {
    return m_mz;
  };
  const XicCstSPtr
  getXicCstSPtr() const
  {
    return msp_xic;
  }

  const XicSPtr &
  getXicSPtr() const
  {
    return msp_xic;
  }
};
} // namespace pappso
