
#pragma once

#include <QString>

#include "../msrun/msrunid.h"


namespace pappso
{


class MsFileReader
{
  protected:
  QString m_fileName;
  MsDataFormat m_fileFormat = MsDataFormat::unknown;

  public:
  MsFileReader(const QString &file_name);
  virtual ~MsFileReader();

  virtual MsDataFormat getFileFormat() = 0;

  virtual std::vector<MsRunIdCstSPtr>
  getMsRunIds(const QString &run_prefix) = 0;
};

} // namespace pappso
