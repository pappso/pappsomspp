
#pragma once

#include <QString>

#include <pwiz/data/msdata/MSData.hpp>

#include "msfilereader.h"
#include "../msrun/msrunid.h"
#include "../msrun/msrunreader.h"


namespace pappso
{


class XyMsFileReader : MsFileReader
{
  private:
  virtual bool initialize(std::size_t &line_count);

  public:
  XyMsFileReader(const QString &file_name);
  virtual ~XyMsFileReader();

  virtual MsDataFormat getFileFormat() override;

  virtual std::vector<MsRunIdCstSPtr>
  getMsRunIds(const QString &run_prefix) override;

  MsRunReader *selectMsRunReader(const QString &file_name) const;
};

} // namespace pappso
