/**
 * \file pappsomspp/msfile/mzformatenumstr.cpp
 * \date 12/2/2021
 * \author Olivier Langella <olivier.langella@universite-paris-saclay.fr>
 * \brief convert mzformat enumerations to strings
 *
 */

/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<olivier.langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "mzformatenumstr.h"
#include "../pappsoexception.h"
#include <QObject>

using namespace pappso;

const QString
pappso::MsDataFormatEnumStr::toString(pappso::MsDataFormat ms_data_format_enum)
{

  QString ms_data_format_str;
  switch(ms_data_format_enum)
    {
      case MsDataFormat::abSciexT2D:
        ms_data_format_str = "abSciexT2D";
        break;
      case MsDataFormat::abSciexWiff:
        ms_data_format_str = "abSciexWiff";
        break;
      case MsDataFormat::agilentMassHunter:
        ms_data_format_str = "agilentMassHunter";
        break;
      case MsDataFormat::brukerBaf:
        ms_data_format_str = "brukerBaf";
        break;
      case MsDataFormat::brukerFid:
        ms_data_format_str = "brukerFid";
        break;
      case MsDataFormat::brukerTims:
        ms_data_format_str = "brukerTims";
        break;
      case MsDataFormat::brukerYep:
        ms_data_format_str = "brukerYep";
        break;
      case MsDataFormat::MGF:
        ms_data_format_str = "MGF";
        break;
      case MsDataFormat::msn:
        ms_data_format_str = "msn";
        break;
      case MsDataFormat::mz5:
        ms_data_format_str = "mz5";
        break;
      case MsDataFormat::mzML:
        ms_data_format_str = "mzML";
        break;
      case MsDataFormat::mzXML:
        ms_data_format_str = "mzXML";
        break;
      case MsDataFormat::SQLite3:
        ms_data_format_str = "SQLite3";
        break;
      case MsDataFormat::thermoRaw:
        ms_data_format_str = "thermoRaw";
        break;
      case MsDataFormat::watersRaw:
        ms_data_format_str = "watersRaw";
        break;
      case MsDataFormat::xy:
        ms_data_format_str = "xy";
        break;
      default:
        throw pappso::PappsoException(
          QObject::tr("MsDataFormat unknown :\n%1")
            .arg((std::uint8_t)ms_data_format_enum));
    }
  return ms_data_format_str;
}
