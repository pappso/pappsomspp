
#pragma once

#include <QString>

#include "msfilereader.h"
#include "../msrun/msrunid.h"
#include "../msrun/msrunreader.h"


namespace pappso
{


class BafAsciiFileReader : MsFileReader
{
  private:
  virtual bool initialize(std::size_t &line_count);

  public:
  BafAsciiFileReader(const QString &file_name);
  virtual ~BafAsciiFileReader();

  virtual MsDataFormat getFileFormat() override;

  virtual std::vector<MsRunIdCstSPtr>
  getMsRunIds(const QString &run_prefix) override;

  MsRunReader *selectMsRunReader(const QString &file_name) const;
};

} // namespace pappso
