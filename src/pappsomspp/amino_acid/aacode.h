/**
 * \file pappsomspp/amino_acid/aacode.h
 * \date 03/05/2023
 * \author Olivier Langella
 * \brief give an integer code to each amino acid
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../exportinmportconfig.h"
#include <vector>
#include "aa.h"

namespace pappso
{
/**
 * @brief collection of integer code for each amino acid
 * 0 => null
 * 1 to 20 => amino acid sorted by there mass (lower to higher). Leucine is
 * replaced by Isoleucine
 */
class PMSPP_LIB_DECL AaCode
{
  public:
  /**
   * Default constructor
   */
  AaCode();

  /**
   * Default copy constructor
   */
  AaCode(const AaCode &other);

  /**
   * Destructor
   */
  ~AaCode();

  uint8_t getAaCode(char aa_letter) const;

  const Aa &getAa(char aa_letter) const;
  const Aa &getAa(uint8_t aa_code) const;


  double getMass(uint8_t aa_code) const;

  void addAaModification(char aa_letter, AaModificationP aaModification);

  std::size_t getSize() const;

  private:
  /** @brief give a number (the code) to each amino acid sorted by mass
   */
  void updateNumbers();
  /** @brief update mass cache
   */
  void updateMass();

  private:
  std::vector<uint8_t> m_asciiTable;

  std::vector<Aa> m_aaCollection;
  std::vector<double> m_massCollection;
};
} // namespace pappso
