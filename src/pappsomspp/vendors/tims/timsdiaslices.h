/**
 * \file pappsomspp/vendors/tims/timsdiaslices.h
 * \date 02/07/2024
 * \brief handle specific data for DIA MS runs
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once
#include "timsdata.h"
#include "../../exportinmportconfig.h"

namespace pappso
{

/**
 * @todo write docs
 */
class PMSPP_LIB_DECL TimsDiaSlices
{
  public:
  /**
   * Default constructor
   */
  TimsDiaSlices(QSqlQuery &query, TimsData *tims_data_origin);

  /**
   * Destructor
   */
  ~TimsDiaSlices();

  struct MsMsWindow
  {
    /** @brief tell if given mz is in range for this window
     */
    bool isMzInRange(double mz) const;

    std::size_t SliceIndex = 0;
    std::size_t WindowGroup;
    std::size_t ScanNumBegin;
    std::size_t ScanNumEnd;
    double IsolationMz;
    double IsolationWidth;
    double CollisionEnergy;
  };

  struct MsMsWindowGroup : std::vector<MsMsWindow>
  {
    std::size_t WindowGroup;
  };

  struct MsMsWindowGroupList : std::vector<MsMsWindowGroup *>
  {
    ~MsMsWindowGroupList();

    void addInGroup(const MsMsWindow &window);
    MsMsWindowGroup *
    getWindowGroupPtrByGroupId(std::size_t window_group_id) const;
  };

  const MsMsWindowGroupList &getMsMsWindowGroupList() const;

  const std::map<std::size_t, MsMsWindowGroup *> &
  getMapFrame2WindowGroupPtr() const;

  std::size_t getGlobalSliceIndexBeginByFrameId(std::size_t frame_id) const;

  std::size_t getFrameIdByGlobalSliceIndex(std::size_t global_slice_id) const;


  /** @brief get the number of DIA MS2 slices analyzed by PASEF
   */
  std::size_t getTotalSlicesCount() const;


  const MsMsWindow &
  getMsMsWindowByGlobalSliceIndex(std::size_t global_slice_index) const;


  TimsDataFastMap &
  getCombinedMs2ScansByGlobalSliceIndex(std::size_t global_slice_index) const;


  void getMs2QualifiedSpectrumByGlobalSliceIndex(
    const MsRunIdCstSPtr &msrun_id,
    QualifiedMassSpectrum &mass_spectrum,
    std::size_t global_slice_index,
    bool want_binary_data) const;


  /** @brief get MS1 spectrum corresponding to the given slice
   * extract MS1 mz range and mobility range corresponding to a global slice
   * @param msrun_id
   * @param mass_spectrum
   * @param global_slice_index
   * @param want_binary_data
   * @param rt_position relative MS1 frame retention time : 0 latest MS1 frame,
   * +1 next MS1 frame, -1 previous MS1 frame
   */
  void getMs1QualifiedSpectrumByGlobalSliceIndex(
    const MsRunIdCstSPtr &msrun_id,
    QualifiedMassSpectrum &mass_spectrum,
    std::size_t global_slice_index,
    bool want_binary_data,
    int rt_position = 0) const;

  std::size_t getLastMs1FrameIdByMs2FrameId(std::size_t frame_id) const;

  private:
  struct FrameSliceRange
  {
    std::size_t frame_id;
    std::size_t begin_global_slice_index;
    std::size_t end_global_slice_index;
  };

  const FrameSliceRange &
  getFrameSliceRangeByGlobalSliceIndex(std::size_t global_slice_index) const;

  private:
  TimsData *mp_timsDataOrigin;
  MsMsWindowGroupList m_msMsWindowGroupList;
  std::map<std::size_t, MsMsWindowGroup *> m_mapFrame2WindowGroupPtr;
  std::vector<std::size_t> m_frameId2GlobalSliceIndexBegin;
  std::vector<std::size_t> m_ms2frameId2Ms1FrameId;
  std::vector<std::size_t> m_ms1frameIdList;
  std::vector<FrameSliceRange> m_frameSliceRangeList;

  /** @brief enable builtin centroid on raw tims integers by default
   */
  bool m_builtinMs2Centroid = true;

  std::size_t m_totalSlicesCount;
};
} // namespace pappso
