/**
 * \file pappsomspp/vendors/tims/timsframerawdatachunck.h
 * \date 18/6/2022
 * \author Olivier Langella
 * \brief stores raw binary tims frame
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QFile>
#include "timsframerecord.h"
#include "../../exportinmportconfig.h"

namespace pappso
{
/**
 * @todo write docs
 */
class PMSPP_LIB_DECL TimsFrameRawDataChunck
{
  public:
  /**
   * Default constructor
   */
  TimsFrameRawDataChunck();

  /**
   * Destructor
   */
  virtual ~TimsFrameRawDataChunck();


  bool
  readTimsFrame(QFile *p_file,
                std::size_t frameId,
                const std::vector<pappso::TimsFrameRecord> &frame_record_list);


  quint32 getFrameNumberOfScans() const;
  quint32 getCompressedSize() const;
  quint32 getFrameLength() const;
  char *getMemoryBuffer() const;
  std::size_t getFrameId() const;

  private:
  char *mpa_memoryBuffer    = nullptr;
  qint64 m_memoryBufferSize = 0;

  std::size_t m_frameId        = 0;
  quint32 m_frameLength        = 0;
  quint32 m_frameNumberOfScans = 0;
};

} // namespace pappso
