/**
 * \file pappsomspp/vendors/tims/xicextractor/timsxicextractorinterface.cpp
 * \date 21/09/2019
 * \author Olivier Langella
 * \brief minimum functions to extract XICs from Tims Data
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "timsxicextractorinterface.h"
#include "../../../msrun/private/timsmsrunreaderms2.h"
#include "../../../exception/exceptionnotpossible.h"

using namespace pappso;

TimsXicExtractorInterface::TimsXicExtractorInterface(
  MsRunReaderSPtr &msrun_reader)
  : MsRunXicExtractorInterface(msrun_reader)
{

  pappso::TimsMsRunReaderMs2 *tims2_reader =
    dynamic_cast<pappso::TimsMsRunReaderMs2 *>(msrun_reader.get());
  if(tims2_reader != nullptr)
    {
      mp_timsData = tims2_reader->getTimsDataSPtr().get();
    }
  else
    {
      throw pappso::ExceptionNotPossible(QObject::tr(
        "error: TimsXicExtractorInterface must use a TimsMsRunReaderMs2"));
    }
}

TimsXicExtractorInterface::~TimsXicExtractorInterface()
{
}
