/**
 * \file pappsomspp/vendors/tims/timsbindec.h
 * \date 23/08/2019
 * \author Olivier Langella
 * \brief binary file handler of Bruker's TimsTof raw data
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QFile>
#include <QFileInfo>
#include "timsframerecord.h"
#include "timsframe.h"
#include "timsframerawdatachunck.h"
#include <vector>


namespace pappso
{

/**
 * @todo write docs
 */
class TimsBinDec
{
  public:
  /**
   * Default constructor
   */
  TimsBinDec(const QFileInfo &timsBinFile, int timsCompressionType);

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  TimsBinDec(const TimsBinDec &other);

  /**
   * Destructor
   */
  virtual ~TimsBinDec();

  // TimsFrameCstSPtr getTimsFrameCstSPtr(std::size_t timsId);

  TimsFrameSPtr getTimsFrameSPtrByOffset(
    std::size_t frameId,
    const std::vector<pappso::TimsFrameRecord> &frame_record_list);

  /** @brief close file access and flush cache
   */
  void closeLinearRead();


  private:
  /** @brief open one QFile handler for linear read
   */
  QFile *
  getQfileLinear(std::size_t frameId,
                 const std::vector<pappso::TimsFrameRecord> &frame_record_list);
  /** @brief open one QFile handler for random read
   */
  QFile *getQfileRandom();
  /** @brief populate a fifo buffer with TimsFrameRawDataChunck
   * accelerates inputs from file
   */
  void startLinearRead(
    std::size_t start_frame_id,
    std::size_t chunk_deque_size,
    const std::vector<pappso::TimsFrameRecord> &frame_record_list);


  TimsFrameSPtr
  getTimsFrameFromRawDataChunck(const TimsFrameRawDataChunck &raw_data_chunck);

  void moveLinearReadForward(
    const std::vector<pappso::TimsFrameRecord> &frame_record_list);

  private:
  int m_timsCompressionType;
  QString m_timsBinFile;
  QFile *mp_fileLinear = nullptr;
  QFile *mp_fileRandom = nullptr;
  // QMutex m_mutex;
  // std::vector<quint64> m_indexArray;
  char *mpa_decompressMemoryBuffer         = nullptr;
  std::size_t m_decompressMemoryBufferSize = 0;

  TimsFrameRawDataChunck m_randemAccessFrameRawDataChunck;


  std::vector<TimsFrameRawDataChunck> m_linearAccessRawDataChunckList;
  std::size_t m_firstFrameId                       = 0;
  std::size_t m_lastFrameId                        = 0;
  std::size_t m_linearAccessRawDataChunckDequeSize = 100;
  std::size_t m_linearForwardThreshold             = 30;
};
} // namespace pappso
