/**
 * \file pappsomspp/vendors/tims/mzcalibration/mzcalibrationinterface.cpp
 * \date 11/11/2020
 * \author Olivier Langella
 * \brief handles different ways to compute m/z using calibration parameters
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "mzcalibrationinterface.h"


namespace pappso
{

MzCalibrationInterface::MzCalibrationInterface(double digitizerTimebase,
                                               double digitizerDelay)
  : m_digitizerTimebase(digitizerTimebase), m_digitizerDelay(digitizerDelay)
{
}

MzCalibrationInterface::~MzCalibrationInterface()
{
}

double
MzCalibrationInterface::getTofFromTofIndex(double index) const
{
  // mz calibration
  return (index * m_digitizerTimebase) + m_digitizerDelay;
}

double
MzCalibrationInterface::getTofFromTofIndex(quint32 index) const
{
  // mz calibration
  return ((double)index * m_digitizerTimebase) + m_digitizerDelay;
}


} // namespace pappso
