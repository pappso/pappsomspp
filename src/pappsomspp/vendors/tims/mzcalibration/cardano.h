/**
 * \file pappsomspp/vendors/tims/mzcalibration/cardano.h
 * \date 17/12/2022
 * \brief cubic solver adapted from
 * https://www.codeproject.com/articles/798474/to-solve-a-cubic-equation
        thanks
         * to "Sergey Bochkanov" <sergey.bochkanov@alglib.net> for his advise
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <vector>
#include <cstdint>

enum class CardanoResultCase : std::int8_t
{
  notvalid,
  zerod,
  negatived,
  positived,
  quadratic,
  line
};


struct InHousePolynomialSolverResult
{
  CardanoResultCase type;
  double x1;
  double x2;
  double x3;
};


InHousePolynomialSolverResult
inHousePolynomialSolve(const std::vector<double> &polynome);
