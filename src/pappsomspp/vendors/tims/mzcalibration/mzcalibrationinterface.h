/**
 * \file pappsomspp/vendors/tims/mzcalibration/mzcalibrationinterface.h
 * \date 11/11/2020
 * \author Olivier Langella
 * \brief handles different ways to compute m/z using calibration parameters
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QtGlobal>
#include <memory>
#include <vector>

namespace pappso
{

class MzCalibrationInterface;
typedef std::shared_ptr<MzCalibrationInterface> MzCalibrationInterfaceSPtr;
/**
 * @todo write docs
 */
class MzCalibrationInterface
{
  public:
  /**
   * Default constructor
   */
  MzCalibrationInterface(double digitizerTimebase, double digitizerDelay);

  /**
   * Destructor
   */
  virtual ~MzCalibrationInterface();

  /**
   * Assignment operator
   *
   * @param other TODO
   * @return TODO
   */
  MzCalibrationInterface &operator=(const MzCalibrationInterface &other);

  /**
   * @todo write docs
   *
   * @param other TODO
   * @return TODO
   */
  bool operator==(const MzCalibrationInterface &other) const;

  /** @brief get time of flight from raw index
   * @param tof_index digitizer x raw value
   * @return tof time of flight
   */
  double getTofFromTofIndex(quint32 tof_index) const;

  /** @brief get time of flight from double index
   */
  double getTofFromTofIndex(double tof_index) const;


  /** @brief get m/z from time of flight raw index
   * @param tof_index time of flight
   * @return m/z value
   */
  virtual double getMzFromTofIndex(quint32 tof_index) = 0;

  /** @brief get raw TOF index of a given m/z
   * @param mz the mass to transform
   * @return integer x raw value
   */
  virtual quint32 getTofIndexFromMz(double mz) = 0;


  protected:
  double m_digitizerTimebase = 0;
  double m_digitizerDelay    = 0;

  /** @brief MZ calibration parameters
   */
  std::vector<double> m_mzCalibrationArr;
};
} // namespace pappso
