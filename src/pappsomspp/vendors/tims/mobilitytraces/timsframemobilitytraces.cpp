/**
 * \file pappsomspp/vendors/tims/mobilitytraces
 * \date 05/07/2024
 * \author Olivier Langella
 * \brief extracting mobility traces
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "timsframemobilitytraces.h"
#include "../../../exception/exceptionnotfound.h"
#include <QDebug>
namespace pappso
{
TimsFrameMobilityTraces::TimsFrameMobilityTraces()
{
}

TimsFrameMobilityTraces::TimsFrameMobilityTraces(
  const TimsFrameMobilityTraces &other)
{
}

TimsFrameMobilityTraces::~TimsFrameMobilityTraces()
{
}
void
TimsFrameMobilityTraces::extractMobilityTraces(
  pappso::TimsFrameCstSPtr timsframe_sptr,
  std::size_t scan_index_begin,
  std::size_t scan_index_end,
  std::size_t max_traces)
{
  m_tofIndexList.clear();
  m_ionMobTraceList.clear();
  m_scanIndexList.clear();
  mcsp_timsFrameCstSPtr = timsframe_sptr;

  pappso::TimsDataFastMap raw_spectrum =
    pappso::TimsDataFastMap::getTimsDataFastMapInstance();
  timsframe_sptr.get()->combineScansInTofIndexIntensityMap(
    raw_spectrum, scan_index_begin, scan_index_end);

  pappso::Trace total_spectrum;

  for(auto index : raw_spectrum.getTofIndexList())
    {

      total_spectrum.push_back(
        {(double)index, (double)raw_spectrum.readIntensity(index)});
    }
  total_spectrum.sortY(SortOrder::descending);

  qDebug() << total_spectrum.size();

  total_spectrum.sortY(pappso::SortOrder::descending);

  for(std::size_t i = scan_index_begin; i < (scan_index_end + 1); i++)
    {
      m_scanIndexList.push_back(i);
    }

  for(auto &datapoint : total_spectrum)
    {
      pappso::Trace trace(
        timsframe_sptr.get()->getIonMobilityTraceByTofIndexRange(
          datapoint.x - 1,
          datapoint.x + 1,
          pappso::XicExtractMethod::sum,
          scan_index_begin,
          scan_index_end));

      m_ionMobTraceList.push_back(trace.makeTraceSPtr());
      m_tofIndexList.push_back(datapoint.x);
      if(m_ionMobTraceList.size() >= max_traces)
        break;
    }
  qDebug();
}

const std::vector<pappso::TraceCstSPtr> &
TimsFrameMobilityTraces::getIonMobTraceList() const
{
  return m_ionMobTraceList;
}
const std::vector<std::size_t> &
TimsFrameMobilityTraces::getTofIndexList() const
{
  return m_tofIndexList;
}
const std::vector<double>
TimsFrameMobilityTraces::getMzList() const
{
  MzCalibrationInterface *mz_calibration_p =
    mcsp_timsFrameCstSPtr.get()->getMzCalibrationInterfaceSPtr().get();
  if(mz_calibration_p == nullptr)
    {
      throw pappso::ExceptionNotFound(
        QObject::tr("mz calibration pointer not found"));
    }
  std::vector<double> mz_list;
  for(std::size_t tof : m_tofIndexList)
    {
      mz_list.push_back(mz_calibration_p->getMzFromTofIndex((quint32)tof));
    }
  return mz_list;
}
const std::vector<std::size_t> &
TimsFrameMobilityTraces::getScanIndexList() const
{
  return m_scanIndexList;
}
} // namespace pappso
