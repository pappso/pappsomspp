/**
 * \file pappsomspp/vendors/tims/mobilitytraces
 * \date 05/07/2024
 * \author Olivier Langella
 * \brief extracting mobility traces
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../timsframe.h"

namespace pappso
{
/**
 * @todo write docs
 */
class TimsFrameMobilityTraces
{
  public:
  /**
   * Default constructor
   */
  TimsFrameMobilityTraces();

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  TimsFrameMobilityTraces(const TimsFrameMobilityTraces &other);

  /**
   * Destructor
   */
  ~TimsFrameMobilityTraces();

  void extractMobilityTraces(pappso::TimsFrameCstSPtr timsframe_sptr,
                             std::size_t ion_mob_begin,
                             std::size_t ion_mob_end,
                             std::size_t max_traces);

  const std::vector<pappso::TraceCstSPtr> &getIonMobTraceList() const;
  const std::vector<std::size_t> &getTofIndexList() const;
  const std::vector<std::size_t> &getScanIndexList() const;
  const std::vector<double> getMzList() const;

  private:
  pappso::TimsFrameCstSPtr mcsp_timsFrameCstSPtr;

  std::vector<pappso::TraceCstSPtr> m_ionMobTraceList;
  std::vector<std::size_t> m_tofIndexList;
  std::vector<std::size_t> m_scanIndexList;
};
} // namespace pappso
