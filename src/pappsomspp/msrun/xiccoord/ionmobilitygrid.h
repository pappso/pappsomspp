/**
 * \file pappsomspp/msrun/xiccoord/ionmobilitygrid.h
 * \date 26/01/2023
 * \author Olivier Langella
 * \brief store observed ion mobility coordinates differences between MS runs
 */


/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../msrunid.h"
#include "xiccoord.h"
#include "../../exportinmportconfig.h"

namespace pappso
{
/**
 * @todo helper to align ion mobility ranges between MS runs
 */
class PMSPP_LIB_DECL IonMobilityGrid
{
  public:
  /**
   * Default constructor
   */
  IonMobilityGrid();

  /**
   * Destructor
   */
  virtual ~IonMobilityGrid();

  void storeObservedIdentityBetween(const MsRunId &msrun_ida,
                                    const XicCoord *xic_coorda,
                                    const MsRunId &msrun_idb,
                                    const XicCoord *xic_coordb);

  void computeCorrections();

  pappso::XicCoordSPtr
  translateXicCoordFromTo(const pappso::XicCoord &source_xic_coord,
                          const MsRunId &source_msrunid,
                          const MsRunId &target_msrunid) const;

  const std::map<QString, std::vector<qint64>> &getMapDiferrencesStart() const;
  const std::map<QString, long> &getMapCorrectionsStart() const;

  private:
  std::map<QString, std::vector<qint64>> m_mapDiferrencesStart;
  std::map<QString, std::vector<qint64>> m_mapDiferrencesStop;


  /** @brief scan num correction on start position stored for each msrun pair
   */
  std::map<QString, long> m_mapCorrectionsStart;


  /** @brief scan num correction on start position stored for each msrun pair
   */
  std::map<QString, long> m_mapCorrectionsStop;
};

} // namespace pappso
