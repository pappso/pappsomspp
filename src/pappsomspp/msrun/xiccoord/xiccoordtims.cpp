/**
 * \file pappsomspp/msrun/xiccoord/xiccoordtims.cpp
 * \date 22/04/2021
 * \author Olivier Langella
 * \brief XIC coordinate in a Tims MSrun
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include <QObject>
#include <QVariant>

#include "xiccoordtims.h"
#include "../../exception/exceptionnotpossible.h"


namespace pappso
{

XicCoordTims::XicCoordTims(const XicCoordTims &other) : XicCoord(other)
{
  scanNumBegin = other.scanNumBegin;
  scanNumEnd   = other.scanNumEnd;
}


XicCoordTims::~XicCoordTims()
{
}


XicCoordSPtr
XicCoordTims::initializeAndClone() const
{

  XicCoordTimsSPtr xic_coord_sp = std::make_shared<XicCoordTims>(*this);

  xic_coord_sp.get()->xicSptr = std::make_shared<Xic>();

  return xic_coord_sp;
}


XicCoordSPtr
XicCoordTims::addition(const XicCoordSPtr &to_add) const
{
  XicCoordTimsSPtr xic_coord_sp = std::make_shared<XicCoordTims>(*this);

  XicCoordTims *toadd = dynamic_cast<XicCoordTims *>(to_add.get());

  if(toadd == nullptr)
    {
      throw ExceptionNotPossible(QObject::tr("XicCoord to add is of a different type"));
    }

  // xic_coord_sp.get()->xicSptr = xic_coord_sp.get()->xicSptr;

  xic_coord_sp.get()->mzRange += to_add.get()->mzRange;
  xic_coord_sp.get()->rtTarget += to_add.get()->rtTarget;
  xic_coord_sp.get()->scanNumBegin += toadd->scanNumBegin;
  xic_coord_sp.get()->scanNumEnd += toadd->scanNumEnd;

  qDebug() << "xic_coord_sp.get()->scanNumBegin=" << xic_coord_sp.get()->scanNumBegin;
  qDebug() << "xic_coord_sp.get()->scanNumEnd=" << xic_coord_sp.get()->scanNumEnd;
  return xic_coord_sp;
}


XicCoordSPtr
XicCoordTims::multiplyBy(double number) const
{
  XicCoordTimsSPtr xic_coord_sp = std::make_shared<XicCoordTims>(*this);

  // xic_coord_sp.get()->xicSptr = nullptr;

  xic_coord_sp.get()->rtTarget *= number;
  xic_coord_sp.get()->mzRange *= number;

  xic_coord_sp.get()->scanNumBegin *= number;
  xic_coord_sp.get()->scanNumEnd *= number;

  return xic_coord_sp;
}

XicCoordSPtr
XicCoordTims::divideBy(double number) const
{

  XicCoordTimsSPtr xic_coord_sp = std::make_shared<XicCoordTims>(*this);

  // xic_coord_sp.get()->xicSptr = nullptr;

  xic_coord_sp.get()->rtTarget /= number;
  xic_coord_sp.get()->mzRange *= (double)((double)1 / number);

  xic_coord_sp.get()->scanNumBegin /= number;
  xic_coord_sp.get()->scanNumEnd /= number;

  qDebug() << "xic_coord_sp.get()->scanNumBegin=" << xic_coord_sp.get()->scanNumBegin;
  qDebug() << "xic_coord_sp.get()->scanNumEnd=" << xic_coord_sp.get()->scanNumEnd;
  return xic_coord_sp;
}


void
XicCoordTims::reset()
{

  xicSptr = nullptr;

  rtTarget     = 0;
  mzRange      = MzRange(0.0, 0.0);
  scanNumBegin = 0;
  scanNumEnd   = 0;
}

QString
XicCoordTims::toString() const
{
  return QString("%1 begin=%2 end=%3").arg(XicCoord::toString()).arg(scanNumBegin).arg(scanNumEnd);
}


const QVariant
XicCoordTims::getParam(XicCoordParam param) const
{
  switch(param)
    {
      case XicCoordParam::TimsTofIonMobilityScanNumberStart:
        return QVariant((quint64)scanNumBegin);
        break;
      case XicCoordParam::TimsTofIonMobilityScanNumberStop:
        return QVariant((quint64)scanNumEnd);
        break;
      default:
        return QVariant();
    }
}
void
XicCoordTims::scanNumBeginRangeCorrection(long start_dev, long stop_dev)
{
  long begin = scanNumBegin + start_dev;
  long end   = scanNumEnd + stop_dev;
  if(begin < 0)
    scanNumBegin = 0;
  else
    scanNumBegin = begin;
  if(end < 0)
    scanNumEnd = 0;
  else
    scanNumEnd = end;
}
} // namespace pappso


void
pappso::XicCoordTims::writeCborStream(QCborStreamWriter &cbor_writer) const
{
  cbor_writer.append(QLatin1String("xic_coord"));
  cbor_writer.startMap(3);
  cbor_writer.append(QLatin1String("mz_range"));
  cbor_writer.startArray(2);
  cbor_writer.append(mzRange.lower());
  cbor_writer.append(mzRange.upper());
  cbor_writer.endArray();

  cbor_writer.append(QLatin1String("rt"));
  cbor_writer.append(rtTarget);

  cbor_writer.append(QLatin1String("tim_im_range"));
  cbor_writer.startArray(2);
  cbor_writer.append((quint64)scanNumBegin);
  cbor_writer.append((quint64)scanNumEnd);
  cbor_writer.endArray();
  cbor_writer.endMap();
}
