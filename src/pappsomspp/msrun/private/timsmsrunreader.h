/**
 * \file pappsomspp/msrun/private/timsmsrunreader.h
 * \date 05/09/2019
 * \author Olivier Langella
 * \brief MSrun file reader for native Bruker TimsTOF raw data
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once

#include "timsmsrunreaderbase.h"

namespace pappso
{

class PMSPP_LIB_DECL TimsMsRunReader : public TimsMsRunReaderBase
{
  friend class MsFileAccessor;
  /**
   * @todo write docs
   */
  public:
  TimsMsRunReader(MsRunIdCstSPtr &msrun_id_csp);
  TimsMsRunReader(const TimsMsRunReaderBase &msrun_reader_base);

  virtual ~TimsMsRunReader();

  virtual MassSpectrumSPtr massSpectrumSPtr(std::size_t spectrum_index) override;
  virtual MassSpectrumCstSPtr massSpectrumCstSPtr(std::size_t spectrum_index) override;

  virtual QualifiedMassSpectrum qualifiedMassSpectrum(std::size_t spectrum_index,
                                                      bool want_binary_data = true) const override;

  virtual void readSpectrumCollection(SpectrumCollectionHandlerInterface &handler) override;

  virtual void readSpectrumCollection2(const MsRunReadConfig &config,
                                       SpectrumCollectionHandlerInterface &handler) override;
  virtual void readSpectrumCollectionByMsLevel(SpectrumCollectionHandlerInterface &handler,
                                               unsigned int ms_level) override;


  virtual std::size_t spectrumListSize() const override;

  virtual std::size_t
  spectrumStringIdentifier2SpectrumIndex(const QString &spectrum_identifier) override;
};

} // namespace pappso
