/**
 * \file pappsomspp/msrun/private/timsmsrunreaderdia.h
 * \date 09/07/2024
 * \author Olivier Langella
 * \brief MSrun file reader for native Bruker TimsTOF specialized for DIA
 * purpose
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once

#include "../../types.h"
#include "../../msfile/msfileaccessor.h"
#include "../../vendors/tims/timsdata.h"
#include "../../vendors/tims/timsdiaslices.h"

#include "../../exportinmportconfig.h"
#include "timsmsrunreaderbase.h"

namespace pappso
{

class PMSPP_LIB_DECL TimsMsRunReaderDia : public TimsMsRunReaderBase
{
  friend class MsFileAccessor;

  public:
  /**
   * Default constructor
   */
  TimsMsRunReaderDia(MsRunIdCstSPtr &msrun_id_csp);
  TimsMsRunReaderDia(const TimsMsRunReaderBase &msrun_reader_base);

  /**
   * Destructor
   */
  virtual ~TimsMsRunReaderDia();


  /** @brief get a MassSpectrumSPtr class given its spectrum index
   */
  virtual MassSpectrumSPtr
  massSpectrumSPtr(std::size_t spectrum_index) override;
  virtual MassSpectrumCstSPtr
  massSpectrumCstSPtr(std::size_t spectrum_index) override;

  /** @brief get a QualifiedMassSpectrum class given its scan number
   */
  virtual QualifiedMassSpectrum
  qualifiedMassSpectrum(std::size_t spectrum_index,
                        bool want_binary_data = true) const override;


  /** @brief get a xic coordinate object from a given spectrum index
   */
  virtual XicCoordSPtr
  newXicCoordSPtrFromSpectrumIndex(std::size_t spectrum_index,
                                   PrecisionPtr precision) const override;

  /** @brief get a xic coordinate object from a given spectrum
   */
  virtual XicCoordSPtr newXicCoordSPtrFromQualifiedMassSpectrum(
    const QualifiedMassSpectrum &mass_spectrum,
    PrecisionPtr precision) const override;

  /** @brief get the totat number of spectrum conained in the MSrun data file
   */
  virtual std::size_t spectrumListSize() const override;

  /** @brief function to visit an MsRunReader and get each Spectrum in a
   * spectrum collection handler
   */
  virtual void
  readSpectrumCollection(SpectrumCollectionHandlerInterface &handler) override;

  virtual void
  readSpectrumCollection2(const MsRunReadConfig &config,
                          SpectrumCollectionHandlerInterface &handler) override;

  /** @brief function to visit an MsRunReader and get each Spectrum in a
   * spectrum collection handler by Ms Levels
   */
  virtual void
  readSpectrumCollectionByMsLevel(SpectrumCollectionHandlerInterface &handler,
                                  unsigned int ms_level) override;


  virtual bool acquireDevice() override;
  
  virtual std::size_t
  spectrumStringIdentifier2SpectrumIndex(const QString &spectrum_identifier) override;


  protected:
  virtual void initialize() override;

  private:
  TimsDiaSlices *mp_timsDiaSlices = nullptr;
};
} // namespace pappso
