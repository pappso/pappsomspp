/**
 * \file pappsomspp/msrun/private/timsmsrunreaderdia.cpp
 * \date 09/07/2024
 * \author Olivier Langella
 * \brief MSrun file reader for native Bruker TimsTOF specialized for DIA
 * purpose
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include "timsmsrunreaderdia.h"
#include "../../exception/exceptionnotfound.h"
#include "../../exception/exceptionnotimplemented.h"
#include <QDebug>

namespace pappso
{
TimsMsRunReaderDia::TimsMsRunReaderDia(MsRunIdCstSPtr &msrun_id_csp)
  : TimsMsRunReaderBase(msrun_id_csp)
{
  // qInfo() << msp_timsData.get();
  initialize();
}


TimsMsRunReaderDia::TimsMsRunReaderDia(const TimsMsRunReaderBase &msrun_reader_base)
  : TimsMsRunReaderBase(msrun_reader_base)
{
  initialize();
}

TimsMsRunReaderDia::~TimsMsRunReaderDia()
{
}


void
pappso::TimsMsRunReaderDia::initialize()
{
  qDebug();

  if(msp_timsData == nullptr)
    msp_timsData = std::make_shared<TimsData>(mcsp_msRunId.get()->getFileName());

  if(msp_timsData.get() == nullptr)
    {
      throw PappsoException(QObject::tr("ERROR in TimsMsRunReaderDia::initialize "
                                        "msp_timsData is null for MsRunId %1")
                              .arg(mcsp_msRunId.get()->toString()));
    }
  qDebug();
  qDebug() << msp_timsData.get();
  mp_timsDiaSlices = msp_timsData.get()->getTimsDiaSlicesPtr();
  qDebug();
}


XicCoordSPtr
TimsMsRunReaderDia::newXicCoordSPtrFromSpectrumIndex(std::size_t spectrum_index [[maybe_unused]],
                                                     pappso::PrecisionPtr precision
                                                     [[maybe_unused]]) const
{
  throw ExceptionNotImplemented(
    QObject::tr("Not implemented %1 %2 %3").arg(__FILE__).arg(__FUNCTION__).arg(__LINE__));
}

pappso::XicCoordSPtr
TimsMsRunReaderDia::newXicCoordSPtrFromQualifiedMassSpectrum(
  const pappso::QualifiedMassSpectrum &mass_spectrum [[maybe_unused]],
  pappso::PrecisionPtr precision [[maybe_unused]]) const
{
  throw ExceptionNotImplemented(
    QObject::tr("Not implemented %1 %2 %3").arg(__FILE__).arg(__FUNCTION__).arg(__LINE__));
}
void
TimsMsRunReaderDia::readSpectrumCollectionByMsLevel(SpectrumCollectionHandlerInterface &handler
                                                    [[maybe_unused]],
                                                    unsigned int ms_level [[maybe_unused]])
{
  throw ExceptionNotImplemented(
    QObject::tr("Not implemented %1 %2 %3").arg(__FILE__).arg(__FUNCTION__).arg(__LINE__));
}

QualifiedMassSpectrum
TimsMsRunReaderDia::qualifiedMassSpectrum(std::size_t spectrum_index, bool want_binary_data) const
{

  // spectrum index is a global slice index
  QualifiedMassSpectrum q_dia_spectrum;
  mp_timsDiaSlices->getMs2QualifiedSpectrumByGlobalSliceIndex(
    getMsRunId(), q_dia_spectrum, spectrum_index, want_binary_data);
  return q_dia_spectrum;
}

void
TimsMsRunReaderDia::readSpectrumCollection(SpectrumCollectionHandlerInterface &handler
                                           [[maybe_unused]])
{
  throw ExceptionNotImplemented(
    QObject::tr("Not implemented %1 %2 %3").arg(__FILE__).arg(__FUNCTION__).arg(__LINE__));
}

void
TimsMsRunReaderDia::readSpectrumCollection2(const MsRunReadConfig &config [[maybe_unused]],
                                            SpectrumCollectionHandlerInterface &handler
                                            [[maybe_unused]])
{
  throw ExceptionNotImplemented(
    QObject::tr("Not implemented %1 %2 %3").arg(__FILE__).arg(__FUNCTION__).arg(__LINE__));
}

MassSpectrumSPtr
TimsMsRunReaderDia::massSpectrumSPtr(std::size_t spectrum_index [[maybe_unused]])
{
  throw ExceptionNotImplemented(
    QObject::tr("Not implemented %1 %2 %3").arg(__FILE__).arg(__FUNCTION__).arg(__LINE__));
}
MassSpectrumCstSPtr
TimsMsRunReaderDia::massSpectrumCstSPtr(std::size_t spectrum_index [[maybe_unused]])
{
  throw ExceptionNotImplemented(
    QObject::tr("Not implemented %1 %2 %3").arg(__FILE__).arg(__FUNCTION__).arg(__LINE__));
}

std::size_t
TimsMsRunReaderDia::spectrumListSize() const
{

  return mp_timsDiaSlices->getTotalSlicesCount();
}

bool
TimsMsRunReaderDia::acquireDevice()
{
  if(TimsMsRunReaderBase::acquireDevice())
    {
      qDebug() << msp_timsData.get();
      mp_timsDiaSlices = msp_timsData.get()->getTimsDiaSlicesPtr();
      return true;
    }
  return false;
  qDebug();
}

std::size_t
TimsMsRunReaderDia::spectrumStringIdentifier2SpectrumIndex(const QString &spectrum_identifier
                                                           [[maybe_unused]])
{
  throw pappso::ExceptionNotImplemented(
    QObject::tr("%1 %2 %3 not implemented").arg(__FILE__).arg(__FUNCTION__).arg(__LINE__));
}
} // namespace pappso
