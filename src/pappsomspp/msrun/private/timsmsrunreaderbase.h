/**
 * \file pappsomspp/msrun/private/timsmsrunreaderbase.h
 * \date 15/07/2024
 * \author Olivier Langella
 * \brief Base class for all tims ms run reader objects
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once

#include "../../msfile/msfileaccessor.h"
#include "../../vendors/tims/timsdata.h"

namespace pappso
{
class PMSPP_LIB_DECL TimsMsRunReaderBase : public MsRunReader
{
  friend class MsFileAccessor;

  public:
  /**
   * Default constructor
   */
  TimsMsRunReaderBase(MsRunIdCstSPtr &msrun_id_csp);
  TimsMsRunReaderBase(const TimsMsRunReaderBase &msrun_reader_base);

  /**
   * Destructor
   */
  virtual ~TimsMsRunReaderBase();


  /** @brief give an access to the underlying raw data pointer
   */
  virtual TimsDataSp getTimsDataSPtr();
  virtual bool hasScanNumbers() const override;

  virtual bool releaseDevice() override;

  virtual bool acquireDevice() override;


  virtual Trace getTicChromatogram() override;

  /** @brief retention timeline
   * get retention times along the MSrun in seconds
   * @return vector of retention times (seconds)
   */
  virtual std::vector<double> getRetentionTimeLine() override;

  virtual pappso::XicCoordSPtr
  newXicCoordSPtrFromSpectrumIndex(std::size_t spectrum_index,
                                   pappso::PrecisionPtr precision) const override;

  virtual pappso::XicCoordSPtr
  newXicCoordSPtrFromQualifiedMassSpectrum(const pappso::QualifiedMassSpectrum &mass_spectrum,
                                           pappso::PrecisionPtr precision) const override;

  virtual const OboPsiModTerm getOboPsiModTermInstrumentModelName() const override;

  protected:
  virtual void initialize() override;
  virtual bool accept(const QString &file_name) const override;

  protected:
  TimsDataSp msp_timsData = nullptr;
};
} // namespace pappso
