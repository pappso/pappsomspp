/**
 * \file pappsomspp/msrun/msrunreader.h
 * \date 29/05/2018
 * \author Olivier Langella
 * \brief base interface to read MSrun files
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


/////////////////////// StdLib includes
#include <memory>
#include <map>


/////////////////////// Qt includes
#include <QMutex>


/////////////////////// pappsomspp includes
#include "../trace/maptrace.h"

/////////////////////// Local includes
#include "msrunid.h"
#include "../massspectrum/qualifiedmassspectrum.h"
#include "../msfile/msfilereader.h"
#include "../exportinmportconfig.h"
#include "xiccoord/xiccoord.h"

namespace pappso
{

/** @brief interface to collect spectrums from the MsRunReader class
 */
class PMSPP_LIB_DECL SpectrumCollectionHandlerInterface
{
  public:
  virtual void
  setQualifiedMassSpectrum(const QualifiedMassSpectrum &spectrum) = 0;

  /** @brief tells if we need the peak list (if we want the binary data) for
   * each spectrum
   */
  virtual bool needPeakList() const = 0;

  /** @brief tells if we need the peak list (if we want the binary data) for
   * each spectrum, given an MS level
   */
  virtual bool needMsLevelPeakList(unsigned int ms_level) const final;

  /** @brief tells if we need the peak list given
   */
  virtual void setNeedMsLevelPeakList(unsigned int ms_level,
                                      bool want_peak_list) final;
  virtual bool shouldStop();
  virtual void loadingEnded();
  virtual void spectrumListHasSize(std::size_t size);


  /** @brief use threads to read a spectrum by batch of batch_size
   * @param is_read_ahead boolean to use threads or not
   */
  virtual void setReadAhead(bool is_read_ahead) final;

  /** @brief tells if we want to read ahead spectrum
   */
  virtual bool isReadAhead() const;

  private:
  bool m_isReadAhead = false;

  std::vector<bool> m_needPeakListByMsLevel = {true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true};
};


/** @brief example of interface to count MS levels of all spectrum in an MSrun
 */
class PMSPP_LIB_DECL MsRunSimpleStatistics
  : public SpectrumCollectionHandlerInterface
{
  private:
  std::vector<unsigned long> m_countMsLevelSpectrum;

  public:
  virtual void
  setQualifiedMassSpectrum(const QualifiedMassSpectrum &spectrum) override;
  virtual bool needPeakList() const override;
  virtual void loadingEnded() override;

  unsigned long getMsLevelCount(unsigned int ms_level) const;

  unsigned long getTotalCount() const;
};

/** @brief provides a multimap to find quickly spectrum index from scan number
 */
class PMSPP_LIB_DECL MsRunReaderScanNumberMultiMap
  : public SpectrumCollectionHandlerInterface
{
  private:
  std::multimap<std::size_t, std::size_t> m_mmap_scan2index;

  public:
  MsRunReaderScanNumberMultiMap();
  virtual ~MsRunReaderScanNumberMultiMap();
  virtual void
  setQualifiedMassSpectrum(const QualifiedMassSpectrum &spectrum) override;
  virtual bool needPeakList() const override;

  std::size_t getSpectrumIndexFromScanNumber(std::size_t scan_number) const;
};


/** @brief collect retention times along MS run */
class PMSPP_LIB_DECL MsRunReaderRetentionTimeLine
  : public SpectrumCollectionHandlerInterface
{
  private:
  std::vector<double> m_retention_time_list;

  public:
  MsRunReaderRetentionTimeLine();
  virtual ~MsRunReaderRetentionTimeLine();
  virtual void
  setQualifiedMassSpectrum(const QualifiedMassSpectrum &spectrum) override;
  virtual bool needPeakList() const override;

  const std::vector<double> &getRetentionTimeLine() const;
};


/** @brief calculate a TIC chromatogram */
class PMSPP_LIB_DECL MsRunReaderTicChromatogram
  : public SpectrumCollectionHandlerInterface
{
  public:
  MsRunReaderTicChromatogram();
  virtual ~MsRunReaderTicChromatogram();
  virtual void setQualifiedMassSpectrum(
    const QualifiedMassSpectrum &qualified_mass_spectrum) override;
  virtual bool needPeakList() const override;

  Trace getTicChromatogram() const;

  private:
  MapTrace m_ticChromMapTrace;
};


/** @brief Load all qualified spectrum into a vector */
class PMSPP_LIB_DECL MsRunQualifiedSpectrumLoader
  : public SpectrumCollectionHandlerInterface
{
  public:
  MsRunQualifiedSpectrumLoader();
  virtual ~MsRunQualifiedSpectrumLoader();
  virtual void setQualifiedMassSpectrum(
    const QualifiedMassSpectrum &qualified_mass_spectrum) override;
  virtual bool needPeakList() const override;
  virtual void spectrumListHasSize(std::size_t size) override;

  const std::vector<QualifiedMassSpectrum> &
  getQualifiedMassSpectrumList() const;

  void clear();

  private:
  std::vector<QualifiedMassSpectrum> m_qualifiedSpectrumList;
};

} // namespace pappso
