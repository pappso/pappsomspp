/**
 * \file pappsomspp/msrun/msrunreader.h
 * \date 29/05/2018
 * \author Olivier Langella
 * \brief base interface to read MSrun files
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


/////////////////////// StdLib includes
#include <memory>
#include <map>


/////////////////////// Qt includes
#include <QMutex>


/////////////////////// pappsomspp includes
#include "../trace/maptrace.h"

/////////////////////// Local includes
#include "spectrumcollectionhandlerinterface.h"
#include "msrunreadconfig.h"
#include "msrunid.h"
#include "../massspectrum/qualifiedmassspectrum.h"
#include "../msfile/msfilereader.h"
#include "../obo/obopsimodterm.h"
#include "../exportinmportconfig.h"
#include "xiccoord/xiccoord.h"

namespace pappso
{

class PMSPP_LIB_DECL MsRunReader;
typedef std::shared_ptr<MsRunReader> MsRunReaderSPtr;
typedef std::shared_ptr<const MsRunReader> MsRunReaderCstSPtr;

/** @brief base class to read MSrun
 * the only way to build a MsRunReader object is to use the MsRunReaderFactory
 */
class PMSPP_LIB_DECL MsRunReader
{

  friend class MsFileAccessor;

  public:
  MsRunReader(const MsRunIdCstSPtr &ms_run_id);
  MsRunReader(const MsRunReader &other);
  virtual ~MsRunReader();

  const MsRunIdCstSPtr &getMsRunId() const;

  /** @brief get a MassSpectrumSPtr class given its spectrum index
   */
  virtual MassSpectrumSPtr massSpectrumSPtr(std::size_t spectrum_index)       = 0;
  virtual MassSpectrumCstSPtr massSpectrumCstSPtr(std::size_t spectrum_index) = 0;

  /** @brief get a QualifiedMassSpectrum class given its scan number
   */
  virtual QualifiedMassSpectrum qualifiedMassSpectrum(std::size_t spectrum_index,
                                                      bool want_binary_data = true) const = 0;


  /** @brief get a xic coordinate object from a given spectrum index
   */
  virtual XicCoordSPtr newXicCoordSPtrFromSpectrumIndex(std::size_t spectrum_index,
                                                        PrecisionPtr precision) const = 0;

  /** @brief get a xic coordinate object from a given spectrum
   */
  virtual XicCoordSPtr
  newXicCoordSPtrFromQualifiedMassSpectrum(const QualifiedMassSpectrum &mass_spectrum,
                                           PrecisionPtr precision) const = 0;

  /** @brief get the totat number of spectrum conained in the MSrun data file
   */
  virtual std::size_t spectrumListSize() const = 0;

  /** @brief function to visit an MsRunReader and get each Spectrum in a
   * spectrum collection handler
   */
  virtual void readSpectrumCollection(SpectrumCollectionHandlerInterface &handler) = 0;

  virtual void readSpectrumCollection2(const MsRunReadConfig &config,
                                       SpectrumCollectionHandlerInterface &handler) = 0;

  /** @brief function to visit an MsRunReader and get each Spectrum in a
   * spectrum collection handler by Ms Levels
   */
  virtual void readSpectrumCollectionByMsLevel(SpectrumCollectionHandlerInterface &handler,
                                               unsigned int ms_level) = 0;


  /** @brief if possible, converts a scan number into a spectrum index
   * This is a convenient function to help transition from the old scan number
   * (not implemented by all vendors) to more secure spectrum index (not vendor
   * dependant).
   * It is better to not rely on this function.
   */
  virtual std::size_t scanNumber2SpectrumIndex(std::size_t scan_number);

  /** @brief tells if spectra can be accessed using scan numbers
   * by default, it returns false. Only overrided functions can check if scan
   * numbers are available in the current file
   */
  virtual bool hasScanNumbers() const;

  /** @brief if possible, get the spectrum index given a string identifier
   * throw a not found exception if spectrum identifier is not found
   * @param spectrum_identifier string identifier of a specific spectrum
   * @return the spectrum index
   */
  virtual std::size_t
  spectrumStringIdentifier2SpectrumIndex(const QString &spectrum_identifier) = 0;


  /** @brief release data back end device
   * if a the data back end is released, the developper has to use acquireDevice
   * before using the msrunreader object
   * @return bool true if done
   */
  virtual bool releaseDevice() = 0;

  /** @brief acquire data back end device
   * @return bool true if done
   */
  virtual bool acquireDevice() = 0;

  /** @brief retention timeline
   * get retention times along the MSrun in seconds
   * @return vector of retention times (seconds)
   */
  virtual std::vector<double> getRetentionTimeLine();

  /** @brief get a TIC chromatogram
   *
   * for each retention time, computes the sum of all intensities.
   * For IM-MS, combines the mobility spectra
   *
   * Note that, formally, a TIC chromatogram is computed only for MS1 spectra.
   *
   * @return a trace (x=rt, y=intensities)
   */
  virtual Trace getTicChromatogram();


  /** @brief set only one is_mono_thread to true
   *
   * this avoid to use qtconcurrent
   */
  void setMonoThread(bool is_mono_thread);

  bool isMonoThread() const;


  /** @brief get OboPsiModTerm corresponding to the instrument model name
   * child of :
   * [Term]
id: MS:1000031
name: instrument model
def: "Instrument model name not including the vendor's name." [PSI:MS]
relationship: part_of MS:1000463 ! instrument
   */
  virtual const OboPsiModTerm getOboPsiModTermInstrumentModelName() const;

  protected:
  MsRunIdCstSPtr mcsp_msRunId;
  MsRunReaderScanNumberMultiMap *mpa_multiMapScanNumber = nullptr;

  virtual void initialize() = 0;

  /** @brief tells if the reader is able to handle this file
   * must be implemented by private MS run reader, specific of one or more file
   * format
   */
  virtual bool accept(const QString &file_name) const = 0;

  private:
  bool m_isMonoThread = false;
};

} // namespace pappso

Q_DECLARE_METATYPE(pappso::MsRunReaderSPtr);
extern int msRunReaderSPtrMetaTypeId;
