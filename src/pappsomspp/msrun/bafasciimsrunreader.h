
#pragma once

#include "../msfile/msfileaccessor.h"


namespace pappso
{

struct PMSPP_LIB_DECL MassSpectrumLineData
{
  double retentionTime;   // minutes
  QString ionizationMode; // + or -
  QString ionSourceType;  // MALDI or ESI
  int msLevel;
  QString dash;
  QString peakShapeType; // profile or line
  std::pair<double, double> mz_range;
  std::size_t peakCount;
  QStringList peakList;
};

class PMSPP_LIB_DECL BafAsciiMsRunReader : public MsRunReader
{
  friend class MsFileAccessor;

  public:
  BafAsciiMsRunReader(MsRunIdCstSPtr &msrun_id_csp);
  virtual ~BafAsciiMsRunReader();

  virtual MassSpectrumSPtr massSpectrumSPtr(std::size_t spectrum_index) override;
  virtual MassSpectrumCstSPtr massSpectrumCstSPtr(std::size_t spectrum_index) override;

  virtual QualifiedMassSpectrum qualifiedMassSpectrum(std::size_t spectrum_index,
                                                      bool want_binary_data = true) const override;

  virtual void readSpectrumCollection(SpectrumCollectionHandlerInterface &handler) override;

  virtual void readSpectrumCollection2(const MsRunReadConfig &config,
                                       SpectrumCollectionHandlerInterface &handler) override;

  virtual pappso::XicCoordSPtr
  newXicCoordSPtrFromSpectrumIndex(std::size_t spectrum_index,
                                   pappso::PrecisionPtr precision) const override;

  virtual pappso::XicCoordSPtr
  newXicCoordSPtrFromQualifiedMassSpectrum(const pappso::QualifiedMassSpectrum &mass_spectrum,
                                           pappso::PrecisionPtr precision) const override;

  virtual void readSpectrumCollectionByMsLevel(SpectrumCollectionHandlerInterface &handler,
                                               unsigned int ms_level) override;


  virtual std::size_t spectrumListSize() const override;

  virtual bool releaseDevice() override;

  virtual bool acquireDevice() override;

  virtual std::size_t
  spectrumStringIdentifier2SpectrumIndex(const QString &spectrum_identifier) override;


  protected:
  QString m_fileName;

  // Set when the accept function is called.
  mutable std::size_t m_spectrumCount = 0;

  QString craftLineParserRegExpPattern() const;
  QRegularExpression craftLineParserRegExp(QString &pattern) const;

  bool parseMassSpectrumLine(QString &line,
                             MassSpectrumLineData &ms_line_data,
                             QRegularExpression &line_regexp) const;

  virtual void initialize() override;
  virtual bool accept(const QString &file_name) const override;

  QualifiedMassSpectrum qualifiedMassSpectrumFromBafAsciiMSDataFile(std::size_t spectrum_index,
                                                                    bool want_binary_data) const;


};

} // namespace pappso
