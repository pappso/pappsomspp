// (C) 2019 Filippo Rusconi, GPL3+

#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "../exportinmportconfig.h"

namespace pappso
{

class MsRunDataSetTreeNode;


class PMSPP_LIB_DECL MsRunDataSetTreeNodeVisitorInterface
{
  public:
  virtual bool visit(const MsRunDataSetTreeNode &node) = 0;
  virtual bool shouldStop() const                      = 0;
  virtual void setNodesToProcessCount(std::size_t)     = 0;
};

} // namespace pappso
