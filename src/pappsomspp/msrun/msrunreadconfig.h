/*******************************************************************************
 * Copyright (c) 2023 Filippo Rusconi
 *<filippo.rusconi@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <map>
#include <QVariant>

#include "../exportinmportconfig.h"

namespace pappso
{

/** \def MsRunReadConfigParameters specific parameters for MsRunReadConfig
 *
 */
enum class MsRunReadConfigParameter
{
  // Only process mobility scans contained in the
  // scan index range below.
  TimsFrameIonMobScanIndexBegin,
  TimsFrameIonMobScanIndexEnd,

  // Only process mobility scans contained in the
  // inverse mobility range below.
  TimsFrameIonMobOneOverK0Begin,
  TimsFrameIonMobOneOverK0End,

  // Only process mass spectra for m/z values contained
  // in the following m/z range.
  MzRangeBegin,
  MzRangeEnd,

  // Merge m/z (TOF, really) indices on the basis of this
  // count of m/z (TOF, really) indices.
  TimsFrameMzIndexMergeWindow,
};

// It is generally admitted that 11 is the max MS levels
// reachable using a Paul trap...
constexpr std::size_t MAX_MS_LEVELS = 12;

class PMSPP_LIB_DECL MsRunReadConfig
{

  public:
  MsRunReadConfig();
  MsRunReadConfig(const MsRunReadConfig &other);

  ~MsRunReadConfig();

  MsRunReadConfig &operator=(const MsRunReadConfig &other);

  void setRetentionTimeStartInSeconds(double retention_time_start_in_seconds);
  double getRetentionTimeStartInSeconds() const;

  void setRetentionTimeEndInSeconds(double retention_time_end_in_seconds);
  double getRetentionTimeEndInSeconds() const;

  void setMsLevels(std::vector<std::size_t> ms_levels);
  const bool *getMsLevels() const;
  QString getMsLevelsAsString() const;

  void setNeedPeakList(bool need_peak_list);
  bool needPeakList() const;

  void setParameterValue(MsRunReadConfigParameter parameter,
                         const QVariant &value);
  const QVariant getParameterValue(MsRunReadConfigParameter parameter) const;

  void reset();

  QString toString() const;

  bool acceptMsLevel(std::size_t ms_level) const;
  bool acceptRetentionTimeInSeconds(double retention_time_in_seconds) const;

  private:
  double m_retentionTimeStartSeconds = -1;
  double m_retentionTimeEndSeconds   = -1;

  bool m_isPeakListNeeded = true;

  // Initialize the ms levels to false.
  // Each index in the array contains a bool value indicating, if true, a
  // requested MS level.
  bool m_msLevels[MAX_MS_LEVELS] = {false};
  // Fixme: maybe we should initialize in the constructor the MS level 1 to
  // true?

  //! map containing any parameter value
  std::map<MsRunReadConfigParameter, QVariant> m_paramsMap;
};

} // namespace pappso
