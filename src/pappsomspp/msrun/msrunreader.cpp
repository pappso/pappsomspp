/**
 * \file pappsomspp/msrun/msrunreader.cpp
 * \date 29/05/2018
 * \author Olivier Langella
 * \brief base interface to read MSrun files
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include <QDebug>
#include <QObject>

#include "msrunreader.h"
#include "../../pappsomspp/exception/exceptionnotfound.h"
#include "../../pappsomspp/exception/exceptionnotimplemented.h"


int msRunReaderSPtrMetaTypeId =
  qRegisterMetaType<pappso::MsRunReaderSPtr>("pappso::MsRunReaderSPtr");


namespace pappso
{


MsRunReader::MsRunReader(const MsRunIdCstSPtr &ms_run_id) : mcsp_msRunId(ms_run_id)
{
}

MsRunReader::MsRunReader(const MsRunReader &other) : mcsp_msRunId(other.mcsp_msRunId)
{
  mpa_multiMapScanNumber = nullptr;
  m_isMonoThread         = other.m_isMonoThread;
}


const MsRunIdCstSPtr &
MsRunReader::getMsRunId() const
{
  return mcsp_msRunId;
}


MsRunReader::~MsRunReader()
{
  if(mpa_multiMapScanNumber == nullptr)
    delete mpa_multiMapScanNumber;
}

void
MsRunReader::setMonoThread(bool is_mono_thread)
{
  m_isMonoThread = is_mono_thread;
}

bool
MsRunReader::isMonoThread() const
{
  return m_isMonoThread;
}


std::size_t
MsRunReader::scanNumber2SpectrumIndex(std::size_t scan_number)
{
  qDebug() << " " << mpa_multiMapScanNumber;

  if(mpa_multiMapScanNumber == nullptr)
    {
      mpa_multiMapScanNumber = new MsRunReaderScanNumberMultiMap();
      readSpectrumCollection(*mpa_multiMapScanNumber);
    }
  try
    {
      return mpa_multiMapScanNumber->getSpectrumIndexFromScanNumber(scan_number);
    }

  catch(ExceptionNotFound &error)
    {
      throw ExceptionNotFound(QObject::tr("error reading file %1 : %2")
                                .arg(mcsp_msRunId.get()->getFileName())
                                .arg(error.qwhat()));
    }
  catch(PappsoException &error)
    {
      throw PappsoException(QObject::tr("error reading file %1 : %2")
                              .arg(mcsp_msRunId.get()->getFileName())
                              .arg(error.qwhat()));
    }
}


bool
MsRunReader::hasScanNumbers() const
{
  return false;
}

std::vector<double>
MsRunReader::getRetentionTimeLine()
{
  qDebug();

  try
    {

      MsRunReaderRetentionTimeLine reader_timeline;
      MsRunReadConfig config;
      config.setMsLevels({1});
      config.setNeedPeakList(false);
      // readSpectrumCollectionByMsLevel(reader_timeline, 1);
      readSpectrumCollection2(config, reader_timeline);

      return reader_timeline.getRetentionTimeLine();
    }

  catch(ExceptionNotFound &error)
    {
      throw ExceptionNotFound(QObject::tr("error reading file %1 : %2")
                                .arg(mcsp_msRunId.get()->getFileName())
                                .arg(error.qwhat()));
    }
  catch(PappsoException &error)
    {
      throw PappsoException(QObject::tr("error reading file %1 : %2")
                              .arg(mcsp_msRunId.get()->getFileName())
                              .arg(error.qwhat()));
    }
}


Trace
MsRunReader::getTicChromatogram()
{
  qDebug();

  try
    {
      MsRunReaderTicChromatogram ms_run_reader;

      readSpectrumCollection(ms_run_reader);

      return ms_run_reader.getTicChromatogram();
    }

  catch(ExceptionNotFound &error)
    {
      throw ExceptionNotFound(QObject::tr("error reading file %1 : %2")
                                .arg(mcsp_msRunId.get()->getFileName())
                                .arg(error.qwhat()));
    }
  catch(PappsoException &error)
    {
      throw PappsoException(QObject::tr("error reading file %1 : %2")
                              .arg(mcsp_msRunId.get()->getFileName())
                              .arg(error.qwhat()));
    }
}

} // namespace pappso

const pappso::OboPsiModTerm
pappso::MsRunReader::getOboPsiModTermInstrumentModelName() const
{
  throw ExceptionNotFound(
    QObject::tr("instrument model name not found in %1").arg(mcsp_msRunId.get()->getFileName()));
}
