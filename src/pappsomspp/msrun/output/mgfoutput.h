/**
 * \file pappsomspp/msrun/output/mgfoutput.h
 * \date 6/5/2020
 * \author Olivier Langella
 * \brief write msrun peaks into MGF output stream
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QIODevice>
#include <QTextStream>
#include "../../massspectrum/qualifiedmassspectrum.h"

namespace pappso
{
/**
 * @todo write docs
 */
class PMSPP_LIB_DECL MgfOutput
{
  public:
  /**
   * Default constructor
   */
  MgfOutput(QIODevice *p_output_device);
  void write(const QualifiedMassSpectrum &mass_spectrum);
  void close();

  /**
   * Destructor
   */
  virtual ~MgfOutput();

  private:
  QTextStream *mpa_outputStream;
};
} // namespace pappso
