/**
 * \file pappsomspp/peptide/peptideproformaparser.h
 * \date 27/11/2023
 * \author Olivier Langella
 * \brief parse peptide string in ProForma to pappso::Peptide
 * https://github.com/HUPO-PSI/ProForma/blob/master/README.md
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once
#include "peptide.h"

namespace pappso
{
/**
 * @todo write docs
 */
class PMSPP_LIB_DECL PeptideProFormaParser
{
  public:
  static PeptideSp parseString(const QString &pepstr);
  static NoConstPeptideSp parseNoConstString(const QString &pepstr);


  private:
  static void parseStringToPeptide(const QString &pepstr, Peptide &peptide);


  private:
  // static QRegularExpression _mod_parser;
  static QRegularExpression _rx_psimod;
  static QRegularExpression _rx_modmass;
};

} // namespace pappso
