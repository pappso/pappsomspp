/**
 * \file pappsomspp/peptide/peptideproformaparser.cpp
 * \date 27/11/2023
 * \author Olivier Langella
 * \brief parse peptide string in ProForma to pappso::Peptide
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "peptideproformaparser.h"
#include "../obo/filterobopsimodtermlabel.h"
#include "../obo/filterobopsimodsink.h"
#include "../exception/exceptionnotpossible.h"

namespace pappso
{


// QRegularExpression PeptideProFormaParser::_mod_parser("\\[[^\\]]*\\]");
QRegularExpression PeptideProFormaParser::_rx_psimod("MOD:[0-9]+");
QRegularExpression PeptideProFormaParser::_rx_modmass("[-+]?[0-9]+\\.?[0-9]*");
void
PeptideProFormaParser::parseStringToPeptide(const QString &pepstr, Peptide &peptide)
{
  // Peptide
  // peptide2("C[MOD:00397][MOD:01160]C[MOD:00397]AADDKEAC[MOD:00397]FAVEGPK");
  // CCAADDKEACFAVEGPK
  /*
  <psimod position="1"  accession="MOD:00397"/>
    <psimod position="2"  accession="MOD:00397"/>
    <psimod position="10"  accession="MOD:00397"/>
    <psimod position="1"  accession="MOD:01160"/>
    */

  std::vector<AaModificationP> nter_mod;

  std::size_t i   = 0;
  std::size_t end = pepstr.size();
  while(i < end)
    {
      QChar aa_char = pepstr[i];
      if(aa_char == '[')
        {
          QString mod;
          i++;
          aa_char = pepstr[i];
          while((i < end) && (aa_char != ']'))
            {
              mod.append(aa_char);
              i++;
              if(i < end)
                aa_char = pepstr[i];
            }

          qDebug() << aa_char;
          if(aa_char != ']')
            {
              throw pappso::ExceptionNotPossible(
                QObject::tr("modification string is malformed %1").arg(mod));
            }
          // we have a mod
          // is it a double ?
          bool is_double    = false;
          double mass_modif = mod.toDouble(&is_double);
          AaModificationP aamod;
          if(is_double)
            {
              aamod = AaModification::getInstanceCustomizedMod(mass_modif);
            }
          else
            {
              aamod = AaModification::getInstance(mod);
            }
          if(peptide.m_aaVec.size() == 0)
            {
              nter_mod.push_back(aamod);
            }
          else
            {
              peptide.m_aaVec.back().addAaModification(aamod);
            }
        }
      else
        {
          if(aa_char.isLetter())
            {
              qDebug() << aa_char;
              Aa pappso_aa(aa_char.toLatin1());
              if(peptide.size() == 0)
                {
                  pappso_aa.addAaModification(
                    AaModification::getInstance("internal:Nter_hydrolytic_cleavage_H"));

                  for(const pappso::AaModificationP &modif_p : nter_mod)
                    {
                      pappso_aa.addAaModification(modif_p);
                    }
                  nter_mod.clear();
                }
              peptide.m_aaVec.push_back(pappso_aa);
            }
        }
      i++;
    }

  peptide.m_aaVec.back().addAaModification(
    AaModification::getInstance("internal:Cter_hydrolytic_cleavage_HO"));

  // qDebug() << peptide.toProForma();
  peptide.m_proxyMass = -1;
  peptide.getMass();
}

PeptideSp
PeptideProFormaParser::parseString(const QString &pepstr)
{

  // QMutexLocker locker(&_mutex);
  Peptide peptide("");
  PeptideProFormaParser::parseStringToPeptide(pepstr, peptide);
  // qDebug() << peptide.toProForma();
  return (peptide.makePeptideSp());
}

NoConstPeptideSp
PeptideProFormaParser::parseNoConstString(const QString &pepstr)
{

  // QMutexLocker locker(&_mutex);
  Peptide peptide("");
  PeptideProFormaParser::parseStringToPeptide(pepstr, peptide);

  return (peptide.makeNoConstPeptideSp());
}
} // namespace pappso
