/**
 * \file pappsomspp/peptide/peptidefragmention.cpp
 * \date 10/3/2015
 * \author Olivier Langella
 * \brief peptide ion model
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include <QDebug>
#include "peptidefragmention.h"
#include "../pappsoexception.h"
#include "peptidenaturalisotopelist.h"
#include "peptiderawfragmentmasses.h"

namespace pappso
{
PeptideFragmentIon::PeptideFragmentIon(const PeptideFragmentSp &sp_fragment,
                                       PeptideIon ion_type)
  : PeptideFragmentIon(sp_fragment, ion_type, 0)
{
}

PeptideFragmentIon::PeptideFragmentIon(
  const PeptideFragmentSp &sp_fragment,
  PeptideIon ion_type,
  unsigned int number_of_neutral_phospho_loss)
  : msp_fragment(sp_fragment), m_ionType(ion_type)
{
  m_mass                     = msp_fragment.get()->getMass();
  PeptideDirection direction = msp_fragment.get()->getPeptideIonDirection();
  if(direction != pappso::getPeptideIonDirection(m_ionType))
    {
      throw PappsoException(
        QString("PeptideIon %1 is not an %2 fragment")
          .arg(PeptideFragmentIon::getPeptideIonName(m_ionType))
          .arg(PeptideFragment::getPeptideIonDirectionName(direction)));
    }
  m_mass -= MASSH2O;
  switch(m_ionType)
    {
        // -MASSH2O
      case PeptideIon::yp:
        m_neutralPhosphoLossNumber = number_of_neutral_phospho_loss;
        m_mass -=
          ((MASSH2O + MASSPHOSPHORYLATEDR) * m_neutralPhosphoLossNumber);
        ion_type = PeptideIon::y;
        break;
      case PeptideIon::bp:
        m_neutralPhosphoLossNumber = number_of_neutral_phospho_loss;
        m_mass -=
          ((MASSH2O + MASSPHOSPHORYLATEDR) * m_neutralPhosphoLossNumber);
        ion_type = PeptideIon::b;
        break;
      default:
        break;
    }
  m_mass += PeptideRawFragmentMasses::getDeltaMass(ion_type);
}

PeptideFragmentIon::PeptideFragmentIon(const PeptideFragmentIon &other)
  : msp_fragment(other.msp_fragment), m_ionType(other.m_ionType)
{
  m_mass = other.m_mass;
}


PeptideFragmentIon::PeptideFragmentIon(
  PeptideFragmentIon &&toCopy) // move constructor
  : msp_fragment(std::move(toCopy.msp_fragment)),
    m_ionType(toCopy.m_ionType),
    m_mass(toCopy.m_mass)
{
}

PeptideFragmentIon::~PeptideFragmentIon()
{
}


PeptideDirection
PeptideFragmentIon::getPeptideIonDirection(PeptideIon ion_type)
{
  return pappso::getPeptideIonDirection(ion_type);
}


const QString
PeptideFragmentIon::getCompletePeptideIonName(unsigned int charge) const
{

  std::size_t size = msp_fragment.get()->size();
  QString plusstr  = "+";
  plusstr          = plusstr.repeated(charge);
  if(m_ionType == PeptideIon::yp)
    {
      return QString("y%1(-P%2)%3")
        .arg(size)
        .arg(m_neutralPhosphoLossNumber)
        .arg(plusstr);
    }
  else if(m_ionType == PeptideIon::bp)
    {
      return QString("b%1(-P%2)%3")
        .arg(size)
        .arg(m_neutralPhosphoLossNumber)
        .arg(plusstr);
    }

  return QString("%1%2%3")
    .arg(getPeptideIonName(m_ionType))
    .arg(size)
    .arg(plusstr);
}

const QString
PeptideFragmentIon::getPeptideIonName() const
{
  if((m_ionType == PeptideIon::yp) || (m_ionType == PeptideIon::bp))
    {
      return QString("%1(%2)")
        .arg(getPeptideIonName(m_ionType))
        .arg(m_neutralPhosphoLossNumber);
    }
  return getPeptideIonName(m_ionType);
}

const QString
PeptideFragmentIon::getPeptideIonName(PeptideIon m_ionType)
{
  switch(m_ionType)
    {
      case PeptideIon::y:
        return "y";
        break;
      case PeptideIon::yp:
        return "yP";
        break;
      case PeptideIon::ystar:
        return "y*";
        break;
      case PeptideIon::yo:
        return "yO";
        break;
      case PeptideIon::bstar:
        return "b*";
        break;
      case PeptideIon::bo:
        return "bO";
        break;
      case PeptideIon::a:
        return "a";
        break;
      case PeptideIon::astar:
        return "a*";
        break;
      case PeptideIon::ao:
        return "aO";
        break;
      case PeptideIon::c:
        return "c";
        break;
        // SvgIon.moxygen - mN
      case PeptideIon::z:
        return "z";
        break;
      case PeptideIon::b:
        return "b";
        break;
      case PeptideIon::bp:
        return "bP";
        break;
      case PeptideIon::x:
        return "x";
        break;
      default:
        throw PappsoException(QString("PeptideIon name not implemented"));
        break;
    }
}

const QColor
PeptideFragmentIon::getPeptideIonColor(PeptideIon m_ionType)
{
  switch(m_ionType)
    {
      case PeptideIon::y:
        return QColor("red");
        break;
      case PeptideIon::yp:
        return QColor("red");
        break;
      case PeptideIon::ystar:
        return QColor("red");
        break;
      case PeptideIon::yo:
        return QColor("orange");
        break;
      case PeptideIon::x:
        return QColor("orange");
        break;
      case PeptideIon::bstar:
        return QColor("blue");
        break;
      case PeptideIon::bo:
        return QColor("#ff00ff");
        break;
      case PeptideIon::a:
        return QColor("green");
        break;
      case PeptideIon::astar:
        return QColor("green");
        break;
      case PeptideIon::ao:
        return QColor("green");
        break;
      case PeptideIon::c:
        return QColor("blue");
        break;
        // SvgIon.moxygen - mN
      case PeptideIon::z:
        return QColor("red");
        break;
      case PeptideIon::b:
        return QColor("blue");
        break;
      case PeptideIon::bp:
        return QColor("blue");
        break;
      default:
        throw PappsoException(QString("PeptideIon color not implemented %1")
                                .arg(getPeptideIonName(m_ionType)));
        break;
    }
}

int
PeptideFragmentIon::getNumberOfAtom(AtomIsotopeSurvey atom) const
{
  int number = msp_fragment.get()->getNumberOfAtom(atom);
  int diff   = 0;
  switch(atom)
    {
      case AtomIsotopeSurvey::C:
        switch(m_ionType)
          {
            case PeptideIon::y:
              break;
            case PeptideIon::yp:
              // H 1 O 3 P 1 + H 2 0
              break;
            case PeptideIon::ystar:
              // m_mass -= MASSNH3;
              break;
            case PeptideIon::yo:
              // m_mass -= MASSH2O;
              break;
            case PeptideIon::bstar:
              // m_mass -= MASSH2O;
              // m_mass -= MASSNH3;
              break;
            case PeptideIon::bo:
              // m_mass -= MASSH2O;
              // m_mass -= MASSH2O;
              break;
            case PeptideIon::a:
              // m_mass -= MASSH2O;
              // m_mass -= MASSCO;
              diff = -1;
              break;
            case PeptideIon::c:
              // m_mass += MASSNH3;
              break;
              // SvgIon.moxygen - mN
            case PeptideIon::z:
              // m_mass -= MASSH2O;
              // m_mass += MASSOXYGEN - MASSNITROGEN - MPROTIUM;
              break;
            case PeptideIon::b:
              // m_mass -= MASSH2O;
              break;
            case PeptideIon::bp:
              // H 1 O 3 P 1 + H 2 0
              break;

            case PeptideIon::astar:
              // m_mass -= MASSH2O;
              // m_mass = - MASSCO - MASSNH3;
              diff = -1;
              break;
            case PeptideIon::ao:
              // m_mass -= MASSH2O;
              // m_mass = - MASSCO - MASSH2O;
              diff = -1;
              break;
            case PeptideIon::x:
              // +MASSCO + MASSOXYGEN
              diff = +1;
              break;
            default:
              throw PappsoException(QString("PeptideIon name not implemented"));
              break;
          }
        break;
      case AtomIsotopeSurvey::H:
        switch(m_ionType)
          {
            case PeptideIon::y:
              break;
            case PeptideIon::yp:
              // H 1 O 3 P 1 + H 2 0
              diff = -3 * m_neutralPhosphoLossNumber;
              break;
            case PeptideIon::ystar:
              // m_mass -= MASSNH3;
              diff = -3;
              break;
            case PeptideIon::yo:
              // m_mass -= MASSH2O;
              diff = -2;
              break;
            case PeptideIon::bstar:
              // m_mass -= MASSH2O;
              // m_mass -= MASSNH3;
              diff = -5;
              break;
            case PeptideIon::bo:
              // m_mass -= MASSH2O;
              // m_mass -= MASSH2O;
              diff = -4;
              break;
            case PeptideIon::a:
              // m_mass -= MASSH2O;
              // m_mass -= MASSCO;
              diff = -2;
              break;
            case PeptideIon::c:
              // m_mass += MASSNH3;
              diff = -3;
              break;
              // SvgIon.moxygen - mN
            case PeptideIon::z:
              // m_mass -= MASSH2O;
              // m_mass += MASSOXYGEN - MASSNITROGEN - MPROTIUM;
              diff = -3;
              break;
            case PeptideIon::b:
              // m_mass -= MASSH2O;
              diff = -2;
              break;
            case PeptideIon::bp:
              // H 1 O 3 P 1 + H 2 0
              diff = -3 * m_neutralPhosphoLossNumber;
              break;


            case PeptideIon::astar:
              // m_mass -= MASSH2O;
              // m_mass = - MASSCO - MASSNH3;
              diff = -5;
              break;
            case PeptideIon::ao:
              // m_mass -= MASSH2O;
              // m_mass = - MASSCO - MASSH2O;
              diff = -4;
              break;
            case PeptideIon::x:
              // +MASSCO + MASSOXYGEN
              diff = -2;
              break;
            default:
              throw PappsoException(QString("PeptideIon name not implemented"));
              break;
          }
        break;
      case AtomIsotopeSurvey::N:
        switch(m_ionType)
          {
            case PeptideIon::y:
              break;
            case PeptideIon::yp:
              // H 1 O 3 P 1 + H 2 0
              break;

            case PeptideIon::ystar:
              // m_mass -= MASSNH3;
              diff = -1;
              break;
            case PeptideIon::yo:
              // m_mass -= MASSH2O;
              break;
            case PeptideIon::bstar:
              // m_mass -= MASSH2O;
              // m_mass -= MASSNH3;
              diff = -1;
              break;
            case PeptideIon::bo:
              // m_mass -= MASSH2O;
              // m_mass -= MASSH2O;
              break;
            case PeptideIon::a:
              // m_mass -= MASSH2O;
              // m_mass -= MASSCO;
              break;
            case PeptideIon::c:
              // m_mass += MASSNH3;
              diff = -1;
              break;
              // SvgIon.moxygen - mN
            case PeptideIon::z:
              // m_mass -= MASSH2O;
              // m_mass += MASSOXYGEN - MASSNITROGEN - MPROTIUM;
              diff = -1;
              break;
            case PeptideIon::b:
              // m_mass -= MASSH2O;
              break;
            case PeptideIon::bp:
              // H 1 O 3 P 1 + H 2 0
              break;


            case PeptideIon::astar:
              // m_mass -= MASSH2O;
              // m_mass = - MASSCO - MASSNH3;
              diff = -1;
              break;
            case PeptideIon::ao:
              // m_mass -= MASSH2O;
              // m_mass = - MASSCO - MASSH2O;
              break;
            case PeptideIon::x:
              // +MASSCO + MASSOXYGEN
              break;
            default:
              throw PappsoException(QString("PeptideIon name not implemented"));
              break;
          }
        break;
      case AtomIsotopeSurvey::O:
        switch(m_ionType)
          {
            case PeptideIon::y:
              break;
            case PeptideIon::yp:
              // H 1 O 3 P 1 + H 2 0
              diff = -4 * m_neutralPhosphoLossNumber;
              break;

            case PeptideIon::ystar:
              // m_mass -= MASSNH3;
              break;
            case PeptideIon::yo:
              // m_mass -= MASSH2O;
              diff = -1;
              break;
            case PeptideIon::bstar:
              // m_mass -= MASSH2O;
              // m_mass -= MASSNH3;
              diff = -1;
              break;
            case PeptideIon::bo:
              // m_mass -= MASSH2O;
              // m_mass -= MASSH2O;
              diff = -2;
              break;
            case PeptideIon::a:
              // m_mass -= MASSH2O;
              // m_mass -= MASSCO;
              diff = -2;
              break;
            case PeptideIon::c:
              // m_mass += MASSNH3;
              break;
              // SvgIon.moxygen - mN
            case PeptideIon::z:
              // m_mass -= MASSH2O;
              // m_mass += MASSOXYGEN - MASSNITROGEN  - MPROTIUM;
              diff = -2;
              break;
            case PeptideIon::b:
              // m_mass -= MASSH2O;
              diff = -1;
              break;
            case PeptideIon::bp:
              // H 1 O 3 P 1 + H 2 0
              diff = -4 * m_neutralPhosphoLossNumber;
              break;


            case PeptideIon::astar:
              // m_mass -= MASSH2O;
              // m_mass = - MASSCO - MASSNH3;
              diff = -2;
              break;
            case PeptideIon::ao:
              // m_mass -= MASSH2O;
              // m_mass = - MASSCO - MASSH2O;
              diff = -3;
              break;
            case PeptideIon::x:
              // +MASSCO + MASSOXYGEN
              diff = +1;
              break;
            default:
              throw PappsoException(
                QObject::tr("PeptideIon name not implemented"));
              break;
          }
        break;
      case AtomIsotopeSurvey::S:
        break;
      default:
        qDebug() << "PeptideFragmentIon::getNumberOfAtom(AtomIsotopeSurvey "
                    "atom) NOT IMPLEMENTED";
    }
  return number + diff;
}


int
PeptideFragmentIon::getNumberOfIsotope(Isotope isotope) const
{
  int number = msp_fragment.get()->getNumberOfIsotope(isotope);
  return number;
}

PeptideFragmentIonSp
PeptideFragmentIon::makePeptideFragmentIonSp() const
{
  return std::make_shared<PeptideFragmentIon>(*this);
}


unsigned int
PeptideFragmentIon::size() const
{
  return msp_fragment.get()->size();
}

const QString
PeptideFragmentIon::getSequence() const
{
  return msp_fragment.get()->getSequence();
}

const PeptideFragmentSp &
PeptideFragmentIon::getPeptideFragmentSp() const
{
  return msp_fragment;
}

const QString
PeptideFragmentIon::getName() const
{
  return QString("%1-%2").arg(getPeptideIonName(m_ionType)).arg(size());
}

pappso_double
PeptideFragmentIon::getMass() const
{
  return m_mass;
}

PeptideIon
PeptideFragmentIon::getPeptideIonType() const
{
  return m_ionType;
}
PeptideDirection
PeptideFragmentIon::getPeptideIonDirection() const
{
  return msp_fragment.get()->getPeptideIonDirection();
}
unsigned int
PeptideFragmentIon::getNumberOfNeutralPhosphoLoss() const
{
  return m_neutralPhosphoLossNumber;
}

bool
PeptideFragmentIon::isPalindrome() const
{
  return msp_fragment.get()->isPalindrome();
}


} // namespace pappso
