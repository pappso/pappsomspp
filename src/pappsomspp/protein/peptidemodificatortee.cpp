
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "peptidemodificatortee.h"

using namespace pappso;
PeptideModificatorTee::PeptideModificatorTee()
{
}

PeptideModificatorTee::PeptideModificatorTee(const PeptideModificatorTee &other)
{
  throw PappsoException(
    QObject::tr("unable to copy PeptideModificatorTee object"));
  m_peptideModPtrList = other.m_peptideModPtrList;
}
PeptideModificatorTee::~PeptideModificatorTee()
{
}

void
PeptideModificatorTee::addModificator(
  PeptideModificatorInterface *p_peptide_mod)
{
  m_peptideModPtrList.push_back(p_peptide_mod);
}


void
PeptideModificatorTee::setPeptideSp(std::int8_t sequence_database_id,
                                    const ProteinSp &protein_sp,
                                    bool is_decoy,
                                    const PeptideSp &peptide_sp_original,
                                    unsigned int start,
                                    bool is_nter,
                                    unsigned int missed_cleavage_number,
                                    bool semi_enzyme)
{

  qDebug() << "PeptideModificatorTee::setPeptide begin";

  for(auto p_peptide_mod : m_peptideModPtrList)
    {
      p_peptide_mod->setPeptideSp(sequence_database_id,
                                  protein_sp,
                                  is_decoy,
                                  peptide_sp_original,
                                  start,
                                  is_nter,
                                  missed_cleavage_number,
                                  semi_enzyme);
    }

  qDebug() << "PeptideModificatorTee::setPeptide end";
}
