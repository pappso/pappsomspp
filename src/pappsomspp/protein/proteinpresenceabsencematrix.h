/**
 * \file protein/proteinpresenceabsencematrix.h
 * \date 07/02/2024
 * \author Olivier Langella
 * \brief presence/absence matrix of amino acid code along the protein sequence
 */

/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of PAPPSOms++.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../exportinmportconfig.h"
#include <vector>
#include <boost/numeric/ublas/matrix.hpp>

namespace pappso
{

class ProteinIntegerCode;

/**
 * @brief
 */
class PMSPP_LIB_DECL ProteinPresenceAbsenceMatrix
{
  public:
  /**
   * Default constructor
   */
  ProteinPresenceAbsenceMatrix();


  /**
   * copy constructor
   */
  ProteinPresenceAbsenceMatrix(const ProteinPresenceAbsenceMatrix &other);

  /**
   * Destructor
   */
  virtual ~ProteinPresenceAbsenceMatrix();

  ProteinPresenceAbsenceMatrix &
  operator=(const ProteinPresenceAbsenceMatrix &other);

  void fillMatrix(const pappso::ProteinIntegerCode &coded_protein,
                  const std::vector<uint32_t> &code_list_from_spectrum);

  /** @brief process convolution of spectrum code list along protein sequence
   */
  std::vector<double> convolution() const;

  enum class ProteinPresenceAbsenceMatrixElement : std::uint8_t
  {
    absent  = 0, ///< the aa sequence code is not present
    present = 1, ///< the aa sequence code is present
  };


  const boost::numeric::ublas::matrix<
    pappso::ProteinPresenceAbsenceMatrix::ProteinPresenceAbsenceMatrixElement> &
  getPresenceAbsenceMatrix() const;


  private:
  double convolutionKernel(std::size_t position) const;
  double getScore(std::size_t seq_position, std::size_t aa_fragment_size) const;


  private:
  boost::numeric::ublas::matrix<ProteinPresenceAbsenceMatrixElement>
    m_presenceAbsenceMatrix;
};
} // namespace pappso
