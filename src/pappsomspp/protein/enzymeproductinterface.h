
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "protein.h"
#include "../peptide/peptide.h"
#include <QStringList>

namespace pappso
{
class EnzymeProductInterface
{
  public:
  /** @brief function to give the products of a protein digestion by an enzyme
   * @param sequence_database_id integer that references the sequence fatabase
   * (file, stream, url...)
   * @param protein_sp shared pointer on the protein that was digested
   * @param is_decoy tell if the current protein is a decoy (true) or normal
   * (false) protein
   * @param peptide amino acid sequence of the peptide (string) produced by the
   * digestion
   * @param start the position of the first amino acid of the peptide in the
   * original protein sequence. the first amino acid of the protein is at
   * position 1.
   * @param is_nter boolean to tell if the peptide is an Nter peptide (to allow
   * Methionin Nter removal)
   * @param missed_cleavage_number number of missed cleavage sites (that the
   * enzyme has not cut) fot the product
   * @param semi_enzyme boolean that tells if this peptide is the produce of a
   * semi enzymatic lysis
   * */
  virtual void setPeptide(std::int8_t sequence_database_id,
                          const ProteinSp &protein_sp,
                          bool is_decoy,
                          const PeptideStr &peptide,
                          unsigned int start,
                          bool is_nter,
                          unsigned int missed_cleavage_number,
                          bool semi_enzyme) = 0;
};

class PeptideModificatorInterface
{
  public:
  virtual ~PeptideModificatorInterface(){};
  /** @brief function to give the products of modifications for a digested
   * peptide
   * @param sequence_database_id integer that references the sequence fatabase
   * (file, stream, url...)
   * @param protein_sp shared pointer on the
   * protein that was initialy digested
   * @param is_decoy tell if the current
   * protein is a decoy (true) or normal (false) protein
   * @param peptide Peptide
   * object containing sequence and possible modifications
   * @param start the
   * position of the first amino acid of the peptide in the original protein
   * sequence. the first amino acid of the protein is at position 1.
   * @param
   * is_nter boolean to tell if the peptide is an Nter peptide (to allow
   * Methionin Nter removal)
   * @param missed_cleavage_number number of missed
   * cleavage sites (that the enzyme has not cut) fot the product
   * @param
   * semi_enzyme boolean that tells if this peptide is the produce of a semi
   * enzymatic lysis
   * */
  virtual void setPeptideSp(std::int8_t sequence_database_id,
                            const ProteinSp &protein_sp,
                            bool is_decoy,
                            const PeptideSp &peptide_sp,
                            unsigned int start,
                            bool is_nter,
                            unsigned int missed_cleavage_number,
                            bool semi_enzyme) = 0;
};

class PeptideSinkInterface
{
  public:
  virtual void setSink(EnzymeProductInterface *sink) = 0;
};


class PeptideSpSinkInterface
{
  public:
  virtual void setSink(PeptideModificatorInterface *sink) = 0;
};
} // namespace pappso
