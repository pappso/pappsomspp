
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "peptidefixedmodificationbuilder.h"

using namespace pappso;

PeptideFixedModificationBuilder::PeptideFixedModificationBuilder(
  AaModificationP p_mod)
  : mp_mod(p_mod)
{
}

PeptideFixedModificationBuilder::~PeptideFixedModificationBuilder()
{
}

void
PeptideFixedModificationBuilder::addAa(char aa)
{


  m_aaModificationList.append(aa);

  m_pattern.setPattern(QString("[%1]").arg(m_aaModificationList));
}


void
PeptideFixedModificationBuilder::setPeptideSp(
  std::int8_t sequence_database_id,
  const ProteinSp &protein_sp,
  bool is_decoy,
  const PeptideSp &peptide_sp_original,
  unsigned int start,
  bool is_nter,
  unsigned int missed_cleavage_number,
  bool semi_enzyme)
{

  qDebug() << "PeptideFixedModificationBuilder::setPeptide begin";

  std::vector<unsigned int> position_list;
  getModificationPositionList(position_list,
                              peptide_sp_original.get()->getSequence());

  Peptide peptide(*peptide_sp_original.get());
  unsigned int size = peptide.size();

  for(unsigned int pos : position_list)
    {
      if((m_isProtNterMod) && (is_nter))
        {
          peptide.addAaModification(mp_mod, pos);
        }
      else if((m_isProtCterMod) && (protein_sp.get()->size() == (start + size)))
        {
          peptide.addAaModification(mp_mod, pos);
        }
      else if(m_isProtElseMod)
        {
          peptide.addAaModification(mp_mod, pos);
        }
    }

  PeptideSp peptide_sp = peptide.makePeptideSp();
  // set fixed modifications
  m_sink->setPeptideSp(sequence_database_id,
                       protein_sp,
                       is_decoy,
                       peptide_sp,
                       start,
                       is_nter,
                       missed_cleavage_number,
                       semi_enzyme);
  qDebug() << "PeptideFixedModificationBuilder::setPeptide end";
}
