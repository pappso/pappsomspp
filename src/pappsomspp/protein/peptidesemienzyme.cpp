
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "peptidesemienzyme.h"
#include "../exception/exceptionnotpossible.h"

namespace pappso
{
PeptideSemiEnzyme::PeptideSemiEnzyme()
{
}

PeptideSemiEnzyme::~PeptideSemiEnzyme()
{
}

void
PeptideSemiEnzyme::setPeptide(std::int8_t sequence_database_id,
                              const ProteinSp &protein_sp,
                              bool is_decoy,
                              const QString &peptide,
                              unsigned int start,
                              bool is_nter,
                              unsigned int missed_cleavage_number,
                              bool semi_enzyme)
{
  // if (start == 1) {
  //    m_sink->setPeptide(protein_sp, peptide, start);
  // 2 begin
  // size = 6
  // i=5

  //}
  if(semi_enzyme)
    {
      throw ExceptionNotPossible(
        QObject::tr("ERROR : semi_ensyme flag is true.\n This is not possible, "
                    "because only PeptideSemiEnzyme is autorized to set it "
                    "true, and this can not be used several times."));
    }

  m_sink->setPeptide(sequence_database_id,
                     protein_sp,
                     is_decoy,
                     peptide,
                     start,
                     is_nter,
                     missed_cleavage_number,
                     false); // transmits orignal peptide
  unsigned int max = peptide.size();
  // unsigned int maxi = max-1;
  for(unsigned int i = 1; i < max; i++)
    {
      m_sink->setPeptide(sequence_database_id,
                         protein_sp,
                         is_decoy,
                         peptide.left(i),
                         start,
                         is_nter,
                         missed_cleavage_number,
                         true);
      m_sink->setPeptide(sequence_database_id,
                         protein_sp,
                         is_decoy,
                         peptide.right(i),
                         start + (max - i),
                         false,
                         missed_cleavage_number,
                         true);
    }
}
} // namespace pappso
