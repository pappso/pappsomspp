
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#include "peptidemethioninremove.h"
namespace pappso
{
PeptideMethioninRemove::PeptideMethioninRemove(bool ism_isPotential)
{
  m_isPotential = ism_isPotential;
}

PeptideMethioninRemove::~PeptideMethioninRemove()
{
}

void
PeptideMethioninRemove::setPeptide(std::int8_t sequence_database_id,
                                   const ProteinSp &protein_sp,
                                   bool is_decoy,
                                   const QString &peptide,
                                   unsigned int start,
                                   bool is_nter,
                                   unsigned int missed_cleavage_number,
                                   bool semi_enzyme)
{
  if(is_nter)
    {
      // Nter is often acetylated : MOD:00394
      if(peptide[0] == 'M')
        {
          if(m_isPotential)
            {
              m_sink->setPeptide(sequence_database_id,
                                 protein_sp,
                                 is_decoy,
                                 peptide,
                                 start,
                                 is_nter,
                                 missed_cleavage_number,
                                 semi_enzyme);
            }
          // peptide = peptide.right(peptide.size()-1);
          m_sink->setPeptide(sequence_database_id,
                             protein_sp,
                             is_decoy,
                             peptide.right(peptide.size() - 1),
                             start + 1,
                             is_nter,
                             missed_cleavage_number,
                             semi_enzyme);
        }
      else
        {
          m_sink->setPeptide(sequence_database_id,
                             protein_sp,
                             is_decoy,
                             peptide,
                             start,
                             is_nter,
                             missed_cleavage_number,
                             semi_enzyme);
        }
    }
  else
    {

      m_sink->setPeptide(sequence_database_id,
                         protein_sp,
                         is_decoy,
                         peptide,
                         start,
                         is_nter,
                         missed_cleavage_number,
                         semi_enzyme);
    }
}
} // namespace pappso
