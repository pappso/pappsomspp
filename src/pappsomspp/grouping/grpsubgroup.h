/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once


#include <memory>
#include <vector>
#include "grpprotein.h"
#include "grppeptideset.h"


namespace pappso
{

class GrpSubGroup;
typedef std::shared_ptr<const GrpSubGroup> GrpSubGroupSpConst;
typedef std::shared_ptr<GrpSubGroup> GrpSubGroupSp;


class GrpSubGroup
{
  private:
  std::vector<GrpProtein *> m_grpProteinPtrList;
  GrpPeptideSet m_peptidePtrList;
  unsigned int m_subGroupNumber = 0;
  unsigned int m_groupNumber    = 0;

  public:
  GrpSubGroup(GrpProtein *p_protein);
  GrpSubGroup(const GrpSubGroup &other);


  /** \brief sort subgroups between each other
   * a subgroup containing less peptides is weaker (less) than the other
   */
  bool operator<(const GrpSubGroup &other) const;
  ~GrpSubGroup();
  GrpSubGroupSp makeGrpSubGroupSp();
  const GrpPeptideSet &getPeptideSet() const;
  bool merge(GrpSubGroup *p_subgroup);
  bool includes(const GrpSubGroup *p_subgroup) const;
  void numbering();
  void setSubGroupNumber(unsigned int i);
  void setGroupNumber(unsigned int i);
  const QString &getFirstAccession() const;

  std::size_t peptideListSize() const;
  unsigned int maxCount() const;


  const QString getGroupingId() const;
  unsigned int getGroupNumber() const;
  unsigned int getSubGroupNumber() const;

  const std::vector<GrpProtein *> &getGrpProteinList() const;
};


} // namespace pappso
