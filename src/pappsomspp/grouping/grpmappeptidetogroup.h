/**
 * \file pappsomspp/grouping/grpmappeptidegroup.h
 * \date 15/12/2017
 * \author Olivier Langella
 * \brief keep trace of peptide to group assignment
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef GRPMAPPEPTIDETOGROUP_H
#define GRPMAPPEPTIDETOGROUP_H

#include <map>

#include "grppeptideset.h"
#include "grpgroup.h"

namespace pappso
{

class GrpMapPeptideToGroup
{
  private:
  std::map<GrpPeptide *, GrpGroupSp> m_mapPeptideToGroup;

  public:
  GrpMapPeptideToGroup();
  GrpMapPeptideToGroup(const GrpMapPeptideToGroup &other);
  ~GrpMapPeptideToGroup();

  /** @brief get all groups concerned by a list of peptides
   */
  void getGroupList(const GrpPeptideSet &peptide_set_in,
                    std::list<GrpGroupSp> &impacted_group_list) const;

  /** @brief set peptide keys pointing on the group
   */
  void set(const GrpPeptideSet &peptide_set_in, GrpGroupSp grp_group);

  void clear(std::list<GrpGroupSp> &grp_group_list);
};
} // namespace pappso
#endif // GRPMAPPEPTIDETOGROUP_H
