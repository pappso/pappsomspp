/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <map>

#include "grppeptide.h"
#include "grpsubgroupset.h"
#include "grppeptideset.h"

namespace pappso
{


class GrpMapPeptideToSubGroupSet
{
  private:
  std::map<GrpPeptide *, GrpSubGroupSet> m_mapPeptideToSubGroupSet;

  public:
  GrpMapPeptideToSubGroupSet();
  GrpMapPeptideToSubGroupSet(const GrpMapPeptideToSubGroupSet &other);
  ~GrpMapPeptideToSubGroupSet();

  /** @brief get all subgroups concerned by a list of peptides
   */
  void getSubGroupSet(const GrpPeptideSet &peptide_set_in,
                      GrpSubGroupSet &impacted_subgroup_set) const;

  /** @brief removes in the map all references of the group to remove
   * (p_remove_sub_group)
   */
  void remove(GrpSubGroup *p_remove_sub_group);

  /** @brief add in the map all peptides of the subgroup to add
   */
  void add(GrpSubGroup *p_add_sub_group);

  /** @brief tells if this subgroup contains a specific peptide
   */
  bool hasSpecificPeptide(const GrpSubGroup *get) const;

  /** @brief check function only usefull for testing purpose
   */
  void check(std::list<GrpSubGroupSp> &m_grpSubGroupSpList) const;

  unsigned int size() const;
  const QString printInfos() const;
};

} // namespace pappso
