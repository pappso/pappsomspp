BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "GlobalMetadata" (
	"Key"	TEXT,
	"Value"	TEXT,
	PRIMARY KEY("Key")
);
CREATE TABLE IF NOT EXISTS "PropertyDefinitions" (
	"Id"	INTEGER,
	"PermanentName"	TEXT NOT NULL,
	"Type"	INTEGER NOT NULL,
	"DisplayGroupName"	TEXT NOT NULL,
	"DisplayName"	TEXT NOT NULL,
	"DisplayValueText"	TEXT NOT NULL,
	"DisplayFormat"	TEXT NOT NULL,
	"DisplayDimension"	TEXT NOT NULL,
	"Description"	TEXT NOT NULL,
	PRIMARY KEY("Id")
);
CREATE TABLE IF NOT EXISTS "PropertyGroups" (
	"Id"	INTEGER,
	PRIMARY KEY("Id")
) WITHOUT ROWID;
CREATE TABLE IF NOT EXISTS "GroupProperties" (
	"PropertyGroup"	INTEGER NOT NULL,
	"Property"	INTEGER NOT NULL,
	"Value"	 NOT NULL,
	FOREIGN KEY("Property") REFERENCES "PropertyDefinitions"("Id"),
	PRIMARY KEY("PropertyGroup","Property"),
	FOREIGN KEY("PropertyGroup") REFERENCES "PropertyGroups"("Id")
) WITHOUT ROWID;
CREATE TABLE IF NOT EXISTS "FrameProperties" (
	"Frame"	INTEGER NOT NULL,
	"Property"	INTEGER NOT NULL,
	"Value"	 NOT NULL,
	FOREIGN KEY("Frame") REFERENCES "Frames"("Id"),
	FOREIGN KEY("Property") REFERENCES "PropertyDefinitions"("Id"),
	PRIMARY KEY("Frame","Property")
) WITHOUT ROWID;
CREATE TABLE IF NOT EXISTS "FrameMsMsInfo" (
	"Frame"	INTEGER,
	"Parent"	INTEGER,
	"TriggerMass"	REAL NOT NULL,
	"IsolationWidth"	REAL NOT NULL,
	"PrecursorCharge"	INTEGER,
	"CollisionEnergy"	REAL NOT NULL,
	FOREIGN KEY("Frame") REFERENCES "Frames"("Id"),
	PRIMARY KEY("Frame")
);
CREATE TABLE IF NOT EXISTS "CalibrationInfo" (
	"KeyPolarity"	CHAR(1) CHECK("KeyPolarity" IN ('+', '-')),
	"KeyName"	TEXT,
	"Value"	TEXT,
	PRIMARY KEY("KeyPolarity","KeyName")
);
CREATE TABLE IF NOT EXISTS "Frames" (
	"Id"	INTEGER,
	"Time"	REAL NOT NULL,
	"Polarity"	CHAR(1) NOT NULL CHECK("Polarity" IN ('+', '-')),
	"ScanMode"	INTEGER NOT NULL,
	"MsMsType"	INTEGER NOT NULL,
	"TimsId"	INTEGER,
	"MaxIntensity"	INTEGER NOT NULL,
	"SummedIntensities"	INTEGER NOT NULL,
	"NumScans"	INTEGER NOT NULL,
	"NumPeaks"	INTEGER NOT NULL,
	"MzCalibration"	INTEGER NOT NULL,
	"T1"	REAL NOT NULL,
	"T2"	REAL NOT NULL,
	"TimsCalibration"	INTEGER NOT NULL,
	"PropertyGroup"	INTEGER,
	"AccumulationTime"	REAL NOT NULL,
	"RampTime"	REAL NOT NULL,
	PRIMARY KEY("Id"),
	FOREIGN KEY("PropertyGroup") REFERENCES "PropertyGroups"("Id"),
	FOREIGN KEY("TimsCalibration") REFERENCES "TimsCalibration"("Id"),
	FOREIGN KEY("MzCalibration") REFERENCES "MzCalibration"("Id")
);
CREATE TABLE IF NOT EXISTS "Segments" (
	"Id"	INTEGER,
	"FirstFrame"	INTEGER NOT NULL,
	"LastFrame"	INTEGER NOT NULL,
	"IsCalibrationSegment"	BOOLEAN NOT NULL,
	FOREIGN KEY("LastFrame") REFERENCES "Frames"("Id"),
	PRIMARY KEY("Id"),
	FOREIGN KEY("FirstFrame") REFERENCES "Frames"("Id")
);
CREATE TABLE IF NOT EXISTS "ErrorLog" (
	"Frame"	INTEGER NOT NULL,
	"Scan"	INTEGER,
	"Message"	TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS "MzCalibration" (
	"Id"	INTEGER,
	"ModelType"	INTEGER NOT NULL,
	"DigitizerTimebase"	REAL NOT NULL,
	"DigitizerDelay"	REAL NOT NULL,
	"T1"	REAL NOT NULL,
	"T2"	REAL NOT NULL,
	"dC1"	REAL NOT NULL,
	"dC2"	REAL NOT NULL,
	"C0"	,
	"C1"	,
	"C2"	,
	"C3"	,
	"C4"	,
	PRIMARY KEY("Id")
);
CREATE TABLE IF NOT EXISTS "TimsCalibration" (
	"Id"	INTEGER,
	"ModelType"	INTEGER NOT NULL,
	"C0"	,
	"C1"	,
	"C2"	,
	"C3"	,
	"C4"	,
	"C5"	,
	"C6"	,
	"C7"	,
	"C8"	,
	"C9"	,
	PRIMARY KEY("Id")
);
CREATE TABLE IF NOT EXISTS "DiaFrameMsMsWindowGroups" (
	"Id"	INTEGER,
	PRIMARY KEY("Id")
);
CREATE TABLE IF NOT EXISTS "DiaFrameMsMsWindows" (
	"WindowGroup"	INTEGER NOT NULL,
	"ScanNumBegin"	INTEGER NOT NULL,
	"ScanNumEnd"	INTEGER NOT NULL,
	"IsolationMz"	REAL NOT NULL,
	"IsolationWidth"	REAL NOT NULL,
	"CollisionEnergy"	REAL NOT NULL,
	FOREIGN KEY("WindowGroup") REFERENCES "FrameMsMsWindowGroups"("Id"),
	PRIMARY KEY("WindowGroup","ScanNumBegin")
) WITHOUT ROWID;
CREATE TABLE IF NOT EXISTS "DiaFrameMsMsInfo" (
	"Frame"	INTEGER,
	"WindowGroup"	INTEGER NOT NULL,
	PRIMARY KEY("Frame"),
	FOREIGN KEY("Frame") REFERENCES "Frames"("Id"),
	FOREIGN KEY("WindowGroup") REFERENCES "FrameMsMsWindowGroups"("Id")
);
CREATE TABLE IF NOT EXISTS "Precursors" (
	"Id"	INTEGER,
	"LargestPeakMz"	REAL NOT NULL,
	"AverageMz"	REAL NOT NULL,
	"MonoisotopicMz"	REAL,
	"Charge"	INTEGER,
	"ScanNumber"	REAL NOT NULL,
	"Intensity"	REAL NOT NULL,
	"Parent"	INTEGER,
	PRIMARY KEY("Id"),
	FOREIGN KEY("Parent") REFERENCES "Frames"("Id")
);
CREATE TABLE IF NOT EXISTS "PasefFrameMsMsInfo" (
	"Frame"	INTEGER NOT NULL,
	"ScanNumBegin"	INTEGER NOT NULL,
	"ScanNumEnd"	INTEGER NOT NULL,
	"IsolationMz"	REAL NOT NULL,
	"IsolationWidth"	REAL NOT NULL,
	"CollisionEnergy"	REAL NOT NULL,
	"Precursor"	INTEGER,
	FOREIGN KEY("Precursor") REFERENCES "Precursors"("Id"),
	FOREIGN KEY("Frame") REFERENCES "Frames"("Id"),
	PRIMARY KEY("Frame","ScanNumBegin")
) WITHOUT ROWID;
CREATE UNIQUE INDEX IF NOT EXISTS "PropertyDefinitionsIndex" ON "PropertyDefinitions" (
	"PermanentName"
);
CREATE UNIQUE INDEX IF NOT EXISTS "FramesTimeIndex" ON "Frames" (
	"Time"
);
CREATE INDEX IF NOT EXISTS "PrecursorsParentIndex" ON "Precursors" (
	"Parent"
);
CREATE INDEX IF NOT EXISTS "PasefFrameMsMsInfoPrecursorIndex" ON "PasefFrameMsMsInfo" (
	"Precursor"
);
CREATE VIEW Properties AS
    SELECT s.Id Frame, pd.Id Property, COALESCE(fp.Value, gp.Value) Value
    FROM Frames s
    JOIN PropertyDefinitions pd
    LEFT JOIN GroupProperties gp ON gp.PropertyGroup=s.PropertyGroup AND gp.Property=pd.Id
    LEFT JOIN FrameProperties fp ON fp.Frame=s.Id AND fp.Property=pd.Id;
COMMIT;
