# File:///home/langella/developpement/git/pappsomspp/CMakeStuff/toolchains/mxe-toolchain.cmake# 
# This file should be included if the command line reads like this:
# x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 -DHOME_DEVEL_DIR=/ ..
# export PATH=/backup2/mxeqt6/usr/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/home/langella/.dotnet/tools
# /backup2/mxeqt6/usr/bin/x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXEOLIVIER=1 ..

message("MXE (M cross environment) https://mxe.cc/")
message("Please run the configuration like this:")
message("x86_64-w64-mingw32.shared-cmake -DMXE= 1 -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../../development")

set(HOME_DEVEL_DIR /win64)
set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /backup2/mxeqt6/usr/x86_64-w64-mingw32.shared/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /backup2/mxeqt6/usr/x86_64-w64-mingw32.shared/include)


if(WIN32 OR _WIN32)
	message(STATUS "Building with WIN32 defined.")
endif()

message(STATUS "${BoldGreen}Setting definition -DPMSPP_LIBRARY for symbol DLL export.${ColourReset}")
add_definitions(-DPMSPP_LIBRARY)

find_package(Qt6 COMPONENTS Widgets Core Gui PrintSupport Svg Xml Sql Concurrent Core5Compat REQUIRED)


find_package(ZLIB REQUIRED)

find_package(SQLite3 REQUIRED)


find_package(HDF5 COMPONENTS CXX)


#install MXE qtsvg package
#langella@piccolo:/media/langella/pappso/mxe$ make qtsvg


set(QCustomPlotQt6_FOUND 1)
set(QCustomPlotQt6_INCLUDE_DIR "/home/langella/developpement/git/qcustomplot-qt6")
set(QCustomPlotQt6_LIBRARIES "/win64/mxeqt6_dll/libQCustomPlotQt6.dll")
if(NOT TARGET QCustomPlotQt6::QCustomPlotQt6)
	add_library(QCustomPlotQt6::QCustomPlotQt6 UNKNOWN IMPORTED)
	set_target_properties(QCustomPlotQt6::QCustomPlotQt6 PROPERTIES
		IMPORTED_LOCATION             "${QCustomPlotQt6_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlotQt6_INCLUDE_DIR}"
		INTERFACE_COMPILE_DEFINITIONS QCUSTOMPLOT_USE_LIBRARY)
endif()


	set(Quazip_FOUND 1)
    set(QUAZIP_INCLUDE_DIR "/backup2/win64qt6/libquazip1-qt6-1.4/quazip")
    set(QUAZIP_LIBRARIES "/backup2/win64qt6/libquazip1-qt6-1.4/build/quazip/libquazip1-qt6.dll")
	if(NOT TARGET Quazip::Quazip)
		add_library(Quazip::Quazip UNKNOWN IMPORTED)
		set_target_properties(Quazip::Quazip PROPERTIES
			IMPORTED_LOCATION             "${QUAZIP_LIBRARIES}"
			INTERFACE_INCLUDE_DIRECTORIES "${QUAZIP_INCLUDE_DIR}")
	endif()

set(OdsStream_FOUND 1)
set(OdsStream_INCLUDE_DIRS "/home/langella/developpement/git/libodsstream/src")
set(OdsStream_LIBRARY "/home/langella/developpement/git/libodsstream/wbuild/src/libodsstream.dll")

if(NOT TARGET OdsStream::Core)
	add_library(OdsStream::Core UNKNOWN IMPORTED)
	set_target_properties(OdsStream::Core PROPERTIES
		IMPORTED_LOCATION "${OdsStream_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${OdsStream_INCLUDE_DIRS}"
		)
endif()



# All this belly dance does not seem necessary. Just perform like for the other
# libraries...
# Look for the necessary header
set(Zstd_INCLUDE_DIR /backup2/mxeqt6/usr/x86_64-w64-mingw32.shared/include)
mark_as_advanced(Zstd_INCLUDE_DIR)
set(Zstd_INCLUDE_DIRS ${Zstd_INCLUDE_DIR})
# Look for the necessary library
set(Zstd_LIBRARY /backup2/mxeqt6/usr/x86_64-w64-mingw32.shared/bin/libzstd.dll)
mark_as_advanced(Zstd_LIBRARY)
# Mark the lib as found
set(Zstd_FOUND 1)
set(Zstd_LIBRARIES ${Zstd_LIBRARY})
if(NOT TARGET Zstd::Zstd)
	add_library(Zstd::Zstd UNKNOWN IMPORTED)
	set_target_properties(Zstd::Zstd PROPERTIES
		IMPORTED_LOCATION             "${Zstd_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${Zstd_INCLUDE_DIR}")
endif()




set(PwizLite_FOUND 1)
set(PwizLite_INCLUDE_DIRS "/home/langella/developpement/git/libpwizlite/src")
set(PwizLite_LIBRARIES "/home/langella/developpement/git/libpwizlite/wbuild/src/libpwizlite.dll")
if(NOT TARGET PwizLite::PwizLite)
    add_library(PwizLite::PwizLite UNKNOWN IMPORTED)
    set_target_properties(PwizLite::PwizLite PROPERTIES
        IMPORTED_LOCATION             "${PwizLite_LIBRARIES}"
        INTERFACE_INCLUDE_DIRECTORIES "${PwizLite_INCLUDE_DIRS}"
    )
endif()


# langella@themis:/win64/liblzf-3.6$ x86_64-w64-mingw32.shared-gcc -c lzf_c.c lzfP.h
# langella@themis:/win64/liblzf-3.6$ x86_64-w64-mingw32.shared-gcc -c lzf_d.c lzfP.h
# langella@themis:/win64/liblzf-3.6$ x86_64-w64-mingw32.shared-gcc -shared -o liblzf.dll lzf_c.o lzf_d.o
set(liblzf_FOUND 1)
set(liblzf_INCLUDE_DIR "/backup2/win64qt6/liblzf-3.6")
set(liblzf_INCLUDE_DIRS "/backup2/win64qt6/liblzf-3.6/")

set(liblzf_LIBRARIES "/backup2/win64qt6/liblzf-3.6/liblzf.dll")
if(NOT TARGET liblzf::liblzf)
	add_library(liblzf::liblzf UNKNOWN IMPORTED)
	set_target_properties(liblzf::liblzf PROPERTIES
		IMPORTED_LOCATION             "${liblzf_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${liblzf_INCLUDE_DIRS}"
		INTERFACE_COMPILE_DEFINITIONS LIBLZF_USE_LIBRARY)
endif()

