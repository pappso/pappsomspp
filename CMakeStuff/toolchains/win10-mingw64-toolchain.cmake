message("\n${BoldRed}WIN10-MINGW64 environment${ColourReset}\n")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../development")


set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/include")
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/bin")

set(HOME_DEVEL_DIR "$ENV{HOME}/devel")


# We do not build the tests under Win10.
set (MAKE_TEST 0)


if(WIN32 OR _WIN32)
	message(STATUS "Building with WIN32 defined.")
endif()

# see https://cmake.org/pipermail/cmake/2015-December/062166.html
set(CMAKE_NO_SYSTEM_FROM_IMPORTED 1)

set(LINKER_FLAGS "${LINKER_FLAGS} -Wl,--no-as-needed")


find_package(Qt6 COMPONENTS Widgets Core Gui PrintSupport Svg Xml Sql Concurrent Core5Compat REQUIRED)


find_package(ZLIB REQUIRED)


set(liblzf_FOUND 1)
set(liblzf_INCLUDE_DIRS "${HOME_DEVEL_DIR}/lzf/development")
set(liblzf_LIBRARIES "${HOME_DEVEL_DIR}/lzf/build-area/mingw64/liblzf.dll")
if(NOT TARGET liblzf::liblzf)
	add_library(liblzf::liblzf UNKNOWN IMPORTED)
	set_target_properties(liblzf::liblzf PROPERTIES
		IMPORTED_LOCATION             "${liblzf_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${liblzf_INCLUDE_DIRS}"
		)
endif()


set(PwizLite_FOUND 1)
set(PwizLite_INCLUDE_DIRS "${HOME_DEVEL_DIR}/pwizlite/development/src")
set(PwizLite_LIBRARIES "${HOME_DEVEL_DIR}/pwizlite/build-area/mingw64/src/libpwizlite.dll")
if(NOT TARGET PwizLite::PwizLite)
	add_library(PwizLite::PwizLite UNKNOWN IMPORTED)
	set_target_properties(PwizLite::PwizLite PROPERTIES
		IMPORTED_LOCATION             "${PwizLite_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${PwizLite_INCLUDE_DIRS}"
		)
endif()


set(QCustomPlotQt6_FOUND 1)
set(QCustomPlotQt6_INCLUDE_DIRS "${HOME_DEVEL_DIR}/qcustomplot/development")
# Note the QCustomPlotQt6_LIBRARIES (plural) because on Debian, the
# QCustomPlotQt6Config.cmake file has this variable name (see the unix-specific
# toolchain file.
set(QCustomPlotQt6_LIBRARIES "${HOME_DEVEL_DIR}/qcustomplot/build-area/mingw64/libQCustomPlotQt6.dll")
# Per instructions of the lib author:
# https://www.qcustomplot.com/index.php/tutorials/settingup
message(STATUS "Setting definition -DQCUSTOMPLOT_USE_LIBRARY.")
if(NOT TARGET QCustomPlotQt6::QCustomPlotQt6)
	add_library(QCustomPlotQt6::QCustomPlotQt6 UNKNOWN IMPORTED)
	set_target_properties(QCustomPlotQt6::QCustomPlotQt6 PROPERTIES
		IMPORTED_LOCATION             "${QCustomPlotQt6_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlotQt6_INCLUDE_DIRS}"
		INTERFACE_COMPILE_DEFINITIONS QCUSTOMPLOT_USE_LIBRARY
		)
endif()


find_package(Boost COMPONENTS iostreams thread filesystem chrono REQUIRED )

find_package(Zstd REQUIRED)


message(STATUS "Add HDF5 libs as a win64-specific link-time dependency.")
set(hdf5_FOUND 1)
set(hdf5_INCLUDE_DIRS "C:/msys64/ucrt64/include")

add_library(hdf5::generic UNKNOWN IMPORTED)
  set_target_properties(hdf5::generic PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES ${hdf5_INCLUDE_DIRS}
    IMPORTED_LOCATION "C:/msys64/ucrt64/bin/libhdf5-310.dll")

add_library(hdf5::cxx UNKNOWN IMPORTED)
  set_target_properties(hdf5::cxx PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES ${hdf5_INCLUDE_DIRS}
    IMPORTED_LOCATION "C:/msys64/ucrt64/bin/libhdf5_cpp-310.dll")

add_library(hdf5::hdf5_cpp INTERFACE IMPORTED)
set_property(TARGET hdf5::hdf5_cpp PROPERTY
  INTERFACE_LINK_LIBRARIES hdf5::generic hdf5::cxx)


# On Win10 all the code is relocatable.
remove_definitions(-fPIC)
