message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)

set(LINKER_FLAGS "-Wl,--no-as-needed")


find_package(Qt6 COMPONENTS Widgets Core Gui PrintSupport Svg Xml Sql Concurrent Core5Compat REQUIRED)

# debian package : libboost-container-dev
find_package(Boost COMPONENTS chrono container filesystem iostreams thread REQUIRED)

find_package(PwizLite REQUIRED)

find_package(ZLIB REQUIRED)

find_package(Zstd REQUIRED)

find_package(liblzf REQUIRED)

find_package(QuaZip-Qt6 REQUIRED)

find_package(OdsStream COMPONENTS Core)


include(FindHDF5)
find_package(HDF5 COMPONENTS CXX)

#message(STATUS "HDF5_LIBRARIES: ${HDF5_LIBRARIES}")
message(STATUS "HDF5_CXX_LIBRARIES: ${HDF5_CXX_LIBRARIES}")
#message(STATUS "HDF5_INCLUDE_DIRS: ${HDF5_INCLUDE_DIRS}")
message(STATUS "HDF5_CXX_INCLUDE_DIRS: ${HDF5_CXX_INCLUDE_DIRS}")


find_package(QCustomPlotQt6 REQUIRED)
# Per instructions of the lib author:
# https://www.qcustomplot.com/index.php/tutorials/settingup
message(STATUS "Setting definition -DQCUSTOMPLOT_USE_LIBRARY.")


find_package(Catch2)
message("Catch2 major version found: " ${Catch2_VERSION_MAJOR})
add_compile_definitions(CATCH2_MAJOR_VERSION_${Catch2_VERSION_MAJOR})



if(MAKE_TESTS)
message(STATUS "MAKE_TESTS: ${MAKE_TESTS}")
# No need to find QuaZip-Qt6 because OdsStream had done that already.
# find_package(QuaZip-Qt6 REQUIRED)
endif(MAKE_TESTS)


add_definitions(-fPIC)

message("unix-toolchain.cmake - LOCAL_CMAKE_MODULE_PATH: ${LOCAL_CMAKE_MODULE_PATH}")

# Install the cmake module
message("LOCAL_CMAKE_MODULE_PATH: ${LOCAL_CMAKE_MODULE_PATH}")
install(FILES ${LOCAL_CMAKE_MODULE_PATH}/FindPappsoMSpp.cmake
  DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/pappsomspp)

# Configure the cmake config
configure_file (${LOCAL_CMAKE_MODULE_PATH}/PappsoMSppConfig.cmake.in
  ${CMAKE_BINARY_DIR}/PappsoMSppConfig.cmake)
# Install the cmake config
install(FILES ${CMAKE_BINARY_DIR}/PappsoMSppConfig.cmake
  DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/pappsomspp)

