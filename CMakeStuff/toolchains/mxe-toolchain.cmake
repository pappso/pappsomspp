message("MXE (M cross environment) https://mxe.cc/")
message("Please run the configuration like this:")
message("x86_64-w64-mingw32.shared-cmake -DMXE=1 -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../../development")

set(HOME_DEVEL_DIR "/home/rusconi/devel")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES ${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES ${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/include)


if(WIN32 OR _WIN32)
	message(STATUS "Building with WIN32 defined.")
endif()

message(STATUS "${BoldGreen}Setting definition -DPMSPP_LIBRARY for symbol DLL export.${ColourReset}")
add_definitions(-DPMSPP_LIBRARY)


find_package(Qt6 COMPONENTS Widgets Core Gui PrintSupport Svg Xml Sql Concurrent Core5Compat REQUIRED)

find_package(ZLIB REQUIRED)

find_package(SQLite3 REQUIRED)

find_package(Boost COMPONENTS chrono container filesystem iostreams thread REQUIRED)

find_package(Catch2)
message("Catch2 major version found: " ${Catch2_VERSION_MAJOR})
add_compile_definitions(CATCH2_MAJOR_VERSION_${Catch2_VERSION_MAJOR})


set(QCustomPlotQt6_FOUND 1)
set(QCustomPlotQt6_INCLUDE_DIR "${HOME_DEVEL_DIR}/qcustomplot/development")
set(QCustomPlotQt6_LIBRARIES "${HOME_DEVEL_DIR}/qcustomplot/build-area/mxe/libQCustomPlotQt6.dll")
if(NOT TARGET QCustomPlotQt6::QCustomPlotQt6)
	add_library(QCustomPlotQt6::QCustomPlotQt6 UNKNOWN IMPORTED)
	set_target_properties(QCustomPlotQt6::QCustomPlotQt6 PROPERTIES
		IMPORTED_LOCATION             "${QCustomPlotQt6_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlotQt6_INCLUDE_DIR}"
		INTERFACE_COMPILE_DEFINITIONS QCUSTOMPLOT_USE_LIBRARY)
endif()


if(MAKE_TEST)

# Unfortunately no CMake configuration file is available.
# find_package(QUAZIP REQUIRED)

set(QUAZIP_FOUND 1)
set(QUAZIP_INCLUDE_DIR "${HOME_DEVEL_DIR}/quazip/development/quazip")
set(QUAZIP_LIBRARIES "${HOME_DEVEL_DIR}/quazip/build-area/mxe/quazip/libquazip1-qt6.dll")
set(QUAZIP_ZLIB_INCLUDE_DIR ${ZLIB_INCLUDE_DIRS})
set(QUAZIP_INCLUDE_DIRS ${QUAZIP_INCLUDE_DIR} ${QUAZIP_ZLIB_INCLUDE_DIR})

message(STATUS "QUAZIP_INCLUDE_DIR :${QUAZIP_INCLUDE_DIR}")

if(NOT TARGET QuaZip::QuaZip)
	add_library(QuaZip::QuaZip UNKNOWN IMPORTED)
	set_target_properties(QuaZip::QuaZip PROPERTIES
		IMPORTED_LOCATION             ${QUAZIP_LIBRARIES}
		INTERFACE_INCLUDE_DIRECTORIES ${QUAZIP_INCLUDE_DIR})
endif()


set(OdsStream_FOUND 1)
set(OdsStream_INCLUDE_DIR "${HOME_DEVEL_DIR}/odsstream/development/src")
set(OdsStream_LIBRARIES "${HOME_DEVEL_DIR}/odsstream/build-area/mxe/src/libodsstream.dll")
if(NOT TARGET OdsStream::Core)
	add_library(OdsStream::Core UNKNOWN IMPORTED)
	set_target_properties(OdsStream::Core PROPERTIES
		IMPORTED_LOCATION             "${OdsStream_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${OdsStream_INCLUDE_DIR}"
		)
endif()

endif(MAKE_TEST)


find_package(Zstd REQUIRED)

set(PwizLite_FOUND 1)
set(PwizLite_INCLUDE_DIRS "${HOME_DEVEL_DIR}/pwizlite/development/src")
set(PwizLite_LIBRARIES "${HOME_DEVEL_DIR}/pwizlite/build-area/mxe/src/libpwizlite.dll")
if(NOT TARGET PwizLite::PwizLite)
    add_library(PwizLite::PwizLite UNKNOWN IMPORTED)
    set_target_properties(PwizLite::PwizLite PROPERTIES
        IMPORTED_LOCATION             "${PwizLite_LIBRARIES}"
        INTERFACE_INCLUDE_DIRECTORIES "${PwizLite_INCLUDE_DIRS}"
    )
endif()


set(liblzf_FOUND 1)
set(liblzf_INCLUDE_DIRS "${HOME_DEVEL_DIR}/lzf/development")

set(liblzf_LIBRARIES "${HOME_DEVEL_DIR}/lzf/build-area/mxe/liblzf.dll")
if(NOT TARGET liblzf::liblzf)
	add_library(liblzf::liblzf UNKNOWN IMPORTED)
	set_target_properties(liblzf::liblzf PROPERTIES
		IMPORTED_LOCATION             "${liblzf_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${liblzf_INCLUDE_DIRS}"
		INTERFACE_COMPILE_DEFINITIONS LIBLZF_USE_LIBRARY)
endif()



#include(FindHDF5)
#find_package(HDF5 COMPONENTS CXX)
# Unfortunately, the libraries are not singled out correctly by
# the find_package call above.
# message(STATUS "HDF5_LIBRARIES: ${HDF5_LIBRARIES}")
# message(STATUS "HDF5_CXX_LIBRARIES: ${HDF5_CXX_LIBRARIES}")
# message(STATUS "HDF5_INCLUDE_DIRS: ${HDF5_INCLUDE_DIRS}")
# message(STATUS "HDF5_CXX_INCLUDE_DIRS: ${HDF5_CXX_INCLUDE_DIRS}")
# We need to provide the lib paths manually below.


message(STATUS "Add HDF5 libs as a MXE-specific link-time dependency.")
set(hdf5_FOUND 1)
set(hdf5_INCLUDE_DIRS "${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/include")

add_library(hdf5::generic UNKNOWN IMPORTED)
  set_target_properties(hdf5::generic PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES ${hdf5_INCLUDE_DIRS}
    IMPORTED_LOCATION "${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/bin/hdf5.dll")

add_library(hdf5::cxx UNKNOWN IMPORTED)
  set_target_properties(hdf5::cxx PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES ${hdf5_INCLUDE_DIRS}
    IMPORTED_LOCATION "${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/bin/hdf5_cpp.dll")

add_library(hdf5::hdf5_cpp INTERFACE IMPORTED)
set_property(TARGET hdf5::hdf5_cpp PROPERTY
  INTERFACE_LINK_LIBRARIES hdf5::generic hdf5::cxx)

