find_path(Alglib_INCLUDE_DIR alglibinternal.h
  PATHS /usr/local/include/libalglib /usr/include/libalglib)


find_library(Alglib_LIBRARY NAMES alglib
  HINTS /usr/lib /usr/local/lib)

if(Alglib_INCLUDE_DIR AND Alglib_LIBRARY)

  mark_as_advanced(Alglib_INCLUDE_DIR)
  mark_as_advanced(Alglib_LIBRARY)

  set(Alglib_FOUND TRUE)

endif()

if(Alglib_FOUND)

  if(NOT Alglib_FIND_QUIETLY)
    message(STATUS "Found Alglib_LIBRARY: ${Alglib_LIBRARY}")
  endif()

  if(NOT TARGET Alglib::Alglib)

    add_library(Alglib::Alglib UNKNOWN IMPORTED)

    set_target_properties(Alglib::Alglib PROPERTIES
      IMPORTED_LOCATION             "${Alglib_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES "${Alglib_INCLUDE_DIR}")

  endif()

else()

  if(Alglib_FIND_REQUIRED)

    message(FATAL_ERROR "Could not find libalglib. Please do specify the
    Alglib_INCLUDE_DIR and Alglib_LIBRARY variables using cmake!")

  endif()

endif()

