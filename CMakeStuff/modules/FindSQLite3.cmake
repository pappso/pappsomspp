find_path(SQLite3_INCLUDE_DIR sqlite3.h
  PATHS /usr/local/include /usr/include)


find_library(SQLite3_LIBRARY NAMES sqlite3
  HINTS /usr/lib /usr/local/lib)

if(SQLite3_INCLUDE_DIR AND SQLite3_LIBRARY)

  mark_as_advanced(SQLite3_INCLUDE_DIR)
  mark_as_advanced(SQLite3_LIBRARY)

  set(SQLite3_FOUND TRUE)

endif()

if(SQLite3_FOUND)

  if(NOT SQLite3_FIND_QUIETLY)
    message(STATUS "Found SQLite3_LIBRARY: ${SQLite3_LIBRARY}")
  endif()

  if(NOT TARGET SQLite::SQLite3)

    add_library(SQLite::SQLite3 UNKNOWN IMPORTED)

    set_target_properties(SQLite::SQLite3 PROPERTIES
      IMPORTED_LOCATION             "${SQLite3_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES "${SQLite3_INCLUDE_DIR}")

  endif()

else()

  if(SQLite3_FIND_REQUIRED)

    message(FATAL_ERROR "Could not find libsqlite3. Please do specify the
    SQLite3_INCLUDE_DIR and SQLite3_LIBRARY variables using cmake!")

  endif()

endif()

