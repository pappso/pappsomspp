
#include "common.h"
#include <pappsomspp/exception/exceptionnotrecognized.h>

using namespace pappso;

MassSpectrum
readMgf(const QString &filename)
{
  try
    {
      qDebug();
      MsFileAccessor accessor(filename, "msrun");
      qDebug();

      std::vector<MsRunIdCstSPtr> msrun_ids = accessor.getMsRunIds();
      if(accessor.getFileFormat() == MsDataFormat::unknown)
        {
          throw pappso::ExceptionNotRecognized("file format not recognized");
        }
      if(msrun_ids.size() == 0)
        {
          throw pappso::PappsoException("msrun_ids.size() == 0");
        }
      MsRunReaderSPtr reader =
        accessor.msRunReaderSPtr(msrun_ids.front());
      qDebug() << msrun_ids.front().get()->getXmlId();
      std::cout << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
                << reader->spectrumListSize() << std::endl;
      MassSpectrumSPtr spectrum_sp = reader->massSpectrumSPtr(0);
      std::cout << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
                << spectrum_sp.get()->size() << std::endl;
      // spectrum_sp.get()->debugPrintValues();
      spectrum_sp.get()->sortMz();
      return *(spectrum_sp.get());
    }
  catch(pappso::PappsoException &error)
    {
      std::cout << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                << QString("ERROR reading file %1 :")
                     .arg(filename)
                     .toStdString()
                     .c_str()
                << " " << error.what();
      throw error;
    }
}

QualifiedMassSpectrum
readQualifiedMassSpectrumMgf(const QString &filename)
{
  try
    {
      qDebug();
      MsFileAccessor accessor(filename, "msrun");
      std::vector<MsRunIdCstSPtr> msrun_ids = accessor.getMsRunIds();
      if(accessor.getFileFormat() == MsDataFormat::unknown)
        {
          throw pappso::ExceptionNotRecognized("file format not recognized");
        }
      if(msrun_ids.size() == 0)
        {
          throw pappso::PappsoException("msrun_ids.size() == 0");
        }
      qDebug();
      MsRunReaderSPtr reader = accessor.msRunReaderSPtr(msrun_ids.front());
      qDebug() << msrun_ids.front().get()->getXmlId();
      std::cout << reader->spectrumListSize() << std::endl;
      QualifiedMassSpectrum spectrum_sp =
        reader->qualifiedMassSpectrum(0, true);
      return spectrum_sp;
    }
  catch(pappso::PappsoException &error)
    {
      std::cout << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                << QString("ERROR reading file %1 : %2")
                     .arg(filename)
                     .arg(error.qwhat())
                     .toStdString()
                     .c_str();
      throw error;
    }
}

bool
compareTextFiles(const QString &file1, const QString &file2)
{

  QFile fnew(file1);
  fnew.open(QFile::ReadOnly | QFile::Text);
  // REQUIRE(fnew.errorString().toStdString() == "");
  QTextStream infnew(&fnew);

  QFile fold(file2);
  fold.open(QFile::ReadOnly | QFile::Text);
  QTextStream infold(&fold);

  return (infold.readAll().toStdString() == infnew.readAll().toStdString());
}


std::vector<std::size_t>
getMapKeys(const std::map<std::size_t, std::size_t> &map_table)
{
  std::vector<std::size_t> vector;

  for(auto &&pair : map_table)
    vector.push_back(pair.first);

  return vector;
}


std::vector<std::size_t>
getMapValues(const std::map<std::size_t, std::size_t> &map_table)
{
  std::vector<std::size_t> vector;

  for(auto &&pair : map_table)
    vector.push_back(pair.second);

  return vector;
}
