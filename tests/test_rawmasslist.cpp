//
// File: test_rawmasslist.cpp
// Created by: Olivier Langella
// Created on: 19/07/2016
//
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

// make test ARGS="-V -I 12,12"
// ./tests/catch2-only-tests [rawmasslist] -s


#include <catch2/catch_test_macros.hpp>

#include <iostream>
#include <pappsomspp/peptide/peptiderawfragmentmasses.h>
#include <pappsomspp/peptide/peptidefragmentionlistbase.h>
#include <pappsomspp/mzrange.h>

#include <list>
#include <QDebug>
#include <QString>

using namespace pappso;
using namespace std;


TEST_CASE("..:: Raw mass list ::..", "[rawmasslist]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: Raw mass list ::..", "[rawmasslist]")
  {
    std::cout << std::endl << "..:: Raw mass list ::.." << std::endl;
    PeptideSp peptide_sp(Peptide("SAMPLER").makePeptideSp());
    std::list<PeptideIon> ion_type_list;
    ion_type_list.push_back(PeptideIon::y);
    PeptideFragmentIonListBase fragmentIonList(peptide_sp, ion_type_list);


    auto ion_list = fragmentIonList.getPeptideFragmentIonSp(PeptideIon::y);


    PeptideRawFragmentMasses calc_mass_list(*peptide_sp.get(),
                                            RawFragmentationMode::full);

    std::vector<pappso_double> mass_list;
    calc_mass_list.pushBackIonMasses(mass_list, PeptideIon::y);


    for(auto current_fragment : ion_list)
      {
        pappso_double lookfor_mz = current_fragment.get()->getMass();

        MzRange mz_range(lookfor_mz, PrecisionFactory::getPpmInstance(0.001));
        std::vector<pappso_double>::const_iterator it = find_if(
          mass_list.begin(), mass_list.end(), [mz_range](double mass_a) {
            return mz_range.contains(mass_a);
          });
        REQUIRE(it != mass_list.end());
      }

    std::vector<pappso_double> mz_list;
    calc_mass_list.pushBackIonMz(mz_list, PeptideIon::y, 1);


    for(auto current_fragment : ion_list)
      {
        pappso_double lookfor_mz = current_fragment.get()->getMz(1);

        MzRange mz_range(lookfor_mz, PrecisionFactory::getPpmInstance(0.001));
        std::vector<pappso_double>::const_iterator it =
          find_if(mz_list.begin(), mz_list.end(), [mz_range](double mass_a) {
            return mz_range.contains(mass_a);
          });
        REQUIRE(it != mass_list.end());
      }

    PeptideRawFragmentMasses calc_mass_list_proline(
      *peptide_sp.get(), RawFragmentationMode::proline_effect);
  }
}
