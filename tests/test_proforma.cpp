/**
 * \file test/test_proforma.cpp
 * \date 21/01/2025
 * \author Olivier Langella
 * \brief test proforma notation
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

// ./tests/catch2-only-tests [proforma] -s

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_approx.hpp>
#include <pappsomspp/peptide/peptideproformaparser.h>
#include <pappsomspp/exception/exceptionnotpossible.h>


#include "common.h"
#include "tests/config.h"
///home/langella/developpement/git/i2masschroq/src/input/sage/sagetsvhandler.cpp@457, SageTsvHandler::parsePeptide(): "[+42.0106]-M[MOD:00719]QNDAGEFVDLYVPR"

TEST_CASE("Test proforma notation", "[proforma]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: Test ProForma parser ::..", "[proforma]")
  {
    REQUIRE_THROWS_AS(
      PeptideProFormaParser::parseString("C[MOD:00397][MOD:01160]C[MOD:00397]AADDKEAC[FAVEGPK"),
      pappso::ExceptionNotPossible);
    PeptideSp peptide_from_str = PeptideProFormaParser::parseString(
      "C[MOD:00397][MOD:01160]C[MOD:00397]AADDKEAC[MOD:00397]FAVEGPK");
    REQUIRE(peptide_from_str.get()->getMass() == Catch::Approx(1909.764470427));

    REQUIRE(peptide_from_str.get()->toProForma().toStdString() ==
            "C[MOD:00397][MOD:01160]C[MOD:00397]AADDKEAC[MOD:00397]FAVEGPK");

    PeptideSp peptide_from_str2 = PeptideProFormaParser::parseString(
      "C[MOD:00397][MOD:01160]C[MOD:00397]AADDK[-18.02]EAC[MOD:00397]FAVEGPK");
    /*REQUIRE(peptide_from_str2.get()->toProForma().toStdString() ==
            "C[MOD:00397][MOD:01160]C[MOD:00397]AADDK[-18.0200]EAC[MOD:00397]"
            "FAVEGPK");*/
    REQUIRE(peptide_from_str2.get()->getMass() == Catch::Approx(1891.744470427));


    REQUIRE(peptide_from_str2.get()->toProForma().toStdString() ==
            "C[MOD:00397][MOD:01160]C[MOD:00397]AADDK[-18.0200]EAC[MOD:00397]FAVEGPK");
    
    
    
    PeptideSp peptide_nter_modif = PeptideProFormaParser::parseString(
      "[+42.0106]-M[MOD:00719]QNDAGEFVDLYVPR");
    REQUIRE(peptide_nter_modif.get()->toProForma().toStdString() ==
            "M[+42.0106][MOD:00719]QNDAGEFVDLYVPR");
  }

}
