//
// File: test_massrange.cpp
// Created by: Olivier Langella
// Created on: 4/3/2015
//
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

// ./tests/catch2-only-tests [massrange] -s


#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_approx.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>

#include <pappsomspp/mzrange.h>
#include <pappsomspp/utils.h>

#include <iostream>
#include <QDebug>
#include <QString>
#include <cmath>

using namespace pappso;
using namespace std;


TEST_CASE("MassRange test suite.", "[massrange]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: test MassRange ::..", "[massrange]")
  {
    PrecisionPtr precision  = PrecisionFactory::getPpmInstance(10);
    PrecisionPtr precisionb = PrecisionFactory::getDaltonInstance(6.0);
    REQUIRE(precision != precisionb);
    precisionb = PrecisionFactory::getPpmInstance(10);

    REQUIRE(precision == precisionb);


    MzRange mz_range1(pappso_double(1200.001),
                      PrecisionFactory::getDaltonInstance(6.0));
    MzRange mz_range2(pappso_double(1200.001),
                      PrecisionFactory::getPpmInstance(10));
    MzRange mz_range3(pappso_double(1200.001),
                      PrecisionFactory::getPpmInstance(10),
                      PrecisionFactory::getPpmInstance(30));

    REQUIRE(mz_range1.toString().toStdString() ==
            "mz=1200 delta=6 : 1194 < 1200 < 1206");
    REQUIRE(mz_range2.toString().toStdString() ==
            "mz=1200 delta=0.012 : 1199.99 < 1200 < 1200.01");
    REQUIRE(mz_range3.toString().toStdString() ==
            "mz=1200.01 delta=0.024 : 1199.99 < 1200.01 < 1200.04");

    std::cout << std::endl << "..:: Contains ::.." << std::endl;
    std::cout << mz_range1.toString().toStdString() << std::endl;
    REQUIRE(!mz_range1.contains(pappso_double(600)));

    std::cout << mz_range1.toString().toStdString() << std::endl;
    REQUIRE(mz_range1.contains(pappso_double(1200)));

    std::cout << mz_range2.toString().toStdString() << std::endl;
    REQUIRE(!mz_range2.contains(pappso_double(600)));

    std::cout << mz_range2.toString().toStdString() << std::endl;
    REQUIRE(mz_range2.contains(pappso_double(1200)));

    std::cout << mz_range2.toString().toStdString() << std::endl;
    REQUIRE(mz_range2.contains(pappso_double(1200.00001)));
    std::cout << mz_range2.toString().toStdString() << std::endl;
    REQUIRE(!mz_range2.contains(pappso_double(1200.1)));

    // test :
    MzRange mz_range_test_ref(pappso_double(633.29706487392),
                              PrecisionFactory::getPpmInstance(10));

    REQUIRE(mz_range_test_ref.getMz() == Catch::Approx(633.29706487392));

    REQUIRE(mz_range_test_ref.lower() == Catch::Approx(633.290731903271));

    REQUIRE(mz_range_test_ref.upper() == Catch::Approx(633.303397844569));
    // test :
    MzRange mz_range_test(pappso_double(633.29706487392),
                          PrecisionFactory::getPpmInstance(10),
                          PrecisionFactory::getPpmInstance(10));
    REQUIRE(mz_range_test.getMz() == Catch::Approx(633.29706487392));


    REQUIRE(mz_range_test.lower() == Catch::Approx(633.290731903271));
    REQUIRE(mz_range_test.upper() == Catch::Approx(633.303397844569));

    MzRange mz_range_lower_upper_test(pappso_double(633.29706487392),
                                      PrecisionFactory::getPpmInstance(10),
                                      PrecisionFactory::getDaltonInstance(1));

    REQUIRE(mz_range_lower_upper_test.getMz() ==
            Catch::Approx(633.79389838859572));
    REQUIRE(mz_range_lower_upper_test.lower() ==
            Catch::Approx(633.290731903271));
    REQUIRE(mz_range_lower_upper_test.upper() ==
            Catch::Approx(634.29706487392));
  }
}
