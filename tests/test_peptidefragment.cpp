//
// File: test_peptidefragment.cpp
// Created by: Olivier Langella
// Created on: 9/3/2015
//
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


// ./tests/catch2-only-tests [peptidefragment] -s


#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_approx.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>


#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/peptide/peptidefragment.h>

#include <pappsomspp/peptide/peptidefragmention.h>
#include <iostream>
#include <QDebug>
#include <QString>

using namespace pappso;
using namespace std;

TEST_CASE("Peptide Fragment test suite.", "[peptidefragment]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: test Peptide Fragment ::..", "[peptidefragment]")
  {
    std::cout << std::endl << "..:: peptide fragment init ::.." << std::endl;

    PeptideFragment pep_fragment(
      std::make_shared<Peptide>("PEPTIDE"), PeptideDirection::Cter, 4);


    REQUIRE(pep_fragment.getMass() == Catch::Approx(476.2118432465));
    // SUCCESS
    PeptideFragment pep_fragmentr(
      std::make_shared<Peptide>("PEPTIDE"), PeptideDirection::Nter, 4);


    REQUIRE(pep_fragmentr.getMass() == Catch::Approx(442.2063639429));
    REQUIRE(pep_fragmentr.getMz(1) == Catch::Approx(443.2136404098));
    REQUIRE(pep_fragmentr.getMz(2) == Catch::Approx(222.1104584383));

    REQUIRE_NOTHROW(PeptideFragmentIon(
      std::make_shared<PeptideFragment>(
        std::make_shared<Peptide>("PEPTIDE"), PeptideDirection::Nter, 4),
      PeptideIon::a));

    REQUIRE_THROWS_AS(
      PeptideFragmentIon(
        std::make_shared<PeptideFragment>(
          std::make_shared<Peptide>("PEPTIDE"), PeptideDirection::Nter, 4),
        PeptideIon::y),
      pappso::PappsoException);
  }
}
