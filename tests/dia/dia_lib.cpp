#include "dia_lib.h"
#include <QDebug>
#include <odsstream/odsdocwriter.h>
#include <odsstream/tsvdirectorywriter.h>

void
writeMobilityTraces(
  const QString &filename,
  const pappso::TimsFrameMobilityTraces &tims_frame_mobility_traces)
{

  QDir file(filename);
  // file.open(QIODevice::WriteOnly);
  TsvDirectoryWriter writer(file);

  writer.writeSheet("mobility");

  std::size_t i = 0;

  writer.writeLine();
  writer.writeCell("m/z");
  writer.writeCell("TOF index");
  qDebug();
  qDebug();

  for(auto scan_index : tims_frame_mobility_traces.getScanIndexList())
    {
      writer.writeCell(scan_index);
      i++;
    }


  qDebug();

  writer.writeLine();
  i = 0;

  qDebug();
  auto tof_index_list         = tims_frame_mobility_traces.getTofIndexList();
  std::vector<double> mz_list = tims_frame_mobility_traces.getMzList();
  qDebug();
  for(auto one_trace : tims_frame_mobility_traces.getIonMobTraceList())
    {
      writer.writeCell(mz_list[i]);
      writer.writeCell(tof_index_list[i]);

      for(auto datapoint : *(one_trace.get()))
        {
          writer.writeCell(datapoint.y);
        }
      writer.writeLine();
      i++;
    }

  qDebug();
  writer.close();
}
