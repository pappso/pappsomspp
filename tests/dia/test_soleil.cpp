//
// File: test_c13n15.cpp
// Created by: Olivier Langella
// Created on: 12/7/2023
//
/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./tests/dia/catch2-only-dia [soleil] -s


#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <pappsomspp/vendors/tims/timsframe.h>
#include <pappsomspp/msrun/private/timsmsrunreaderms2.h>
#include <pappsomspp/msrun/private/timsmsrunreader.h>

#include <QDebug>
#include "../common.h"
#include "tests/config.h"
#include "dia_lib.h"


TEST_CASE("soleil test suite.", "[soleil]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("Test TIMS TDF parsing DIA")
  {
    INFO("Test case mobility trace extractor");

    pappso::MsFileAccessor dia_accessor(
      "/gorgone/pappso/jouy/raw/2023_TimsTOF_Pro/20231006_Guillaume_Test_DIA3/"
      "DIA/10-5-2023_AT01_20ng_DIA3_1_6490.d",
      "a1");


    dia_accessor.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                            pappso::FileReaderType::tims);


    pappso::MsRunReaderSPtr p_ms_data_file =
      dia_accessor.msRunReaderSPtr(dia_accessor.getMsRunIds().front());
    pappso::TimsMsRunReader *p_msreader_tims_ms =
      dynamic_cast<pappso::TimsMsRunReader *>(p_ms_data_file.get());


    /*
  E:\Guillaume_S\10-05-2023_Test_DIA3\10-5-2023_AT01_20ng_DIA3_1_6490.d
  10-5-2023_AT01_20ng_DIA3_1_6490	AT3G20410.1;AT3G20410.2
  AT3G20410.1;AT3G20410.2	|	|	186,004	639,495	843,356	3494850	13979800
  6624770	6624770	AAAAAPGLSPK	AAAAAPGLSPK	AAAAAPGLSPK2	2	0,00439932
  0,0805417	0,00222608	0,333333	0,0191446	0,000202963	0,333333	0	1 186,004
  639,495	150,324		206,898	0,678122	18,7117	18,6216	18,8019	-10,6384
  18,6526	-10,4488		0,00317137	0,000236798	0,490145	256,006	1,83272
  0,330662	0,086652	0	0,945514	0
  -10000000	71.0021;0;115.002;0;134.003;0;0;0;0;0;0;0;	71.0021;0;115.002;0;134.003;0;0;0;0;0;0;0;
  0.628757;0;0.708601;0;0.940796;0;0;0;0;0;0;0;	19916	0,852614	0,866875
  0,850309	0,869149
  */
    /*
E:\Guillaume_S\10-05-2023_Test_DIA3\10-5-2023_AT01_20ng_DIA3_1_6490.d
10-5-2023_AT01_20ng_DIA3_1_6490	AT1G13060.1 AT1G13060.1;AT1G13060.2;AT1G13060.3
|	|	4426,15	16651,3	14524,4	3494850	13979800	6624770	6624770	GGVMVAADSR
GGVMVAADSR	GGVMVAADSR2	2	0,0000591509	0,0000591509	0,0000236407	0,333333
0,000270929	0,000202963	0,333333	0	1	3169,08	10895,5	2561,17		11503,5 0,948014
18,7124	18,6223	18,8026	-11,0752	18,7199	-11,0749		0,00000383822 0,000236798
0,937327	9936,22	4,89793	0,289596	0,993191	1,64454	0,999998	0,889416
0,0101333	4868.11;2112.05;859.021;611.016;543.012;889.022;
4868.11;2112.05;859.021;611.016;543.012;889.022;
0.863006;0.982854;0.900798;0.840894;0.839806;0.910868;	19916	0,834792 0,834792
0,832547	0,83742
*/
    /*
    E:\Guillaume_S\10-05-2023_Test_DIA3\10-5-2023_AT01_20ng_DIA3_1_6490.d
    10-5-2023_AT01_20ng_DIA3_1_6490	AT3G04840.1	AT3G04840.1	|	|	42216 161027
    152055	3494850	13979800	6624770	6624770	TTDSYTLR	TTDSYTLR	TTDSYTLR2 2
    0,00000841411	0,00000841411	0,0000859555	0,333333	0,000270929 0,000202963
    0,333333	0	1	9335,2	32095	7544,48		137159	0,937962	18,7137	18,6235
    18,8038	-11,4401	18,694	-11,413		0,00000179435	0,000236798	0,988696
    142929	5,42457	0,672722	1	1,67381	1	0	-10000000
    23852.4;17278.4;19233.4;3404.05;796.02;570.011;
    23852.4;17278.4;19233.4;3404.05;796.02;570.011;
    0.918867;0.956514;0.944977;0.922522;0.808919;0.970996;	19916	0,8	0,788977
    0,787844	0,801915
    */


    // scan 19916
    // AAAAAPGLSPK
    // mass 952.534180433232
    // mz 477.274
    // rt 18,7117
    // GGVMVAADSR mz 481.7398
    // TTDSYTLR mz 478.7378
    // "global_slice_index=19917 frame=10581 begin=632 end=926 group=6 slice=1"
    // "frame_id=10575 begin=632 end=926 group=6 slice=1"
    pappso::TimsFrameCstSPtr timsframe_sptr;
    timsframe_sptr =
      p_msreader_tims_ms->getTimsDataSPtr().get()->getTimsFrameCstSPtr(10581);

    pappso::TimsFrameMobilityTraces tims_frame_mobility_traces;

    tims_frame_mobility_traces.extractMobilityTraces(
      timsframe_sptr, 632, 926, 1000);

    writeMobilityTraces("mobility_dia_nano_10581_ms2",
                        tims_frame_mobility_traces);


    timsframe_sptr =
      p_msreader_tims_ms->getTimsDataSPtr().get()->getTimsFrameCstSPtr(10575);
    tims_frame_mobility_traces.extractMobilityTraces(
      timsframe_sptr, 632, 926, 1000);
    writeMobilityTraces("mobility_dia_nano_10575_ms1",
                        tims_frame_mobility_traces);


    timsframe_sptr =
      p_msreader_tims_ms->getTimsDataSPtr().get()->getTimsFrameCstSPtr(13338);

    tims_frame_mobility_traces.extractMobilityTraces(
      timsframe_sptr, 552, 877, 1000);

    writeMobilityTraces("mobility_dia_nano_13338_ms2",
                        tims_frame_mobility_traces);


    timsframe_sptr =
      p_msreader_tims_ms->getTimsDataSPtr().get()->getTimsFrameCstSPtr(13329);
    tims_frame_mobility_traces.extractMobilityTraces(
      timsframe_sptr, 552, 877, 1000);
    writeMobilityTraces("mobility_dia_nano_13329_ms1",
                        tims_frame_mobility_traces);
  }

  SECTION("Test TIMS TDF parsing")
  {
    INFO("Test case mobility trace extractor");

    pappso::MsFileAccessor accessor(
      "/gorgone/pappso/versions_logiciels_pappso/bruker/"
      "200ngHeLaPASEF_2min_compressed.d/analysis.tdf",
      "a1");


    accessor.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                        pappso::FileReaderType::tims_ms2);


    pappso::MsRunReaderSPtr p_ms_data_file =
      accessor.msRunReaderSPtr(accessor.getMsRunIds().front());
    pappso::TimsMsRunReaderMs2 *p_msreader_tims_ms2 =
      dynamic_cast<pappso::TimsMsRunReaderMs2 *>(p_ms_data_file.get());
    /*
     * 36	1101.0713085818	1101.2352981594	1100.56987430806	2	206.34670754313
27481.0	7

37	1032.50460066644	1032.61931820008	1032.00354342756	2
255.636031950547	7776.0	7

38	849.889511073675	850.36577904552
849.889511073675	2	301.081436858545	10674.0	7

39	732.368888386124
732.7994188422	732.368888386124	2	366.846506680738	171577.0	7


40 613.834985672256	614.145093980785	613.834985672256	2	408.353586866372
100182.0	7

41	590.663111414989	590.993423163968	590.663111414989	3
480.650093737275	11285.0	7

42	1186.54230445333	1186.72886658555
1186.04087739917	2	209.745072692947	3388.0	7

43	675.889879111058
676.29815138184	675.889879111058	2	357.036750108284	78563.0	7

44
625.325372791376	625.579928168419	625.325372791376	1	408.432368202426
103456.0	7

45	1007.51213556948	1007.6267841152	1007.01070658348	2
247.074788254945	7187.0	7

46	913.959890656205	913.979678622373
913.458905261467	2	294.654254399024	8314.0	7 47	755.38529656205
755.841674769364	755.38529656205	2	353.86257989541	12945.0	7

48
591.302490003786	591.590497194694	591.302490003786	2	403.254182106694
29830.0	7

49	595.771231095101	596.059238283215	595.771231095101	2
446.612758885196	9435.0	7

50	413.752273442763	413.939477660886
413.752273442763	1	509.25342989311	6639.0	7 51	762.399893323618
762.856367409854	762.399893323618	2	322.644976866487	8675.0	7 52
735.843048722056	736.273579192029	735.843048722056	2	373.54662077597 18891.0
7 53	568.32624886187	568.614256051375	568.32624886187	2	429.363085740713
10674.0	7 54	643.816041172731	644.126137985901	643.816041172731	2
399.427994121969	15462.0	7 55	687.390246797748	687.798519066724
687.390246797748	2	363.680236411293	11928.0	7 56	562.806530794766
563.094537974134	562.806530794766	2	405.175837742504	13135.0	7 57
546.269682718966	546.539497651969	546.269682718966	2	450.784556792487 7570.0
7 58	509.960123743293	510.26725044115	509.960123743293	3	496.035031847134
6328.0	7 59	883.463248838573	883.962935681494	883.463248838573	2
225.932830827769	5538.0	7 60	938.950034464637	938.969822427669
938.4490490699	2	300.353033535402	7770.0	7 61	729.330713601629
729.761244061435	729.330713601629	2	378.503772398617	10847.0	7 62
578.827527999194	579.115535190246	578.827527999194	2	418.782881695176 8674.0
7 63	497.283111759912	497.529947222637	497.283111759912	2	467.090705461384
7569.0	7 64	417.732442590361	417.732442590361			514.431244560487	3550.0
7 65	874.942898591935	875.442503475723	874.942898591935	2	305.569882347679
6660.0	7 66	751.906613734472	752.363087819094	751.906613734472	2
352.740672716387	9542.0	7 67	791.370289287521	791.490672694711
791.036000584609	3	396.117318241086	8795.0	7 68	455.606517405094
455.889596549243	455.606517405094	3	497.462564102564	5917.0	7
     * */
    pappso::TimsFrameCstSPtr timsframe_sptr;
    timsframe_sptr =
      p_msreader_tims_ms2->getTimsDataSPtr().get()->getTimsFrameCstSPtr(13);
    // LVSNHSLHETSSVFVDSLTK 71
    // prec 36	1101.0713085818	1101.2352981594	1100.56987430806	2
    // 206.34670754313	27481.0	7
    // frameprec 13	194	219 1101.2352981594	3.0	68.0 36

    /*
     *
      <peptide id="pd3148" seq="LVSNHSLHETSSVFVDSLTK"/>
              <peptide_evidence id="peb34" peptide_id="pd3148" idx="71"
     rt="2402.244825" eng="1" evalue="2.25835e-09" exp_mass="2199.125195"
     charge="2" checked="true"> <param key="0" value="44.2"/> <param key="1"
     value="2.25835e-09"/>
              </peptide_evidence>*/

    // AWGPGLEGGVVGK 79
    // prec 40	613.834985672256	614.145093980785	613.834985672256	2
    // 408.353586866372	100182.0	7
    // frameprec 13	396	421 614.145093980785	2.0	42.0	40
    /*
     *
        <peptide id="pd1376" seq="AWGPGLEGGVVGK"/>
                <peptide_evidence id="peb38" peptide_id="pd1376" idx="79"
     rt="2402.244825" eng="1" evalue="3.55081e-06" exp_mass="1225.655418"
     charge="2" checked="true"> <param key="0" value="42"/> <param key="1"
     value="3.55081e-06"/>
                </peptide_evidence>*/

    // AVEGC[MOD:00397]VSASQAATEDGQLLR 73
    // prec 37	1032.50460066644	1032.61931820008	1032.00354342756	2
    // 255.636031950547	7776.0	7
    // frameprec 13	243	268  1032.61931820008	3.0	58.0	37
    // 14	243	268	1032.61931820008	3.0	58.0	37
    // 16	243	268	1032.61931820008	3.0	58.0	37
    /*
     *
        <peptide id="pd1946" seq="AVEGCVSASQAATEDGQLLR">
            <mod ref="moda6" position="4" aa="C"/>
        </peptide>
                <peptide_evidence id="peb35" peptide_id="pd1946" idx="73"
     rt="2402.244825" eng="1" evalue="1.25003e-08" exp_mass="2061.992533"
     charge="2" checked="true"> <param key="0" value="38.8"/> <param key="1"
     value="1.25003e-08"/>
                </peptide_evidence>*/

    /*

                <peptide_evidence id="peb34" peptide_id="pd3148" idx="71"
       rt="2402.244825" eng="1" evalue="2.25835e-09" exp_mass="2199.125195"
       charge="2" checked="true"> <param key="0" value="44.2"/> <param key="1"
       value="2.25835e-09"/><peptide id="pd3148" seq="LVSNHSLHETSSVFVDSLTK"/>
                </peptide_evidence>
                <peptide_evidence id="peb35" peptide_id="pd1946" idx="73"
       rt="2402.244825" eng="1" evalue="1.25003e-08" exp_mass="2061.992533"
       charge="2" checked="true"> <param key="0" value="38.8"/> <param key="1"
       value="1.25003e-08"/><peptide id="pd1946" seq="AVEGCVSASQAATEDGQLLR">
            <mod ref="moda6" position="4" aa="C"/>
        </peptide>
                </peptide_evidence>
                <peptide_evidence id="peb36" peptide_id="pd1997" idx="75"
       rt="2402.244825" eng="1" evalue="3.33021" exp_mass="1697.764469"
       charge="2" checked="true"> <param key="0" value="18.8"/> <param key="1"
       value="3.33021"/>
        <peptide id="pd1997" seq="NHKDVTDSFYPAMR">
            <mod ref="moda7" position="12" aa="M"/>
        </peptide>
                </peptide_evidence>
                <peptide_evidence id="peb37" peptide_id="pd1357" idx="77"
       rt="2402.244825" eng="1" evalue="6.40312e-06" exp_mass="1462.723223"
       charge="2" checked="true"> <param key="0" value="32.3"/> <param key="1"
       value="6.40312e-06"/> <peptide id="pd1357" seq="SPYQEFTDHLVK"/>
                </peptide_evidence>
                <peptide_evidence id="peb38" peptide_id="pd1376" idx="79"
       rt="2402.244825" eng="1" evalue="3.55081e-06" exp_mass="1225.655418"
       charge="2" checked="true"> <param key="0" value="42"/> <param key="1"
       value="3.55081e-06"/><peptide id="pd1376" seq="AWGPGLEGGVVGK"/>
                </peptide_evidence>
                <peptide_evidence id="peb39" peptide_id="pd1748" idx="81"
       rt="2402.244825" eng="1" evalue="7.82161e-12" exp_mass="1768.967504"
       charge="3" checked="true"> <param key="0" value="48"/> <param key="1"
       value="7.82161e-12"/><peptide id="pd1748" seq="MEKPPAPPSLPAGPPGVK"/>

                    */

    pappso::TimsFrameMobilityTraces tims_frame_mobility_traces;

    tims_frame_mobility_traces.extractMobilityTraces(
      timsframe_sptr,
      0,
      timsframe_sptr.get()->getTotalNumberOfScans() - 1,
      1000);

    writeMobilityTraces("mobility_71_73_79", tims_frame_mobility_traces);
  }
}
