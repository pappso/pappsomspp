#pragma once


#include <pappsomspp/trace/trace.h>
#include <pappsomspp/vendors/tims/mobilitytraces/timsframemobilitytraces.h>

void writeMobilityTraces(
  const QString &filename,
  const pappso::TimsFrameMobilityTraces &tims_frame_mobility_traces);
