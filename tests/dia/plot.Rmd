---
title: "plot DIA"
output: html_document
---


# DIA

```{r}
library(knitr)
library(ggplot2)
library(data.table)


setwd("~/developpement/git/pappsomspp/tests/dia")

df <- read.csv("../../build/tests/dia/mobility_dia_nano_10575_ms1/mobility.tsv", sep = "\t")


mz = df$m.z
tof = df$TOF.index
df$m.z = NULL
df$TOF.index = NULL
scans = colnames(df)

df = transpose(df)

rownames(df) = scans
df$scans = scans

dfc = df[,0:4]
dfc$scans = scans

long <- melt(setDT(dfc), variable.name = "ions")

ggplot(data=long, aes(x = scans, y=value, group=ions)) + geom_line(aes(col = ions))+  geom_point()

```
