
/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

// cmake .. -DCMAKE_BUILD_TYPE=Debug  -DMAKE_TESTS=1  -DUSEPAPPSOTREE=1

//./tests/catch2-only-tests [MsRunReader] -s
//./tests/catch2-only-tests [MsRunReaderPerf] -s
//./tests/catch2-only-tests [MsRunReaderPerf] -s --benchmark-samples 5

#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do
                          // this in one cpp file

#define CATCH_CONFIG_ENABLE_BENCHMARKING

#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>


#include <iostream>

#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionnotpossible.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/msrun/output/mzxmloutput.h>
#include <pappsomspp/processing/uimonitor/uimonitorvoid.h>
#include <pappsomspp/msrun/private/timsframesmsrunreader.h>
#include <QDebug>
#include <QtCore>
#include <QFile>
#include <QtConcurrent>
#include "tests/config.h"
// #include "common.h"

using namespace std;


TEST_CASE("Test MsRunReader new API performance", "[MsRunReaderPerf]")
{

  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

#if USEPAPPSOTREE == 1

  SECTION("..::  MsRunReader performance check ::..")
  {
    qDebug();
    pappso::MsFileAccessor file_access_A01(
      "/gorgone/pappso/versions_logiciels_pappso/bruker/"
      "200ngHeLaPASEF_2min_compressed.d",
      "");

    file_access_A01.setPreferredFileReaderType(
      pappso::MsDataFormat::brukerTims, pappso::FileReaderType::tims_frames);
    pappso::MsRunReaderSPtr msrunA01 =
      file_access_A01.getMsRunReaderSPtrByRunId("", "runa01");

    pappso::MsRunReadConfig config;
    // config.setRetentionTimeStartInSeconds(2490);
    // config.setRetentionTimeEndInSeconds(2600);
    config.setParameterValue(pappso::MsRunReadConfigParameter::
                               TimsFrameIonMobScanIndexBegin,
                             100);
    config.setParameterValue(pappso::MsRunReadConfigParameter::
                               TimsFrameIonMobScanIndexEnd,
                             200);
    config.setNeedPeakList(true);
    config.setMsLevels({1});

    // msrunA01.get()->setMonoThread(false);

    pappso::MsRunQualifiedSpectrumLoader spectrum_list_reader;
    BENCHMARK("without mz merge")
    {
      msrunA01.get()->readSpectrumCollection2(config, spectrum_list_reader);
    };

    // REQUIRE(spectrum_list_reader.getQualifiedMassSpectrumList().size() ==
    // 33);

    config.setParameterValue(pappso::MsRunReadConfigParameter::
                               TimsFrameMzIndexMergeWindow,
                             7);
    BENCHMARK("with TimsFramesMsRunReader_mz_index_merge_window 7")
    {
      msrunA01.get()->readSpectrumCollection2(config, spectrum_list_reader);
    };

    config.setParameterValue(pappso::MsRunReadConfigParameter::
                               TimsFrameMzIndexMergeWindow,
                             15);
    BENCHMARK("with TimsFramesMsRunReader_mz_index_merge_window 15")
    {
      msrunA01.get()->readSpectrumCollection2(config, spectrum_list_reader);
    };


    config.setParameterValue(pappso::MsRunReadConfigParameter::
                               TimsFrameMzIndexMergeWindow,
                             30);
    BENCHMARK("with TimsFramesMsRunReader_mz_index_merge_window 30")
    {
      msrunA01.get()->readSpectrumCollection2(config, spectrum_list_reader);
    };


    config.setParameterValue(pappso::MsRunReadConfigParameter::
                               TimsFrameMzIndexMergeWindow,
                             600);

    BENCHMARK("with TimsFramesMsRunReader_mz_index_merge_window 600")
    {
      msrunA01.get()->readSpectrumCollection2(config, spectrum_list_reader);
    };
  }

#endif
}
