
  # These source files are compiled into a single Catch2-based test binary.
  # tests dedicated to check speed performances (benchmarks)
  set(catch2_only_cpp_benchmark_SRCS
    test_msrunreaderng.cpp
    test_proteinycodefasta.cpp
    test_timsxicextractor.cpp
    test_timsxicextractor_based_masspec.cpp
  )

  add_executable(catch2-only-benchmarks
    ../common.cpp
    ../proteincode/proteincode_lib.cpp
    ${catch2_only_cpp_benchmark_SRCS}
  )

    
  set_property(TARGET catch2-only-benchmarks PROPERTY CXX_STANDARD 17) # we want C++17

    
  target_link_libraries(catch2-only-benchmarks
    pappsomspp-shared
    Catch2::Catch2
    Catch2::Catch2WithMain
    liblzf::liblzf
    Boost::chrono
    QuaZip::QuaZip
    OdsStream::Core
    )

