
langella@piccolo:~/developpement/git/pappsomspp/build$ ./tests/catch2-only-benchmarks [MsRunReaderPerf] -s --benchmark-samples 5
Filters: [MsRunReaderPerf]
Randomness seeded to: 1432262163

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
catch2-only-benchmarks is a Catch2 v3.4.0 host application.
Run with -? for options

-------------------------------------------------------------------------------
Test MsRunReader new API performance
  ..::  MsRunReader performance check ::..
-------------------------------------------------------------------------------
/home/langella/developpement/git/pappsomspp/tests/benchmarks/test_msrunreaderng.cpp:60
...............................................................................

benchmark name                       samples       iterations    estimated
                                     mean          low mean      high mean
                                     std dev       low std dev   high std dev
-------------------------------------------------------------------------------
without mz merge                                 5             1     43.9432 s 
                                         2.97486 s     1.36682 s     7.73235 s 
                                         3.17559 s    29.3991 ms     3.91776 s 
                                                                               
with                                                                           
TimsFramesMsRunReader_mz_index_mer-                                            
ge_window 7                                      5             1     7.39553 s 
                                         1.43994 s     1.43515 s     1.44609 s 
                                         6.1088 ms    3.51406 ms     8.1425 ms 
                                                                               
with                                                                           
TimsFramesMsRunReader_mz_index_mer-                                            
ge_window 15                                     5             1     6.91842 s 
                                         1.36389 s     1.33695 s     1.39444 s 
                                        32.7829 ms    15.0304 ms    43.0582 ms 
                                                                               
with                                                                           
TimsFramesMsRunReader_mz_index_mer-                                            
ge_window 30                                     5             1     6.24158 s 
                                         1.24922 s     1.24685 s     1.25028 s 
                                        1.70175 ms    424.728 us    2.38718 ms 
                                                                               
with                                                                           
TimsFramesMsRunReader_mz_index_mer-                                            
ge_window 600                                    5             1     5.56334 s 
                                         1.11556 s     1.10545 s     1.13878 s 
                                        16.1878 ms    635.141 us     20.887 ms 
                                                                               

===============================================================================
test cases: 1 | 1 passed
assertions: - none -



langella@piccolo:~/developpement/git/pappsomspp/build$ ./tests/catch2-only-benchmarks [TimsXicExtractor] -s --benchmark-samples 5
Filters: [TimsXicExtractor]
Randomness seeded to: 3027466247
/home/langella/developpement/git/pappsomspp/tests/benchmarks/test_timsxicextractor.cpp:91 The file to use as a test base is: /gorgone/pappso/versions_logiciels_pappso/bruker/200ngHeLaPASEF_2min_compressed.d
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
catch2-only-benchmarks is a Catch2 v3.4.0 host application.
Run with -? for options

-------------------------------------------------------------------------------
Extracting XIC from timsdata
-------------------------------------------------------------------------------
/home/langella/developpement/git/pappsomspp/tests/benchmarks/test_timsxicextractor.cpp:51
...............................................................................

/home/langella/developpement/git/pappsomspp/tests/benchmarks/test_timsxicextractor.cpp:100: 
warning:
  get reader

/home/langella/developpement/git/pappsomspp/tests/benchmarks/test_timsxicextractor.cpp:103: 
warning:
  get reader OK

/home/langella/developpement/git/pappsomspp/tests/benchmarks/test_timsxicextractor.cpp:105: PASSED:
  REQUIRE( p_msreader != nullptr )
with expansion:
  0x56139c000eb0 != nullptr

/home/langella/developpement/git/pappsomspp/tests/benchmarks/test_timsxicextractor.cpp:107: PASSED:
  REQUIRE( accessor.getFileReaderType() == pappso::FileReaderType::tims_ms2 )
with expansion:
  4 == 4

/home/langella/developpement/git/pappsomspp/tests/benchmarks/test_timsxicextractor.cpp:111: PASSED:
  REQUIRE( tims2_reader != nullptr )
with expansion:
  0x000056139c000eb0 != nullptr

/home/langella/developpement/git/pappsomspp/tests/benchmarks/test_timsxicextractor.cpp:141: warning:
  buildMsRunXicExtractorSp

/home/langella/developpement/git/pappsomspp/tests/benchmarks/test_timsxicextractor.cpp:146: warning:
  buildMsRunXicExtractorSp OK

/home/langella/developpement/git/pappsomspp/tests/benchmarks/test_timsxicextractor.cpp:151: warning:
  monitor.setStatus

Actually starting the XIC extraction.
benchmark name                       samples       iterations    estimated
                                     mean          low mean      high mean
                                     std dev       low std dev   high std dev
-------------------------------------------------------------------------------
extractXicCoordSPtrList XIC                                                    
extraction                           parallelized extraction of 9 XICs
100% 
            5             1     50.6728 s parallelized extraction of 9 XICs
100% 
parallelized extraction of 9 XICs
100% 
parallelized extraction of 9 XICs
100% 
parallelized extraction of 9 XICs
100% 
parallelized extraction of 9 XICs
100% 

                                         2.23556 s    712.415 ms     6.78972 s 
                                         3.03677 s    6.32588 ms     3.72626 s 
