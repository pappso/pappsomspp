#ifndef XTANDEMRESULTSHANDLER_H
#define XTANDEMRESULTSHANDLER_H

#include <QXmlDefaultHandler>

#include <pappsomspp/peptide/peptidefragmention.h>
#include <pappsomspp/mzrange.h>

#include <pappsomspp/massspectrum/massspectrum.h>
#include <pappsomspp/pappsoexception.h>

using namespace pappso;

class XtandemResultsHandler : public QXmlDefaultHandler
{
  public:
  XtandemResultsHandler(PrecisionPtr precision);
  virtual ~XtandemResultsHandler();

  bool startElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName,
                    const QXmlAttributes &attributes);

  bool endElement(const QString &namespaceURI,
                  const QString &localName,
                  const QString &qName);

  bool startDocument();

  bool endDocument();

  bool characters(const QString &str);

  bool fatalError(const QXmlParseException &exception);
  bool error(const QXmlParseException &exception);

  QString errorString() const;

  private:
  std::vector<QString> _tag_stack;
  MassSpectrum _curent_spectrum;
  NoConstPeptideSp _current_peptide_sp;
  PrecisionPtr _precision;
  std::list<PeptideIon> _ion_list;
  unsigned int _max_charge;
  bool _refine_spectrum_synthesis;
  /// current parsed text
  QString _currentText;

  QString _current_group_id;
  QString _current_domain_id;
  float _current_hyperscore;
  uint _current_z;
  uint _current_y_ions;
  uint _current_b_ions;
  QString _current_seq;
  pappso_double _current_mh;
  uint _current_peptide_start;
  uint _current_peptide_end;
  uint _current_domain_start;
  uint _current_domain_end;
  bool _in_GAML_spectrum = false;
  QStringList _current_xdata;
  QString _current_protein_label;


  /// error message during parsing
  QString _errorStr;
  bool startElement_group(QXmlAttributes attributes);
  bool endElement_group();
  bool startElement_peptide(QXmlAttributes attributes);
  bool startElement_domain(QXmlAttributes attributes);
  bool startElement_aa(QXmlAttributes attributes);
  bool endElement_domain();
  bool endElement_GAML__values();
};


#endif // XTANDEMRESULTSHANDLER_H
