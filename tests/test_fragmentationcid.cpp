//
// File: test_fragmentationcid.cpp
// Created by: Olivier Langella
// Created on: 10/3/2015
//
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


// ./tests/catch2-only-tests [fragmentationcid] -s

#include <catch2/catch_test_macros.hpp>


#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/mzrange.h>

#include <pappsomspp/peptide/peptidefragmentionlistbase.h>
#include <iostream>
#include <QDebug>
#include <QString>

using namespace pappso;
using namespace std;


TEST_CASE("fragmentationcid test suite.", "[fragmentationcid]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: test fragmentationcid ::..", "[fragmentationcid]")
  {
    std::cout << std::endl << "..:: test ion direction ::.." << std::endl;
    REQUIRE(getPeptideIonDirection(PeptideIon::b) == PeptideDirection::Nter);
    REQUIRE(getPeptideIonDirection(PeptideIon::bstar) ==
            PeptideDirection::Nter);
    REQUIRE(getPeptideIonDirection(PeptideIon::bo) == PeptideDirection::Nter);
    REQUIRE(getPeptideIonDirection(PeptideIon::a) == PeptideDirection::Nter);
    REQUIRE(getPeptideIonDirection(PeptideIon::astar) ==
            PeptideDirection::Nter);
    REQUIRE(getPeptideIonDirection(PeptideIon::ao) == PeptideDirection::Nter);
    REQUIRE(getPeptideIonDirection(PeptideIon::bp) == PeptideDirection::Nter);
    REQUIRE(getPeptideIonDirection(PeptideIon::c) == PeptideDirection::Nter);
    REQUIRE(getPeptideIonDirection(PeptideIon::y) == PeptideDirection::Cter);
    REQUIRE(getPeptideIonDirection(PeptideIon::ystar) ==
            PeptideDirection::Cter);
    REQUIRE(getPeptideIonDirection(PeptideIon::yo) == PeptideDirection::Cter);
    REQUIRE(getPeptideIonDirection(PeptideIon::z) == PeptideDirection::Cter);
    REQUIRE(getPeptideIonDirection(PeptideIon::yp) == PeptideDirection::Cter);
    REQUIRE(getPeptideIonDirection(PeptideIon::x) == PeptideDirection::Cter);

    INFO("..:: peptide fragment init ::..");
    // http://proteus.moulon.inra.fr/w2dpage/proticdb/angular/#/peptide_hits/10053478
    Peptide peptide("DSTIPDKQITASSFYK");
    std::list<PeptideIon> cid_ion = PeptideFragmentIonListBase::getCIDionList();
    PeptideFragmentIonListBase frag_cid(peptide.makePeptideSp(), cid_ion);

    std::list<PeptideFragmentIonSp>::const_iterator it = frag_cid.begin();

    while(it != frag_cid.end())
      {
        // unsigned int size = it->get()->size();
        QString name = it->get()->getPeptideIonName();
        std::cout << it->get()->getSequence().toStdString() << " "
                  << it->get()->getPeptideIonName().toStdString() << " "
                  << it->get()->getMz(1) << " "
                  << it->get()->getFormula(1).toStdString() << std::endl;
        it++;
      }
  }
}
