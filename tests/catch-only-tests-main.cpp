#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do
                          // this in one cpp file

#define CATCH_CONFIG_ENABLE_BENCHMARKING

#ifdef CATCH2_MAJOR_VERSION_2
#include <catch2/catch.hpp>
#elif CATCH2_MAJOR_VERSION_3
#include <catch2/catch_all.hpp>
using namespace Catch;
#endif
