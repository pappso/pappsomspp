//
// File: test_tims_dia.cpp
// Created by: Olivier Langella
// Created on: 7/6/2024
//
/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./tests/pappsotree/catch2-only-pappsotree [diamicroslice] -s


#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <pappsomspp/msrun/private/timsmsrunreader.h>
#include "../common.h"
#include "tests/config.h"


#include <QDebug>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <pappsomspp/exception/exceptionoutofrange.h>
#include <pappsomspp/vendors/tims/timsdiaslices.h>


TEST_CASE("test DIA file access.", "[diamicroslice]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));


  pappso::MsFileAccessor dia_accessor(
    "/gorgone/pappso/jouy/raw/2023_TimsTOF_Pro/20231214_Guillaume_DIA_DDA/"
    "Slice_PASEF/ara1_18min_1500ng_DIA_Slice_1_1_7049.d",
    "a1");


  dia_accessor.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                          pappso::FileReaderType::tims);

  pappso::MsRunReaderSPtr p_tims_reader =
    dia_accessor.msRunReaderSPtr(dia_accessor.getMsRunIds().front());

  SECTION("..:: test DIA file access ::..", "[diamicroslice]")
  {

    pappso::TimsMsRunReader *p_msreader_tims =
      dynamic_cast<pappso::TimsMsRunReader *>(p_tims_reader.get());

    qDebug();
    REQUIRE(p_msreader_tims->getTimsDataSPtr().get()->isDdaRun() == false);

    REQUIRE(p_msreader_tims->getTimsDataSPtr().get()->isDiaRun() == true);

    std::vector<double> rt_line = p_msreader_tims->getRetentionTimeLine();
    REQUIRE((rt_line.back() / 60) == Catch::Approx(17.4969693833));


    pappso::TimsDiaSlices *dia_slices =
      p_msreader_tims->getTimsDataSPtr().get()->getTimsDiaSlicesPtr();
    REQUIRE(dia_slices->getMsMsWindowGroupList().size() == 5);

    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->size() == 12);

    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->at(0).IsolationWidth ==
            Catch::Approx(125.21));

    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->at(0).ScanNumBegin ==
            234);
    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->at(0).ScanNumEnd ==
            294);
  }
}
