//
// File: test_tims_dia.cpp
// Created by: Olivier Langella
// Created on: 7/6/2024
//
/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./tests/pappsotree/catch2-only-pappsotree [dianano] -s


#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <pappsomspp/msrun/private/timsmsrunreader.h>
#include "../common.h"
#include "tests/config.h"


#include <QDebug>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <pappsomspp/exception/exceptionoutofrange.h>
#include <pappsomspp/vendors/tims/timsdiaslices.h>
#include <pappsomspp/processing/filters/filterpass.h>


TEST_CASE("test DIA file access.", "[dianano]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));


  pappso::MsFileAccessor dia_accessor(
    "/gorgone/pappso/jouy/raw/2023_TimsTOF_Pro/20231006_Guillaume_Test_DIA3/"
    "DIA/10-5-2023_AT01_20ng_DIA3_1_6490.d",
    "a1");
  // /gorgone/pappso/moulon/users/Guillaume/TimsToF/Nano_DIA/DIA-NN_Inference/06-10-2023_Test_3/Resultats/report.tsv'


  dia_accessor.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                          pappso::FileReaderType::tims);

  pappso::MsRunReaderSPtr p_tims_reader =
    dia_accessor.msRunReaderSPtr(dia_accessor.getMsRunIds().front());

  SECTION("..:: test DIA file access ::..", "[dianano]")
  {

    pappso::TimsMsRunReader *p_msreader_tims =
      dynamic_cast<pappso::TimsMsRunReader *>(p_tims_reader.get());

    qDebug();
    REQUIRE(p_msreader_tims->getTimsDataSPtr().get()->isDdaRun() == false);

    REQUIRE(p_msreader_tims->getTimsDataSPtr().get()->isDiaRun() == true);

    std::vector<double> rt_line = p_msreader_tims->getRetentionTimeLine();
    REQUIRE((rt_line.back() / 60) == Catch::Approx(46.9988938167));


    pappso::TimsDiaSlices *dia_slices =
      p_msreader_tims->getTimsDataSPtr().get()->getTimsDiaSlicesPtr();
    REQUIRE(dia_slices->getMsMsWindowGroupList().size() == 16);

    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->size() == 3);

    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->at(0).IsolationWidth ==
            Catch::Approx(25.0));

    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->at(0).ScanNumBegin ==
            34);
    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->at(0).ScanNumEnd ==
            385);

    // scan 19916
    // AAAAAPGLSPK
    // mass 952.534180433232
    // mz 477.274
    // rt 18,7117
    // GGVMVAADSR mz 481.7398
    // TTDSYTLR mz 478.7378

    pappso::QualifiedMassSpectrum qspectrum;
    dia_slices->getMs2QualifiedSpectrumByGlobalSliceIndex(
      p_msreader_tims->getMsRunId(), qspectrum, 19917, true);
    REQUIRE(
      qspectrum.getMassSpectrumId().getNativeId().toStdString() ==
      "global_slice_index=19917 frame=10581 begin=632 end=926 group=6 slice=1");

    REQUIRE(qspectrum.getPrecursorNativeId().toStdString() ==
            "frame_id=10575 begin=632 end=926 group=6 slice=1");
    REQUIRE(dia_slices->getMsMsWindowGroupList()
              .getWindowGroupPtrByGroupId(6)
              ->at(1)
              .isMzInRange(477.274) == true);
    REQUIRE(dia_slices->getMsMsWindowGroupList()
              .getWindowGroupPtrByGroupId(6)
              ->at(1)
              .isMzInRange(481.7398) == true);
    REQUIRE(dia_slices->getMsMsWindowGroupList()
              .getWindowGroupPtrByGroupId(6)
              ->at(1)
              .isMzInRange(478.7378) == true);
    REQUIRE(qspectrum.getRtInMinutes() == Catch::Approx(18.711695));
    REQUIRE(
      qspectrum.getParameterValue(QualifiedMassSpectrumParameter::IsolationMz)
        .toDouble() == Catch::Approx(469.7674261218));
    REQUIRE(
      qspectrum
        .getParameterValue(QualifiedMassSpectrumParameter::IsolationMzWidth)
        .toDouble() == Catch::Approx(25.0));


    dia_slices->getMs2QualifiedSpectrumByGlobalSliceIndex(
      p_msreader_tims->getMsRunId(), qspectrum, 19919, true);
    REQUIRE(qspectrum.getRtInMinutes() == Catch::Approx(18.7134834833));
    REQUIRE(
      qspectrum.getParameterValue(QualifiedMassSpectrumParameter::IsolationMz)
        .toDouble() == Catch::Approx(494.7674261218));
    REQUIRE(
      qspectrum
        .getParameterValue(QualifiedMassSpectrumParameter::IsolationMzWidth)
        .toDouble() == Catch::Approx(25.0));
    REQUIRE(
      qspectrum.getMassSpectrumId().getNativeId().toStdString() ==
      "global_slice_index=19919 frame=10582 begin=605 end=922 group=7 slice=1");
    REQUIRE(dia_slices->getMsMsWindowGroupList()
              .getWindowGroupPtrByGroupId(7)
              ->at(1)
              .isMzInRange(477.274) == false);

    /*
    E:\Guillaume_S\10-05-2023_Test_DIA3\10-5-2023_AT01_20ng_DIA3_1_6490.d
    10-5-2023_AT01_20ng_DIA3_1_6490	AT3G20410.1;AT3G20410.2
    AT3G20410.1;AT3G20410.2	|	|	186,004	639,495	843,356	3494850	13979800
    6624770	6624770	AAAAAPGLSPK	AAAAAPGLSPK	AAAAAPGLSPK2	2	0,00439932
    0,0805417	0,00222608	0,333333	0,0191446	0,000202963	0,333333	0	1 186,004
    639,495	150,324		206,898	0,678122	18,7117	18,6216	18,8019	-10,6384
    18,6526	-10,4488		0,00317137	0,000236798	0,490145	256,006	1,83272
    0,330662	0,086652	0	0,945514	0
    -10000000	71.0021;0;115.002;0;134.003;0;0;0;0;0;0;0;	71.0021;0;115.002;0;134.003;0;0;0;0;0;0;0;
    0.628757;0;0.708601;0;0.940796;0;0;0;0;0;0;0;	19916	0,852614	0,866875
    0,850309	0,869149
    */

    /*
E:\Guillaume_S\10-05-2023_Test_DIA3\10-5-2023_AT01_20ng_DIA3_1_6490.d
10-5-2023_AT01_20ng_DIA3_1_6490	AT1G13060.1 AT1G13060.1;AT1G13060.2;AT1G13060.3
|	|	4426,15	16651,3	14524,4	3494850	13979800	6624770	6624770	GGVMVAADSR
GGVMVAADSR	GGVMVAADSR2	2	0,0000591509	0,0000591509	0,0000236407	0,333333
0,000270929	0,000202963	0,333333	0	1	3169,08	10895,5	2561,17		11503,5 0,948014
18,7124	18,6223	18,8026	-11,0752	18,7199	-11,0749		0,00000383822 0,000236798
0,937327	9936,22	4,89793	0,289596	0,993191	1,64454	0,999998	0,889416
0,0101333	4868.11;2112.05;859.021;611.016;543.012;889.022;
4868.11;2112.05;859.021;611.016;543.012;889.022;
0.863006;0.982854;0.900798;0.840894;0.839806;0.910868;	19916	0,834792 0,834792
0,832547	0,83742
*/

    /*
    E:\Guillaume_S\10-05-2023_Test_DIA3\10-5-2023_AT01_20ng_DIA3_1_6490.d
    10-5-2023_AT01_20ng_DIA3_1_6490	AT3G04840.1	AT3G04840.1	|	|	42216 161027
    152055	3494850	13979800	6624770	6624770	TTDSYTLR	TTDSYTLR	TTDSYTLR2 2
    0,00000841411	0,00000841411	0,0000859555	0,333333	0,000270929 0,000202963
    0,333333	0	1	9335,2	32095	7544,48		137159	0,937962	18,7137	18,6235
    18,8038	-11,4401	18,694	-11,413		0,00000179435	0,000236798	0,988696
    142929	5,42457	0,672722	1	1,67381	1	0	-10000000
    23852.4;17278.4;19233.4;3404.05;796.02;570.011;
    23852.4;17278.4;19233.4;3404.05;796.02;570.011;
    0.918867;0.956514;0.944977;0.922522;0.808919;0.970996;	19916	0,8	0,788977
    0,787844	0,801915
    */
  }

  SECTION("..:: test DIA file access good slice ::..", "[dianano]")
  {

    pappso::TimsMsRunReader *p_msreader_tims =
      dynamic_cast<pappso::TimsMsRunReader *>(p_tims_reader.get());


    pappso::TimsDiaSlices *dia_slices =
      p_msreader_tims->getTimsDataSPtr().get()->getTimsDiaSlicesPtr();


    pappso::QualifiedMassSpectrum qspectrum;
    dia_slices->getMs2QualifiedSpectrumByGlobalSliceIndex(
      p_msreader_tims->getMsRunId(), qspectrum, 25107, true);
    // E:\Guillaume_S\10-05-2023_Test_DIA3\10-5-2023_AT01_20ng_DIA3_1_6490.d
    // 10-5-2023_AT01_20ng_DIA3_1_6490	AT3G52920.2
    // AT2G36410.1;AT2G36410.2;AT3G52920.1;AT3G52920.2	|	|	2864,08
    // 10371,6	11644	3494850	13979800	6624770	6624770	EALDTFNEK
    // EALDTFNEK	EALDTFNEK2	2	0,00000666464	0,00000666464	0,000278061
    // 0,333333	0,000270929	0,000202963	0,333333	0	1	2686,06	9889,75
    // 2324,75		3545,98	0,938915	23,5847	23,4945	23,6749	5,66408	23,5797
    // 5,68511		0,0000422969	0,000236798	0,98044	4097,09	3,93059
    // 0,648712	0,102634	0	0,999995	0,842401	0,0982911
    // 1106.02;1128.03;578.014;1002.02;87.0022;224.007;88.0022;117.002;149.004;0;0;115.002;
    // 1106.02;1128.03;578.014;1002.02;87.0022;224.007;88.0022;117.002;149.004;0;0;115.002;
    // 0.956143;0.878501;0.876536;0.955882;0.591027;0.525758;0.0313227;0;0.303984;0;0;0.0313227;
    // 25106	0,863295	0,879479	0,861661	0,880787

    REQUIRE(qspectrum.getRtInMinutes() == Catch::Approx(23.5847));
    REQUIRE(
      qspectrum.getParameterValue(QualifiedMassSpectrumParameter::IsolationMz)
        .toDouble() == Catch::Approx(544.7674261218));
    REQUIRE(
      qspectrum
        .getParameterValue(QualifiedMassSpectrumParameter::IsolationMzWidth)
        .toDouble() == Catch::Approx(25));
    REQUIRE(
      qspectrum.getMassSpectrumId().getNativeId().toStdString() ==
      "global_slice_index=25107 frame=13338 begin=552 end=877 group=9 slice=1");
    REQUIRE(qspectrum.getPrecursorNativeId().toStdString() ==
            "frame_id=13329 begin=552 end=877 group=9 slice=1");
    REQUIRE(dia_slices->getMsMsWindowGroupList()
              .getWindowGroupPtrByGroupId(9)
              ->at(1)
              .isMzInRange(533.456) == true);

    Trace trace(*(qspectrum.getMassSpectrumCstSPtr().get()));
    pappso::FilterHighPassPercentage(0.1).filter(trace);
    REQUIRE_THAT(
      trace.xValues(),
      Catch::Matchers::Approx(std::vector<double>(
        {318.5634519078, 376.824502947,  381.7890516672, 387.06985387,
         388.0654085061, 389.7817550302, 391.40465826,   391.7936526171,
         393.7887081797, 398.9552339271, 401.2128804225, 408.8057413794,
         415.3544029765, 419.8346170353, 428.68003864,   428.8211856189,
         429.1331043329, 429.6291242951, 430.8161345748, 434.8529784824,
         436.1200080808, 436.2557523203, 436.8817732621, 438.7957356984,
         438.8322643532, 439.7293534516, 439.7559478419, 440.0718177023,
         440.7870998078, 442.3094195648, 442.7562865806, 442.7829723455,
         443.7942877215, 445.2281230074, 446.2321790368, 446.7145276172,
         446.8686661017, 448.2537526048, 449.2141726097, 449.2343322883,
         449.2410522817, 449.7351094043, 449.7451950299, 453.7378617047,
         453.8020228665, 454.2478993213, 458.2400976147, 458.2706395847,
         458.8613177232, 460.7649332252, 463.7574626104, 463.8393989645,
         464.744594029,  464.7514290448, 464.7650992272, 464.792440195,
         464.8231997451, 466.7357043884, 466.7596783544, 467.7602837469,
         468.840905218,  471.7837487421, 472.748359489,  474.8152454164,
         475.852108086,  476.7412531252, 476.7585598971, 477.2605929284,
         477.8841738805, 478.7578711656, 480.761365774,  482.0761484062,
         482.7376960048, 482.7516281979, 482.7585943699, 483.556553407,
         483.7587611499, 486.7549789038, 490.7397422272, 492.7751646903,
         494.2331272845, 496.7667774787, 498.8004858692, 501.7576300134,
         524.777528152,  531.2871161155, 536.2864959558, 536.7785395334,
         537.2671342464, 538.28544728,   543.2621442196, 543.2732290473,
         543.5910421962, 546.9818881809, 547.2488644809, 547.2599899069,
         547.2711154461, 547.3044927422, 547.779302236,  564.787253706,
         567.276482815,  600.3054300114, 602.1829008156, 616.9616393397,
         624.2716329729, 632.3021563943, 651.3218910966, 661.3205991144,
         674.3438620965, 681.3395940942, 691.3401460745, 697.351169148,
         718.3418248432, 719.3321364228, 754.3444261244, 774.3953459112,
         774.4041688518, 801.4178405439, 835.4478159327, 836.4286636985,
         851.423199792,  852.4180066224, 852.4272633661, 852.4504054452,
         866.4109555006, 897.4577570737, 897.4672552212, 897.4767534189,
         922.4479361855})));
    REQUIRE_THAT(
      trace.yValues(),
      Catch::Matchers::Approx(std::vector<double>(
        {215.9827213823,  219.9824014079, 250.9799216063, 231.9814414847,
         223.9820814335,  258.9792816575, 233.9812814975, 234.9812015039,
         217.9825613951,  219.9824014079, 233.9812814975, 244.9804015679,
         227.9817614591,  270.9783217343, 231.9814414847, 272.9781617471,
         270.9783217343,  246.9802415807, 216.9826413887, 343.9724822014,
         298.9760819134,  382.969362451,  262.9789616831, 336.9730421566,
         238.9808815295,  240.9807215423, 269.9784017279, 249.9800015999,
         234.9812015039,  284.9772018239, 222.9821614271, 234.9812015039,
         260.9791216703,  217.9825613951, 233.9812814975, 661.9470442365,
         261.9790416767,  324.9740020798, 218.9824814015, 1709.8632109431,
         343.9724822014,  953.9236861051, 505.9595232381, 335.9731221502,
         281.9774418047,  240.9807215423, 449.9640028798, 274.9780017599,
         248.9800815935,  251.9798416127, 262.9789616831, 217.9825613951,
         2152.8277737781, 428.9656827454, 293.9764818814, 256.9794416447,
         225.9819214463,  285.9771218303, 230.9815214783, 630.9495240381,
         309.9752019838,  341.9726421886, 389.9688024958, 221.9822414207,
         262.9789616831,  306.9754419646, 268.9784817215, 276.9778417727,
         218.9824814015,  421.9662427006, 307.975361971,  225.9819214463,
         260.9791216703,  738.9408847292, 641.9486441085, 253.9796816255,
         688.9448844092,  374.9700023998, 276.9778417727, 277.9777617791,
         276.9778417727,  316.9746420286, 288.9768818495, 272.9781617471,
         305.9755219582,  216.9826413887, 322.974162067,  270.9783217343,
         265.9787217023,  515.9587233021, 771.9382449404, 335.9731221502,
         256.9794416447,  391.9686425086, 264.9788016959, 368.9704823614,
         284.9772018239,  397.968162547,  262.9789616831, 250.9799216063,
         633.9492840573,  228.9816814655, 234.9812015039, 261.9790416767,
         290.9767218623,  254.9796016319, 239.9808015359, 281.9774418047,
         286.9770418367,  257.9793616511, 247.9801615871, 255.9795216383,
         222.9821614271,  309.9752019838, 407.967362611,  237.9809615231,
         287.9769618431,  372.970162387,  282.9773618111, 223.9820814335,
         666.9466442685,  328.9736821054, 310.9751219902, 219.9824014079,
         338.9728821694,  874.9300055996, 431.9654427646, 301.9758419326,
         242.9805615551})));
    // E:\Guillaume_S\10-05-2023_Test_DIA3\10-5-2023_AT01_20ng_DIA3_1_6490.d
    // 10-5-2023_AT01_20ng_DIA3_1_6490	AT2G32240.1
    // AT1G05320.1;AT1G05320.2;AT1G05320.3;AT1G05320.4;AT1G05320.5;AT1G05320.6;AT1G05320.7;AT1G05320.8;AT1G05320.9;AT2G32240.1
    // |	|	4136,11	15399,2	15531,2	3494850	13979800	6624770	6624770
    // NSELEEDLR	NSELEEDLR	NSELEEDLR2	2	0,0000284085	0,0000284085
    // 0,000122384	0,333333	0,000270929	0,000202963	0,333333	0	1	1980,06
    // 7290,32	1713,71		3554,64	0,90225	23,5847	23,4945	23,6749	5,66408
    // 23,5797	5,68511		0,000018815	0,000236798	0,981901	4107,1	3,60966
    // 0,524766	1	0,736924	0,99998	0,760906	0,0842984
    // 837.027;794.021;349.009;516.013;787.021;63.0011;114.002;89.0023;160.005;0;85.0023;0;
    // 837.027;794.021;349.009;516.013;787.021;63.0011;114.002;89.0023;160.005;0;85.0023;0;
    // 0.873523;0.936488;0.89325;0.69449;0.752621;0.687662;0.687662;0.256281;0.24152;0;0;0;
    // 25106	0,880114	0,896364	0,877961	0,89836


    REQUIRE(dia_slices->getMsMsWindowGroupList()
              .getWindowGroupPtrByGroupId(9)
              ->at(1)
              .isMzInRange(552.762) == true);

    // E:\Guillaume_S\10-05-2023_Test_DIA3\10-5-2023_AT01_20ng_DIA3_1_6490.d
    // 10-5-2023_AT01_20ng_DIA3_1_6490	AT5G48540.1	AT5G48540.1	|	|
    // 3426,09	11985,4	10523,6	3494850	13979800	6624770	6624770
    // YSQENFIGK	YSQENFIGK	YSQENFIGK2	2	0,000220313	0,00175182
    // 0,0000235188	0,333333	0,000270929	0,000202963	0,333333	0	1
    // 2289,06	8428,02	1981,15		5230,28	0,801486	23,5847	23,4945	23,6749
    // 5,5809	23,5553	5,68511		0,00000323298	0,000236798	0,950567
    // 6043,17	2,95404	0,782493	1	0	0,998748	1,03901	0,118124
    // 892.024;750.015;647.018;430.009;0;0;
    // 892.024;750.015;647.018;430.009;0;0;
    // 0.924789;0.765787;0.672875;0.942847;0;0;	25106	0,873636	0,876667
    // 0,859065	0,892108


    REQUIRE(dia_slices->getMsMsWindowGroupList()
              .getWindowGroupPtrByGroupId(9)
              ->at(1)
              .isMzInRange(543.266) == true);
    // E:\Guillaume_S\10-05-2023_Test_DIA3\10-5-2023_AT01_20ng_DIA3_1_6490.d
    // 10-5-2023_AT01_20ng_DIA3_1_6490
    // AT5G13480.1;AT5G13480.2;AT5G13480.3;AT5G13480.4
    // AT5G13480.1;AT5G13480.2;AT5G13480.3;AT5G13480.4	|	|	493,015
    // 1815,22	1815,66	3494850	13979800	6624770	6624770	AVDYTSTVVR
    // AVDYTSTVVR	AVDYTSTVVR2	2	0,00353707	0,070023	0,00052614	0,333333
    // 0,0153019	0,000202963	0,333333	0	1	493,015	1815,22	426,698
    // 918,306	0,283584	23,5847	23,4945	23,6749	5,88844	23,6454	5,68511
    // 0,000226321	0,000397931	0,526515	1061,03	1,61183	0,174341	0,98708
    // 0	0,95229	0,683212	0,0746583
    // 761.019;31.0006;263.008;93.0026;199.006;0;
    // 761.019;31.0006;263.008;93.0026;199.006;0;
    // 0.816564;0.816497;0.435345;0.0434922;0;0;	25106	0,8925	0,91375
    // 0,895512	0,911112
    // E:\Guillaume_S\10-05-2023_Test_DIA3\10-5-2023_AT01_20ng_DIA3_1_6490.d
    // 10-5-2023_AT01_20ng_DIA3_1_6490	AT2G20760.1	AT2G20760.1	|	|
    // 8655,18	34606,6	39219,7	3494850	13979800	6624770	6624770
    // TNPPPHMMPPPPPAK	TNPPPHMMPPPPPAK	TNPPPHMMPPPPPAK3	3	0,00969779
    // 0,194169	0,00222608	0,333333	0,000270929	0,000202963	0,333333	0	1
    // 1067,03	3928,66	923,496		509,783	0,825448	23,5847	23,4945	23,6749
    // 5,55394	23,5474	5,68511		0,000649907	0,000236798	0	589,014
    // 0,478809	0,52235	0,190638	0	0,878022	0	-10000000
    // 808.019;169.004;144.004;90.0022;0;144.004;112.002;87.0021;0;0;343.007;0;
    // 808.019;169.004;144.004;90.0022;0;144.004;112.002;87.0021;0;0;343.007;0;
    // 0.922378;0.682152;0.521821;0.224315;0;0.409902;0.409902;0.409902;0;0;0.673989;0;
    // 25106	0,86375	0,869271	0,852466	0,881247


    pappso::QualifiedMassSpectrum qspectrum_ms1;
    dia_slices->getMs1QualifiedSpectrumByGlobalSliceIndex(
      p_msreader_tims->getMsRunId(), qspectrum_ms1, 25107, true);

    REQUIRE(
      qspectrum_ms1.getMassSpectrumId().getNativeId().toStdString() ==
      "global_slice_index=25107 frame=13329 begin=552 end=877 group=9 slice=1");

    // tof_index_begin= 164906  tof_index_end= 171663
    qDebug() << qspectrum_ms1.getMassSpectrumCstSPtr().get();

    // AVDYTSTVVR 555.79312
    Trace trace_ms1(*(qspectrum_ms1.getMassSpectrumCstSPtr().get()));
    auto it =
      findFirstEqualOrGreaterX(trace_ms1.begin(), trace_ms1.end(), 555.7921);
    REQUIRE(it->x == Catch::Approx(555.792862930693786));
    REQUIRE(it->y == Catch::Approx(442.964562834973208));

    //"555.800337543402179 207.983361331093533"

    // TNPPPHMMPPPPPAK 536.939
    it = findFirstEqualOrGreaterX(trace_ms1.begin(), trace_ms1.end(), 536.91);
    REQUIRE(it->x == Catch::Approx(536.9438286315));
    REQUIRE(it->y == Catch::Approx(248.9800815935));
    
    
    //15740 INTDESPNTANR
  }
}
