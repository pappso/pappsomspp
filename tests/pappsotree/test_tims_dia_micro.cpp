//
// File: test_tims_dia.cpp
// Created by: Olivier Langella
// Created on: 7/6/2024
//
/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./tests/pappsotree/catch2-only-pappsotree [diamicro] -s


#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <pappsomspp/msrun/private/timsmsrunreader.h>
#include "../common.h"
#include "tests/config.h"


#include <QDebug>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <pappsomspp/exception/exceptionoutofrange.h>
#include <pappsomspp/vendors/tims/timsdiaslices.h>


TEST_CASE("test DIA file access.", "[diamicro]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));


  pappso::MsFileAccessor dia_accessor(
    "/gorgone/pappso/jouy/raw/2023_TimsTOF_Pro/20231214_Guillaume_DIA_DDA/"
    "Short_Gradient_V2/ara1_8min_1500ng_DIA_short_gradientV2_1_1_7079.d",
    "a1");
  // report :
  // /gorgone/pappso/moulon/users/Guillaume/TimsToF/Micro_DIA/DIANN_Method_3Rep/Short_GradientV2/8min/report.tsv'


  dia_accessor.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                          pappso::FileReaderType::tims);

  pappso::MsRunReaderSPtr p_tims_reader =
    dia_accessor.msRunReaderSPtr(dia_accessor.getMsRunIds().front());

  SECTION("..:: test DIA file access ::..", "[diamicro]")
  {

    pappso::TimsMsRunReader *p_msreader_tims =
      dynamic_cast<pappso::TimsMsRunReader *>(p_tims_reader.get());

    qDebug();
    REQUIRE(p_msreader_tims->getTimsDataSPtr().get()->isDdaRun() == false);

    REQUIRE(p_msreader_tims->getTimsDataSPtr().get()->isDiaRun() == true);

    std::vector<double> rt_line = p_msreader_tims->getRetentionTimeLine();
    REQUIRE((rt_line.back() / 60) == Catch::Approx(6.9968716));


    pappso::TimsDiaSlices *dia_slices =
      p_msreader_tims->getTimsDataSPtr().get()->getTimsDiaSlicesPtr();
    REQUIRE(dia_slices->getMsMsWindowGroupList().size() == 7);

    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->size() == 3);

    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->at(0).IsolationWidth ==
            Catch::Approx(25.0));

    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->at(0).ScanNumBegin ==
            458);
    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->at(0).ScanNumEnd ==
            650);

    // 5565
    // AAAEVLALQK
    // mass 1012.59169531072
    // mz 507.303
    // rt 3,7727
    pappso::QualifiedMassSpectrum qspectrum;
    dia_slices->getMs2QualifiedSpectrumByGlobalSliceIndex(
      p_msreader_tims->getMsRunId(), qspectrum, 5565, true);
    REQUIRE(qspectrum.getRtInMinutes() == Catch::Approx(3.7727006667));
    REQUIRE(
      qspectrum.getParameterValue(QualifiedMassSpectrumParameter::IsolationMz)
        .toDouble() == Catch::Approx(852.8037150987));
    REQUIRE(
      qspectrum
        .getParameterValue(QualifiedMassSpectrumParameter::IsolationMzWidth)
        .toDouble() == Catch::Approx(25.0));

    dia_slices->getMs2QualifiedSpectrumByGlobalSliceIndex(
      p_msreader_tims->getMsRunId(), qspectrum, 5566, true);
    REQUIRE(qspectrum.getRtInMinutes() == Catch::Approx(3.7727006667));
    REQUIRE(
      qspectrum.getParameterValue(QualifiedMassSpectrumParameter::IsolationMz)
        .toDouble() == Catch::Approx(677.8037150987));
    REQUIRE(
      qspectrum
        .getParameterValue(QualifiedMassSpectrumParameter::IsolationMzWidth)
        .toDouble() == Catch::Approx(25.0));


    dia_slices->getMs2QualifiedSpectrumByGlobalSliceIndex(
      p_msreader_tims->getMsRunId(), qspectrum, 5567, true);
    REQUIRE(qspectrum.getRtInMinutes() == Catch::Approx(3.7727006667));
    REQUIRE(
      qspectrum.getParameterValue(QualifiedMassSpectrumParameter::IsolationMz)
        .toDouble() == Catch::Approx(502.8037150987));
    REQUIRE(
      qspectrum
        .getParameterValue(QualifiedMassSpectrumParameter::IsolationMzWidth)
        .toDouble() == Catch::Approx(25.0));
  }
}
