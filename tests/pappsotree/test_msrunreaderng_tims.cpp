
/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

// cmake .. -DCMAKE_BUILD_TYPE=Debug  -DMAKE_TESTS=1  -DUSEPAPPSOTREE=1

// ./tests/pappsotree/catch2-only-pappsotree [MsRunReaderTims] -s


#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_approx.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>


#include <iostream>

#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionnotpossible.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/msrun/output/mzxmloutput.h>
#include <pappsomspp/processing/uimonitor/uimonitorvoid.h>
#include <pappsomspp/msrun/private/timsframesmsrunreader.h>
#include <pappsomspp/msrun/private/timsmsrunreader.h>
#include <pappsomspp/vendors/tims/timsframebase.h>
#include <pappsomspp/vendors/tims/timsddaprecursors.h>
#include <pappsomspp/processing/filters/filterpass.h>
#include <QDebug>
#include <QtCore>
#include <QFile>
#include <QtConcurrent>
#include "config.h"
// #include "common.h"

using namespace std;


TEST_CASE("Test MsRunReaderTims new API", "[MsRunReaderTims]")
{

  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));


  SECTION("..::  test retrieving precursor with simple tims reader ::..")

  {
    INFO("test on Bruker sample file");
    pappso::MsFileAccessor file_access_bruker(
      "/gorgone/pappso/versions_logiciels_pappso/bruker/"
      "200ngHeLaPASEF_2min_compressed.d",
      "");
    file_access_bruker.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                                  pappso::FileReaderType::tims);
    pappso::MsRunReaderSPtr msrunA01 = file_access_bruker.getMsRunReaderSPtrByRunId("", "runa01");

    REQUIRE(file_access_bruker.getFileReaderType() == pappso::FileReaderType::tims);

    REQUIRE(msrunA01.get()->getOboPsiModTermInstrumentModelName().m_accession.toStdString() == "MS:1003005");
    REQUIRE(msrunA01.get()->getOboPsiModTermInstrumentModelName().m_name.toStdString() == "timsTOF Pro");

    pappso::TimsMsRunReader *p_msreader_tims =
      dynamic_cast<pappso::TimsMsRunReader *>(msrunA01.get());

    qDebug();
    REQUIRE(p_msreader_tims->getTimsDataSPtr().get()->isDdaRun() == true);

    REQUIRE(p_msreader_tims->getTimsDataSPtr().get()->isDiaRun() == false);

    qDebug();
    // check frame 1178
    pappso::TimsMsRunReader *tims1_reader = dynamic_cast<pappso::TimsMsRunReader *>(msrunA01.get());
    std::vector<pappso::TimsDdaPrecursors::SpectrumDescr> frame_descr_list =
      tims1_reader->getTimsDataSPtr()
        .get()
        ->getTimsDdaPrecursorsPtr()
        ->getSpectrumDescrListByFrameId(1178);

    qDebug();
    REQUIRE(frame_descr_list.size() == 8);
    REQUIRE(frame_descr_list[0].precursor_id == 4107);
    REQUIRE(frame_descr_list[0].scan_mobility_end == 181);

    qDebug();
    std::size_t scan_index = 182;
    auto it_spectrum_descr =
      std::find_if(frame_descr_list.begin(),
                   frame_descr_list.end(),
                   [scan_index](const pappso::TimsDdaPrecursors::SpectrumDescr &spectrum_descr) {
                     if(scan_index < spectrum_descr.scan_mobility_start)
                       return false;
                     if(scan_index > spectrum_descr.scan_mobility_end)
                       return false;
                     return true;
                   });

    REQUIRE(it_spectrum_descr == frame_descr_list.end());

    pappso::MsRunQualifiedSpectrumLoader spectrum_list_reader;

    pappso::MsRunReadConfig config;
    config.setNeedPeakList(true);
    config.setMsLevels({2});
    config.setRetentionTimeStartInSeconds(2499);
    config.setRetentionTimeEndInSeconds(2499.5);

    qDebug();
    msrunA01.get()->readSpectrumCollection2(config, spectrum_list_reader);
    qDebug();
    REQUIRE(spectrum_list_reader.getQualifiedMassSpectrumList().size() == 4026);

    qDebug();
    auto spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(100);

    qDebug();
    REQUIRE(spectrum.getMsLevel() == 2);
    // REQUIRE(spectrum.getMassSpectrumId().getNativeId().toStdString() == "");
    REQUIRE(spectrum.getPrecursorNativeId().toStdString() == "");

    spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(200);

    REQUIRE(spectrum.getMsLevel() == 2);
    REQUIRE(spectrum.getMassSpectrumId().getNativeId().toStdString() ==
            "frame_id=1178 scan_index=200 global_scan_index=789967");
    REQUIRE(spectrum.getPrecursorNativeId().toStdString() == "");


    spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(156);

    REQUIRE(spectrum.getMsLevel() == 2);
    REQUIRE(spectrum.getMassSpectrumId().getNativeId().toStdString() ==
            "frame_id=1178 scan_index=156 global_scan_index=789923");
    REQUIRE(spectrum.getPrecursorNativeId().toStdString() ==
            "frame_id=1162 begin=156 end=181 precursor=4107 idxms1=8212");

    REQUIRE(spectrum.getPrecursorCharge() == 2);
    REQUIRE(spectrum.getPrecursorIntensity() == Catch::Approx(3094));


    spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(155);
    REQUIRE(spectrum.getMsLevel() == 2);
    REQUIRE(spectrum.getMassSpectrumId().getNativeId().toStdString() ==
            "frame_id=1178 scan_index=155 global_scan_index=789922");
    REQUIRE(spectrum.getPrecursorNativeId().toStdString() == "");


    spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(181);
    REQUIRE(spectrum.getMsLevel() == 2);
    REQUIRE(spectrum.getMassSpectrumId().getNativeId().toStdString() ==
            "frame_id=1178 scan_index=181 global_scan_index=789948");
    REQUIRE(spectrum.getPrecursorNativeId().toStdString() ==
            "frame_id=1162 begin=156 end=181 precursor=4107 idxms1=8212");


    spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(182);
    REQUIRE(spectrum.getMsLevel() == 2);
    REQUIRE(spectrum.getMassSpectrumId().getNativeId().toStdString() ==
            "frame_id=1178 scan_index=182 global_scan_index=789949");
    REQUIRE(spectrum.getPrecursorNativeId().toStdString() == "");
  }


  SECTION("..::  test mz merge on tims ::..")

  {
    INFO("test on Bruker sample file");
    pappso::MsFileAccessor file_access_bruker(
      "/gorgone/pappso/versions_logiciels_pappso/bruker/"
      "200ngHeLaPASEF_2min_compressed.d",
      "");
    file_access_bruker.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                                  pappso::FileReaderType::tims);
    pappso::MsRunReaderSPtr msrunA01 = file_access_bruker.getMsRunReaderSPtrByRunId("", "runa01");

    REQUIRE(file_access_bruker.getFileReaderType() == pappso::FileReaderType::tims);

    pappso::MsRunQualifiedSpectrumLoader spectrum_list_reader;

    pappso::MsRunReadConfig config;
    config.setNeedPeakList(true);
    config.setMsLevels({1});
    config.setRetentionTimeStartInSeconds(2499);
    config.setRetentionTimeEndInSeconds(2499.5);

    qDebug();
    msrunA01.get()->readSpectrumCollection2(config, spectrum_list_reader);
    REQUIRE(spectrum_list_reader.getQualifiedMassSpectrumList().size() == 671);
    auto spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(100);

    REQUIRE(spectrum.getMassSpectrumId().getSpectrumIndex() == 793893);
    REQUIRE(spectrum_list_reader.getQualifiedMassSpectrumList()
              .at(101)
              .getMassSpectrumId()
              .getSpectrumIndex() == 793893 + 1);

    auto spetrum_793893 = msrunA01.get()->qualifiedMassSpectrum(793893, true);
    REQUIRE(spetrum_793893.getMassSpectrumId().getSpectrumIndex() == 793893);

    REQUIRE(spectrum.getDtInMilliSeconds() == Catch::Approx(spetrum_793893.getDtInMilliSeconds()));
    REQUIRE(spectrum.getParameterValue(pappso::QualifiedMassSpectrumParameter::IonMobOneOverK0)
              .toDouble() == Catch::Approx(1.4254876992));

    REQUIRE(spectrum_list_reader.getQualifiedMassSpectrumList()
              .at(101)
              .getParameterValue(pappso::QualifiedMassSpectrumParameter::IonMobOneOverK0)
              .toDouble() == Catch::Approx(1.4239005361));
    REQUIRE(
      spectrum.getParameterValue(pappso::QualifiedMassSpectrumParameter::IonMobOneOverK0)
        .toDouble() ==
      Catch::Approx(
        spetrum_793893.getParameterValue(pappso::QualifiedMassSpectrumParameter::IonMobOneOverK0)
          .toDouble()));


    REQUIRE(spetrum_793893.getMassSpectrumId().getNativeId().toStdString() ==
            "frame_id=1184 scan_index=100 global_scan_index=793893");
    REQUIRE(spectrum.getMassSpectrumId().getNativeId().toStdString() ==
            "frame_id=1184 scan_index=100 global_scan_index=793893");
    REQUIRE(spetrum_793893.getMassSpectrumCstSPtr().get()->xValues().size() == 373);
    REQUIRE_THAT(spetrum_793893.getMassSpectrumCstSPtr().get()->xValues(),
                 Catch::Matchers::Approx(spectrum.getMassSpectrumCstSPtr().get()->xValues()));
    REQUIRE_THAT(spetrum_793893.getMassSpectrumCstSPtr().get()->yValues(),
                 Catch::Matchers::Approx(spectrum.getMassSpectrumCstSPtr().get()->yValues()));
  }

  SECTION("..::  test mz merge on tims  restrict mobility range ::..")
  {
    INFO("test on Bruker sample file");
    pappso::MsFileAccessor file_access_bruker(
      "/gorgone/pappso/versions_logiciels_pappso/bruker/"
      "200ngHeLaPASEF_2min_compressed.d",
      "");
    file_access_bruker.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                                  pappso::FileReaderType::tims);
    pappso::MsRunReaderSPtr msrunA01 = file_access_bruker.getMsRunReaderSPtrByRunId("", "runa01");

    REQUIRE(file_access_bruker.getFileReaderType() == pappso::FileReaderType::tims);

    pappso::MsRunQualifiedSpectrumLoader spectrum_list_reader;

    pappso::MsRunReadConfig config;
    config.setNeedPeakList(true);
    config.setMsLevels({1});
    config.setRetentionTimeStartInSeconds(2499);
    config.setRetentionTimeEndInSeconds(2499.5);
    config.setParameterValue(pappso::MsRunReadConfigParameter::TimsFrameIonMobScanIndexBegin, 200);
    config.setParameterValue(pappso::MsRunReadConfigParameter::TimsFrameIonMobScanIndexEnd, 300);
    config.setRetentionTimeEndInSeconds(2499.5);

    qDebug();
    msrunA01.get()->readSpectrumCollection2(config, spectrum_list_reader);
    REQUIRE(spectrum_list_reader.getQualifiedMassSpectrumList().size() == 101);
    auto spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(2);

    REQUIRE(spectrum.getMassSpectrumId().getSpectrumIndex() == 793995);
    REQUIRE(spectrum_list_reader.getQualifiedMassSpectrumList()
              .at(3)
              .getMassSpectrumId()
              .getSpectrumIndex() == 793995 + 1);

    auto spetrum_793995 = msrunA01.get()->qualifiedMassSpectrum(793995, true);
    REQUIRE(spetrum_793995.getMassSpectrumId().getSpectrumIndex() == 793995);

    REQUIRE(spetrum_793995.getMassSpectrumId().getNativeId().toStdString() ==
            "frame_id=1184 scan_index=202 global_scan_index=793995");
    REQUIRE(spectrum.getMassSpectrumId().getNativeId().toStdString() ==
            "frame_id=1184 scan_index=202 global_scan_index=793995");
    REQUIRE(spetrum_793995.getMassSpectrumCstSPtr().get()->xValues().size() == 636);
    REQUIRE_THAT(spetrum_793995.getMassSpectrumCstSPtr().get()->xValues(),
                 Catch::Matchers::Approx(spectrum.getMassSpectrumCstSPtr().get()->xValues()));
    REQUIRE_THAT(spetrum_793995.getMassSpectrumCstSPtr().get()->yValues(),
                 Catch::Matchers::Approx(spectrum.getMassSpectrumCstSPtr().get()->yValues()));
  }

  SECTION("..::  test mz merge on tims  restrict mobility range + mz range::..")
  {
    INFO("test on Bruker sample file");
    pappso::MsFileAccessor file_access_bruker(
      "/gorgone/pappso/versions_logiciels_pappso/bruker/"
      "200ngHeLaPASEF_2min_compressed.d",
      "");
    file_access_bruker.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                                  pappso::FileReaderType::tims);
    pappso::MsRunReaderSPtr msrunA01 = file_access_bruker.getMsRunReaderSPtrByRunId("", "runa01");

    REQUIRE(file_access_bruker.getFileReaderType() == pappso::FileReaderType::tims);

    pappso::MsRunQualifiedSpectrumLoader spectrum_list_reader;

    pappso::MsRunReadConfig config;
    config.setNeedPeakList(true);
    config.setMsLevels({1});
    config.setRetentionTimeStartInSeconds(2499);
    config.setRetentionTimeEndInSeconds(2499.5);
    config.setParameterValue(pappso::MsRunReadConfigParameter::TimsFrameIonMobScanIndexBegin, 200);
    config.setParameterValue(pappso::MsRunReadConfigParameter::TimsFrameIonMobScanIndexEnd, 300);
    config.setParameterValue(pappso::MsRunReadConfigParameter::MzRangeBegin, 300);
    config.setParameterValue(pappso::MsRunReadConfigParameter::MzRangeEnd, 400);
    config.setRetentionTimeEndInSeconds(2499.5);

    qDebug();
    msrunA01.get()->readSpectrumCollection2(config, spectrum_list_reader);
    REQUIRE(spectrum_list_reader.getQualifiedMassSpectrumList().size() == 101);
    auto spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(2);

    REQUIRE(spectrum.getMassSpectrumId().getSpectrumIndex() == 793995);
    REQUIRE(spectrum_list_reader.getQualifiedMassSpectrumList()
              .at(3)
              .getMassSpectrumId()
              .getSpectrumIndex() == 793995 + 1);

    auto spetrum_793995 = msrunA01.get()->qualifiedMassSpectrum(793995, true);
    REQUIRE(spetrum_793995.getMassSpectrumId().getSpectrumIndex() == 793995);

    REQUIRE(spetrum_793995.getMassSpectrumId().getNativeId().toStdString() ==
            "frame_id=1184 scan_index=202 global_scan_index=793995");
    REQUIRE(spectrum.getMassSpectrumId().getNativeId().toStdString() ==
            "frame_id=1184 scan_index=202 global_scan_index=793995");
    REQUIRE(spectrum.getMassSpectrumCstSPtr().get()->xValues().size() == 2);
    REQUIRE_THAT(spectrum.getMassSpectrumCstSPtr().get()->xValues(),
                 Catch::Matchers::Approx(std::vector<double>({330.63218024, 331.5967379429})));
    REQUIRE_THAT(spectrum.getMassSpectrumCstSPtr().get()->yValues(),
                 Catch::Matchers::Approx(std::vector<double>({30.1246063262, 56.1413117897})));
  }


  SECTION(
    "..::  test mz merge on tims  restrict mobility range + mz range + mz "
    "merge window::..")
  {
    INFO("test on Bruker sample file");
    pappso::MsFileAccessor file_access_bruker(
      "/gorgone/pappso/versions_logiciels_pappso/bruker/"
      "200ngHeLaPASEF_2min_compressed.d",
      "");
    file_access_bruker.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                                  pappso::FileReaderType::tims);
    pappso::MsRunReaderSPtr msrunA01 = file_access_bruker.getMsRunReaderSPtrByRunId("", "runa01");

    REQUIRE(file_access_bruker.getFileReaderType() == pappso::FileReaderType::tims);

    pappso::MsRunQualifiedSpectrumLoader spectrum_list_reader;

    pappso::MsRunReadConfig config;
    config.setNeedPeakList(true);
    config.setMsLevels({1});
    config.setRetentionTimeStartInSeconds(2499);
    config.setRetentionTimeEndInSeconds(2499.5);
    config.setParameterValue(pappso::MsRunReadConfigParameter::TimsFrameIonMobScanIndexBegin, 200);
    config.setParameterValue(pappso::MsRunReadConfigParameter::TimsFrameIonMobScanIndexEnd, 300);
    config.setParameterValue(pappso::MsRunReadConfigParameter::MzRangeBegin, 300);
    config.setParameterValue(pappso::MsRunReadConfigParameter::MzRangeEnd, 400);
    config.setParameterValue(pappso::MsRunReadConfigParameter::TimsFrameMzIndexMergeWindow, 1000);
    config.setRetentionTimeEndInSeconds(2499.5);

    qDebug();
    msrunA01.get()->readSpectrumCollection2(config, spectrum_list_reader);
    REQUIRE(spectrum_list_reader.getQualifiedMassSpectrumList().size() == 101);
    auto spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(2);

    REQUIRE(spectrum.getMassSpectrumId().getSpectrumIndex() == 793995);
    REQUIRE(spectrum_list_reader.getQualifiedMassSpectrumList()
              .at(3)
              .getMassSpectrumId()
              .getSpectrumIndex() == 793995 + 1);

    auto spetrum_793995 = msrunA01.get()->qualifiedMassSpectrum(793995, true);
    REQUIRE(spetrum_793995.getMassSpectrumId().getSpectrumIndex() == 793995);

    REQUIRE(spetrum_793995.getMassSpectrumId().getNativeId().toStdString() ==
            "frame_id=1184 scan_index=202 global_scan_index=793995");
    REQUIRE(spectrum.getMassSpectrumId().getNativeId().toStdString() ==
            "frame_id=1184 scan_index=202 global_scan_index=793995");
    REQUIRE(spectrum.getMassSpectrumCstSPtr().get()->xValues().size() == 1);
    REQUIRE_THAT(spectrum.getMassSpectrumCstSPtr().get()->xValues(),
                 Catch::Matchers::Approx(std::vector<double>({330.9439194641})));
    REQUIRE_THAT(spectrum.getMassSpectrumCstSPtr().get()->yValues(),
                 Catch::Matchers::Approx(std::vector<double>({86.2659181158})));
  }


  SECTION("Test TIMS frame and get mobility scan")
  {
    INFO("Test case get mobility scan");

    pappso::MsFileAccessor accessor(
      "/gorgone/pappso/versions_logiciels_pappso/bruker/"
      "200ngHeLaPASEF_2min_compressed.d",
      "");


    accessor.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                        pappso::FileReaderType::tims);


    pappso::MsRunReaderSPtr p_msreader_tims_ms1 =
      accessor.msRunReaderSPtr(accessor.getMsRunIds().front());

    pappso::TimsMsRunReader *tims1_reader =
      dynamic_cast<pappso::TimsMsRunReader *>(p_msreader_tims_ms1.get());

    pappso::TimsDataSp tims_data = tims1_reader->getTimsDataSPtr();

    pappso::TimsFrameCstSPtr frame_test = tims_data.get()->getTimsFrameCstSPtr(1184);

    quint32 mz_minimum_index_out;
    quint32 mz_maximum_index_out;

    pappso::Trace trace_all =
      frame_test.get()->getMobilityScan(202, 0, 0, 0, mz_minimum_index_out, mz_maximum_index_out);

    // This REQUIRE_THAT is only to print out the actual values for
    // visual inspection.
    // REQUIRE_THAT(trace_all.xValues(),
    //              Catch::Matchers::Approx(std::vector<double>({})));

    double min_mz =
      frame_test->getMzCalibrationInterfaceSPtr().get()->getMzFromTofIndex(mz_minimum_index_out);
    double max_mz =
      frame_test->getMzCalibrationInterfaceSPtr().get()->getMzFromTofIndex(mz_maximum_index_out);

    REQUIRE_THAT(min_mz, Catch::Matchers::WithinAbs(330.63218024, 0.0000001));
    REQUIRE_THAT(max_mz, Catch::Matchers::WithinAbs(1704.5162237193, 0.0000001));


    pappso::Trace trace = frame_test.get()->getMobilityScan(
      202, 0, 660, 664, mz_minimum_index_out, mz_maximum_index_out);

    auto raw_spectrum = frame_test.get()->getRawValuePairList(
      202,
      frame_test.get()->getMzCalibrationInterfaceSPtr().get()->getTofIndexFromMz(660),
      frame_test.get()->getMzCalibrationInterfaceSPtr().get()->getTofIndexFromMz(664));

    // 200997
    REQUIRE(raw_spectrum.size() == 3);

    REQUIRE_THAT(trace.xValues(),
                 Catch::Matchers::Approx(
                   std::vector<double>({661.4401585923, 662.489561997, 663.4498612338})));
  }

  SECTION("..::  test mz range on tims  restrict rt + mobility range + mz range ::..")
  {
    INFO("test on Bruker sample file");
    pappso::MsFileAccessor file_access_bruker(
      "/gorgone/pappso/versions_logiciels_pappso/bruker/"
      "200ngHeLaPASEF_2min_compressed.d",
      "");
    file_access_bruker.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                                  pappso::FileReaderType::tims);
    pappso::MsRunReaderSPtr msrunA01 = file_access_bruker.getMsRunReaderSPtrByRunId("", "runa01");

    REQUIRE(file_access_bruker.getFileReaderType() == pappso::FileReaderType::tims);

    pappso::MsRunQualifiedSpectrumLoader spectrum_list_reader;

    pappso::MsRunReadConfig config;
    config.setNeedPeakList(true);
    config.setMsLevels({1});
    config.setRetentionTimeStartInSeconds(2499);
    config.setRetentionTimeEndInSeconds(2499.5);
    config.setParameterValue(pappso::MsRunReadConfigParameter::TimsFrameIonMobScanIndexBegin, 200);
    config.setParameterValue(pappso::MsRunReadConfigParameter::TimsFrameIonMobScanIndexEnd, 300);
    config.setParameterValue(pappso::MsRunReadConfigParameter::MzRangeBegin, 630);
    config.setParameterValue(pappso::MsRunReadConfigParameter::MzRangeEnd, 650);
    config.setRetentionTimeEndInSeconds(2499.5);

    qDebug();
    msrunA01.get()->readSpectrumCollection2(config, spectrum_list_reader);
    REQUIRE(spectrum_list_reader.getQualifiedMassSpectrumList().size() == 101);
    auto spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(2);

    REQUIRE(spectrum.getMassSpectrumId().getSpectrumIndex() == 793995);
    REQUIRE(spectrum_list_reader.getQualifiedMassSpectrumList()
              .at(3)
              .getMassSpectrumId()
              .getSpectrumIndex() == 793995 + 1);

    auto spetrum_793995 = msrunA01.get()->qualifiedMassSpectrum(793995, true);
    REQUIRE(spetrum_793995.getMassSpectrumId().getSpectrumIndex() == 793995);

    REQUIRE(spetrum_793995.getMassSpectrumId().getNativeId().toStdString() ==
            "frame_id=1184 scan_index=202 global_scan_index=793995");
    REQUIRE(spectrum.getMassSpectrumId().getNativeId().toStdString() ==
            "frame_id=1184 scan_index=202 global_scan_index=793995");
    REQUIRE_THAT(spectrum.getMassSpectrumCstSPtr().get()->xValues(),
                 Catch::Matchers::Approx(std::vector<double>({})));
    REQUIRE_THAT(spectrum.getMassSpectrumCstSPtr().get()->yValues(),
                 Catch::Matchers::Approx(std::vector<double>({})));


    spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(3);

    REQUIRE_THAT(spectrum.getMassSpectrumCstSPtr().get()->xValues(),
                 Catch::Matchers::Approx(std::vector<double>({})));
    spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(4);

    REQUIRE_THAT(spectrum.getMassSpectrumCstSPtr().get()->xValues(),
                 Catch::Matchers::Approx(std::vector<double>({})));


    spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(5);
    REQUIRE_THAT(spectrum.getMassSpectrumCstSPtr().get()->xValues(),
                 Catch::Matchers::Approx(std::vector<double>({641.4022702399})));


    spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(6);
    REQUIRE_THAT(spectrum.getMassSpectrumCstSPtr().get()->xValues(),
                 Catch::Matchers::Approx(std::vector<double>({})));


    spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(7);
    REQUIRE_THAT(spectrum.getMassSpectrumCstSPtr().get()->xValues(),
                 Catch::Matchers::Approx(std::vector<double>({634.3317080693})));


    spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(8);
    REQUIRE_THAT(spectrum.getMassSpectrumCstSPtr().get()->xValues(),
                 Catch::Matchers::Approx(std::vector<double>({646.065136105})));
    auto spetrum_794001 = msrunA01.get()->qualifiedMassSpectrum(794001, true);
    REQUIRE(spectrum.getMassSpectrumId().getNativeId().toStdString() ==
            spetrum_794001.getMassSpectrumId().getNativeId().toStdString());
    /* REQUIRE_THAT(spetrum_794001.getMassSpectrumCstSPtr().get()->xValues(),
                  Catch::Matchers::Approx(std::vector<double>({})));
                  */


    spectrum = spectrum_list_reader.getQualifiedMassSpectrumList().at(50);
    // REQUIRE(spectrum.getMassSpectrumId().getNativeId().toStdString() == "");

    auto spetrum_794043 = msrunA01.get()->qualifiedMassSpectrum(794043, true);


    REQUIRE(spectrum.getMassSpectrumId().getNativeId().toStdString() ==
            spetrum_794043.getMassSpectrumId().getNativeId().toStdString());


    REQUIRE_THAT(spectrum.getMassSpectrumCstSPtr().get()->xValues(),
                 Catch::Matchers::Approx(std::vector<double>(
                   {630.3886728619, 630.7832203873, 630.831052709,  631.1539683485, 631.3613157883,
                    631.3972062944, 632.0473998551, 632.3546628915, 632.3746176373, 632.3905816201,
                    632.7059120584, 633.31683884,   633.3368087274, 633.3767494806, 633.9440440064,
                    634.4196483635, 637.2609940978, 637.3050649046, 637.3210910334, 639.305878635,
                    639.3379813683, 640.4219205967, 641.358057993,  642.3190122654, 642.3833696286,
                    643.2967869901, 643.3249643681, 643.9490499435, 644.3639321806, 645.2948820973,
                    645.3634201283, 646.3475432179, 646.3677175013, 646.4968404656, 647.0296089484,
                    647.3324160529, 647.6999171723, 647.7968579117, 647.9503622399, 648.0998444767,
                    648.3382437135, 648.6696510301, 649.2558834495, 649.3084551592, 649.5228079784,
                    649.7776507161})));
    /* REQUIRE_THAT(
       spetrum_794043.getMassSpectrumCstSPtr().get()->xValues(),
       Catch::Matchers::Approx(std::vector<double>({  })));
     */
    /*
     * 630.7832203873, 630.831052709, 631.1539683485, 631.
  3613157883, 631.3972062944, 632.0473998551, 632.3546628915, 632.3746176373,
  632.3905816201, 632.7059120584, 633.31683884, 633.3368087274, 633.3767494806,
  633.9440440064, 634.4196483635, 637.2609940978, 637.3050649046, 637.
  3210910334, 639.305878635, 639.3379813683, 640.4219205967
  */
  }
}
