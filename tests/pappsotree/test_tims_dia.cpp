//
// File: test_tims_dia.cpp
// Created by: Olivier Langella
// Created on: 7/6/2024
//
/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./tests/pappsotree/catch2-only-pappsotree [dia] -s


#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <pappsomspp/msrun/private/timsmsrunreaderdia.h>
#include "../common.h"
#include "tests/config.h"


#include <QDebug>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <pappsomspp/exception/exceptionoutofrange.h>
#include <pappsomspp/vendors/tims/timsdiaslices.h>


TEST_CASE("test DIA file access.", "[dia]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));


  SECTION("..:: test DIA file access ::..", "[dia]")
  {


    pappso::MsFileAccessor dia_accessor(
      "/gorgone/pappso/jouy/raw/2024_timsTOF_Pro/20240402_03_Thais_DIA_plaqueB/"
      "4-5-2024_Super_bulk_Thais_DIA_plaqueB_8464.d/",
      "a1");


    dia_accessor.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                            pappso::FileReaderType::tims_dia);
    pappso::MsRunReaderSPtr p_tims_reader =
      dia_accessor.msRunReaderSPtr(dia_accessor.getMsRunIds().front());

    REQUIRE(p_tims_reader.get() != nullptr);
    pappso::TimsMsRunReaderDia *p_msreader_tims =
      dynamic_cast<pappso::TimsMsRunReaderDia *>(p_tims_reader.get());

    REQUIRE(p_msreader_tims != nullptr);
    qDebug();

    REQUIRE(p_msreader_tims->getTimsDataSPtr().get()->isDdaRun() == false);

    REQUIRE(p_msreader_tims->getTimsDataSPtr().get()->isDiaRun() == true);
    qDebug();
    pappso::TimsDiaSlices *dia_slices =
      p_msreader_tims->getTimsDataSPtr().get()->getTimsDiaSlicesPtr();
    REQUIRE(dia_slices->getMsMsWindowGroupList().size() == 13);


    REQUIRE(dia_slices->getMsMsWindowGroupList().at(11)->size() == 2);
    REQUIRE(dia_slices->getMsMsWindowGroupList().at(12)->size() == 1);

    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->at(0).ScanNumBegin ==
            146);
    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->at(1).ScanNumBegin ==
            507);
    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->at(2).ScanNumBegin ==
            833);
    pappso::TimsDiaSlices::MsMsWindow window;
    REQUIRE(dia_slices->getMsMsWindowByGlobalSliceIndex(0).SliceIndex == 0);
    REQUIRE(dia_slices->getMsMsWindowByGlobalSliceIndex(1).SliceIndex == 1);
    REQUIRE(dia_slices->getMsMsWindowByGlobalSliceIndex(2).SliceIndex == 2);
    REQUIRE(dia_slices->getFrameIdByGlobalSliceIndex(2) == 2);
    REQUIRE(dia_slices->getMsMsWindowByGlobalSliceIndex(3).SliceIndex == 0);
    REQUIRE(dia_slices->getFrameIdByGlobalSliceIndex(3) == 3);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(3) == 1);

    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(6) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(7) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(8) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(9) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(10) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(11) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(12) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(13) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(14) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(15) == 0);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(16) == 15);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(17) == 15);


    REQUIRE(dia_slices->getFrameIdByGlobalSliceIndex(
              dia_slices->getTotalSlicesCount() - 1) == 26496);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(26496) == 26489);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(26489) == 0);
  }


  SECTION("..:: test DIA file access ::..", "[dia_a1]")
  {


    pappso::MsFileAccessor dia_accessor(
      "/gorgone/pappso/jouy/raw/2024_timsTOF_Pro/20240402_03_Thais_DIA_plaqueB/"
      "4-5-2024_Super_bulk_Thais_DIA_plaqueB_8464.d/",
      "a1");

    dia_accessor.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                            pappso::FileReaderType::tims);

    pappso::MsRunReaderSPtr p_tims_reader =
      dia_accessor.msRunReaderSPtr(dia_accessor.getMsRunIds().front());

    pappso::MsRunQualifiedSpectrumLoader spectrum_list_reader;

    pappso::MsRunReadConfig config;
    config.setNeedPeakList(true);
    config.setMsLevels({2});
    config.setRetentionTimeStartInSeconds(330);
    config.setRetentionTimeEndInSeconds(335);

    p_tims_reader.get()->readSpectrumCollection2(config, spectrum_list_reader);
    REQUIRE(spectrum_list_reader.getQualifiedMassSpectrumList().size() ==
            40788);

    REQUIRE(spectrum_list_reader.getQualifiedMassSpectrumList()
              .at(200)
              .getPrecursorNativeId()
              .toStdString() ==
            "window_group=2 begin=98 end=424 frame=3095 scan=200 "
            "global_slice_id=7075");
  }


  SECTION("..:: test DIA Mass Spectrum ::..", "[dia_a2]")
  {


    pappso::MsFileAccessor dia_accessor(
      "/gorgone/pappso/jouy/raw/2024_timsTOF_Pro/20240402_03_Thais_DIA_plaqueB/"
      "4-5-2024_Super_bulk_Thais_DIA_plaqueB_8464.d/",
      "a1");

    dia_accessor.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                            pappso::FileReaderType::tims_dia);

    pappso::MsRunReaderSPtr p_tims_reader =
      dia_accessor.msRunReaderSPtr(dia_accessor.getMsRunIds().front());
    pappso::TimsMsRunReaderDia *p_msreader_tims =
      dynamic_cast<pappso::TimsMsRunReaderDia *>(p_tims_reader.get());

    qDebug();
    QualifiedMassSpectrum qspectrum;

    p_msreader_tims->getTimsDataSPtr()
      .get()
      ->getTimsDiaSlicesPtr()
      ->getMs2QualifiedSpectrumByGlobalSliceIndex(
        p_tims_reader.get()->getMsRunId(), qspectrum, 7075, true);

    REQUIRE(
      qspectrum.getMassSpectrumId().getNativeId().toStdString() ==
      "global_slice_index=7075 frame=3097 begin=98 end=424 group=2 slice=0");


    QualifiedMassSpectrum qspectrum_dia_reader;

    qspectrum_dia_reader = p_msreader_tims->qualifiedMassSpectrum(7075, true);
    REQUIRE(
      qspectrum_dia_reader.getMassSpectrumId().getNativeId().toStdString() ==
      "global_slice_index=7075 frame=3097 begin=98 end=424 group=2 slice=0");
  }
  
  
  SECTION("..:: test DIA file access ::..", "[dda_celine]")
  {


    pappso::MsFileAccessor dda_celine(
      "/gorgone/pappso/jouy/raw/2024_timsTOF_Pro/20240826_30_Zuzana/8-30-2024_Gal_3_pool_9501.d",
      "a1");


    dda_celine.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                            pappso::FileReaderType::tims_ms2);
    pappso::MsRunReaderSPtr p_tims_reader =
      dda_celine.msRunReaderSPtr(dda_celine.getMsRunIds().front());

    REQUIRE(p_tims_reader.get() != nullptr);
    pappso::TimsMsRunReaderDia *p_msreader_tims =
      dynamic_cast<pappso::TimsMsRunReaderDia *>(p_tims_reader.get());

    REQUIRE(p_msreader_tims != nullptr);
    qDebug();

    REQUIRE(p_msreader_tims->getTimsDataSPtr().get()->isDdaRun() == false);

    REQUIRE(p_msreader_tims->getTimsDataSPtr().get()->isDiaRun() == true);
    qDebug();
    pappso::TimsDiaSlices *dia_slices =
      p_msreader_tims->getTimsDataSPtr().get()->getTimsDiaSlicesPtr();
    REQUIRE(dia_slices->getMsMsWindowGroupList().size() == 13);


    REQUIRE(dia_slices->getMsMsWindowGroupList().at(11)->size() == 2);
    REQUIRE(dia_slices->getMsMsWindowGroupList().at(12)->size() == 1);

    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->at(0).ScanNumBegin ==
            146);
    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->at(1).ScanNumBegin ==
            507);
    REQUIRE(dia_slices->getMsMsWindowGroupList().at(0)->at(2).ScanNumBegin ==
            833);
    pappso::TimsDiaSlices::MsMsWindow window;
    REQUIRE(dia_slices->getMsMsWindowByGlobalSliceIndex(0).SliceIndex == 0);
    REQUIRE(dia_slices->getMsMsWindowByGlobalSliceIndex(1).SliceIndex == 1);
    REQUIRE(dia_slices->getMsMsWindowByGlobalSliceIndex(2).SliceIndex == 2);
    REQUIRE(dia_slices->getFrameIdByGlobalSliceIndex(2) == 2);
    REQUIRE(dia_slices->getMsMsWindowByGlobalSliceIndex(3).SliceIndex == 0);
    REQUIRE(dia_slices->getFrameIdByGlobalSliceIndex(3) == 3);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(3) == 1);

    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(6) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(7) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(8) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(9) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(10) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(11) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(12) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(13) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(14) == 1);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(15) == 0);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(16) == 15);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(17) == 15);


    REQUIRE(dia_slices->getFrameIdByGlobalSliceIndex(
              dia_slices->getTotalSlicesCount() - 1) == 26496);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(26496) == 26489);
    REQUIRE(dia_slices->getLastMs1FrameIdByMs2FrameId(26489) == 0);
  }


}
