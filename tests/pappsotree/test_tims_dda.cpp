//
// File: test_tims_dda.cpp
// Created by: Olivier Langella
// Created on: 6/9/2024
//
/*******************************************************************************
 * Copyright (c) 2024 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./tests/pappsotree/catch2-only-pappsotree [dda] -s


#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include "../common.h"
#include "tests/config.h"


#include <QDebug>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <pappsomspp/exception/exceptionoutofrange.h>


TEST_CASE("test DDA file access.", "[dda]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));


  SECTION("..:: test DIA file access ::..", "[dda]")
  {


    pappso::MsFileAccessor dda_accessor(
      "/gorgone/pappso/jouy/raw/2024_timsTOF_Pro/20240826_30_Zuzana/8-30-2024_Gal_3_pool_9501.d",
      "a1");


    dda_accessor.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                            pappso::FileReaderType::tims_ms2);
    pappso::MsRunReaderSPtr p_tims_reader =
      dda_accessor.msRunReaderSPtr(dda_accessor.getMsRunIds().front());

    REQUIRE(p_tims_reader.get()->getOboPsiModTermInstrumentModelName().m_accession.toStdString() ==
            "MS:1003005");
    REQUIRE(p_tims_reader.get()->getOboPsiModTermInstrumentModelName().isA("MS:1003123"));

    REQUIRE(p_tims_reader.get() != nullptr);

    pappso::MsRunSimpleStatistics stats;
    pappso::MsRunReadConfig config;
    config.setNeedPeakList(false);
    p_tims_reader.get()->readSpectrumCollection2(config, stats);

    REQUIRE(stats.getMsLevelCount(1) == 98511);
    REQUIRE(stats.getMsLevelCount(2) == 98511);
  }
}
