//
// File: test_project.cpp
// Created by: Olivier Langella
// Created on: 16/1/2025
//
/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include "common.h"
#include "tests/config.h"


#include <QDebug>
#include <QJsonDocument>
#include <QCborStreamReader>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <pappsomspp/exception/exceptionoutofrange.h>
#include <pappsomspp/processing/project/projectparameters.h>

// ./tests/catch2-only-tests [project] -s

TEST_CASE("Project parameters test suite.", "[project]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: project parameters ::..", "[self]")
  {

    qDebug();
    QFile json_file(QString(CMAKE_SOURCE_DIR).append("/tests/data/project_parameters.json"));
    REQUIRE(json_file.open(QIODevice::ReadOnly));
    QJsonDocument doc;
    QByteArray bproject = json_file.readAll();

    // qDebug() << bproject;
    doc = doc.fromJson(bproject);
    qDebug() << doc.toJson();
    pappso::ProjectParameters parameters(doc.object().value("project_parameters").toObject());

    REQUIRE(parameters.size() == 129);

    REQUIRE(parameters.toJsonObject().contains("xtandem_preset_file"));
    REQUIRE(parameters.toJsonObject().contains("AnalysisSoftware_name"));
    QByteArray buffer;
    QCborStreamWriter writer(&buffer);

    QCborValue cbor_params;

    QJsonObject json_value;
    json_value.insert("project_parameters", parameters.toJsonObject());

    REQUIRE(json_value.contains("project_parameters"));
    cbor_params = cbor_params.fromJsonValue(json_value);

    REQUIRE(cbor_params.isContainer());

    cbor_params.toCbor(writer);


    QCborStreamReader reader(buffer);
    QJsonValue all_cbor;
    REQUIRE(reader.enterContainer());
    all_cbor = cbor_params.fromCbor(reader).toJsonValue();
    qDebug() << all_cbor;
    REQUIRE(all_cbor.isString());
    REQUIRE(all_cbor.toString() == "project_parameters");

    all_cbor = cbor_params.fromCbor(reader).toJsonValue();

    REQUIRE(all_cbor.toObject().contains("xtandem_preset_file"));

    pappso::ProjectParameters parameters_fromcbor(all_cbor.toObject());
    REQUIRE(parameters_fromcbor.size() == 129);

    qDebug() << buffer;
  }
}
