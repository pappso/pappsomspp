#pragma once


#include <odsstream/odsdocwriter.h>
#include <pappsomspp/fasta/fastareader.h>
#include <pappsomspp/protein/protein.h>
#include <pappsomspp/protein/proteinpresenceabsencematrix.h>
#include <pappsomspp/processing/specglob/spectralalignment.h>
#include <pappsomspp/processing/specself/selfspectrum.h>

class FastaProteinArrayHandler : public pappso::FastaHandlerInterface
{
  public:
  const std::vector<pappso::Protein> &getProteinArray() const;
  void setSequence(const QString &description_in,
                   const QString &sequence_in) override;

  private:
  std::vector<pappso::Protein> m_proteinArray;
};


void writeProteinConvolutionScore(
  const QString &filename,
  const pappso::Protein &protein,
  const pappso::ProteinPresenceAbsenceMatrix *pmatrix,
  const std::vector<double> &convolution_score);

void writeSpectralAlignmentMatrix(
  const QString &filename,
  const pappso::specglob::SpectralAlignment &spectral_alignment);


void writeSelfSpectrum(const QString &filename,
                       const pappso::specself::SelfSpectrum &self_spectrum,
                       const pappso::AaStringCodec &aa_codec);
