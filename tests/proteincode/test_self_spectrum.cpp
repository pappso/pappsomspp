//
// File: test_c13n15.cpp
// Created by: Olivier Langella
// Created on: 12/7/2023
//
/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./tests/proteincode/catch2-only-proteincode [self] -s


#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include "../common.h"
#include "tests/config.h"
#include "proteincode_lib.h"


#include <QDebug>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <pappsomspp/exception/exceptionoutofrange.h>
#include <pappsomspp/processing/specself/selfspectrum.h>


TEST_CASE("self spectrum test suite.", "[self]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: self spectrum ::..", "[self]")
  {

    pappso::MassSpectrum spectrum = readMgf(
      QString(CMAKE_SOURCE_DIR).append("/tests/data/peaklist_15046.mgf"));


    pappso::AaCode aa_code;
    aa_code.addAaModification('C',
                              pappso::AaModification::getInstance("MOD:00397"));
    pappso::AaStringCodec aa_codec(aa_code);


    pappso::AaStringCodeMassMatching aaMatching(
      aa_code, 5, pappso::PrecisionFactory::getDaltonInstance(0.01));

    qDebug();
    pappso::specself::SelfSpectrum self_spectrum(aaMatching, spectrum);

    qDebug();
    writeSelfSpectrum("self_spectrum_15046.ods", self_spectrum, aa_codec);
  }
}
