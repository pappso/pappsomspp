//
// File: test_c13n15.cpp
// Created by: Olivier Langella
// Created on: 12/7/2023
//
/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./tests/proteincode/catch2-only-proteincode [met] -s


#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>

#include <QDebug>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <pappsomspp/exception/exceptionoutofrange.h>
#include <pappsomspp/protein/protein.h>
#include <pappsomspp/protein/proteinintegercode.h>
#include <pappsomspp/protein/proteinpresenceabsencematrix.h>
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>
#include <pappsomspp/processing/filters/filterresample.h>
#include <pappsomspp/processing/filters/filterpass.h>
#include <pappsomspp/processing/filters/filterchargedeconvolution.h>
#include <pappsomspp/processing/filters/filterpeakdelta.h>
#include <pappsomspp/amino_acid/aastringcodemassmatching.h>
#include <pappsomspp/fasta/fastareader.h>
#include "proteincode_lib.h"
#include "../common.h"
#include "tests/config.h"


TEST_CASE("proteincode test suite.", "[met]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

#if USEPAPPSOTREE == 1

  std::cout << std::endl
            << "..:: Test Fasta big file indexer ::.." << std::endl;
  QFile file2("/gorgone/pappso/moulon/database/Genome_Z_mays_5a.fasta");

  FastaProteinArrayHandler protein_handler;
  pappso::FastaReader reader(protein_handler);
  reader.parse(file2);


  pappso::AaCode aa_code;
  aa_code.addAaModification('C',
                            pappso::AaModification::getInstance("MOD:00397"));
  pappso::AaStringCodec aa_codec(aa_code);

  pappso::AaCode aa_code_metoxy(aa_code);
  aa_code_metoxy.addAaModification(
    'M', pappso::AaModification::getInstance("MOD:00719"));
  pappso::AaStringCodec aa_codec_metoxy(aa_code_metoxy);


  std::vector<pappso::ProteinIntegerCode> arr_codedProtein;

  for(auto &protein : protein_handler.getProteinArray())
    {
      pappso::ProteinSp protein_sp = protein.makeProteinSp();

      qDebug() << protein_sp.get()->getAccession();
      // pappso::ProteinIntegerCode protein_code(protein_sp, aa_codec, 5);

      arr_codedProtein.push_back({protein_sp, aa_codec, 5});
    }

  SECTION(
    "..:: scan_3302 spectrum easy met on fasta file GKQQVM[MOD:00719]VGYSDSGK "
    "::..",
    "[met]")
  {
    // compute spectrum code :

    pappso::MassSpectrum spectrum =
      readMgf(QString(CMAKE_SOURCE_DIR)
                .append("/tests/data/scans/"
                        "20120906_balliau_extract_1_A08_teal-2_scan_3302.mgf"));


    pappso::FilterResampleKeepGreater(160).filter(spectrum);
    pappso::FilterChargeDeconvolution(
      pappso::PrecisionFactory::getDaltonInstance(0.02))
      .filter(spectrum);
    //.applyCutOff(150).takeNmostIntense(100).applyDynamicRange(100);
    pappso::FilterGreatestY(100).filter(spectrum);

    pappso::FilterPeakDelta filter_peak_delta;
    filter_peak_delta.filter(spectrum);

    pappso::FilterGreatestY(200).filter(spectrum);
    std::vector<double> delta_mass_list = spectrum.xValues();


    // ok spectrum code
    pappso::ProteinSp max_protein;
    double max_score = 0;

    //


    pappso::AaStringCodeMassMatching aaMatching(
      aa_code, 7, pappso::PrecisionFactory::getDaltonInstance(0.01));

    std::vector<uint32_t> code_list_from_spectrum =
      aaMatching.getAaCodeFromMassList(delta_mass_list);


    std::sort(code_list_from_spectrum.begin(), code_list_from_spectrum.end());
    code_list_from_spectrum.erase(std::unique(code_list_from_spectrum.begin(),
                                              code_list_from_spectrum.end()),
                                  code_list_from_spectrum.end());

    std::vector<double> convolution_score;
    pappso::ProteinPresenceAbsenceMatrix presence_absence_matrix;
    pappso::ProteinPresenceAbsenceMatrix best_presencce_absence_matrix;

    for(auto &coded_protein : arr_codedProtein)
      {
        pappso::ProteinSp protein_sp = coded_protein.getProteinSp();

        qDebug() << protein_sp.get()->getAccession();

        presence_absence_matrix.fillMatrix(coded_protein,
                                           code_list_from_spectrum);

        // qDebug() << protein_sp.get()->getAccession();
        std::vector<double> vec_score = presence_absence_matrix.convolution();
        auto it_score = std::max_element(vec_score.begin(), vec_score.end());

        // double score = std::accumulate(it_score - 2, it_score + 5, 0);

        if(it_score != vec_score.end())
          {
            // std::cout << protein_sp.get()->getAccession().toStdString() << "
            // "
            //         << score << std::endl;
            double score = std::accumulate(it_score - 2, it_score + 5, 0);
            if(score > max_score)
              {
                max_score                     = score;
                max_protein                   = protein_sp;
                convolution_score             = vec_score;
                best_presencce_absence_matrix = presence_absence_matrix;

                std::cout << protein_sp.get()->getAccession().toStdString()
                          << " " << max_score << std::endl;
              }
          }
      }


    writeProteinConvolutionScore("best_fit_scan_3302.ods",
                                 *(max_protein.get()),
                                 &best_presencce_absence_matrix,
                                 convolution_score);
    REQUIRE(
      max_protein.get()->getAccession().toStdString() ==
      "GRMZM2G083841_P01"); // //GRMZM2G083841_P01 GKQQVM[MOD:00719]VGYSDSGK
  }

  SECTION(
    "..:: scan_3340 spectrum easy met on fasta file GKQQVM[MOD:00719]VGYSDSGK "
    "Evalue 0.0069"
    "::..",
    "[met]")
  {
    // compute spectrum code :

    pappso::MassSpectrum spectrum =
      readMgf(QString(CMAKE_SOURCE_DIR)
                .append("/tests/data/scans/"
                        "20120906_balliau_extract_1_A05_urnb-2_scan_3340.mgf"));


    pappso::FilterResampleKeepGreater(200).filter(spectrum);
    pappso::FilterChargeDeconvolution(
      pappso::PrecisionFactory::getDaltonInstance(0.02))
      .filter(spectrum);
    //.applyCutOff(150).takeNmostIntense(100).applyDynamicRange(100);
    pappso::FilterGreatestY(100).filter(spectrum);

    pappso::FilterPeakDelta filter_peak_delta;
    filter_peak_delta.filter(spectrum);

    pappso::FilterGreatestY(500).filter(spectrum);
    std::vector<double> mass_list = spectrum.xValues();


    // ok spectrum code
    pappso::ProteinSp max_protein;
    double max_score = 0;

    //


    pappso::AaStringCodeMassMatching aaMatching(
      aa_code, 7, pappso::PrecisionFactory::getDaltonInstance(0.01));

    std::vector<uint32_t> code_list_from_spectrum =
      aaMatching.getAaCodeFromMassList(mass_list);


    std::sort(code_list_from_spectrum.begin(), code_list_from_spectrum.end());
    code_list_from_spectrum.erase(std::unique(code_list_from_spectrum.begin(),
                                              code_list_from_spectrum.end()),
                                  code_list_from_spectrum.end());

    std::vector<double> convolution_score;
    pappso::ProteinPresenceAbsenceMatrix presence_absence_matrix;
    pappso::ProteinPresenceAbsenceMatrix best_presencce_absence_matrix;


    for(auto &coded_protein : arr_codedProtein)
      {
        pappso::ProteinSp protein_sp = coded_protein.getProteinSp();

        presence_absence_matrix.fillMatrix(coded_protein,
                                           code_list_from_spectrum);

        // qDebug() << protein_sp.get()->getAccession();
        std::vector<double> vec_score = presence_absence_matrix.convolution();

        auto it_score = std::max_element(vec_score.begin(), vec_score.end());

        // double score = std::accumulate(it_score - 2, it_score + 5, 0);

        if(it_score != vec_score.end())
          {
            // std::cout << protein_sp.get()->getAccession().toStdString() << "
            // "
            //         << score << std::endl;
            double score = std::accumulate(it_score - 2, it_score + 5, 0);

            if(protein_sp.get()->getAccession() == "GRMZM2G083841_P01")
              {

                std::cout << protein_sp.get()->getAccession().toStdString()
                          << " " << score << std::endl;

                writeProteinConvolutionScore("best_fit_scan_3340_target.ods",
                                             *(protein_sp.get()),
                                             &presence_absence_matrix,
                                             vec_score);
              }
            /*
                        if(score > 6000)
                          {

                            std::cout <<
               protein_sp.get()->getAccession().toStdString()
                                      << " " << score << std::endl;
                          }
            */
            if(score > max_score)
              {
                max_score                     = score;
                max_protein                   = protein_sp;
                convolution_score             = vec_score;
                best_presencce_absence_matrix = presence_absence_matrix;

                std::cout << protein_sp.get()->getAccession().toStdString()
                          << " " << max_score << std::endl;
              }
          }
      }


    writeProteinConvolutionScore("best_fit_scan_3340.ods",
                                 *(max_protein.get()),
                                 &best_presencce_absence_matrix,
                                 convolution_score);
    REQUIRE(
      max_protein.get()->getAccession().toStdString() ==
      "GRMZM2G083841_P01"); // //GRMZM2G083841_P01 GKQQVM[MOD:00719]VGYSDSGK
  }
#endif
}
