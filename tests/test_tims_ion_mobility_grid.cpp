
// File: test_timsxicextractor.cpp
// Created by: Olivier Langella
// Created on: 3/2/2021
//
/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<olivier.langella@universite-paris-saclay.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

// make test ARGS="-V -I 1,1"

// ./tests/catch2-only-tests [TimsIonMobilityGrid] -s


#include <catch2/catch_test_macros.hpp>


#include <QDebug>

#include <pappsomspp/msrun/xiccoord/ionmobilitygrid.h>
#include "config.h"
#include "common.h"
#include <time.h>


using namespace pappso;

TEST_CASE("test TimsIonMobilityGrid", "[TimsIonMobilityGrid]")
{
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

#if USEPAPPSOTREE == 1

  IonMobilityGrid ion_mobility_grid;


  pappso::MsFileAccessor accessor(
    "/gorgone/pappso/versions_logiciels_pappso/masschroq/donnees/"
    "PXD010012_msfragger_timstof/"
    "20180819_TIMS2_12-2_AnBr_SA_200ng_HeLa_50cm_120min_100ms_11CT_1_A1_01_"
    "2767.d/analysis.tdf",
    "a1");

  accessor.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                      pappso::FileReaderType::tims_ms2);

  pappso::MsRunReaderSPtr p_msreader =
    accessor.msRunReaderSPtr(accessor.getMsRunIds().front());

  REQUIRE(p_msreader != nullptr);

  REQUIRE(accessor.getFileReaderType() == pappso::FileReaderType::tims_ms2);


  pappso::MsFileAccessor accessor_a2(
    "/gorgone/pappso/versions_logiciels_pappso/masschroq/donnees/"
    "PXD010012_msfragger_timstof/"
    "20180819_TIMS2_12-2_AnBr_SA_200ng_HeLa_50cm_120min_100ms_11CT_2_A1_01_"
    "2768.d/analysis.tdf",
    "a2");

  accessor_a2.setPreferredFileReaderType(pappso::MsDataFormat::brukerTims,
                                         pappso::FileReaderType::tims_ms2);

  pappso::MsRunReaderSPtr p_msreader_a2 =
    accessor_a2.msRunReaderSPtr(accessor_a2.getMsRunIds().front());

  REQUIRE(p_msreader_a2 != nullptr);

  REQUIRE(accessor_a2.getFileReaderType() == pappso::FileReaderType::tims_ms2);

  auto msrunid_a1 = *p_msreader->getMsRunId().get();

  auto msrunid_a2 = *p_msreader_a2->getMsRunId().get();

  qInfo() << "store observed";
  INFO("store observed");


  auto a1 = p_msreader.get()->newXicCoordSPtrFromSpectrumIndex(
    106171, pappso::PrecisionFactory::getPpmInstance(10));

  auto a2 = p_msreader_a2.get()->newXicCoordSPtrFromSpectrumIndex(
    106889, pappso::PrecisionFactory::getPpmInstance(10));
  qInfo() << "first a1" << a1.get()->toString();
  qInfo() << "first a2" << a2.get()->toString();

  ion_mobility_grid.storeObservedIdentityBetween(
    msrunid_a1, a1.get(), msrunid_a2, a2.get());


  INFO("store observed pepa1e17369");
  // pepa1e17369
  // 20180819_TIMS2_12-2_AnBr_SA_200ng_HeLa_50cm_120min_100ms_11CT_1_A1_01_2767
  // 261103	2586,61	650,346968967	ILDEIGADVQAR pepa1e17369
  // 20180819_TIMS2_12-2_AnBr_SA_200ng_HeLa_50cm_120min_100ms_11CT_2_A1_01_2768
  // 259019	2593,3	650,347328967	ILDEIGADVQAR

  a1 = p_msreader.get()->newXicCoordSPtrFromSpectrumIndex(
    261103, pappso::PrecisionFactory::getPpmInstance(10));

  a2 = p_msreader_a2.get()->newXicCoordSPtrFromSpectrumIndex(
    259019, pappso::PrecisionFactory::getPpmInstance(10));
  qInfo() << "2 a1" << a1.get()->toString();
  qInfo() << "2 a2" << a2.get()->toString();

  ion_mobility_grid.storeObservedIdentityBetween(
    msrunid_a1, a1.get(), msrunid_a2, a2.get());


  INFO("store observed pepa1e31234");
  // pepa1e31234
  // 20180819_TIMS2_12-2_AnBr_SA_200ng_HeLa_50cm_120min_100ms_11CT_1_A1_01_2767
  // 245667	2454,61	781,849418967	SNMEAQHNDLEFK
  // pepa1e31234
  // 20180819_TIMS2_12-2_AnBr_SA_200ng_HeLa_50cm_120min_100ms_11CT_2_A1_01_2768
  // 243535	2460,95	781,849797967	SNMEAQHNDLEFK

  a1 = p_msreader.get()->newXicCoordSPtrFromSpectrumIndex(
    245667, pappso::PrecisionFactory::getPpmInstance(10));

  a2 = p_msreader_a2.get()->newXicCoordSPtrFromSpectrumIndex(
    243535, pappso::PrecisionFactory::getPpmInstance(10));
  qInfo() << "3 a1" << a1.get()->toString();
  qInfo() << "3 a2" << a2.get()->toString();

  ion_mobility_grid.storeObservedIdentityBetween(
    msrunid_a1, a1.get(), msrunid_a2, a2.get());


  INFO("store observed pepb15b13");
  // pepb15b13
  // 20180819_TIMS2_12-2_AnBr_SA_200ng_HeLa_50cm_120min_100ms_11CT_1_A1_01_2767
  // 249403	2486,14	699,856334967	DPEKPQLGMIDR pepb15b13
  // 20180819_TIMS2_12-2_AnBr_SA_200ng_HeLa_50cm_120min_100ms_11CT_2_A1_01_2768
  // 247155	2491,71	699,856816467	DPEKPQLGMIDR


  ion_mobility_grid.storeObservedIdentityBetween(
    msrunid_a1,
    p_msreader.get()
      ->newXicCoordSPtrFromSpectrumIndex(
        249403, pappso::PrecisionFactory::getPpmInstance(10))
      .get(),
    msrunid_a2,
    p_msreader_a2.get()
      ->newXicCoordSPtrFromSpectrumIndex(
        247155, pappso::PrecisionFactory::getPpmInstance(10))
      .get());


  INFO("store observed pepb54b12");
  //  pepb54b12
  //  20180819_TIMS2_12-2_AnBr_SA_200ng_HeLa_50cm_120min_100ms_11CT_1_A1_01_2767
  //  79843	1106,79	832,442628967	EAGTEVVK
  // pepb54b12
  // 20180819_TIMS2_12-2_AnBr_SA_200ng_HeLa_50cm_120min_100ms_11CT_2_A1_01_2768
  // 79969	1104,68	832,440325967	EAGTEVVK

  ion_mobility_grid.storeObservedIdentityBetween(
    msrunid_a1,
    p_msreader.get()
      ->newXicCoordSPtrFromSpectrumIndex(
        79843, pappso::PrecisionFactory::getPpmInstance(10))
      .get(),
    msrunid_a2,
    p_msreader_a2.get()
      ->newXicCoordSPtrFromSpectrumIndex(
        79969, pappso::PrecisionFactory::getPpmInstance(10))
      .get());

  INFO("store observed pepb83a6");
  // pepb83a6
  // 20180819_TIMS2_12-2_AnBr_SA_200ng_HeLa_50cm_120min_100ms_11CT_1_A1_01_2767
  // 289387	2830,6	610,302429967	AAYFGIYDTAK pepb83a6
  // 20180819_TIMS2_12-2_AnBr_SA_200ng_HeLa_50cm_120min_100ms_11CT_2_A1_01_2768
  // 286699	2828,24	610,303296967	AAYFGIYDTAK

  ion_mobility_grid.storeObservedIdentityBetween(
    msrunid_a1,
    p_msreader.get()
      ->newXicCoordSPtrFromSpectrumIndex(
        289387, pappso::PrecisionFactory::getPpmInstance(10))
      .get(),
    msrunid_a2,
    p_msreader_a2.get()
      ->newXicCoordSPtrFromSpectrumIndex(
        286699, pappso::PrecisionFactory::getPpmInstance(10))
      .get());


  auto map_diff = ion_mobility_grid.getMapDiferrencesStart();

  for(auto itmap : map_diff)
    {
      qInfo() << (itmap.first);
      for(auto list_diff : itmap.second)
        {

          qInfo() << (list_diff);
        }
    }

  WARN("ion_mobility_grid.computeCorrections()");
  ion_mobility_grid.computeCorrections();


  auto map_delta = ion_mobility_grid.getMapCorrectionsStart();

  REQUIRE(map_delta["a1a1-a2a1"] == -1);

  // pepa1e31244
  // 20180819_TIMS2_12-2_AnBr_SA_200ng_HeLa_50cm_120min_100ms_11CT_1_A1_01_2767
  // 452469	4297,94	718,417257967	SNPDQPAVILLLR
  // pepa1e31244
  // 20180819_TIMS2_12-2_AnBr_SA_200ng_HeLa_50cm_120min_100ms_11CT_2_A1_01_2768
  // 448731	4288,53	718,417001467	SNPDQPAVILLLR

  auto target = p_msreader_a2.get()->newXicCoordSPtrFromSpectrumIndex(
    448731, pappso::PrecisionFactory::getPpmInstance(10));

  auto source = p_msreader.get()->newXicCoordSPtrFromSpectrumIndex(
    452469, pappso::PrecisionFactory::getPpmInstance(10));

  WARN("source");
  REQUIRE(source->toString().toStdString() ==
          "mz=mz=718.417 delta=0.00718417 : 718.41 < 718.417 < 718.424 "
          "rt=4297.94 begin=532 end=557");

  REQUIRE(source.get()
            ->getParam(XicCoordParam::TimsTofIonMobilityScanNumberStart)
            .toInt() == 532);
  WARN("target");
  REQUIRE(target->toString().toStdString() ==
          "mz=mz=718.417 delta=0.00718417 : 718.41 < 718.417 < 718.424 "
          "rt=4288.53 begin=534 end=559");

  auto result = ion_mobility_grid.translateXicCoordFromTo(
    *source.get(), msrunid_a1, msrunid_a2);


  REQUIRE(source.get()
            ->getParam(XicCoordParam::TimsTofIonMobilityScanNumberStart)
            .toInt() == 532);
  REQUIRE(result.get()
            ->getParam(XicCoordParam::TimsTofIonMobilityScanNumberStart)
            .toInt() ==
          (source.get()
             ->getParam(XicCoordParam::TimsTofIonMobilityScanNumberStart)
             .toInt() -
           1));
  WARN("result");
  REQUIRE(result->toString().toStdString() ==
          "mz=mz=718.417 delta=0.00718417 : 718.41 < 718.417 < 718.424 "
          "rt=4297.94 begin=531 end=556");

  result = ion_mobility_grid.translateXicCoordFromTo(
    *target.get(), msrunid_a2, msrunid_a1);

  REQUIRE(target.get()
            ->getParam(XicCoordParam::TimsTofIonMobilityScanNumberStart)
            .toInt() == 534);

  REQUIRE(result.get()
            ->getParam(XicCoordParam::TimsTofIonMobilityScanNumberStart)
            .toInt() ==
          (target.get()
             ->getParam(XicCoordParam::TimsTofIonMobilityScanNumberStart)
             .toInt() +
           1));
  WARN("result");
  REQUIRE(result->toString().toStdString() ==
          "mz=mz=718.417 delta=0.00718417 : 718.41 < 718.417 < 718.424 "
          "rt=4288.53 begin=535 end=560");
#elif USEPAPPSOTREE == 1

  std::cout << std::endl << "..:: NO test TIMS TDF parsing ::.." << std::endl;

#endif
}
